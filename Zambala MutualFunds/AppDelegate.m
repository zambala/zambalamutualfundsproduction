//
//  AppDelegate.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "Utility.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <UserNotifications/UserNotifications.h>
@import Firebase;
@import Mixpanel;
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface AppDelegate ()<UNUserNotificationCenterDelegate>
{
    UINavigationController * navigation;
    Mixpanel * mixpanelFunds;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [Fabric with:@[[Crashlytics class]]];
    [FIRApp configure];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:(247.0/255.0) green:(219.0/255.0) blue:(131.0/255.0) alpha:1.0]];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:(44/255.0) green:(44/255.0) blue:(44/255.0) alpha:1.0]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:(44/255.0) green:(44/255.0) blue:(44/255.0) alpha:1.0]}];
    
    [navigation.navigationItem.leftBarButtonItem setTitle:@" "];
    self.portfolioDataArray = [[NSMutableArray alloc]init];
    self.transactionDetailsArray = [[NSMutableArray alloc]init];
    self.transactionDataArray = [[NSMutableArray alloc] init];
    
    [Mixpanel sharedInstanceWithToken:@"931de01c8b1604507529d8dd9038b742"];
    //Change base URL here
    self.zambalaBaseURL = @"http://projects.zenwise.net:8013/api/";
#pragma mark - Notifications
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.deviceToken = [NSString stringWithFormat:@"%@", deviceToken];
        //Format token as you need:
        self.mixpanelDeviceToken = deviceToken;
        self.deviceToken = [self.deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.deviceToken = [self.deviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
        self.deviceToken = [self.deviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        self.deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        if(prefs !=nil)
        {
            [prefs setObject:self.deviceToken forKey:@"devicetoken"];
            [prefs setObject:deviceToken forKey:@"devicetokenData"];
            [prefs synchronize];
        }
        mixpanelFunds = [Mixpanel sharedInstance];
        [mixpanelFunds.people addPushDeviceToken:self.mixpanelDeviceToken];
    });
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    //NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    //NSLog(@"%@", notification.request.content.userInfo);
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
    //NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    //NSLog(@"%@", response.notification.request.content.userInfo);
    
    return;
    
}
-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void
                                                                                                                               (^)(UIBackgroundFetchResult))completionHandler
{
     //iOS 10 will handle notifications through other methods
    self.notificationInfo=[[NSDictionary alloc]init];
    //    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
    //    {
    //        //NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
    //        // set a member variable to tell the new delegate that this is background
    //        self.notificationInfo=userInfo;
    //        return;
    //    }
    //NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );

    // custom code to handle notification content

    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        //NSLog( @"INACTIVE" );
        self.notificationInfo=userInfo;
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        //NSLog( @"BACKGROUND" );
        self.notificationInfo=userInfo;
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else
    {
        //NSLog( @"FOREGROUND" );
        self.notificationInfo=userInfo;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:nil];
        completionHandler( UIBackgroundFetchResultNewData );
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//    LAContext *myContext = [[LAContext alloc] init];
//    NSError *authError = nil;
//    NSString *myLocalizedReasonString = @"Place Your Finger on Tocuh ID.";
//
//    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthentication error:&authError]) {
//        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthentication
//                  localizedReason:myLocalizedReasonString
//                            reply:^(BOOL success, NSError *error) {
//                                if (success) {
//                                    dispatch_async(dispatch_get_main_queue(), ^{
//
//                                    });
//                                } else {
//                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                                            message:error.description
//                                                                                           delegate:self
//                                                                                  cancelButtonTitle:@"OK"
//                                                                                  otherButtonTitles:nil, nil];
//                                        [alertView show];
//                                        NSLog(@"Switch to fall back authentication - ie, display a keypad or password entry box");
//                                    });
//                                }
//                            }];
//    } else {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                message:authError.description
//                                                               delegate:self
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil, nil];
//            [alertView show];
//        });
//    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if(self.notificationInfo)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:nil];
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end

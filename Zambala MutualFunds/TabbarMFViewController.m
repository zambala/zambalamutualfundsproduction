//
//  TabbarMFViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "TabbarMFViewController.h"
#import "VCFloatingActionButton.h"
#import "LoginViewController.h"
#import "TransactionsViewController.h"
#import "ProfileViewController.h"
#import "NewProfileViewController.h"
#import "AboutViewController.h"
#import "Utility.h"
@import Mixpanel;


@interface TabbarMFViewController ()
{
    Utility * mfSharedManager;
    NSString * isGuest;
    Mixpanel * mixpanelFunds;
}

@end

@implementation TabbarMFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mfSharedManager=[Utility MF];
    float X_Co = (self.view.frame.size.width-35)/2;
    float Y_Co;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    if(screenHeight>736)
    {
        
        Y_Co = self.view.frame.size.height - 71;
    }
    
    else
    {
        Y_Co = self.view.frame.size.height - 40;
        
    }
    
    self.addButton = [[VCFloatingActionButton alloc]initWithFrame:CGRectMake(X_Co,Y_Co, 30, 30) normalImage:[UIImage imageNamed:@""] andPressedImage:[UIImage imageNamed:@"close"] withScrollview:self.dummyTable];
    
  //  self.addButton = [[VCFloatingActionButton alloc]initWithFrame:CGRectMake(X_Co,Y_Co, 30, 30) normalImage:[UIImage imageNamed:@"more"] andPressedImage:[UIImage imageNamed:@"more"] withScrollview:self.dummyTable];
    
    //Profile, Notifications, Feed, Transactions, About, Logout
    //Logout, About, Transactions, Feed, Notifications, Profile.
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    
    isGuest = [userSessionData objectForKey:@"isguest"];
//    if([isGuest isEqualToString:@"0"])
//    {
//        self.addButton.labelArray = @[@"Login as Client",@"About",@"Transactions",@"Feed",@"Notifications",@"Profile"];
//    }else
//    {
//        self.addButton.labelArray = @[@"Logout",@"About",@"Transactions",@"Feed",@"Notifications",@"Profile"];
//    }
//    self.addButton.imageArray = @[@"logoutNew",@"aboutNew",@"transactionsNew",@"feedNew",@"notificationsNew",@"profileNew"];
    
    
//    if([isGuest isEqualToString:@"0"])
//    {
//        self.addButton.labelArray = @[@"Login as Client",@"About",@"Transactions",@"Notifications",@"Profile"];
//    }else
//    {
//        self.addButton.labelArray = @[@"Logout",@"About",@"Transactions",@"Notifications",@"Profile"];
//    }
    self.addButton.labelArray = @[@"Logout",@"About",@"Transactions",@"Notifications",@"Profile"];
    self.addButton.imageArray = @[@"logoutNew",@"aboutNew",@"transactionsNew",@"notificationsNew",@"profileNew"];
    
    
//    self.addButton.imageArray = @[@"logout",@"transaction",@"feed",@"profile"];
//    self.addButton.labelArray = @[@"Logout",@"Transaction",@"Feed",@"Profile"];
    self.addButton.hideWhileScrolling = YES;
    self.addButton.delegate = self;
    
    self.dummyTable.delegate = self;
    self.dummyTable.dataSource = self;

    
    [self.view addSubview:self.addButton];
    
    
    // Do any additional setup after loading the view.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%tu",indexPath.row];
    
    
    
    cell.imageView.image =[UIImage imageNamed:[NSString stringWithFormat:@"%tu",indexPath.row]];
    
    //    cell.imageView.image=[self.imageArray objectAtIndex:indexPath.row];
    return cell;
}


-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    //Logout, About, Transactions, Feed, Notifications, Profile.
    
   // @"logoutNew",@"aboutNew",@"transactionsNew",@"notificationsNew",@"profileNew"
    if(row==4)
    {
        NewProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileViewController"];
        [self presentViewController:profile animated:YES completion:nil];
        //        ProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        //        [self presentViewController:profile animated:YES completion:nil];
    }else if (row==3)
    {
        //Notifications
    }else if (row==2)
    {
        TransactionsViewController * transactions = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionsViewController"];
        [self presentViewController:transactions animated:YES completion:nil];
    }else if (row==1)
    {
     //About
        AboutViewController * about = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
        [self presentViewController:about animated:YES completion:nil];
    }
    else if(row==0)
    {
//        if([isGuest isEqualToString:@"0"])
//        {
//            [mixpanelFunds track:@"login_as_client_page"];
//            LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//            login.logoutCheckString = @"logout";
//            [self presentViewController:login animated:YES completion:nil];
//        }else
//        {
//            [mixpanelFunds track:@"log_out_page"];
//            [self logoutRequest];
//        }
        
        [self logoutRequest];
        
//        LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//        login.logoutCheckString = @"logout";
//        [self presentViewController:login animated:YES completion:nil];
        
    }
}

-(void)logoutRequest
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *cid = [prefs stringForKey:@"cid"];
    NSString *sid = [prefs stringForKey:@"sessionid"];
    

    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"logoutrequest"];
    NSString * inputParams = [NSString stringWithFormat:@"%@|%@",cid,sid];
    [inputArray addObject:[NSString stringWithFormat:@"%@applogout_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
    [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        
        NSLog(@"Response:%@",dict);
        
        
        
        LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:login animated:YES completion:nil];
        
    }];
}

//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
//{
//    if(tabBarController.selectedIndex==2)
//    {
//        return NO;
//    }
//    return YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

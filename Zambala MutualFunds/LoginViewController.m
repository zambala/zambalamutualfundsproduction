//
//  LoginViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 05/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "LoginViewController.h"
#import "TabbarMFViewController.h"
#import "SignUpViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import <QuartzCore/QuartzCore.h>
#import "ForgetPasswordViewController.h"
#import "AppDelegate.h"
#import "OpenAccountWebViewController.h"
#import "Reachability.h"

@import Mixpanel;

@interface LoginViewController ()<UITextFieldDelegate>
{
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    NSString * isGuest;
    Mixpanel *mixpanelFunds;
    AppDelegate * delegate;
    UITextField * selectedTextField;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mfSharedManager=[Utility MF];
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.loginButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.loginButton.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.loginButton.layer.shadowOpacity = 0.6f;
    self.loginButton.layer.shadowRadius = 6.5f;
    self.loginButton.layer.masksToBounds = NO;
    
    self.signUpButton.layer.cornerRadius = 10.0f;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.userNameTF.leftView = paddingView;
    self.userNameTF.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.passwordTF.leftView = paddingView1;
    self.passwordTF.leftViewMode = UITextFieldViewModeAlways;
    
//    self.userNameTF.text= @"revanth1425@gmail.com";
//    self.passwordTF.text= @"Revanth@123";
//    self.userNameTF.text= @"prabhat1068@gmail.com";
//    self.passwordTF.text= @"Abcd@123";
    self.userNameTF.text= @"amitabh71@gmail.com";
    self.passwordTF.text= @"Abcd@1234";
    self.backButton.hidden=YES;
   
    self.userNameTF.delegate = self;
    self.passwordTF.delegate = self;
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds track:@"Launched"];
    [mixpanelFunds identify:mixpanelFunds.distinctId];
    
    [mixpanelFunds track:@"login_page"];
    if(delegate.mixpanelDeviceToken.length>0)
    {
        [mixpanelFunds.people addPushDeviceToken:delegate.mixpanelDeviceToken];
    }
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    isGuest = [userSessionData objectForKey:@"isguest"];
//    if([isGuest isEqualToString:@"0"])
//    {
    
    if([self.logoutCheckString isEqualToString:@"logout"])
    {
        self.backButton.hidden = NO;
        [self.signUpButton setTitle:@"OPEN ACCOUNT" forState:UIControlStateNormal];
//        self.accountLabel.hidden = YES;
//        self.signUpButton.hidden = YES;
    }else
    {
        self.backButton.hidden = YES;
        if([self.guestLoginString isEqualToString:@"Guest"])
        {
            
        }else
        {
            [self forceUpdateServer];
            //[self isKycServer];
        }
    }
    //}
    [self.forgetPasswordButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.signUpButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.touchIDButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)isKycServer
{
 //DGActivityIndicatorAnimationTypeBallClipRotate
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid;
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    
        dispatch_async(dispatch_get_main_queue(), ^{
            backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
            [self.view addSubview:backView];
            activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
            activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
            [activityIndicatorView setCenter:self.view.center];
            [backView addSubview:activityIndicatorView];
            [activityIndicatorView startAnimating];
        });
    if([guestCheck isEqualToString:@"0"])
    {
        cid = [userSessionData objectForKey:@"zenwiseID"];
    }else if([guestCheck isEqualToString:@"1"])
    {
        cid = [userSessionData objectForKey:@"cid"];
    }
        NSMutableArray * inputArray = [[NSMutableArray alloc]init];
        [inputArray addObject:@"isKyc"];
       NSString * endPoint = [NSString stringWithFormat:@"%@signup/iskyced/%@/%@/%@",mfSharedManager.zambalaBaseUrl,cid,delegate.deviceID,delegate.deviceToken];
    //NSString * endPoint = [NSString stringWithFormat:@"%@signup/iskyced/%@",mfSharedManager.zambalaBaseUrl,cid];
        [inputArray addObject:endPoint];
        [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            if([[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"status"]isEqualToString:@"success"])
            {
                NSString * kyc = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"kyc"]];
                NSString * clientID = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"clientid"]];
                NSString * email = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"email"]];
                 NSString * mobilenumber = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"mobilenumber"]];
                 NSString * name = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"name"]];
                
                [mixpanelFunds identify:clientID];
                
                if([kyc isEqualToString:@"1"])
                {
                    
                }else if ([kyc isEqualToString:@"0"])
                {
                    [mixpanelFunds.people set:@{@"$name":name,@"$email":email,@"$phone":mobilenumber,@"Client Id":clientID,@"Device Token":delegate.deviceToken}];
                    [self onButtonTap:self.loginButton];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                [backView removeFromSuperview];
                [activityIndicatorView stopAnimating];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self isKycServer];
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
         }];
}



-(void)forceUpdateServer
{
        dispatch_async(dispatch_get_main_queue(), ^{
            backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
            [self.view addSubview:backView];
            activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
            activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
            [activityIndicatorView setCenter:self.view.center];
            [backView addSubview:activityIndicatorView];
            [activityIndicatorView startAnimating];
        });
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        NSString * buildAndVersion = [NSString stringWithFormat:@"%@(%@)",version,build];
        NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];


        NSMutableArray * inputArray = [[NSMutableArray alloc]init];
        [inputArray addObject:@"forceUpdate"];
        NSString * endPoint = [NSString stringWithFormat:@"%@init",mfSharedManager.zambalaBaseUrl];
        [inputArray addObject:endPoint];
        [inputArray addObject:version];
        [inputArray addObject:@"iOS"];
        [inputArray addObject:uniqueIdentifier];
        [mfSharedManager zambalaPOSTRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            if([[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"status"]isEqualToString:@"error"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [backView removeFromSuperview];
                    [activityIndicatorView stopAnimating];
                    NSString * message = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"messages"] objectForKey:@"msg"]];
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Update" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        NSString *iTunesLink = @"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?mt=8";
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                    }];
                    
                    [alert addAction:okAction];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                [backView removeFromSuperview];
                [activityIndicatorView stopAnimating];
                
                 });
            }
            
            NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
            NSString * cid;
            NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
            if([guestCheck isEqual:[NSNull null]]||guestCheck==nil)
            {
                
            }else
            {
            [self isKycServer];
            }
        }];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [activityIndicatorView stopAnimating];
    [backView removeFromSuperview];
    
//    NSString *userName = [[NSUserDefaults standardUserDefaults]stringForKey:@"username"];
//    NSString *password = [[NSUserDefaults standardUserDefaults]stringForKey:@"password"];
//    if(userName.length>0&&password.length>0)
//    {
//        LAContext *myContext = [[LAContext alloc] init];
//        NSError *authError = nil;
//        NSString *myLocalizedReasonString = @"Place Your Finger on Tocuh ID.";
//
//        if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
//            [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
//                      localizedReason:myLocalizedReasonString
//                                reply:^(BOOL success, NSError *error) {
//                                    if (success) {
//                                        dispatch_async(dispatch_get_main_queue(), ^{
//                                            TabbarMFViewController * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
//                                            [self presentViewController:tabbar animated:YES completion:nil];
//                                        });
//                                    } else {
//                                        dispatch_async(dispatch_get_main_queue(), ^{
//                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                                                message:error.description
//                                                                                               delegate:self
//                                                                                      cancelButtonTitle:@"OK"
//                                                                                      otherButtonTitles:nil, nil];
//                                            [alertView show];
//                                            NSLog(@"Switch to fall back authentication - ie, display a keypad or password entry box");
//                                        });
//                                    }
//                                }];
//        } else {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                    message:authError.description
//                                                                   delegate:self
//                                                          cancelButtonTitle:@"OK"
//                                                          otherButtonTitles:nil, nil];
//                [alertView show];
//            });
//        }
//    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
        
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    selectedTextField = textField;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound) {
        return NO;
    }
    return YES;
}


-(void)onButtonTap:(UIButton*)sender
{
    if([self networkStatus]==YES)
    {
    if(sender == self.touchIDButton)
    {
        
    }else
    {
     dispatch_async(dispatch_get_main_queue(), ^{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
     });
    }
    if(sender == self.loginButton)
    {
         dispatch_async(dispatch_get_main_queue(), ^{
        NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
        NSString * zenwiseID = [userSessionData objectForKey:@"zenwiseID"];
        
        if(self.userNameTF.text.length>0&&self.passwordTF.text.length>0)
        {
            [self loginServer];
        }else if (zenwiseID.length>0)
        {
            [self loginServer];
        }
       else
       {
           dispatch_async(dispatch_get_main_queue(), ^{
               UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter missing fields" preferredStyle:UIAlertControllerStyleAlert];
               
               [self presentViewController:alert animated:YES completion:^{
                   
               }];
               
               
             
               UIAlertAction * ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                   [activityIndicatorView stopAnimating];
                   [backView removeFromSuperview];
               }];
               
               [alert addAction:ok];
               
           });
       }
       });
        
    }else if (sender == self.signUpButton)
    {
        NSString * buttonTitle = [NSString stringWithFormat:@"%@",self.signUpButton.titleLabel.text];
        if([buttonTitle isEqualToString:@"OPEN ACCOUNT"])
        {
            OpenAccountWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountWebViewController"];
            [self presentViewController:webView animated:YES completion:nil];
        }else
        {
        SignUpViewController * signup = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
        CATransition *transition = [[CATransition alloc] init];
        transition.duration = 0.5;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self presentViewController:signup animated:false completion:nil];
        }
    }
    else if (sender == self.forgetPasswordButton)
    {
        ForgetPasswordViewController * forget = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgetPasswordViewController"];
        CATransition *transition = [[CATransition alloc] init];
        transition.duration = 0.5;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self presentViewController:forget animated:false completion:nil];
    }else if (sender == self.touchIDButton)
    {
        
    }else if (sender == self.backButton)
    {
        [selectedTextField resignFirstResponder];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    }else
    {
        [self networkStatus];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
        self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
        self.reach = [Reachability reachabilityForInternetConnection];
        [self.reach startNotifier];
    }
}

-(void)loginServer
{
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"loginrequest"];
    NSString * paramsString;
    if([isGuest isEqualToString:@"0"])
    {
        paramsString = [NSString stringWithFormat:@"%@|%@",@"guest",@"guest123"];
    }else
    {
        paramsString = [NSString stringWithFormat:@"%@|%@",self.userNameTF.text,self.passwordTF.text];
    }
    [inputArray addObject:[NSString stringWithFormat:@"%@applogin_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:paramsString]]];
    [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        mfSharedManager.cid=[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"]objectForKey:@"logindata"]objectAtIndex:0]objectForKey:@"cid"]];
        mfSharedManager.sessionId=[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"]objectForKey:@"logindata"]objectAtIndex:0]objectForKey:@"sessionid"]];
        if(mfSharedManager.cid.length>0&&mfSharedManager.sessionId.length>0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
                [userSessionData setObject:mfSharedManager.cid forKey:@"cid"];
                [userSessionData setObject:mfSharedManager.sessionId forKey:@"sessionid"];
                [userSessionData setObject:self.userNameTF.text forKey:@"username"];
                [userSessionData setObject:self.passwordTF.text forKey:@"password"];
                [userSessionData synchronize];
                if([isGuest isEqualToString:@"0"])
                {
                    [userSessionData setObject:@"0" forKey:@"isguest"];
                    [mixpanelFunds identify:[userSessionData objectForKey:[userSessionData objectForKey:@"zenwiseID"]]];
                }else
                {
                    [userSessionData setObject:@"1" forKey:@"isguest"];
//                    [userSessionData removeObjectForKey:@"isguest"];
//                    [userSessionData removeObjectForKey:@"zenwiseID"];
                    [mixpanelFunds identify:[userSessionData objectForKey:mfSharedManager.cid]];
                }
               
                NSString * name = [NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"]objectForKey:@"applicantdata"] objectAtIndex:0] objectForKey:@"name"]];
                [mixpanelFunds track:@"login_success_page"];
                if([isGuest isEqualToString:@"0"])
                {
                    [mixpanelFunds.people set:@{@"User Type":@"Guest",@"Is KYCed":@"False",@"Client Id":[userSessionData objectForKey:@"zenwiseID"]}];
                }else
                {
                    [mixpanelFunds.people set:@{@"User Type":@"Client",@"Is KYCed":@"True",@"Client Id":mfSharedManager.cid,@"$name":name}];
                }
                
                
                TabbarMFViewController * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
                [self presentViewController:tabbar animated:YES completion:nil];
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                
                UIAlertAction * ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                }];
                
                [alert addAction:ok];
                
            });
        }
        
    }];
}

-(void)forgetPasswordMethod
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

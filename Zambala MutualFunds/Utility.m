//
//  Utility.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "Utility.h"
#import <CommonCrypto/CommonCryptor.h>
#import "STEncryptorDES.h"
#import "NSData+Base64.h"
#import "Reachability.h"

@implementation Utility
- (id)init {
    if (self = [super init]) {
        self.baseUrl=@"http://zenwiseapi.aceonlineplatform.com/Zenwise.svc/";
        self.zambalaBaseUrl = @"http://projects.zenwise.net:8073/api/";  //8073
    }
    return self;
}

+ (id)MF {
    static Utility *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
-(void)inputRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    NSString * urlString;
    NSString * requestName=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:0]];
    if([requestName isEqualToString:@"loginrequest"]||[requestName isEqualToString:@"holdingrequest"]||[requestName isEqualToString:@"getAmcList"]||[requestName isEqualToString:@"logoutrequest"]||[requestName isEqualToString:@"getAssettypeList"]||[requestName isEqualToString:@"getCategoryByAsset"]||[requestName isEqualToString:@"getSchemeRec"]||[requestName isEqualToString:@"getschemedetails"]||[requestName isEqualToString:@"getschemecategoryperformance"]||[requestName isEqualToString:@"getTransactDetails"]||[requestName isEqualToString:@"getRecommendations"]||[requestName isEqualToString:@"insertRedeemCart"]||[requestName isEqualToString:@"excuteRedeemCart"]||[requestName isEqualToString:@"insertSIPCart"]||[requestName isEqualToString:@"getBankList"]||[requestName isEqualToString:@"createMandate"]||[requestName isEqualToString:@"getMandateList"]||[requestName isEqualToString:@"insertLumpsumCart"]||[requestName isEqualToString:@"excuteLumpsumCart"]||[requestName isEqualToString:@"insertSwitchCart"]||[requestName isEqualToString:@"executeSwitchCart"]||[requestName isEqualToString:@"getSchemeDetails"]||[requestName isEqualToString:@"getAccountStatement"]||[requestName isEqualToString:@"smartMoney"]||[requestName isEqualToString:@"riskprofiledata"]||[requestName isEqualToString:@"forgetrequest"])
    {
        urlString=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:1]];
    }
   
    
    
    
//    NSDictionary *headers = @{ @"content-type": @"application/json",
//                               @"cache-control": @"no-cache",
//                               @"Accept":@"application/json"
//                               
//                               };

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
//    [request setAllHTTPHeaderFields:headers];
    
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                            
                                                            [dict setValue:array forKey:@"data"];
                                                            [dict mutableCopy];
                                                            handler(dict);
                                                            
                                                        }
//                                                        dispatch_async(dispatch_get_main_queue(), ^{
//                                                
//                                                        });
//                                                        
                                                    }
                                                    
                                                    
                                                }];
    [dataTask resume];
}

-(void)inputRequestMethodDES:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    
}


-(void)zambalaGETRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    NSString * urlString;
    NSString * requestName=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:0]];
    if([requestName isEqualToString:@"Learn"]||[requestName isEqualToString:@"Signup"]||[requestName isEqualToString:@"validateotp"]||[requestName isEqualToString:@"sendotp"]||[requestName isEqualToString:@"getprofileDetails"]||[requestName isEqualToString:@"isKyc"]||[requestName isEqualToString:@"forgetpassword"]||[requestName isEqualToString:@"getImage"])
    {
        urlString=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:1]];
    }
//        NSDictionary *headers = @{ @"content-type": @"application/json",
//                                   @"cache-control": @"no-cache",
//                                   @"Accept":@"application/json"
//    
//                                   };
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    //    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                            
                                                            [dict setValue:array forKey:@"data"];
                                                            [dict mutableCopy];
                                                            handler(dict);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        handler([NSDictionary new]);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)zambalaPOSTRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    NSString * urlString;
    
    NSDictionary *headers;
    NSString * requestName=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:0]];
    headers = @{ @"content-type": @"application/json",
                 @"cache-control": @"no-cache"
                 };
    NSDictionary * params=[[NSDictionary alloc]init];
    urlString = [NSString stringWithFormat:@"%@",[inputArray objectAtIndex:1]];
    if([requestName isEqualToString:@"addfundwatch"])
    {
        params = @{@"action":[inputArray objectAtIndex:2],
                   @"fundids":[inputArray objectAtIndex:3],
                   @"clientid":[inputArray objectAtIndex:4]
                   };
    }else if ([requestName isEqualToString:@"getfundwatch"])
    {
        params = @{@"action":[inputArray objectAtIndex:2],
                   @"clientid":[inputArray objectAtIndex:3]
                   };
    }else if ([requestName isEqualToString:@"uploadImage"])
    {
        params = @{@"url":[inputArray objectAtIndex:2]};
    }else if ([requestName isEqualToString:@"forceUpdate"])
    {
        params = @{@"versioninfo":[inputArray objectAtIndex:2],
                   @"devicetype":[inputArray objectAtIndex:3],
                   @"deviceid":[inputArray objectAtIndex:4]
                   };
    }
    NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:50.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            if(([httpResponse statusCode]==200)||([httpResponse statusCode]==201))
                                                            {
                                                            NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                            [dict setValue:array forKey:@"data"];
                                                            [dict mutableCopy];
                                                            handler(dict);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        handler([NSDictionary new]);
                                                    }
                                                }];
    [dataTask resume];
}
- (NSString*) encodeStringTo64:(NSString*)encryptValue {
    const void *vplainText;
    size_t plainTextBufferSize = [encryptValue length];
    vplainText = (const void *) [encryptValue UTF8String];
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    bufferPtrSize = (plainTextBufferSize + kCCBlockSizeDES) & ~(kCCBlockSizeDES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    Byte iv [] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef};
    NSString * key = @"acc[zenwiseapid201816110";
    const void *vkey = (const void *) [key UTF8String];
    ccStatus = CCCrypt(kCCEncrypt,
                       kCCAlgorithm3DES,
                       kCCOptionECBMode|kCCOptionPKCS7Padding,
                       vkey,
                       kCCKeySize3DES,
                       iv,
                       vplainText,
                       plainTextBufferSize,
                       (void *)bufferPtr,
                       bufferPtrSize,
                       &movedBytes);
    
    NSData *myData = [NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes];
    NSString *result = [myData base64EncodedStringWithOptions:0];
    if([result containsString:@"/"])
    {
        result = [result stringByReplacingOccurrencesOfString:@"/" withString:@"@"];
    }
    if([result containsString:@"+"])
    {
        result = [result stringByReplacingOccurrencesOfString:@"+" withString:@"_"];
    }
    return result;
}

- (NSString*)encodeStringOldTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    } else {
        base64String = [plainData base64Encoding];                              // pre iOS7
    }
    return base64String;
}







@end

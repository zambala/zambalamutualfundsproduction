//
//  Utility.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface Utility : NSObject
+ (id)MF;
-(void)inputRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler;
@property NSString * baseUrl;
@property NSString * zambalaBaseUrl;
@property NSString * cid;
@property NSString * sessionId;
- (NSString*)encodeStringTo64:(NSString*)fromString;
-(void)zambalaGETRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler;
-(void)zambalaPOSTRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler;
@property Reachability * reach;

@end

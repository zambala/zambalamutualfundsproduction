//
//  AppDelegate.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property NSString * signUpCheck;
@property NSMutableArray * portfolioDataArray;
@property NSMutableArray * transactionDetailsArray;
@property NSMutableArray * transactionDataArray;
@property NSString * fundWatchSchemeCodeString;
@property NSString * zambalaBaseURL;
@property NSString * deviceToken;
@property NSData * mixpanelDeviceToken;
@property NSDictionary * notificationInfo;
@property NSString * pushNotificationSection;
@property NSString *deviceID;

@end


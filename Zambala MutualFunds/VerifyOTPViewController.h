//
//  VerifyOTPViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 03/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface VerifyOTPViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIView *popUpInsideView;
@property (weak, nonatomic) IBOutlet UIImageView *rocketImageView;
@property (weak, nonatomic) IBOutlet UIView *getStartedView;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;
@property (weak, nonatomic) IBOutlet UITextField *TFOne;
@property (weak, nonatomic) IBOutlet UITextField *TFTwo;
@property (weak, nonatomic) IBOutlet UITextField *TFThree;
@property (weak, nonatomic) IBOutlet UITextField *TFFour;
@property (weak, nonatomic) IBOutlet UITextField *TFFive;
@property (weak, nonatomic) IBOutlet UITextField *TFSix;
@property (weak, nonatomic) IBOutlet UIButton *resendOTP;

@property NSString * fullName;
@property NSString * emailID;
@property NSString * password;
@property NSString * mobileNumber;
@property NSString * countryCode;
@property Reachability * reach;

@property NSMutableDictionary * responseDataDictionary;

@end

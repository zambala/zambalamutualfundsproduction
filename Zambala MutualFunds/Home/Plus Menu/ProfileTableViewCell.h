//
//  ProfileTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 12/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;

@end

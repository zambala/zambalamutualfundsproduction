//
//  TransactionsViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 06/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HVTableView.h"
#import "Reachability.h"

@interface TransactionsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
//@property (weak, nonatomic) IBOutlet UITableView *transactionsTableView;
@property (weak, nonatomic) IBOutlet UIView *segmentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentViewHeightConstraint;

@property NSMutableDictionary * dataDictionary;
@property (weak, nonatomic) IBOutlet HVTableView *transactionsTableView;
@property Reachability * reach;


@end

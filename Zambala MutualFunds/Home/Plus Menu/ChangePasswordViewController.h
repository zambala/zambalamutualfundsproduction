//
//  ChangePasswordViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 12/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface ChangePasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property Reachability * reach;


@end

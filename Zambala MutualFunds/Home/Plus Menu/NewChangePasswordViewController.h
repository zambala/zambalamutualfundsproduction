//
//  NewChangePasswordViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 25/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewChangePasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *backMainView;
@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTF;
@property (weak, nonatomic) IBOutlet UITextField *nPasswordTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmNPassword;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property NSMutableDictionary * responseDataDictionary;

@property Reachability * reach;



@end

NS_ASSUME_NONNULL_END

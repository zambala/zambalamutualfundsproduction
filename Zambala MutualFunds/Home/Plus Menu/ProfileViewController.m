//
//  ProfileViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 06/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ProfileViewController.h"
#import "HMSegmentedControl.h"
#import "EkycThreeViewController.h"
#import "EkycFiveViewController.h"
#import "EkycSixViewController.h"
#import "ChangePasswordViewController.h"
#import "ProfileTableViewCell.h"


@interface ProfileViewController ()
{
    HMSegmentedControl * segmentedControl;
    NSMutableArray * questionsArray;
    int questionNumber;
    UIVisualEffect *blurEffect;
    UIVisualEffectView *visualEffectView;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    self.bankDetailsView.hidden=YES;
    self.nomineeDetailsView.hidden=YES;
    self.personalDetailsView.hidden=NO;
    self.analyseButton.layer.cornerRadius = 20.0f;
    
    self.popUpView.hidden=YES;
    self.innerPopUPView.hidden=YES;
    
    questionsArray = [[NSMutableArray alloc]init];
    [questionsArray addObject:@"Yourself"];
    [questionsArray addObject:@"2 people"];
    [questionsArray addObject:@"3-5 people"];
    [questionsArray addObject:@"More than 5 people"];
    NSMutableArray * localSegmentsArray = [[NSMutableArray alloc]init];
    
    [localSegmentsArray addObject:@"Personal Details"];
    [localSegmentsArray addObject:@"Bank Details"];
    [localSegmentsArray addObject:@"Risk Profile"];
    [localSegmentsArray addObject:@"Nominee Details"];
    
    self.segmentView.layer.cornerRadius=20.0f;
    self.segmentView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.segmentView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.segmentView.layer.shadowOpacity = 3.0f;
    self.segmentView.layer.shadowRadius = 3.0f;
    self.segmentView.layer.masksToBounds = NO;
    
    self.previousButton.layer.cornerRadius=20.0f;
    self.previousButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.previousButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.previousButton.layer.shadowOpacity = 3.0f;
    self.previousButton.layer.shadowRadius = 3.0f;
    self.previousButton.layer.masksToBounds = NO;
    
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 3.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.masksToBounds = NO;
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.segmentView.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    [self.segmentView addSubview:segmentedControl];
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bankChangeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.nomineeChangeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.personalChangeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.chnagePasswordButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.analyseButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.previousButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.bankChangeButton)
    {
        EkycFiveViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycFiveViewController"];
        kyc.profileCheck=@"profile";
        [self presentViewController:kyc animated:YES completion:nil];
    }else if (sender == self.nomineeChangeButton)
    {
        EkycSixViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycSixViewController"];
        kyc.profileCheck=@"profile";
        [self presentViewController:kyc animated:YES completion:nil];
    }else if (sender == self.personalChangeButton)
    {
        EkycThreeViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycThreeViewController"];
        kyc.profileCheck=@"profile";
        [self presentViewController:kyc animated:YES completion:nil];
    }else if (sender == self.chnagePasswordButton)
    {
        ChangePasswordViewController * changePassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
        [self presentViewController:changePassword animated:YES completion:nil];
    }else if (sender == self.analyseButton)
    {
        self.popUpView.hidden=NO;
        self.innerPopUPView.hidden=NO;
        visualEffectView.hidden=NO;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        visualEffectView.frame = self.view.bounds;
        visualEffectView.backgroundColor= [UIColor blackColor];
        visualEffectView.alpha=0.5;
        [self.popUpView addSubview:visualEffectView];
        self.questionsTableView.delegate=self;
        self.questionsTableView.dataSource=self;
        [self.questionsTableView reloadData];
        questionNumber=0;
        self.previousButton.hidden=YES;
        [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
    }else if (sender == self.nextButton)
    {
        
        questionNumber++;
        if(questionNumber>0)
        {
            self.previousButton.hidden=NO;
            
        }
        if(questionNumber == 2)
        {
            [self.nextButton setTitle:@"Submit" forState:UIControlStateNormal];
        }
        if(questionNumber == 3)
        {
            [self.analyseButton setTitle:@"Analyse again" forState:UIControlStateNormal];
            self.logoImageView.image = [UIImage imageNamed:@"riskprofile-balanced"];
            self.logoLabel.text=@"Your Risk appetite is Balanced";
            self.popUpView.hidden=YES;
            self.innerPopUPView.hidden=YES;
            visualEffectView.hidden=YES;
        }
        if(questionNumber == 0)
        {
            self.questionNumberLabel.text= @"Ques: 1 of 3";
        }else if (questionNumber==1)
        {
            self.questionNumberLabel.text=@"Ques: 2 of 3";
        }else if (questionNumber ==2)
        {
            self.questionNumberLabel.text=@"Ques: 3 of 3";
        }
        [self.questionsTableView reloadData];
    }else if (sender == self.previousButton)
    {
        questionNumber--;
    }else if (sender == self.closeButton)
    {
        self.popUpView.hidden=YES;
        self.innerPopUPView.hidden=YES;
        visualEffectView.hidden=YES;
        [self.analyseButton setTitle:@"Let's Analyse" forState:UIControlStateNormal];
        self.logoImageView.image= [UIImage imageNamed:@"logo"];
        self.logoLabel.text=@"Lets Analyse your risk appetite to recommend funds";
        
    }
}

-(void)onSegmentTap
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
    self.bankDetailsView.hidden=YES;
    self.nomineeDetailsView.hidden=YES;
    self.personalDetailsView.hidden=NO;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        self.bankDetailsView.hidden=NO;
        self.nomineeDetailsView.hidden=YES;
        self.personalDetailsView.hidden=YES;
    }else if (segmentedControl.selectedSegmentIndex==2)
    {
        
    }else if (segmentedControl.selectedSegmentIndex==3)
    {
        self.bankDetailsView.hidden=YES;
        self.nomineeDetailsView.hidden=NO;
        self.personalDetailsView.hidden=YES;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return questionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileTableViewCell" forIndexPath:indexPath];
    cell.questionLabel.text= [NSString stringWithFormat:@"%@",[questionsArray objectAtIndex:indexPath.row]];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

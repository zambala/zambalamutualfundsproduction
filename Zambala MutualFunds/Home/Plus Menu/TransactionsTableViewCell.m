//
//  TransactionsTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 06/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "TransactionsTableViewCell.h"

@implementation TransactionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backMainView.layer.cornerRadius=5.0f;
    self.backMainView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.backMainView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.backMainView.layer.shadowOpacity = 0.6f;
    self.backMainView.layer.shadowRadius = 3.5f;
    self.backMainView.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

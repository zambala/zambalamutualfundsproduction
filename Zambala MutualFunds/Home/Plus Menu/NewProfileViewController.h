//
//  NewProfileViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 25/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewProfileViewController : UIViewController<UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *profileNameView;
@property (weak, nonatomic) IBOutlet UIView *topDetailsView;
@property (weak, nonatomic) IBOutlet UIView *personalDetailsView;
@property (weak, nonatomic) IBOutlet UIView *bankDetailsView;
@property (weak, nonatomic) IBOutlet UIView *nomineeDetailsView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dobLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *marritalStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *nationalityLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *bankDetailsLabel;
@property (weak, nonatomic) IBOutlet UIButton *personalChangeButton;
@property (weak, nonatomic) IBOutlet UIButton *bankChangeButton;
@property (weak, nonatomic) IBOutlet UILabel *shortNameLabel;

@property NSMutableDictionary * responseDataDictionary;
@property BOOL newMedia;
@property (weak, nonatomic) IBOutlet UIButton *riskButton;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property NSString * imageURLString;
@property UIAlertAction * camera;
@property UIAlertAction * gallery;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *personalDetailsViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomPersonalDetailHeightConstraint;

@property Reachability * reach;
@property (weak, nonatomic) IBOutlet UILabel *nomineeLabel;


@end

NS_ASSUME_NONNULL_END

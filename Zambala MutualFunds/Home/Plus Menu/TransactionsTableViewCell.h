//
//  TransactionsTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 06/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *imageButton;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UILabel *schemeName;
@property (weak, nonatomic) IBOutlet UILabel *investedLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *swichView;
@property (weak, nonatomic) IBOutlet UIImageView *expandImageView;
@property (weak, nonatomic) IBOutlet UIView *backMainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *transactionType;

@end

//
//  RiskProfileTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 15/10/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RiskProfileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *answersButton;

@end

NS_ASSUME_NONNULL_END

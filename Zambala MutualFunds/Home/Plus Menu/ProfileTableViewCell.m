//
//  ProfileTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 12/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

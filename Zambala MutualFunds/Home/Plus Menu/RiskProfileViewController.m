//
//  RiskProfileViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 15/10/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "RiskProfileViewController.h"
#import "Utility.h"
#import "RiskProfileTableViewCell.h"

@interface RiskProfileViewController ()
{
    NSString * screenNumber;
    NSString * selectedCheck;
    Utility * mfSharedManager;
    NSString * questionID, *QuestID;
    //NSString * answerOne,answerTwo,answerThree,answerFour,answerFive;
}

@end

@implementation RiskProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenNumber = @"1";
    selectedCheck = @"unSelected";
    mfSharedManager=[Utility MF];
    
    self.imageViewOne.image = [UIImage imageNamed:@"dotSelected"];
    self.imageViewTwo.image = [UIImage imageNamed:@"dotUnSelected"];
    self.imageViewThree.image = [UIImage imageNamed:@"dotUnSelected"];
    self.imageViewFour.image = [UIImage imageNamed:@"dotUnSelected"];
    self.imageViewFive.image = [UIImage imageNamed:@"dotUnSelected"];
    
    [self.answerOneButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.answerTwoButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.answerThreeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.answerFourButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.answerFiveButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.retakeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.answerOneButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    [self.answerTwoButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    [self.answerThreeButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    [self.answerFourButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    [self.answerFiveButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    
    
    [self.answerOneButton setImage:[UIImage imageNamed:@"radioUnSelected"] forState:UIControlStateNormal];
    [self.answerTwoButton setImage:[UIImage imageNamed:@"radioUnSelected"] forState:UIControlStateNormal];
    [self.answerThreeButton setImage:[UIImage imageNamed:@"radioUnSelected"] forState:UIControlStateNormal];
    [self.answerFourButton setImage:[UIImage imageNamed:@"radioUnSelected"] forState:UIControlStateNormal];
    [self.answerFiveButton setImage:[UIImage imageNamed:@"radioUnSelected"] forState:UIControlStateNormal];
    self.riskProfileTableView.delegate = self;
    self.riskProfileTableView.dataSource = self;
    [self.riskProfileTableView reloadData];
    [self getRiskProfile];
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(id)sender
{
    if(sender == self.answerOneButton)
    {
        self.answerOneButton.selected = YES;
        self.answerTwoButton.selected = NO;
        self.answerThreeButton.selected = NO;
        self.answerFourButton.selected = NO;
        self.answerFiveButton.selected = NO;
        selectedCheck = @"selected";
    }else if (sender == self.answerTwoButton)
    {
        self.answerOneButton.selected = NO;
        self.answerTwoButton.selected = YES;
        self.answerThreeButton.selected = NO;
        self.answerFourButton.selected = NO;
        self.answerFiveButton.selected = NO;
         selectedCheck = @"selected";
    }else if (sender == self.answerThreeButton)
    {
        self.answerOneButton.selected = NO;
        self.answerTwoButton.selected = NO;
        self.answerThreeButton.selected = YES;
        self.answerFourButton.selected = NO;
        self.answerFiveButton.selected = NO;
        selectedCheck = @"selected";
    }else if (sender == self.answerFourButton)
    {
        self.answerOneButton.selected = NO;
        self.answerTwoButton.selected = NO;
        self.answerThreeButton.selected = NO;
        self.answerFourButton.selected = YES;
        self.answerFiveButton.selected = NO;
        selectedCheck = @"selected";
    }else if (sender == self.answerFiveButton)
    {
        self.answerOneButton.selected = NO;
        self.answerTwoButton.selected = NO;
        self.answerThreeButton.selected = NO;
        self.answerFourButton.selected = NO;
        self.answerFiveButton.selected = YES;
        selectedCheck = @"selected";
    }else if (sender == self.nextButton)
    {
        if([screenNumber isEqualToString:@"null"]||[screenNumber isEqual:[NSNull null]])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Please select a choice." preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:okAction];
            });
        }else
        {
        [self logicMethod];
        }
    }else if (sender == self.retakeButton)
    {
        self.profileView.hidden=YES;
        [self getRiskProfile];
    }
}

-(void)getRiskProfile
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getRecommendations"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@",cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@get_riskprofiledata/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString * data = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"clientriskdata"]objectAtIndex:0] objectForKey:@"Description"]];
                if([data isEqualToString:@"Aggressive"])
                {
                    self.profileImageView.image = [UIImage imageNamed:@"Aggressive"];
                }else if ([data isEqualToString:@"Moderately Conservative"])
                {
                    self.profileImageView.image = [UIImage imageNamed:@"ModeratelyConservative"];
                }else if ([data isEqualToString:@"Moderately aggressive"])
                {
                    self.profileImageView.image = [UIImage imageNamed:@"ModeratelyAggressive"];
                }else if ([data containsString:@"Conservative"])
                {
                    self.profileImageView.image = [UIImage imageNamed:@"Conservative"];
                }else if ([data containsString:@"Balanced"])
                {
                    self.profileImageView.image = [UIImage imageNamed:@"Balanced"];
                }else
                {
                    self.profileView.hidden=YES;
                }
                [self logicMethod];
                //[self.riskProfileTableView reloadData];
            });
        }];
    }
}

-(void)logicMethod
{
    
    if([screenNumber isEqualToString:@"1"])
    {
        //NSString * answer = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"ANswerMaster"] objectAtIndex:0] objectForKey:@""]];
        NSString * question = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:0] objectForKey:@"Quest_Name"]];
        self.questionLabel.text = question;
        [self.riskProfileTableView reloadData];
    }else if([screenNumber isEqualToString:@"2"])
    {
        NSString * question = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:1] objectForKey:@"Quest_Name"]];
        self.questionLabel.text = question;
        [self.riskProfileTableView reloadData];
    }else if([screenNumber isEqualToString:@"3"])
    {
        NSString * question = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:2] objectForKey:@"Quest_Name"]];
        self.questionLabel.text = question;
        [self.riskProfileTableView reloadData];
    }else if([screenNumber isEqualToString:@"4"])
    {
        NSString * question = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:3] objectForKey:@"Quest_Name"]];
        self.questionLabel.text = question;
        [self.riskProfileTableView reloadData];
    }else if([screenNumber isEqualToString:@"5"])
    {
        NSString * question = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:4] objectForKey:@"Quest_Name"]];
        self.questionLabel.text = question;
        [self.riskProfileTableView reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RiskProfileTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"RiskProfileTableViewCell" forIndexPath:indexPath];
    if([screenNumber isEqualToString:@"1"])
    {
        questionID=[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:0] objectForKey:@"Quest_ID"]];
    }else if ([screenNumber isEqualToString:@"2"])
    {
        questionID=[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:1] objectForKey:@"Quest_ID"]];
    }else if ([screenNumber isEqualToString:@"3"])
    {
        questionID=[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:2] objectForKey:@"Quest_ID"]];
    }else if ([screenNumber isEqualToString:@"4"])
    {
        questionID=[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:3] objectForKey:@"Quest_ID"]];
    }else if ([screenNumber isEqualToString:@"5"])
    {
        questionID=[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"QuestionMaster"] objectAtIndex:4] objectForKey:@"Quest_ID"]];
    }
    
   QuestID = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"OptionMaster"] objectAtIndex:indexPath.row] objectForKey:@"Quest_ID"]];
    if([QuestID isEqualToString:questionID])
    {
        NSString * answer = [NSString stringWithFormat:@"  %@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"OptionMaster"] objectAtIndex:indexPath.row] objectForKey:@"Option_Description"]];
        [cell.answersButton setTitle:answer forState:UIControlStateNormal];
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  NewChangePasswordViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 25/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "NewChangePasswordViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "LoginViewController.h"

@interface NewChangePasswordViewController ()
{
    Utility * mfSharedManager;
    DGActivityIndicatorView * activityIndicatorView;
    UIView * backView;
}

@end

@implementation NewChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mfSharedManager = [Utility MF];
    self.backMainView.layer.cornerRadius=5.0f;
    self.backMainView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.backMainView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.backMainView.layer.shadowOpacity = 1.0f;
    self.backMainView.layer.shadowRadius = 3.0f;
    self.backMainView.layer.masksToBounds = NO;
    
    self.oldPasswordTF.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.oldPasswordTF.leftView = paddingView;
    
    self.nPasswordTF.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.nPasswordTF.leftView = paddingView1;
    
    self.confirmNPassword.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.confirmNPassword.leftView = paddingView2;
//    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,15,15)];
//    imageView.image = [UIImage imageNamed:@"keyNew"];
//    self.oldPasswordTF.leftView = imageView;
   // self.oldPasswordTF.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"keyNew"]];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.changePasswordButton addTarget:self action:@selector(onChangePasswordTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onBackButtonTap
{
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)onChangePasswordTap
{
    if([self networkStatus]==YES)
    {
    if([self.nPasswordTF.text isEqualToString:self.confirmNPassword.text]&&self.nPasswordTF.text.length!=0&&self.confirmNPassword.text.length!=0)
    {
        backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        [self.view addSubview:backView];
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
        activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
        [activityIndicatorView setCenter:self.view.center];
        [backView addSubview:activityIndicatorView];
        [activityIndicatorView startAnimating];
                
                NSMutableArray * inputArray=[[NSMutableArray alloc]init];
                [inputArray addObject:@"forgetrequest"];
                NSString * paramsString;
                NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
                NSString * cid = [userSessionData objectForKey:@"cid"];
                paramsString = [NSString stringWithFormat:@"%@|%@",cid,self.confirmNPassword.text];
                [inputArray addObject:[NSString stringWithFormat:@"%@updatepassword_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:paramsString]]];
                [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                    self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
                    if(self.responseDataDictionary)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [activityIndicatorView stopAnimating];
                            [backView removeFromSuperview];
                            CATransition *transition = [[CATransition alloc] init];
                            transition.duration = 0.5;
                            transition.type = kCATransitionPush;
                            transition.subtype = kCATransitionFromLeft;
                            [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                            [self.view.window.layer addAnimation:transition forKey:kCATransition];
                            LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                            [self presentViewController:login animated:YES completion:nil];
                           // [self dismissViewControllerAnimated:YES completion:nil];
                        });
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert animated:YES completion:^{
                                
                            }];
                            
                            
                            
                            UIAlertAction * ok=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                            }];
                            
                            [alert addAction:ok];
                            
                        });
                    }
                }];
    }
                else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"New password and Confirm password should be same." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            UIAlertAction * ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:ok];
            
        });
    }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

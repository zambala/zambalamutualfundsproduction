//
//  TransactionsViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 06/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "TransactionsViewController.h"
#import "TransactionsTableViewCell.h"
#import "EquityDetailViewController.h"
#import "HMSegmentedControl.h"
#import "FilterViewController.h"
#import "DGActivityIndicatorView.h"
#import "Utility.h"
#import "HVTableView.h"
@import Mixpanel;

@interface TransactionsViewController ()<HVTableViewDelegate,HVTableViewDataSource>
{
    HMSegmentedControl * segmentedControl;
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    Mixpanel * mixpanelFunds;
}

@end

@implementation TransactionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     mfSharedManager=[Utility MF];
    NSMutableArray * localSegmentsArray = [[NSMutableArray alloc]init];
    
    [localSegmentsArray addObject:@"Past Transactions"];
    [localSegmentsArray addObject:@"Upcoming Transactions"];
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.segmentView.frame.size.width, 50);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    [self.segmentView addSubview:segmentedControl];
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    [mixpanelFunds track:@"transactions_page"];
    
    self.segmentView.hidden=YES;
    self.segmentViewHeightConstraint.constant=0;
    [self.filterButton setTintColor:[UIColor blackColor]];
    
    self.transactionsTableView.HVTableViewDelegate=self;
    self.transactionsTableView.HVTableViewDataSource=self;
    [self.transactionsTableView reloadData];
    
    segmentedControl.selectedSegmentIndex = 0;
    [self onSegmentTap];
    [self getPastTransactions];
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.filterButton addTarget:self action:@selector(onFilterButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)indicatorMethod
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)onFilterButtonTap
{
    FilterViewController * filter = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    [self presentViewController:filter animated:YES completion:nil];
}

-(void)onSegmentTap
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
    [self.transactionsTableView reloadData];
    }
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getPastTransactions
{
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"YYYY-MM-dd"];
        // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
        NSString * toDate = [dateFormatter stringFromDate:[NSDate date]];
         NSCalendar *cal = [NSCalendar currentCalendar];
        NSDate *yourDate = [NSDate date];
        NSDate *someDate = [cal dateByAddingUnit:NSCalendarUnitMonth value:-12 toDate:yourDate options:0];
        [dateFormatter setDateFormat:@"YYYY-MM-dd"];
        NSString * fromDate = [dateFormatter stringFromDate:someDate];
        
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getAccountStatement"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@",cid,sessionid,fromDate,toDate];
        [inputArray addObject:[NSString stringWithFormat:@"%@getAccountStatement_report_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.dataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.dataDictionary);
            if(self.dataDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.transactionsTableView reloadData];
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] count];
}


//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    TransactionsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionsTableViewCell" forIndexPath:indexPath];
//    cell.imageButton.layer.cornerRadius = cell.imageButton.frame.size.width/2;
//    [cell.imageButton setTintColor:[UIColor whiteColor]];
//    if(segmentedControl.selectedSegmentIndex==0)
//    {
//        cell.deleteButton.hidden=YES;
//    }else
//    {
//        cell.deleteButton.hidden=NO;
//    }
//
//    NSString * schemeName = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"FUND"]];
//    cell.schemeName.text=schemeName;
//
//    NSString * investedAmount = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Amount"]];
//    cell.investedLabel.text = investedAmount;
//
//    NSString * units = [NSString stringWithFormat:@"%.2f",[[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Units"]floatValue]];
//    cell.unitsLabel.text = units;
//
//    NSString * date = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Date"]];
//    cell.dateLabel.text = date;
//    return cell;
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 220;
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    EquityDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
//    [self.navigationController pushViewController:detail animated:YES];
//}

#pragma mark - HVTableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}

-(void)tableView:(UITableView *)tableView expandCell:(TransactionsTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    [UIView animateWithDuration:.5 animations:^{
        cell.expandImageView.image = [UIImage imageNamed:@"collapseArrow"];
    }];
}

-(void)tableView:(UITableView *)tableView collapseCell:(TransactionsTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    
    [UIView animateWithDuration:.5 animations:^{
        cell.expandImageView.image = [UIImage imageNamed:@"expandArrow"];
    }];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isExpanded
{
    TransactionsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionsTableViewCell" forIndexPath:indexPath];
    cell.imageButton.layer.cornerRadius = cell.imageButton.frame.size.width/2;
    [cell.imageButton setTintColor:[UIColor whiteColor]];
    if(segmentedControl.selectedSegmentIndex==0)
    {
        cell.deleteButton.hidden=YES;
    }else
    {
        cell.deleteButton.hidden=NO;
    }
    
    //40
    
    
    NSString * schemeName = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"FUND"]];
    cell.schemeName.text=schemeName;
    
    NSString * transactionType = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Transaction_Type"]];
    cell.transactionType.text = transactionType;
    
    if([transactionType containsString:@"SWITCH"])
    {
        cell.swichView.hidden=NO;
        cell.switchHeightConstraint.constant=40;
    }else
    {
    cell.swichView.hidden=YES;
    cell.switchHeightConstraint.constant=0;
    }
    
    NSString * investedAmount = [NSString stringWithFormat:@"₹%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Amount"]];
    cell.investedLabel.text = investedAmount;
    
    NSString * nav = [NSString stringWithFormat:@"₹%.2f",[[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"NAVRS"]floatValue]];
    cell.navLabel.text = nav;
    NSString * units = [NSString stringWithFormat:@"%.2f",[[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Units"]floatValue]];
    cell.unitsLabel.text = units;
    
    NSString * date = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Date"]];
    cell.dateLabel.text = date;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isexpanded
{
    NSString * transactionType = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"Transaction_Type"]];
    if (isexpanded)
    {
        if([transactionType containsString:@"SWITCH"])
        {
            return 205;
        }else
        {
            return 165;
        }
    }else
    {
        if([transactionType containsString:@"SWITCH"])
        {
            return 80;
        }else
        {
            return 91;
        }
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

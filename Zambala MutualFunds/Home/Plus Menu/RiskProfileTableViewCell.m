//
//  RiskProfileTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 15/10/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "RiskProfileTableViewCell.h"

@implementation RiskProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  AboutViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 25/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "AboutViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "OpenAccountWebViewController.h"
@import Mixpanel;

@interface AboutViewController ()
{
    Mixpanel * mixpanelFunds;
}

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    [mixpanelFunds track:@"about_us_page"];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.termsAndConditionsButton addTarget:self action:@selector(onTermsTap) forControlEvents:UIControlEventTouchUpInside];
    [self.privacyPolicyButton addTarget:self action:@selector(onPrivacyTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onTermsTap
{
    [mixpanelFunds track:@"terms_and_conditions_page"];
    OpenAccountWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountWebViewController"];
    webView.sourceString = @"Terms";
    [self presentViewController:webView animated:YES completion:nil];
}

-(void)onPrivacyTap
{
    [mixpanelFunds track:@"privacy_policy_page"];
    OpenAccountWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountWebViewController"];
    webView.sourceString = @"Privacy";
    [self presentViewController:webView animated:YES completion:nil];
}

-(void)onBackButtonTap
{
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ProfileViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 06/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *logoLabel;
@property (weak, nonatomic) IBOutlet UIButton *analyseButton;
@property (weak, nonatomic) IBOutlet UIView *segmentView;
@property (weak, nonatomic) IBOutlet UIView *nomineeDetailsView;
@property (weak, nonatomic) IBOutlet UIView *bankDetailsView;
@property (weak, nonatomic) IBOutlet UIView *personalDetailsView;
@property (weak, nonatomic) IBOutlet UIButton *personalChangeButton;
@property (weak, nonatomic) IBOutlet UIButton *bankChangeButton;
@property (weak, nonatomic) IBOutlet UIButton *nomineeChangeButton;
@property (weak, nonatomic) IBOutlet UIButton *chnagePasswordButton;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIView *innerPopUPView;
@property (weak, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (weak, nonatomic) IBOutlet UITableView *questionsTableView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;

@end

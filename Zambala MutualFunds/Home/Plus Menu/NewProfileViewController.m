//
//  NewProfileViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 25/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "NewProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NewChangePasswordViewController.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "RiskProfileViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "LoginViewController.h"
@import Mixpanel;

@interface NewProfileViewController ()

@end

@implementation NewProfileViewController
{
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backIndicatorView;
    UIImagePickerController * pickerController;
    Mixpanel * mixpanelFunds;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mfSharedManager = [Utility MF];
    self.topView.layer.cornerRadius=5.0f;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.topView.layer.shadowOpacity = 1.0f;
    self.topView.layer.shadowRadius = 3.0f;
    self.topView.layer.masksToBounds = NO;
    
    self.profileNameView.layer.cornerRadius = self.profileNameView.frame.size.width/2;
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
    self.profileImageView.hidden = YES;
    
    self.topDetailsView.layer.cornerRadius=5.0f;
    self.topDetailsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.topDetailsView.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.topDetailsView.layer.shadowOpacity = 0.6f;
    self.topDetailsView.layer.shadowRadius = 3.0f;
    self.topDetailsView.layer.masksToBounds = NO;
    
    
    self.personalDetailsView.layer.cornerRadius=5.0f;
    self.personalDetailsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.personalDetailsView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.personalDetailsView.layer.shadowOpacity = 1.0f;
    self.personalDetailsView.layer.shadowRadius = 3.0f;
    self.personalDetailsView.layer.masksToBounds = NO;
    
    self.bankDetailsView.layer.cornerRadius=5.0f;
    self.bankDetailsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.bankDetailsView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.bankDetailsView.layer.shadowOpacity = 1.0f;
    self.bankDetailsView.layer.shadowRadius = 3.0f;
    self.bankDetailsView.layer.masksToBounds = NO;
    
    self.nomineeDetailsView.layer.cornerRadius=5.0f;
    self.nomineeDetailsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.nomineeDetailsView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.nomineeDetailsView.layer.shadowOpacity = 1.0f;
    self.nomineeDetailsView.layer.shadowRadius = 3.0f;
    self.nomineeDetailsView.layer.masksToBounds = NO;
    
    
    self.personalChangeButton.hidden=YES;
    self.bankChangeButton.hidden=YES;
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    
    
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.changePasswordButton addTarget:self action:@selector(onChangePasswordTap) forControlEvents:UIControlEventTouchUpInside];
    [self.riskButton addTarget:self action:@selector(onRiskButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.cameraButton addTarget:self action:@selector(onCameraButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    if([guestCheck isEqualToString:@"0"])
    {
//        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"You currently can not access this feature as a Guest user." preferredStyle:UIAlertControllerStyleActionSheet];
//        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Login as Client" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//            // OK button tapped.
//            LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//            login.guestLoginString = @"Guest";
//            login.logoutCheckString = @"logout";
//            [self presentViewController:login animated:YES completion:nil];
//
//
//        }]];
//
//        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//
//            // Cancel button tappped.
//           // [self.navigationController popViewControllerAnimated:YES];
//                        [self dismissViewControllerAnimated:YES completion:^{
//                        }];
//        }]];
//
//
//        // Present action sheet.
//        [self presentViewController:actionSheet animated:YES completion:nil];
        
        self.personalDetailsViewHeightConstraint.constant = 100;
        self.bottomPersonalDetailHeightConstraint.constant = 150;
        self.genderLabel.hidden = YES;
        self.marritalStatusLabel.hidden = YES;
        self.nationalityLabel.hidden= YES;
        self.addressLabel.hidden = YES;
        self.bankDetailsView.hidden = YES;
        self.nomineeDetailsView.hidden = YES;
    }else
    {
        self.personalDetailsViewHeightConstraint.constant = 144;
        self.bottomPersonalDetailHeightConstraint.constant = 253;
        self.genderLabel.hidden = NO;
        self.marritalStatusLabel.hidden = NO;
        self.nationalityLabel.hidden= NO;
        self.addressLabel.hidden = NO;
        self.bankDetailsView.hidden = NO;
        self.nomineeDetailsView.hidden = NO;
        [self getDetails];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)onCameraButtonTap
{
    [self cameraMethod];
}

-(void)getImage
{
//    backIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    backIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
//    [self.view addSubview:backIndicatorView];
//    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
//    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
//    [activityIndicatorView setCenter:self.view.center];
//    [backIndicatorView addSubview:activityIndicatorView];
//    [activityIndicatorView startAnimating];
    dispatch_async(dispatch_get_main_queue(), ^{
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * clientID;
    NSString * isGuest = [userSessionData objectForKey:@"isguest"];
    if([isGuest isEqualToString:@"0"])
    {
        clientID = [userSessionData objectForKey:@"zenwiseID"];
    }else
    {
        clientID = [userSessionData objectForKey:@"cid"];
    }
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"getImage"];
    NSString * endPoint = [NSString stringWithFormat:@"%@profile/%@",mfSharedManager.zambalaBaseUrl,clientID];
    [inputArray addObject:endPoint];
    [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        if([[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"message"]isEqualToString:@"Success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
//                [activityIndicatorView stopAnimating];
//                [backIndicatorView removeFromSuperview];
                NSString * logoURL = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"logourl"]];
                if(logoURL.length!=0)
                {
                    self.profileNameView.layer.cornerRadius = self.profileNameView.frame.size.width/2;
                    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
                    self.profileImageView.hidden=NO;
                    self.shortNameLabel.hidden = NO;
                    dispatch_async(dispatch_get_main_queue(), ^{
                    self.profileImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:logoURL]]];
                     });
                }
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backIndicatorView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }];
  });
}


-(void)sendTOS3
{
    backIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backIndicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backIndicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    @try {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *str_Filename = [documentsDirectory stringByAppendingPathComponent:@"One.png"];
        NSData *multiMedaiData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:str_Filename]];
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        NSMutableData *body = [NSMutableData data];
        if (multiMedaiData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; filename=\"%@\"\r\n",str_Filename] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:multiMedaiData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        NSDictionary *headers = @{ @"content-type": contentType,@"Content-Length":postLength,@"x-access-token": @"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2UxOTAiLCJpc3MiOiJodHRwczovL3d3dy5oY3VlLmNvIiwicGVybWlzc2lvbnMiOiJhdXRoZW50aWNhdGVkIiwidXNlcmlkIjpudWxsLCJpYXQiOjE0ODYyMDczNDB9.ewu2QtoHl1kBSrM1y6yL9Jr58kiGmhCsy2YWoFrE2OU" };
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://zambalauploaddev.ap-southeast-1.elasticbeanstalk.com/uploadfunds"]
                                        
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                        
                                                           timeoutInterval:10.0];
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:body];
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                          
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        
                                                        if(data !=nil)
                                                            
                                                        {
                                                            
                                                            if (error) {
                                                                
                                                                //NSLog(@"%@", error);
                                                                
                                                            } else {
                                                                
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                
                                                                //NSLog(@"%@", httpResponse);
                                                                
                                                                if([httpResponse statusCode]==200)
                                                                {
                                                                
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        [activityIndicatorView stopAnimating];
                                                                        [backIndicatorView removeFromSuperview];
                                                                    });
                                                                
                                                                NSMutableDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
                                                                        self.imageURLString = [[responseDictionary objectForKey:@"message"] objectForKey:@"Location"];
                                                                        self.profileImageView.hidden=NO;
                                                                        self.shortNameLabel.hidden = YES;
                                                                        self.profileImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imageURLString]]];
                                                                        //self.profileImageView.image=[UIImage imageNamed:self.imageURLString];
                                                                        [self sendToServer];
                                                                    });
                                                            }
                                                            }
                                                            
                                                        }
                                                        
                                                        else
                                                            
                                                        {
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                    [activityIndicatorView stopAnimating];
                                                                    [backIndicatorView removeFromSuperview];
                                                              
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Oops! There was a problem with the network" preferredStyle:UIAlertControllerStyleAlert];
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                }];
                                                                UIAlertAction * retryAction=[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                    [self sendTOS3];
                                                                    
                                                                }];
                                                                
                                                                
                                                                
                                                                UIAlertAction * cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                    
                                                                    
                                                                }];
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                [alert addAction:retryAction];
                                                                
                                                                [alert addAction:cancel];
                                                                
                                                                
                                                                
                                                            });
                                                            
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    }];
        
        [dataTask resume];
        
        
        
    }
    
    @catch (NSException * e) {
        
        //NSLog(@"Exception: %@", e);
        
    }
    
    @finally {
        
        //NSLog(@"finally");
        
    }
}

-(void)sendToServer
{
    backIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backIndicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backIndicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    // http://13.127.251.76:8010/api/kyc/?clientid=143
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * clientID;
    NSString * isGuest = [userSessionData objectForKey:@"isguest"];
    if([isGuest isEqualToString:@"0"])
    {
     clientID = [userSessionData objectForKey:@"zenwiseID"];
    }else
    {
        clientID = [userSessionData objectForKey:@"cid"];
    }
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"uploadImage"];
    NSString * endPoint = [NSString stringWithFormat:@"%@profile/%@",mfSharedManager.zambalaBaseUrl,clientID];
    [inputArray addObject:endPoint];
    [inputArray addObject:self.imageURLString];
    [mfSharedManager zambalaPOSTRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        if([[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"message"]isEqualToString:@"success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backIndicatorView removeFromSuperview];
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backIndicatorView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }];
}



- (void)takePhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)selectPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)cameraMethod
{
    @try
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Choose one" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        [self presentViewController:alert animated:YES completion:nil];
        self.camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            UIImagePickerController * cameraPicker = [[UIImagePickerController alloc]init];
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            }
            cameraPicker.delegate=self;
            [self presentViewController:cameraPicker animated:YES completion:nil];
            cameraPicker.mediaTypes = [NSArray arrayWithObjects:
                                       (NSString *) kUTTypeImage,
                                       nil];
            cameraPicker.allowsEditing = NO;
            // [self presentViewController:cameraPicker animated:YES completion:nil];
            self.newMedia = YES;
        }];
        
        self.gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            pickerController.delegate=self;
            [self presentViewController:pickerController animated:YES completion:nil];
            
            
        }];
        
        UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:self.camera];
        [alert addAction:self.gallery];
        [alert addAction:cancel];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    @try
    {
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        NSData *png_Data = UIImageJPEGRepresentation(image, 0);
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *str_localFilePath = [documentsDirectory stringByAppendingPathComponent:@"One.png"];
        [png_Data writeToFile:str_localFilePath atomically:YES];
        [self sendTOS3];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

- (NSString *)documentsPathForFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

-(void)onRiskButtonTap
{
    RiskProfileViewController * risk = [self.storyboard instantiateViewControllerWithIdentifier:@"RiskProfileViewController"];
    [self presentViewController:risk animated:YES completion:nil];
}

-(void)getDetails
{
    backIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backIndicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backIndicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    // http://13.127.251.76:8010/api/kyc/?clientid=143
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"getprofileDetails"];
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * clientID;
    NSString * isGuest = [userSessionData objectForKey:@"isguest"];
    if([isGuest isEqualToString:@"0"])
    {
        clientID = [userSessionData objectForKey:@"zenwiseID"];
    }else
    {
        clientID = [userSessionData objectForKey:@"cid"];
    }
    NSString * endPoint = [NSString stringWithFormat:@"http://13.127.251.76:8010/api/kyc/?clientid=%@",clientID];
    [inputArray addObject:endPoint];
    [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        if([[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"message"]isEqualToString:@"success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backIndicatorView removeFromSuperview];
                [self getImage];
                [self assignUI];
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backIndicatorView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }];
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)onChangePasswordTap
{
    NewChangePasswordViewController * password = [self.storyboard instantiateViewControllerWithIdentifier:@"NewChangePasswordViewController"];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:password animated:false completion:nil];
}

-(void)assignUI
{
    NSString * firstName = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"firstname"]];
    NSString * lastName = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"lastname"]];
    NSString * fullName = [firstName stringByAppendingString:lastName];
    NSString * shortName = [NSString stringWithFormat:@"%@%@",[firstName substringToIndex:1],[lastName substringToIndex:1]];
    self.fullNameLabel.text = fullName;
    self.shortNameLabel.text = shortName;
    
    NSString * fatherName = [NSString stringWithFormat:@"S/O Mr. %@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"identityinfo"]objectForKey:@"APP_F_NAME"]];
    self.fatherNameLabel.text = fatherName;
     NSString * gender = [NSString stringWithFormat:@"S/O Mr. %@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"identityinfo"]objectForKey:@"APP_GEN"]];
    if([gender isEqualToString:@"M"])
    {
        self.genderLabel.text = @"Male";
    }else if ([gender isEqualToString:@"F"])
    {
        self.genderLabel.text = @"Female";
    }
    NSString * address1 = [NSString stringWithFormat:@"%@,%@\n%@%@\n%@%@ - %@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"addressinfo"]objectForKey:@"APP_PER_ADD1"],[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"addressinfo"]objectForKey:@"APP_PER_ADD2"],[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"addressinfo"]objectForKey:@"APP_PER_ADD3"],[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"addressinfo"]objectForKey:@"APP_PER_CITY"],[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"addressinfo"]objectForKey:@"APP_PER_STATE"],[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"addressinfo"]objectForKey:@"APP_PER_CTRY"],[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"addressinfo"]objectForKey:@"APP_PER_PINCD"]];
    self.addressLabel.text = address1;
    NSString * mobileNumber = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"contactinfo"]objectForKey:@"APP_MOB_NO"]];
    self.mobileNumberLabel.text = mobileNumber;
    NSString * email = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"contactinfo"]objectForKey:@"APP_EMAIL"]];
    self.emailLabel.text = email;
    
    NSString * maritalStatus = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"identityinfo"]objectForKey:@"APP_MAR_STATUS_FULLNAME"]];
    self.marritalStatusLabel.text = maritalStatus;
    
    NSString * nationality = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"identityinfo"]objectForKey:@"APP_NATIONALITY_NAME"]];
    self.nationalityLabel.text = nationality;
    NSString * accountNumber = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"bankinfo"] objectForKey:@"bankAccNo"]];
    NSString *code = [accountNumber substringFromIndex: [accountNumber length] - 4];
    
    NSString * fullAccountNumber = [NSString stringWithFormat:@"XXXXXXXXXXXX%@",code];
    NSString * bankDetails = [NSString stringWithFormat:@"%@\n%@\n%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"bankinfo"] objectForKey:@"bankName"],[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"bankinfo"] objectForKey:@"bankAddress"],fullAccountNumber];
    self.bankDetailsLabel.text = bankDetails;
    
    NSString * nomineeDetails = [NSString stringWithFormat:@"%@,\n%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"nominationinfo"] objectForKey:@"nom_name"],[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"nominationinfo"] objectForKey:@"nom_relationship"]];
    self.nomineeLabel.text = nomineeDetails;
    
    [mixpanelFunds track:@"profile_page" properties:@{@"$name":self.fullNameLabel.text,@"Guardian Name":self.fatherNameLabel.text,@"DOB":self.dobLabel.text,@"Gender":self.genderLabel.text,@"Marital Status":self.marritalStatusLabel.text,@"Nationality":self.nationalityLabel.text,@"Address":self.addressLabel.text,@"$phone":self.mobileNumberLabel.text,@"$email":self.emailLabel.text,@"Risk Appetite":@""}];
     // Risk Appetite
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

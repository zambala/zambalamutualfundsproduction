//
//  GoalsListViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "GoalsListViewController.h"
#import "GoalsListTableViewCell.h"
#import "GoalsListDetailViewController.h"

@interface GoalsListViewController ()

@end

@implementation GoalsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"Goals";
    
    self.goalsListTableView.delegate=self;
    self.goalsListTableView.dataSource=self;
    [self.goalsListTableView reloadData];
    // Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GoalsListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"GoalsListTableViewCell" forIndexPath:indexPath];
    if(indexPath.row==1)
    {
        cell.riskImageView.image= [UIImage imageNamed:@"need-attention"];
        cell.riskLabel.text =@"Need Attention";
        cell.riskLabel.textColor = [UIColor redColor];
    }else
    {
        cell.riskImageView.image = [UIImage imageNamed:@"on-track"];
        cell.riskLabel.text=@"On Track";
        cell.riskLabel.textColor= [UIColor colorWithRed:(52.0/255.0) green:(185.0/255.0) blue:(146.0/255.0) alpha:1];
    }
    return cell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 240;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GoalsListDetailViewController * goalsDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"GoalsListDetailViewController"];
    if(indexPath.row==1)
    {
        goalsDetail.riskCheck=@"risk";
    }
    [self.navigationController pushViewController:goalsDetail animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

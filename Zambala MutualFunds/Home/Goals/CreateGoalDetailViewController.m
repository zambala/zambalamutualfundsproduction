//
//  CreateGoalDetailViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "CreateGoalDetailViewController.h"
#import "LinkGoalsViewController.h"

@interface CreateGoalDetailViewController ()

@end

@implementation CreateGoalDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.continueButton.layer.cornerRadius=20.0f;
    self.continueButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.continueButton.layer.shadowOffset = CGSizeMake(0, 10.0f);
    self.continueButton.layer.shadowOpacity = 10.0f;
    self.continueButton.layer.shadowRadius = 3.0f;
    self.continueButton.layer.masksToBounds = NO;
    
    self.targetAmountTF.text = [NSString stringWithFormat:@"%i",(int)self.targetAmountSlider.value];
    self.timePeriodTF.text = [NSString stringWithFormat:@"%i",(int)self.timePeriodSlider.value];
    
    [self.continueButton addTarget:self action:@selector(onContinueButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.targetAmountSlider addTarget:self action:@selector(onSliderChange:) forControlEvents:UIControlEventValueChanged];
     [self.timePeriodSlider addTarget:self action:@selector(onSliderChange:) forControlEvents:UIControlEventValueChanged];
    // Do any additional setup after loading the view.
}

-(void)onContinueButtonTap
{
    LinkGoalsViewController * linkGoals = [self.storyboard instantiateViewControllerWithIdentifier:@"LinkGoalsViewController"];
    [self.navigationController pushViewController:linkGoals animated:YES];
}

-(void)onSliderChange:(id)sender
{
    if(sender == self.targetAmountSlider)
    {
        self.targetAmountTF.text = [NSString stringWithFormat:@"%i",(int)self.targetAmountSlider.value];
    }else if (sender == self.timePeriodSlider)
    {
        self.timePeriodTF.text = [NSString stringWithFormat:@"%i",(int)self.timePeriodSlider.value];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

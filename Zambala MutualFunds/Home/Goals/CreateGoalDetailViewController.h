//
//  CreateGoalDetailViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateGoalDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *targetAmountTF;
@property (weak, nonatomic) IBOutlet UITextField *timePeriodTF;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UISlider *targetAmountSlider;
@property (weak, nonatomic) IBOutlet UISlider *timePeriodSlider;

@end

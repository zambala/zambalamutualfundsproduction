//
//  GoalsHomeViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalsHomeViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *goalsHomeCollectionView;

@property NSMutableArray * imagesArray;
@property NSMutableArray * namesArray;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;

@end

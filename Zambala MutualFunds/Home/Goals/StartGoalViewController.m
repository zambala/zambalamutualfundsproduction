//
//  StartGoalViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "StartGoalViewController.h"
#import "GoalsHomeViewController.h"
#import "GoalsViewController.h"

@interface StartGoalViewController ()

@end

@implementation StartGoalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.startButton.layer.cornerRadius=20.0f;
    self.startButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.startButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.startButton.layer.shadowOpacity = 3.0f;
    self.startButton.layer.shadowRadius = 3.0f;
    self.startButton.layer.masksToBounds = NO;
    
    [self.startButton addTarget:self action:@selector(onStartButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)onStartButtonTap
{
    
    GoalsViewController * goals = [self.storyboard instantiateViewControllerWithIdentifier:@"GoalsViewController"];
    [self.navigationController pushViewController:goals animated:YES];
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

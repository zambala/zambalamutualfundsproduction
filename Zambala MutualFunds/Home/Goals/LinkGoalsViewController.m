//
//  LinkGoalsViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "LinkGoalsViewController.h"
#import "LinkFundsTableViewCell.h"
#import "GoalInvestmentAllocationViewController.h"


@interface LinkGoalsViewController ()
{
    NSString * buttonTitle;
}

@end

@implementation LinkGoalsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.continueButton.layer.cornerRadius=20.0f;
    self.continueButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.continueButton.layer.shadowOffset = CGSizeMake(0, 10.0f);
    self.continueButton.layer.shadowOpacity = 10.0f;
    self.continueButton.layer.shadowRadius = 3.0f;
    self.continueButton.layer.masksToBounds = NO;
    
    self.topView.layer.cornerRadius=10.0f;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.topView.layer.shadowOpacity = 10.0f;
    self.topView.layer.shadowRadius = 3.0f;
    self.topView.layer.masksToBounds = NO;
    
    
    
    [self.linkFundSwitch setOn:NO];
    [self.lumpSumSwitch setOn:NO];
    
    if([self.lumpSumSwitch isOn])
    {
        self.lumpSumView.hidden=NO;
        self.lumpSumheightConstraint.constant=30;
    }else
    {
        self.lumpSumView.hidden=YES;
        self.lumpSumheightConstraint.constant=0;
    }
    
    
    if([self.linkFundSwitch isOn])
    {
        self.goalsTableView.hidden=NO;
        self.goalsTableViewHeightConstraint.constant=550.5;
        self.goalsTableView.delegate=self;
        self.goalsTableView.dataSource=self;
        [self.goalsTableView reloadData];
    }else
    {
        self.goalsTableView.hidden=YES;
        self.goalsTableViewHeightConstraint.constant=0;
    }
    
    [self.lumpSumSwitch addTarget:self action:@selector(onSwitchTap:) forControlEvents:UIControlEventValueChanged];
    [self.linkFundSwitch addTarget:self action:@selector(onSwitchTap:) forControlEvents:UIControlEventValueChanged];
    [self.continueButton addTarget:self action:@selector(onContinueButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onSwitchTap:(id)sender
{
    if(sender == self.lumpSumSwitch)
    {
        if([self.lumpSumSwitch isOn])
        {
            self.lumpSumView.hidden=NO;
            self.lumpSumheightConstraint.constant=30;
        }else
        {
            self.lumpSumView.hidden=YES;
            self.lumpSumheightConstraint.constant=0;
        }
    }else if (sender == self.linkFundSwitch)
    {
        if([self.linkFundSwitch isOn])
        {
            self.goalsTableView.hidden=NO;
            self.goalsTableViewHeightConstraint.constant=550.5;
            self.goalsTableView.delegate=self;
            self.goalsTableView.dataSource=self;
            [self.goalsTableView reloadData];
        }else
        {
            self.goalsTableView.hidden=YES;
            self.goalsTableViewHeightConstraint.constant=0;
        }
    }
}

-(void)onContinueButtonTap
{
    GoalInvestmentAllocationViewController * goal = [self.storyboard instantiateViewControllerWithIdentifier:@"GoalInvestmentAllocationViewController"];
    [self.navigationController pushViewController:goal animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LinkFundsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"LinkFundsTableViewCell" forIndexPath:indexPath];
    cell.linkButton.layer.cornerRadius = 15;
    buttonTitle = [NSString stringWithFormat:@"%@",cell.linkButton.titleLabel.text];
    cell.allocationTF.text = [NSString stringWithFormat:@"%i",(int)cell.allocationSlider.value];
    if([buttonTitle isEqualToString:@"Unlink"])
    {
       // [cell.linkButton setTitle:@"Link" forState:UIControlStateNormal];
        cell.linkView.hidden=NO;
        cell.linkViewHeightConstraint.constant=100;
       // [cell.linkButton setBackgroundColor:[UIColor colorWithRed:52/255.0 green:185/255.0 blue:146/255.0 alpha:1]];
        
    }else if([buttonTitle isEqualToString:@"Link"])
    {
      // [cell.linkButton setTitle:@"Unlink" forState:UIControlStateNormal];
        cell.linkView.hidden=YES;
        cell.linkViewHeightConstraint.constant=0;
       // [cell.linkButton setBackgroundColor:[UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1]];
        
    }
    cell.linkButton.tag = indexPath.row;
    [cell.linkButton addTarget:self action:@selector(onLinkButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [cell.allocationSlider addTarget:self action:@selector(onSliderChange:) forControlEvents:UIControlEventValueChanged];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    LinkFundsTableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
//    NSString * buttonTitle = [NSString stringWithFormat:@"%@",cell.linkButton.titleLabel.text];
    if([buttonTitle isEqualToString:@"Unlink"])
    {
        buttonTitle=@"";
        return 243;
    }else
    {
        buttonTitle=@"";
        return 130;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)onSliderChange:(UISlider*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.goalsTableView];
    NSIndexPath *indexPath = [self.goalsTableView indexPathForRowAtPoint:buttonPosition];
    LinkFundsTableViewCell *cell = [self.goalsTableView cellForRowAtIndexPath:indexPath];
    cell.allocationTF.text = [NSString stringWithFormat:@"%i",(int)cell.allocationSlider.value];
}

-(void)onLinkButtonTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.goalsTableView];
    NSIndexPath *indexPath = [self.goalsTableView indexPathForRowAtPoint:buttonPosition];
     NSString * buttonTitle = [NSString stringWithFormat:@"%@",sender.titleLabel.text];

    if([buttonTitle isEqualToString:@"Link"])
    {
        [sender setTitle:@"Unlink" forState:UIControlStateNormal];
//        cell.linkView.hidden=NO;
//        cell.linkViewHeightConstraint.constant=100;
        [sender setBackgroundColor:[UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1]];
    }else if([buttonTitle isEqualToString:@"Unlink"])
    {
        [sender setTitle:@"Link" forState:UIControlStateNormal];
//        cell.linkView.hidden=YES;
//        cell.linkViewHeightConstraint.constant=0;
        [sender setBackgroundColor:[UIColor colorWithRed:52/255.0 green:185/255.0 blue:146/255.0 alpha:1]];
    }
    [self.goalsTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

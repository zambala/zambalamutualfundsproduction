//
//  LinkFundsTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkFundsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *linkButton;
@property (weak, nonatomic) IBOutlet UIView *linkView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *linkViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *allocationTF;
@property (weak, nonatomic) IBOutlet UISlider *allocationSlider;

@end

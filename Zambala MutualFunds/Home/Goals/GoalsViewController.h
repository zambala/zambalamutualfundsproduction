//
//  GoalsViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ViewController.h"

@interface GoalsViewController : ViewController
@property (weak, nonatomic) IBOutlet UIButton *analyseButton;

@end

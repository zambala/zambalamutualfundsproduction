//
//  GoalAlloctionDetailViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalAlloctionDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topview;
@property (weak, nonatomic) IBOutlet UITableView *lumpSumTableView;
@property (weak, nonatomic) IBOutlet UIButton *lumpSumInvestNowButton;
@property (weak, nonatomic) IBOutlet UITableView *sipTableView;
@property (weak, nonatomic) IBOutlet UIButton *sipInvestNowButton;

@end

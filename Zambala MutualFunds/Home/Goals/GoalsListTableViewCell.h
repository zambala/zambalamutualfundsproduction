//
//  GoalsListTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoalsListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *riskImageView;
@property (weak, nonatomic) IBOutlet UILabel *riskLabel;

@end

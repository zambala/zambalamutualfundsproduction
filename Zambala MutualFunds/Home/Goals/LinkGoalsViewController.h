//
//  LinkGoalsViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkGoalsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *lumpSumView;
@property (weak, nonatomic) IBOutlet UISwitch *lumpSumSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lumpSumheightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *goalsTableView;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UISwitch *linkFundSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *goalsTableViewHeightConstraint;

@end

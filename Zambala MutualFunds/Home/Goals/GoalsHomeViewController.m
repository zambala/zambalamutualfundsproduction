//
//  GoalsHomeViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "GoalsHomeViewController.h"
#import "GoalsHomeCollectionViewCell.h"
#import "CreateGoalDetailViewController.h"

@interface GoalsHomeViewController ()

@end

@implementation GoalsHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imagesArray = [[NSMutableArray alloc]init];
    self.namesArray = [[NSMutableArray alloc]init];
    
    self.proceedButton.layer.cornerRadius=20.0f;
    self.proceedButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.proceedButton.layer.shadowOffset = CGSizeMake(0, 10.0f);
    self.proceedButton.layer.shadowOpacity = 10.0f;
    self.proceedButton.layer.shadowRadius = 3.0f;
    self.proceedButton.layer.masksToBounds = NO;
    
    self.proceedButton.hidden=YES;
    
    [self.imagesArray addObject:@"home"];
    [self.imagesArray addObject:@"four-wheeler"];
    [self.imagesArray addObject:@"two-wheeler"];
    [self.imagesArray addObject:@"marriage"];
    [self.imagesArray addObject:@"education"];
    [self.imagesArray addObject:@"retirement"];
    [self.imagesArray addObject:@"vacation"];
    [self.imagesArray addObject:@"grow-wealth"];
    [self.imagesArray addObject:@"goal-for-business"];
    [self.imagesArray addObject:@"custom-goal"];
    
    
    [self.namesArray addObject:@"House"];
    [self.namesArray addObject:@"Four Wheeler"];
    [self.namesArray addObject:@"Two Wheeler"];
    [self.namesArray addObject:@"Marriage"];
    [self.namesArray addObject:@"Education"];
    [self.namesArray addObject:@"Retirement"];
    [self.namesArray addObject:@"Vacation"];
    [self.namesArray addObject:@"Grow Wealth"];
    [self.namesArray addObject:@"Capital for Business"];
    [self.namesArray addObject:@"Custom Goal"];
    
    self.goalsHomeCollectionView.delegate=self;
    self.goalsHomeCollectionView.dataSource=self;
    [self.goalsHomeCollectionView reloadData];
    
    [self.proceedButton addTarget:self action:@selector(onProceedButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoalsHomeCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoalsHomeCollectionViewCell" forIndexPath:indexPath];
    
    cell.homeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.imagesArray objectAtIndex:indexPath.row]]];
    cell.homeLabel.text = [NSString stringWithFormat:@"%@",[self.namesArray objectAtIndex:indexPath.row]];
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor colorWithRed:231/255.0 green:231/255.0 blue:230/255.0 alpha:1]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [UIView animateWithDuration:0.5
                          delay:1.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         self.proceedButton.hidden=NO;
                         self.proceedButton.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                     }];
}

-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)onProceedButtonTap
{
    CreateGoalDetailViewController * goal = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateGoalDetailViewController"];
    [self.navigationController pushViewController:goal animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  GoalsListDetailViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 01/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "GoalsListDetailViewController.h"
#import "HMSegmentedControl.h"
#import "GoalsListDetailFundsTableViewCell.h"
#import "EquityDetailViewController.h"

@interface GoalsListDetailViewController ()
{
     HMSegmentedControl * segmentedControl;
}

@end

@implementation GoalsListDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableArray * localSegmentsArray = [[NSMutableArray alloc]init];
    
    [localSegmentsArray addObject:@"Overview"];
    [localSegmentsArray addObject:@"Performance"];
    [localSegmentsArray addObject:@"Funds"];
    
    self.performanceView.hidden=YES;
    self.fundsView.hidden=YES;
    self.overviewView.hidden=NO;
    
    
    self.topView.layer.cornerRadius=10.0f;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.topView.layer.shadowOpacity = 10.0f;
    self.topView.layer.shadowRadius = 3.0f;
    self.topView.layer.masksToBounds = NO;
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.topView.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    if([self.riskCheck isEqualToString:@"risk"])
    {
        [segmentedControl setSelectedSegmentIndex:2];
        [self onSegmentTap];
    }else
    {
    [segmentedControl setSelectedSegmentIndex:0];
    }
    [self.segmentedControlView addSubview:segmentedControl];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //[self setupButtons];
    
    self.view.backgroundColor = [UIColor gk_cloudsColor];
    [self setupGraph];
    // Do any additional setup after loading the view.
}
-(void)onSegmentTap
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.performanceView.hidden=YES;
        self.fundsView.hidden=YES;
        self.overviewView.hidden=NO;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        self.performanceView.hidden=NO;
        self.fundsView.hidden=YES;
        self.overviewView.hidden=YES;
    }else if (segmentedControl.selectedSegmentIndex==2)
    {
        self.performanceView.hidden=YES;
        self.fundsView.hidden=NO;
        self.overviewView.hidden=YES;
        self.fundsTableView.delegate=self;
        self.fundsTableView.dataSource=self;
        [self.fundsTableView reloadData];
    }
}

-(void)setupGraph
{
    self.data = @[@[@20.0, @40.6, @80.9, @100.0],
                  @[@20.0, @40, @50, @60, @70, @80, @100]];
    self.labels = @[@"Oct'15", @"Oct'16", @"Sep'17", @"Oct'17"];
    
    self.graphView.dataSource = self;
    self.graphView.lineWidth = 3.0;
    
    self.graphView.valueLabelCount = 6;
    
    [self.graphView draw];
}

- (NSInteger)numberOfLines {
    return [self.data count];
}

- (UIColor *)colorForLineAtIndex:(NSInteger)index {
    id colors = @[[UIColor gk_turquoiseColor],
                  [UIColor gk_peterRiverColor]
                  ];
    return [colors objectAtIndex:index];
}

- (NSArray *)valuesForLineAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index {
    return [[@[@1,@1.6] objectAtIndex:index] doubleValue];
}

- (NSString *)titleForLineAtIndex:(NSInteger)index {
    return [self.labels objectAtIndex:index];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GoalsListDetailFundsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"GoalsListDetailFundsTableViewCell" forIndexPath:indexPath];
    return cell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 128;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EquityDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

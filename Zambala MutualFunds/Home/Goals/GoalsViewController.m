//
//  GoalsViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "GoalsViewController.h"
#import "GoalsHomeViewController.h"

@interface GoalsViewController ()

@end

@implementation GoalsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.analyseButton.layer.cornerRadius=20.0f;
    self.analyseButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.analyseButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.analyseButton.layer.shadowOpacity = 3.0f;
    self.analyseButton.layer.shadowRadius = 3.0f;
    self.analyseButton.layer.masksToBounds = NO;
    
    
    [self.analyseButton addTarget:self action:@selector(onAnalyseButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)onAnalyseButtonTap
{
    GoalsHomeViewController * goals = [self.storyboard instantiateViewControllerWithIdentifier:@"GoalsHomeViewController"];
    goals.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:goals animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  GoalsListDetailViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 01/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphKit.h"

@interface GoalsListDetailViewController : UIViewController<GKLineGraphDataSource,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *fundsView;
@property (weak, nonatomic) IBOutlet UITableView *fundsTableView;
@property (weak, nonatomic) IBOutlet UIView *performanceView;
@property (weak, nonatomic) IBOutlet UIView *overviewView;
@property (weak, nonatomic) IBOutlet GKLineGraph *graphView;

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *labels;
@property (weak, nonatomic) IBOutlet UIView *segmentedControlView;
@property NSString * riskCheck;


@end

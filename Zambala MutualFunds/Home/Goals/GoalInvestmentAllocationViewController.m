//
//  GoalInvestmentAllocationViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "GoalInvestmentAllocationViewController.h"
#import "GoalAlloctionDetailViewController.h"

@interface GoalInvestmentAllocationViewController ()

@end

@implementation GoalInvestmentAllocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nextBuutton.layer.cornerRadius=20.0f;
    self.nextBuutton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.nextBuutton.layer.shadowOffset = CGSizeMake(0, 10.0f);
    self.nextBuutton.layer.shadowOpacity = 10.0f;
    self.nextBuutton.layer.shadowRadius = 3.0f;
    self.nextBuutton.layer.masksToBounds = NO;
    
    self.topView.layer.cornerRadius=10.0f;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.topView.layer.shadowOpacity = 10.0f;
    self.topView.layer.shadowRadius = 3.0f;
    self.topView.layer.masksToBounds = NO;
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //[self setupButtons];
    
    self.view.backgroundColor = [UIColor gk_cloudsColor];
    [self setupGraph];
    self.lineView.layer.cornerRadius=10.0f;
    self.lineView.progressValue=50.0;
    self.lineView.barColor = [UIColor colorWithRed:224.0f/255.0f green:82.0f/255.0f blue:88.0f/255.0f alpha:1.0f];
    self.lineView.trackColor = [UIColor colorWithRed:178.0/255.0 green:138.0/255.0 blue:182.0/255.0 alpha:1];
    self.lineView.barThickness = self.lineView.frame.size.height;
    self.lineView.showPercentageText = NO;
    
    self.sipTF.text = [NSString stringWithFormat:@"%i",(int)self.sipSlider.value];
    
    [self.nextBuutton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.sipSlider addTarget:self action:@selector(onSliderChange) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onSliderChange
{
    self.sipTF.text = [NSString stringWithFormat:@"%i",(int)self.sipSlider.value];
}

-(void)setupGraph
{
    self.data = @[@[@20.0, @40.6, @80.9, @100.0],
                  @[@20.0, @40, @50, @60, @70, @80, @100]];
    self.labels = @[@"Oct'15", @"Oct'16", @"Sep'17", @"Oct'17"];
    
    self.graphView.dataSource = self;
    self.graphView.lineWidth = 3.0;
    
    self.graphView.valueLabelCount = 6;
    
    [self.graphView draw];
}

- (NSInteger)numberOfLines {
    return [self.data count];
}

- (UIColor *)colorForLineAtIndex:(NSInteger)index {
    id colors = @[[UIColor gk_turquoiseColor],
                  [UIColor gk_peterRiverColor]
                  ];
    return [colors objectAtIndex:index];
}

- (NSArray *)valuesForLineAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index {
    return [[@[@1,@1.6] objectAtIndex:index] doubleValue];
}

- (NSString *)titleForLineAtIndex:(NSInteger)index {
    return [self.labels objectAtIndex:index];
}

-(void)onNextButtonTap
{
    GoalAlloctionDetailViewController * goal = [self.storyboard instantiateViewControllerWithIdentifier:@"GoalAlloctionDetailViewController"];
    [self.navigationController pushViewController:goal animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

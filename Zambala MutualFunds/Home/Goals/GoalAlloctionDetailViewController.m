//
//  GoalAlloctionDetailViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "GoalAlloctionDetailViewController.h"
#import "GoalAllocationDetailSIPTableViewCell.h"
#import "GoalAllocationDetailLumpSumTableViewCell.h"
#import "OrderConfirmationViewController.h"

@interface GoalAlloctionDetailViewController ()

@end

@implementation GoalAlloctionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.topview.layer.cornerRadius=10.0f;
    self.topview.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.topview.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.topview.layer.shadowOpacity = 10.0f;
    self.topview.layer.shadowRadius = 3.0f;
    self.topview.layer.masksToBounds = NO;
    
    self.sipInvestNowButton.layer.cornerRadius = 10;
    self.lumpSumInvestNowButton.layer.cornerRadius = 10;
    
    self.lumpSumTableView.delegate=self;
    self.lumpSumTableView.dataSource=self;
    [self.lumpSumTableView reloadData];
    self.sipTableView.delegate=self;
    self.sipTableView.dataSource=self;
    [self.sipTableView reloadData];
    
    [self.lumpSumInvestNowButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.sipInvestNowButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.sipTableView)
    {
        GoalAllocationDetailSIPTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"GoalAllocationDetailSIPTableViewCell" forIndexPath:indexPath];
        return cell;
    }else if (tableView==self.lumpSumTableView)
    {
        GoalAllocationDetailLumpSumTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"GoalAllocationDetailLumpSumTableViewCell" forIndexPath:indexPath];
        return cell;
    }
    return nil;
    
}

-(void)onButtonTap:(UIButton*)sender
{
    OrderConfirmationViewController * order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderConfirmationViewController"];
    [self.navigationController pushViewController:order animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  GoalInvestmentAllocationViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphKit.h"
#import "MKSHorizontalLineProgressView.h"


@interface GoalInvestmentAllocationViewController : UIViewController<GKLineGraphDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UITextField *sipTF;
@property (weak, nonatomic) IBOutlet UISlider *sipSlider;
@property (weak, nonatomic) IBOutlet UIButton *nextBuutton;
@property (weak, nonatomic) IBOutlet GKLineGraph *graphView;
@property (weak, nonatomic) IBOutlet MKSHorizontalLineProgressView *lineView;

@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *labels;

@end

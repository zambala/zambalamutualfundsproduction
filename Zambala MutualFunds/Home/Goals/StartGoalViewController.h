//
//  StartGoalViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartGoalViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@end

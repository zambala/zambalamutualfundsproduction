//
//  HomeRecomendedTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 08/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "HomeRecomendedTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation HomeRecomendedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.backView.layer.cornerRadius=5.0f;
    self.backView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.backView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.backView.layer.shadowOpacity = 1.0f;
    self.backView.layer.shadowRadius = 3.0f;
    self.backView.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

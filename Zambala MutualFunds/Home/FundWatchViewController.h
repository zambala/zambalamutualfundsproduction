//
//  FundWatchViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ViewController.h"
#import "Reachability.h"

@interface FundWatchViewController : ViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *fundWatchTableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *plusButton;
- (IBAction)onPlusButtonTap:(id)sender;
- (IBAction)onFilterTap:(id)sender;

@property NSString * schemeCodeString;
@property NSMutableDictionary * dataDictionary;
@property NSMutableDictionary * zambalaResponseDataDictionary;
@property Reachability * reach;



@end

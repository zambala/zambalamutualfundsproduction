//
//  SmarMoneyViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 01/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface SmarMoneyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *tick1;
@property (weak, nonatomic) IBOutlet UIImageView *tick2;
@property (weak, nonatomic) IBOutlet UIImageView *tick3;
@property (weak, nonatomic) IBOutlet UIImageView *tick4;
@property (weak, nonatomic) IBOutlet UIImageView *tick5;
@property (weak, nonatomic) IBOutlet UIButton *startNowButton;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIButton *lessMonthsButton;
@property (weak, nonatomic) IBOutlet UIButton *moreMonthsButton;
@property (weak, nonatomic) IBOutlet UIImageView *backImgaeView;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *firstPopUpView;
@property Reachability * reach;



@end

//
//  OpenAccountWebViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 28/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "OpenAccountWebViewController.h"
#import <WebKit/WebKit.h>
#import "DGActivityIndicatorView.h"
#import "OpenAccountStatusViewController.h"
@import Mixpanel;


@interface OpenAccountWebViewController ()
{
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    NSString * urlString;
    Mixpanel * mixpanelFunds;
}

@end

@implementation OpenAccountWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.openAccountWebView addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    [mixpanelFunds track:@"create_account_initiated_page"];
    
    
    if([self.sourceString isEqualToString:@"Privacy"])
    {
        urlString = @"https://zambalafunds.com/static/privacy-policy.aspx";
    }else if ([self.sourceString isEqualToString:@"Terms"])
    {
        urlString = @"https://zambalafunds.com/static/terms-and-conditions.aspx";
    }else
    {
        NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
        NSString * isGuest = [userSessionData objectForKey:@"isguest"];
        NSString * zenwiseID = [userSessionData objectForKey:@"zenwiseID"];
        NSString * clientID = [userSessionData objectForKey:@"cid"];
        if([isGuest isEqualToString:@"0"])
        {
            urlString = [NSString stringWithFormat:@"https://www.zenwise.net/stagingwebsite/customer/%@",zenwiseID];
            [mixpanelFunds.people set:@{@"Client Id":zenwiseID}];
        }else if ([isGuest isEqualToString:@"1"])
        {
            urlString = [NSString stringWithFormat:@"https://www.zenwise.net/stagingwebsite/customer/%@",clientID];
             [mixpanelFunds.people set:@{@"Client Id":clientID}];
        }
    
    }
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    self.openAccountWebView.navigationDelegate = self;
    NSURL *nsurl=[NSURL URLWithString:urlString];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [self.openAccountWebView loadRequest:nsrequest];
    // Do any additional setup after loading the view.
}
-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(nonnull WKNavigationAction *)navigationAction decisionHandler:(nonnull void (^)(WKNavigationActionPolicy))decisionHandler {
    decisionHandler(WKNavigationActionPolicyAllow);
    [activityIndicatorView stopAnimating];
    [backView removeFromSuperview];
    NSString * url = [NSString stringWithFormat:@"%@",navigationAction.request.URL.absoluteString];
    NSLog(@"Loading URL :%@", navigationAction.request.URL.absoluteString);
    if([self.sourceString isEqualToString:@"Terms"]||[self.sourceString isEqualToString:@"Privacy"])
    {
        
    }else
    {
    if([url containsString:@"success"])
    {
        OpenAccountStatusViewController * status = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountStatusViewController"];
        status.stautsString = @"success";
        [self.navigationController pushViewController:status animated:YES];
    }else if ([url containsString:@"failure"])
    {
        OpenAccountStatusViewController * status = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountStatusViewController"];
        status.stautsString = @"failure";
        [self.navigationController pushViewController:status animated:YES];
    }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

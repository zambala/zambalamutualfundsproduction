//
//  OpenAccountStatusViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 15/10/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "OpenAccountStatusViewController.h"
#import "TabbarMFViewController.h"
#import "OpenAccountWebViewController.h"
@import Mixpanel;

@interface OpenAccountStatusViewController ()
{
    Mixpanel * mixpanelFunds;
}

@end

@implementation OpenAccountStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    if([self.stautsString isEqualToString:@"success"])
    {
        self.successView.hidden=NO;
        self.failureView.hidden = YES;
        [mixpanelFunds track:@"create_account_success_page"];
    }else if ([self.stautsString isEqualToString:@"failure"])
    {
        self.successView.hidden = YES;
        self.failureView.hidden = NO;
        [mixpanelFunds track:@"create_account_failure_page"];
    }
    
    [self.continueButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.homeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.tryAgainButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(id)sender
{
    if(sender == self.continueButton)
    {
        TabbarMFViewController * tabBar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
        [self presentViewController:tabBar animated:YES completion:nil];
    }else if (sender == self.homeButton)
    {
        TabbarMFViewController * tabBar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
        [self presentViewController:tabBar animated:YES completion:nil];
    }else if (sender == self.tryAgainButton)
    {
        OpenAccountWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountWebViewController"];
        [self.navigationController pushViewController:webView animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

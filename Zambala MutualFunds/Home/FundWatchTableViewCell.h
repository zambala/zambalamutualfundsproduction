//
//  FundWatchTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 03/10/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundWatchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *threeYearReturn;
@property (weak, nonatomic) IBOutlet UILabel *fundSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;
@property (weak, nonatomic) IBOutlet UILabel *navDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *minInvestmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *navChangePercentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *upDownImageView;
@property (weak, nonatomic) IBOutlet UIImageView *riskoMeterImageView;
@property (weak, nonatomic) IBOutlet UIButton *investNowButton;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *fundWatchButton;

@end

NS_ASSUME_NONNULL_END

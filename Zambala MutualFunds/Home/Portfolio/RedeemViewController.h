//
//  RedeemViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 02/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedeemViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *unitsButton;
@property (weak, nonatomic) IBOutlet UIButton *amountButton;
@property (weak, nonatomic) IBOutlet UILabel *noofUnitsLabel;
@property (weak, nonatomic) IBOutlet UITextField *unitsTF;
@property (weak, nonatomic) IBOutlet UISlider *unitsSlider;
@property (weak, nonatomic) IBOutlet UILabel *sliderLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *sliderRightLabel;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *folioNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitsLabel;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIButton *acceptAndInvestButton;
@property (weak, nonatomic) IBOutlet UIView *bottomMainView;
@property (weak, nonatomic) IBOutlet UIView *insidePopUpView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *finalValueLabel;

@property NSString * schemeCode;
@property NSMutableDictionary * responseDataDictionary;
@property NSMutableDictionary * excecuteDataDictionary;
@end

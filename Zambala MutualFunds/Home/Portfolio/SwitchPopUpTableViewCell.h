//
//  SwitchPopUpTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 14/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchPopUpTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *popUpLabel;

@end

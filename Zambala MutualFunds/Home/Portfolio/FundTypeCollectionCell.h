//
//  FundTypeCollectionCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 28/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FundTypeCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *identificationView;
@property (weak, nonatomic) IBOutlet UILabel *fundTypeLbl;

@end

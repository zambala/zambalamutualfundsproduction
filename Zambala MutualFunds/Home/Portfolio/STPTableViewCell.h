//
//  STPTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 18/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STPTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

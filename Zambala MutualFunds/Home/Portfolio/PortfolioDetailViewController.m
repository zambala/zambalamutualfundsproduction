//
//  PortfolioDetailViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 01/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "PortfolioDetailViewController.h"
#import "PortfolioDetailTableViewCell.h"
#import "TrasactTableViewCell.h"
#import "RedeemViewController.h"
#import "SwitchViewController.h"
#import "STPViewController.h"
#import "SWPViewController.h"
#import "OrderConfirmationViewController.h"
#import "EquityDetailViewController.h"
#import "AppDelegate.h"
#import "Utility.h"

@interface PortfolioDetailViewController ()
{
    AppDelegate * delegate;
    Utility * mfSharedManager;
}

@end

@implementation PortfolioDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mfSharedManager=[Utility MF];
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //self.topView.layer.cornerRadius=10.0f;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 1.5f);
    self.topView.layer.shadowOpacity = 0.6f;
    self.topView.layer.shadowRadius = 6.5f;
    self.topView.layer.masksToBounds = NO;
    
    self.topPopUpView.tag=99;
    
    self.imagesArray = [[NSMutableArray alloc]init];
    self.namesArray = [[NSMutableArray alloc]init];
    
   
    self.popUpView.layer.cornerRadius = 10.0f;
    self.transactTableView.layer.cornerRadius = 10.0f;
    self.popUpView.layer.masksToBounds = NO;
    //self.transactTableView.layer.masksToBounds = NO;
    
    self.popUpView.hidden=YES;
    self.topPopUpView.hidden=YES;
    self.closeButton.hidden=YES;
    
    [self.closeButton addTarget:self action:@selector(onCloseButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self assignTopUI];
    
    self.portfolioDetailTableView.delegate=self;
    self.portfolioDetailTableView.dataSource=self;
    [self.portfolioDetailTableView reloadData];
    
    
    self.transactTableView.delegate=self;
    self.transactTableView.dataSource=self;
    
    
    // Do any additional setup after loading the view.
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.topPopUpView.hidden=YES;
    }
}

-(void)onCloseButtonTap
{
    self.popUpView.hidden=YES;
    self.topPopUpView.hidden=YES;
}

-(void)assignTopUI
{
   NSLog(@"%@",delegate.portfolioDataArray);
    
    NSMutableArray * array = [[NSMutableArray alloc]init];
    for (int i=0; i<delegate.portfolioDataArray.count; i++) {
        [array addObject:[[delegate.portfolioDataArray objectAtIndex:i]objectForKey:@"Folio_No"]];
    }
    NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:array];
    NSMutableArray * folio = [[NSMutableArray alloc] initWithArray:[mySet array]];
    self.folioLabel.text = [NSString stringWithFormat:@"%lu Folios",(unsigned long)folio.count];
    self.fundLabel.text = [NSString stringWithFormat:@"%lu Funds",(unsigned long)delegate.portfolioDataArray.count];
    NSMutableArray * sumArray = [[NSMutableArray alloc]init];
    for (int i=0; i<delegate.portfolioDataArray.count; i++) {
        [sumArray addObject:[[delegate.portfolioDataArray objectAtIndex:i]objectForKey:@"Present Value"]];
    }
    NSNumber *sum = [sumArray valueForKeyPath:@"@sum.floatValue"];
    self.currentValueLabel.text = [NSString stringWithFormat:@"%@",sum];
    NSMutableArray * investedArray = [[NSMutableArray alloc]init];
    for (int i=0; i<delegate.portfolioDataArray.count; i++) {
        [investedArray addObject:[[delegate.portfolioDataArray objectAtIndex:i]objectForKey:@"Total Investment"]];
    }
    NSNumber *sumInvested = [sumArray valueForKeyPath:@"@sum.floatValue"];
    self.investedLabel.text = [NSString stringWithFormat:@"%@",sumInvested];
    NSMutableArray * gainArray = [[NSMutableArray alloc]init];
    for (int i=0; i<delegate.portfolioDataArray.count; i++) {
        [gainArray addObject:[[delegate.portfolioDataArray objectAtIndex:i]objectForKey:@"Notional_Gain/Loss"]];
    }
    NSNumber *sumGain = [sumArray valueForKeyPath:@"@sum.floatValue"];
    self.gainLabel.text = [NSString stringWithFormat:@"%@",sumGain];
    float totalGain = [sumGain floatValue];
    float totalInvestment = [sumInvested floatValue];
    float gainPercent = ((totalGain/totalInvestment)*100);
    NSString * percent = @"%";
    self.gainPercentLabel.text = [NSString stringWithFormat:@"(%.2f%@)",gainPercent,percent];
//    if([self.gainPercentLabel.text containsString:@"-"])
//    {
//        self.gainPercentLabel.textColor = [UIColor colorWithRed:234.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0];
//    }else
//    {
//        self.gainPercentLabel.textColor = [UIColor colorWithRed:23.0/255.0 green:165.0/255.0 blue:145.0/255.0 alpha:1.0];
//    }
}

-(void)viewWillAppear:(BOOL)animated
{
    self.topPopUpView.hidden=YES;
    self.popUpView.hidden=YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)getTransactDetails
{
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.portfolioDetailTableView)
    {
    return delegate.portfolioDataArray.count;
    }else if (tableView == self.transactTableView)
    {
        return self.namesArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.portfolioDetailTableView)
    {
    PortfolioDetailTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PortfolioDetailTableViewCell" forIndexPath:indexPath];
        NSString * fundName = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Scheme Name"]];
        if([fundName isEqual:[NSNull null]])
        {
            cell.fundNameLabel.text = @"N/A";
        }else
        {
            cell.fundNameLabel.text = fundName;
        }
        NSString * currentValue = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Present Value"]];
        if([currentValue isEqual:[NSNull null]])
        {
            cell.currentValueLabel.text = @"N/A";
        }else
        {
            cell.currentValueLabel.text = currentValue;
        }
        NSString * units = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Present Units"]];
        if([units isEqual:[NSNull null]])
        {
            cell.unitsLabel.text = @"N/A";
        }else
        {
            cell.unitsLabel.text = [NSString stringWithFormat:@"%.2f",[units floatValue]];
        }
        NSString * gain = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Notional_Gain/Loss"]];
        if([gain isEqual:[NSNull null]])
        {
            cell.gainLabel.text = @"N/A";
        }else
        {
            cell.gainLabel.text = [NSString stringWithFormat:@"%.2f",[gain floatValue]];
        }
        NSString * invested = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Total Investment"]];
        if([invested isEqual:[NSNull null]])
        {
            cell.investedLabel.text =@"N/A";
        }else
        {
            cell.investedLabel.text = invested;
        }
        
        float gainFloat = [gain floatValue];
        float totalInvestFloat = [invested floatValue];
        float gainPercentFloat = ((gainFloat/totalInvestFloat)*100);
        cell.gainPercentLabel.text = [NSString stringWithFormat:@"%.2f",gainPercentFloat];
        if([cell.gainPercentLabel.text containsString:@"-"])
        {
            cell.gainPercentLabel.textColor = [UIColor colorWithRed:234.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0];
        }else
        {
            cell.gainPercentLabel.textColor = [UIColor colorWithRed:23.0/255.0 green:165.0/255.0 blue:145.0/255.0 alpha:1.0];
        }
        NSString * nav = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Present NAV"]];
        if([nav isEqual:[NSNull null]])
        {
            cell.navLabel.text =@"N/A";
        }else
        {
            cell.navLabel.text = [NSString stringWithFormat:@"%.2f",[nav floatValue]];
        }
        cell.navPercentLabel.text =@"N/A";
        cell.navDateLabel.text = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"NAV Date"]];
        cell.assetTypeLabel.text = [NSString stringWithFormat:@"%@:",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"ASSET_TYPE"]];
        NSString * scheme = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"SUB_CATEGORY"]];
        NSArray * schemeArray = [scheme componentsSeparatedByString:@":"];
        cell.categoryTypeLabel.text = [NSString stringWithFormat:@"%@",[schemeArray objectAtIndex:2]];
        cell.folioNumber.text = [NSString stringWithFormat:@"Folio Number:%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Folio_No"]];
    [cell.transactButton addTarget:self action:@selector(onTransactButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    }else if (tableView == self.transactTableView)
    {
        TrasactTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TrasactTableViewCell" forIndexPath:indexPath];
        [cell.transactButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.imagesArray objectAtIndex:indexPath.row]]] forState:UIControlStateNormal];
        cell.transactLabel.text = [NSString stringWithFormat:@"%@",[self.namesArray objectAtIndex:indexPath.row]];
        
        [cell.transactButton setTintColor:[UIColor colorWithRed:(52.0/255.0) green:(185.0/255.0) blue:(146.0/255.0) alpha:1.0]];
        
      //  [cell.transactButton addTarget:self action:@selector(onTransactChooose:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.portfolioDetailTableView)
    {
    return 240;
    }else if (tableView == self.transactTableView)
    {
        return 60;
    }
    return 0;
}

-(void)onTransactButtonTap:(UIButton*)sender
{
    
    if(delegate.transactionDetailsArray.count>0)
    {
        [delegate.transactionDetailsArray removeAllObjects];
    }
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.portfolioDetailTableView];
    NSIndexPath *indexPath = [self.portfolioDetailTableView indexPathForRowAtPoint:buttonPosition];
    [delegate.transactionDetailsArray addObject:[delegate.portfolioDataArray objectAtIndex:indexPath.row]];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSString * schemecode = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Scheme Code"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getTransactDetails"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",schemecode,cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@get_shemeinfo_for_invest_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            NSLog(@"%@",dict);
            if(dict)
            {
                if([[[dict objectForKey:@"data"] objectForKey:@"Table"] count]>0)
                {
                    if(delegate.transactionDataArray.count>0)
                    {
                        [delegate.transactionDataArray removeAllObjects];
                    }
                    [delegate.transactionDataArray addObjectsFromArray:[[dict objectForKey:@"data"]objectForKey:@"Table"]];
                self.transactArray = [[NSMutableArray alloc]init];
                if(self.imagesArray.count>0)
                {
                    [self.imagesArray removeAllObjects];
                }
                if(self.namesArray.count>0)
                {
                    [self.namesArray removeAllObjects];
                }
                [self.transactArray addObjectsFromArray:[[dict objectForKey:@"data"]objectForKey:@"Table"]];
//                for (int i=0; i<self.transactArray.count; i++) {
                    if([[[self.transactArray objectAtIndex:0] objectForKey:@"IsPurchaseAvailable"]isEqualToString:@"Y"])
                    {
                        [self.namesArray addObject:@" Purchase"];
                        [self.imagesArray addObject:@"purchaseNew"];
                    }
                    if([[[self.transactArray objectAtIndex:0] objectForKey:@"IsRedeemAvailable"]isEqualToString:@"Y"])
                    {
                        [self.namesArray addObject:@" Redeem"];
                        [self.imagesArray addObject:@"redeemNew"];
                    }
                    if ([[[self.transactArray objectAtIndex:0] objectForKey:@"SwitchOut"]isEqualToString:@"T"])
                    {
                        [self.namesArray addObject:@" Switch"];
                        [self.imagesArray addObject:@"switchNew"];
                    }
                    if ([[[self.transactArray objectAtIndex:0] objectForKey:@"STP"]isEqualToString:@"T"])
                    {
                        [self.namesArray addObject:@" STP"];
                        [self.imagesArray addObject:@"stpNew"];
                    }
                    if ([[[self.transactArray objectAtIndex:0] objectForKey:@"SWP"]isEqualToString:@"T"])
                    {
                        [self.namesArray addObject:@" SWP"];
                        [self.imagesArray addObject:@"swpNew"];
                    }
//                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(self.namesArray.count>0)
                    {
                        
                        [UIView transitionWithView:self.topPopUpView
                                          duration:0.4
                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                        animations:^{
                                            self.closeButton.hidden=NO;
                                            self.topPopUpView.hidden=NO;
                                            self.popUpView.hidden=NO;
                                        }
                                        completion:NULL];
                        
                    [self.transactTableView reloadData];
                    }else
                    {
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Oops" message:@"No Transactions" preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        
                        
                        UIAlertAction * ok=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                        }];
                        
                        [alert addAction:ok];
                    }
                });
            }else
            {
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Oops" message:@"No Transactions" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                UIAlertAction * ok=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:ok];
            }
            }
        }];
    }
    
}

//-(void)onTransactChooose:(UIButton*)sender
//{
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.portfolioDetailTableView];
//    NSIndexPath *indexPath = [self.portfolioDetailTableView indexPathForRowAtPoint:buttonPosition];
////    if(indexPath.row==0)
////    {
////
////    }else if(indexPath.row==1)
////    {
////        RedeemViewController * redeem = [self.storyboard instantiateViewControllerWithIdentifier:@"RedeemViewController"];
////        [self.navigationController pushViewController:redeem animated:YES];
////    }else if (indexPath.row==2)
////    {
////
////    }else if (indexPath.row==3)
////    {
////
////    }else if (indexPath.row==4)
////    {
////
////    }
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.transactTableView)
    {
    if(indexPath.row==0)
    {
        OrderConfirmationViewController * order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderConfirmationViewController"];
        order.schemeCode=[NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Scheme Code"]];
        order.check=@"portfolio";
        [self.navigationController pushViewController:order animated:YES];
    }else if(indexPath.row==1)
    {
        RedeemViewController * redeem = [self.storyboard instantiateViewControllerWithIdentifier:@"RedeemViewController"];
        [self.navigationController pushViewController:redeem animated:YES];
        //[self presentViewController:redeem animated:YES completion:nil];
    }else if (indexPath.row==2)
    {
        SwitchViewController * redeem = [self.storyboard instantiateViewControllerWithIdentifier:@"SwitchViewController"];
        [self.navigationController pushViewController:redeem animated:YES];
    }else if (indexPath.row==3)
    {
        STPViewController * stp = [self.storyboard instantiateViewControllerWithIdentifier:@"STPViewController"];
        [self.navigationController pushViewController:stp animated:YES];
    }else if (indexPath.row==4)
    {
        SWPViewController * swp = [self.storyboard instantiateViewControllerWithIdentifier:@"SWPViewController"];
        [self.navigationController pushViewController:swp animated:YES];
    }
        self.popUpView.hidden=YES;
    }else if (tableView == self.portfolioDetailTableView)
    {
        EquityDetailViewController * equtiyDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
        equtiyDetail.schemeCodeString = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:indexPath.row]objectForKey:@"Scheme Code"]];
        [self.navigationController pushViewController:equtiyDetail animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

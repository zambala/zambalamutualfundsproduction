//
//  PortfolioDetailViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 01/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UITableView *portfolioDetailTableView;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UITableView *transactTableView;

@property NSMutableArray * imagesArray;
@property NSMutableArray * namesArray;
@property (weak, nonatomic) IBOutlet UIView *topPopUpView;
@property (weak, nonatomic) IBOutlet UILabel *folioLabel;
@property (weak, nonatomic) IBOutlet UILabel *fundLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *investedLabel;
@property (weak, nonatomic) IBOutlet UILabel *gainLabel;
@property (weak, nonatomic) IBOutlet UILabel *gainPercentLabel;
@property NSMutableArray * transactArray;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end

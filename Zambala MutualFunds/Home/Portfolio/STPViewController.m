//
//  STPViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 05/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "STPViewController.h"
#import "AppDelegate.h"
#import "STPTableViewCell.h"
#import "Utility.h"

@interface STPViewController ()
{
    UIToolbar * toolbar;
    AppDelegate * delegate;
    NSString * check;
    NSOrderedSet *mySet;
    NSOrderedSet * schemeSet;
    NSMutableArray * categoryUniqueArray;
    NSMutableArray * schemeuniqueArray;
    NSMutableArray * categoryArray;
    NSMutableArray * schemeArray;
    Utility * mfSharedManager;
    NSString * toSchemeCode;
    NSString * selectedCategory;
    NSString * selectedSchemeName;
    NSString * selectedFrequency;
    NSString * stpDatesDetailString;
    NSArray * stpDetailsArrayOne;
    NSString * stpDetailsStringTwo;
    NSArray * stpDetailsSeperatedArray;
}

@end

@implementation STPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     mfSharedManager=[Utility MF];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    categoryArray = [[NSMutableArray alloc]init];
    schemeArray = [[NSMutableArray alloc]init];
    
    
    self.somePickerView.hidden=YES;
    self.doneView.hidden=YES;
    self.topView.layer.cornerRadius=10.0f;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.topView.layer.shadowOpacity = 0.6f;
    self.topView.layer.shadowRadius = 6.5f;
    self.topView.layer.masksToBounds = NO;
    
    self.insidePopUpView.layer.cornerRadius = 5.0f;
    
    self.confirmButton.layer.cornerRadius=20.0f;
    self.confirmButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.confirmButton.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.confirmButton.layer.shadowOpacity = 0.6f;
    self.confirmButton.layer.shadowRadius = 6.5f;
    self.confirmButton.layer.masksToBounds = NO;
    
    self.acceptAndInvestButton.layer.cornerRadius=20.0f;
    self.acceptAndInvestButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.acceptAndInvestButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.acceptAndInvestButton.layer.shadowOpacity = 0.6f;
    self.acceptAndInvestButton.layer.shadowRadius = 6.5f;
    self.acceptAndInvestButton.layer.masksToBounds = NO;
    
    
    self.installmentsTF.text = [NSString stringWithFormat:@"%i",(int)self.installmentsSlider.value];
    self.amountTF.text = [NSString stringWithFormat:@"%i",(int)self.amountSlider.value];
    self.stpDayTF.text = [NSString stringWithFormat:@"%i",(int)self.stpDaySlider.value];
    
    
    self.blurView.hidden=YES;
    self.blurView.tag = 99;
    self.acceptPopUpView.hidden=YES;
    self.acceptPopUpView.tag = 88;
    
    self.somePickerView.delegate = self;
    self.somePickerView.dataSource = self;
    [self.somePickerView reloadAllComponents];
    
    self.frequencyArray = [[NSMutableArray alloc]initWithObjects:@"Daily",@"Weekly",@"Monthly",@"Quarterly", nil];
    
    [self.dailyButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.dailyButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    [self.weeklyButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.weeklyButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    [self.monthlyButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.monthlyButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    [self.quarterlyButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.quarterlyButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    self.dailyButton.selected=NO;
    self.weeklyButton.selected=NO;
    self.monthlyButton.selected=YES;
    self.quarterlyButton.selected=NO;
    
    selectedFrequency = @"monthly";
    if(delegate.transactionDetailsArray.count>0)
    {
        [self assignUI];
        [self getAmcList];
    }
    
    self.STPTableView.delegate = self;
    self.STPTableView.dataSource = self;
    
    
    self.dailyButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.weeklyButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.monthlyButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.quarterlyButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.fundCategoryButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.schemeButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    
   
    [self.fundCategoryButton addTarget:self action:@selector(onFundButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.schemeButton addTarget:self action:@selector(onSchemeTap) forControlEvents:UIControlEventTouchUpInside];
    [self.dailyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.weeklyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.monthlyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.quarterlyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.acceptAndInvestButton addTarget:self action:@selector(onAcceptTap) forControlEvents:UIControlEventTouchUpInside];
    [self.popUpCancelButton addTarget:self action:@selector(onCancelButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.stpDayButton addTarget:self action:@selector(onStpDayTap) forControlEvents:UIControlEventTouchUpInside];
    [self.selectFrequencyButton addTarget:self action:@selector(onFrequencyTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.amountSlider addTarget:self action:@selector(onSliderChange:) forControlEvents:UIControlEventTouchUpInside];
    [self.installmentsSlider addTarget:self action:@selector(onSliderChange:) forControlEvents:UIControlEventTouchUpInside];
    [self.stpDaySlider addTarget:self action:@selector(onSliderChange:) forControlEvents:UIControlEventTouchUpInside];
    [self.doneButton addTarget:self action:@selector(onDoneButtonTap) forControlEvents:UIControlEventTouchUpInside];
    

    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;  // Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    if(self.selectFrequencyButton.selected==YES)
    {
        return self.frequencyArray.count;
    }else
    {
        return stpDetailsArrayOne.count;
    }
    return 0;
}
- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if(self.selectFrequencyButton.selected==YES)
    {
        return [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[self.frequencyArray objectAtIndex:row]]];
    }else
    {
        return [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[stpDetailsArrayOne objectAtIndex:row]]];
    }
    //Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    if(self.selectFrequencyButton.selected==YES)
    {
    selectedFrequency = [NSString stringWithFormat:@"%@",[self.frequencyArray objectAtIndex:row]];
    [self.selectFrequencyButton setTitle:selectedFrequency forState:UIControlStateNormal];
    }else
    {
        [self.stpDayButton setTitle:[NSString stringWithFormat:@"%@%@",[stpDetailsArrayOne objectAtIndex:row],[self suffixForDayInDate:[stpDetailsArrayOne objectAtIndex:row]]] forState:UIControlStateNormal];
    }
}

-(void)onCancelButtonTap
{
    self.acceptPopUpView.hidden=YES;
}

-(void)assignUI
{
    self.fundNameLabel.text = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Scheme Name"]];
    self.folioNumberLabel.text = [NSString stringWithFormat:@"Folio No %@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Folio_No"]];
    self.currentValueLabel.text = [NSString stringWithFormat:@"₹%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Value"]];
    self.unitsLabel.text = [NSString stringWithFormat:@"%.2f",[[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Units"]floatValue]];
    NSString * stpDates = [NSString stringWithFormat:@"%@",[[delegate.transactionDataArray objectAtIndex:0]objectForKey:@"stpdates"]];
    NSArray * stpDatesArray = [stpDates componentsSeparatedByString:@"|"];
    if([selectedFrequency isEqualToString:@"daily"])
    {
        stpDatesDetailString = [stpDatesArray objectAtIndex:0];
    }else if ([selectedFrequency isEqualToString:@"weekly"])
    {
        stpDatesDetailString = [stpDatesArray objectAtIndex:1];
    }else if ([selectedFrequency isEqualToString:@"monthly"])
    {
        stpDatesDetailString = [stpDatesArray objectAtIndex:2];
    }else if ([selectedFrequency isEqualToString:@"quarterly"])
    {
        stpDatesDetailString = [stpDatesArray objectAtIndex:3];
    }
    NSArray * stpDetailsArray = [stpDatesDetailString componentsSeparatedByString:@"~"];
    NSString * stpDetailsStringOne = [NSString stringWithFormat:@"%@",[stpDetailsArray objectAtIndex:1]];
    stpDetailsArrayOne = [stpDetailsStringOne componentsSeparatedByString:@";"];
    self.stpDayLeftLabel.text = [NSString stringWithFormat:@"%@",[stpDetailsArrayOne objectAtIndex:0]];
    int lastCount = (int)[stpDetailsArrayOne count]-1;
    self.stpDayRightLabel.text = [NSString stringWithFormat:@"%@",[stpDetailsArrayOne objectAtIndex:lastCount]];
    self.stpDayTF.text = [NSString stringWithFormat:@"%@",[stpDetailsArrayOne objectAtIndex:0]];
    
    [self.stpDayButton setTitle:[NSString stringWithFormat:@"%@",[stpDetailsArrayOne objectAtIndex:0]] forState:UIControlStateNormal];
    [self.selectFrequencyButton setTitle:selectedFrequency forState:UIControlStateNormal];
    
    int startDate = (int)[(NSNumber *)[stpDetailsArrayOne objectAtIndex:0] integerValue];
    int endDate = (int)[(NSNumber *)[stpDetailsArrayOne objectAtIndex:lastCount] integerValue];
    self.stpDaySlider.minimumValue=startDate;
    self.stpDaySlider.maximumValue=endDate;
    self.stpDaySlider.value = startDate;
    
    
    NSString * maximumMonths = [NSString stringWithFormat:@"%d",[[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Units"]intValue]];
    self.monthsRightLabel.text = maximumMonths;
    int endMonth = (int)[(NSNumber *)[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Units"] integerValue];
    self.installmentsSlider.maximumValue = endMonth;
    
    NSString * stpDetails = [NSString stringWithFormat:@"%@",[[delegate.transactionDataArray objectAtIndex:0]objectForKey:@"stpdtls"]];
    NSArray * stpDetailsArrayTwo = [stpDetails componentsSeparatedByString:@"|"];
    
    
    if([selectedFrequency isEqualToString:@"daily"])
    {
        stpDetailsStringTwo = [stpDetailsArrayTwo objectAtIndex:0];
    }else if ([selectedFrequency isEqualToString:@"weekly"])
    {
        stpDetailsStringTwo = [stpDetailsArrayTwo objectAtIndex:3];
    }else if ([selectedFrequency isEqualToString:@"monthly"])
    {
        stpDetailsStringTwo = [stpDetailsArrayTwo objectAtIndex:1];
    }else if ([selectedFrequency isEqualToString:@"quarterly"])
    {
        stpDetailsStringTwo = [stpDetailsArrayTwo objectAtIndex:2];
    }
    
   stpDetailsSeperatedArray = [stpDetailsStringTwo componentsSeparatedByString:@"~"];
    
    //stpdtls = M~1000~1~1~1
    // Format : “Frquency~Minimum SWP Amount~multiples for Amount~minimum Units~Multiples for Units”
    //self.installmentsSlider.minimumValue =
    int Units = (int)[(NSNumber *)[stpDetailsSeperatedArray objectAtIndex:3] integerValue];
    self.monthsLeftLabel.text = [NSString stringWithFormat:@"%d",Units];
    self.installmentsSlider.minimumValue = Units;
    self.installmentsSlider.value = Units;
    
    
    NSString * maximumAmount = [NSString stringWithFormat:@"%d",[[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Value"]intValue]];
    self.amountRightLabel.text = maximumAmount;
    int endAmount = (int)[(NSNumber *)[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Value"] integerValue];
    self.amountSlider.maximumValue = endAmount;
    
    
    int Amount = (int)[(NSNumber *)[stpDetailsSeperatedArray objectAtIndex:1] integerValue];
    self.amountLeftLabel.text = [NSString stringWithFormat:@"%d",Amount];
    self.amountSlider.minimumValue = Amount;
    self.amountSlider.value = Amount;
    
    
    self.amountTF.text = [NSString stringWithFormat:@"%d",Amount];
    self.installmentsTF.text = [NSString stringWithFormat:@"%d",Units];
    
}

-(void)onSliderChange:(id)sender
{
    if(sender == self.amountSlider)
    {
        int multiple = [[stpDetailsSeperatedArray objectAtIndex:2] intValue];
        int roundedSliderValue = roundf(self.amountSlider.value / multiple) * multiple;
        self.amountTF.text = [NSString stringWithFormat:@"%d",roundedSliderValue];
    }else if (sender == self.installmentsSlider)
    {
        int multiple = [[stpDetailsSeperatedArray objectAtIndex:4] intValue];
        int roundedSliderValue = roundf(self.installmentsSlider.value / multiple) * multiple;
        self.installmentsTF.text = [NSString stringWithFormat:@"%d",roundedSliderValue];
    }else if (sender == self.stpDaySlider)
    {
        self.stpDayTF.text = [NSString stringWithFormat:@"%i",(int)self.stpDaySlider.value];
    }
}

-(void)onStpDayTap
{
    self.stpDayButton.selected = YES;
    self.selectFrequencyButton.selected = NO;
    self.somePickerView.hidden=NO;
    self.doneView.hidden=NO;
    [self.somePickerView reloadAllComponents];
}

-(void)onFrequencyTap
{
    self.stpDayButton.selected = NO;
    self.selectFrequencyButton.selected = YES;
    self.somePickerView.hidden=NO;
    self.doneView.hidden=NO;
    [self.somePickerView reloadAllComponents];
}

-(void)onDoneButtonTap
{
    selectedFrequency = [selectedFrequency lowercaseString];
    self.doneView.hidden=YES;
    self.somePickerView.hidden = YES;
}

-(void)getAmcList
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSString * assetType =@"all";
        NSString * catagory =@"all";
        NSString * amc = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"AMC_CODE"]];
        NSLog(@"AMC:%@",amc);
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getAmcList"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@|%@",assetType,catagory,amc,cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@searchscheme_option_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataArray = [[NSMutableArray alloc]initWithArray:[[dict objectForKey:@"data"]objectForKey:@"Table"]];
            NSLog(@"%@",self.responseDataArray);
            self.swittchArray = [[NSMutableArray alloc]init];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                for (int i=0; i<self.responseDataArray.count; i++) {
                    NSDictionary *dict = @{@"SCHEMECODE" : [[self.responseDataArray objectAtIndex:i]objectForKey:@"SCHEMECODE"],
                                           @"CATEGORY"   : [[self.responseDataArray objectAtIndex:i]objectForKey:@"CATEGORY"],
                                           @"S_NAME" : [[self.responseDataArray objectAtIndex:i]objectForKey:@"S_NAME"]
                                           };
                    [self.swittchArray addObject: dict];
                }
                if(categoryArray.count>0)
                {
                    [categoryArray removeAllObjects];
                }
                if(schemeArray.count>0)
                {
                    [schemeArray removeAllObjects];
                }
                for (int i=0; i<self.swittchArray.count; i++) {
                    [categoryArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"CATEGORY"]];
                    //[schemeArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"S_NAME"]];
                }
                mySet = [[NSOrderedSet alloc] initWithArray:categoryArray];
                schemeSet = [[NSOrderedSet alloc]initWithArray:schemeArray];
                categoryUniqueArray = [[NSMutableArray alloc] initWithArray:[mySet array]];
               // schemeuniqueArray = [[NSMutableArray alloc] initWithArray:[schemeSet array]];
                [self.fundCategoryButton setTitle:[NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
                selectedCategory = [NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:0]];
                
                for (int i=0; i<self.swittchArray.count; i++) {
                    NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"CATEGORY"]];
                    if([schemeName isEqualToString:selectedCategory])
                    {
                        [schemeArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"S_NAME"]];
                        schemeSet = [[NSOrderedSet alloc]initWithArray:schemeArray];
                        schemeuniqueArray = [[NSMutableArray alloc] initWithArray:[schemeSet array]];
                        [self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
                    }
                }
                
                selectedSchemeName = [NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]];
                for (int i=0; i<self.swittchArray.count; i++) {
                    NSString * category = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i] objectForKey:@"CATEGORY"]];
                    NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i] objectForKey:@"S_NAME"]];
                    if([selectedCategory isEqualToString:category]&&[selectedSchemeName isEqualToString:schemeName])
                    {
                        toSchemeCode = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"SCHEMECODE"]];
                    }
                }
            });
        }];
        //
    }
}

-(void)onFundButtonTap
{
    check=@"fund";
    self.blurView.hidden=NO;
    [self.STPTableView reloadData];
}

-(void)onSchemeTap
{
    check=@"scheme";
    self.blurView.hidden = NO;
    if(schemeArray.count>0)
    {
        [schemeArray removeAllObjects];
    }
    for (int i=0; i<self.swittchArray.count; i++) {
        NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"CATEGORY"]];
        if([schemeName isEqualToString:selectedCategory])
        {
            [schemeArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"S_NAME"]];
            schemeSet = [[NSOrderedSet alloc]initWithArray:schemeArray];
            schemeuniqueArray = [[NSMutableArray alloc] initWithArray:[schemeSet array]];
            //[self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
        }
    }
    [self.STPTableView reloadData];
}



-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.dailyButton)
    {
        selectedFrequency = @"daily";
        self.dailyButton.selected=YES;
        self.weeklyButton.selected=NO;
        self.monthlyButton.selected=NO;
        self.quarterlyButton.selected=NO;
    }else if (sender == self.weeklyButton)
    {
        selectedFrequency = @"weekly";
        self.dailyButton.selected=NO;
        self.weeklyButton.selected=YES;
        self.monthlyButton.selected=NO;
        self.quarterlyButton.selected=NO;
    }else if (sender == self.monthlyButton)
    {
        selectedFrequency = @"monthly";
        self.dailyButton.selected=NO;
        self.weeklyButton.selected=NO;
        self.monthlyButton.selected=YES;
        self.quarterlyButton.selected=NO;
    }else if (sender == self.quarterlyButton)
    {
        selectedFrequency = @"quarterly";
        self.dailyButton.selected=NO;
        self.weeklyButton.selected=NO;
        self.monthlyButton.selected=NO;
        self.quarterlyButton.selected=YES;
    }
    [self assignUI];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STPTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"STPTableViewCell" forIndexPath:indexPath];
    if([check isEqualToString:@"fund"])
    {
        cell.nameLabel.text = [NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:indexPath.row]];
    }else if ([check isEqualToString:@"scheme"])
    {
        cell.nameLabel.text = [NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:indexPath.row]];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([check isEqualToString:@"fund"])
    {
        return categoryUniqueArray.count;
    }else if ([check isEqualToString:@"scheme"])
    {
        return schemeuniqueArray.count;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([check isEqualToString:@"fund"])
    {
        [self.fundCategoryButton setTitle:[NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        selectedCategory = [NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:indexPath.row]];
        if(schemeArray.count>0)
        {
            [schemeArray removeAllObjects];
        }
        for (int i=0; i<self.swittchArray.count; i++) {
            NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"CATEGORY"]];
            if([schemeName isEqualToString:selectedCategory])
            {
                [schemeArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"S_NAME"]];
                schemeSet = [[NSOrderedSet alloc]initWithArray:schemeArray];
                schemeuniqueArray = [[NSMutableArray alloc] initWithArray:[schemeSet array]];
                [self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
            }
        }
    }else if ([check isEqualToString:@"scheme"])
    {
        [self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        selectedSchemeName = [NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:indexPath.row]];
        for (int i=0; i<self.swittchArray.count; i++) {
            NSString * category = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i] objectForKey:@"CATEGORY"]];
            NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i] objectForKey:@"S_NAME"]];
            if([selectedCategory isEqualToString:category]&&[selectedSchemeName isEqualToString:schemeName])
            {
                toSchemeCode = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"SCHEMECODE"]];
            }
        }
    }
    self.blurView.hidden=YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.blurView.hidden=YES;
    }
    if(touch.view.tag==88){
        self.acceptPopUpView.hidden=YES;
    }
}
-(void)onNextButtonTap
{
    if(toSchemeCode.length!=0)
    {
        [UIView transitionWithView:self.acceptPopUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpCategoryLabel.text = [NSString stringWithFormat:@"%@",self.fundCategoryButton.titleLabel.text];
                            self.popUpschemeLabel.text = [NSString stringWithFormat:@"%@",self.schemeButton.titleLabel.text];
                            self.popUpFrequencyLabel.text = [NSString stringWithFormat:@"%@",selectedFrequency];
                            self.popUpInstallmentsLabel.text = [NSString stringWithFormat:@"%@ Months",self.installmentsTF.text];
                            self.popUpAmountLabel.text = [NSString stringWithFormat:@"%@",self.amountTF.text];
                            self.popUpSTPDayLabel.text = [NSString stringWithFormat:@"%@%@",self.stpDayTF.text,[self suffixForDayInDate:self.stpDayTF.text]];
                            self.acceptPopUpView.hidden = NO;
                        }
                        completion:NULL];
    }
}

- (NSString *)suffixForDayInDate:(NSString *)date
{
    int day = [date intValue];
    if (day >= 11 && day <= 13) {
        return @"th";
    } else if (day % 10 == 1) {
        return @"st";
    } else if (day % 10 == 2) {
        return @"nd";
    } else if (day % 10 == 3) {
        return @"rd";
    } else {
        return @"th";
    }
}

-(void)onAcceptTap
{
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  SwitchViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 05/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "SwitchViewController.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "SwitchPopUpTableViewCell.h"
#import "PaymentWebViewController.h"
#import "DGActivityIndicatorView.h"

@interface SwitchViewController ()
{
     UIToolbar*toolbar;
    Utility * mfSharedManager;
    AppDelegate * delegate;
    NSOrderedSet *mySet;
    NSOrderedSet * schemeSet;
    NSMutableArray * categoryUniqueArray;
    NSMutableArray * schemeuniqueArray;
    NSString * check;
    NSString * buttonCheck;
    NSMutableArray * categoryArray;
    NSMutableArray * schemeArray;
    UIView * backView;
    NSString * toSchemeCode;
    NSString * selectedCategory;
    NSString * selectedSchemeName;
    DGActivityIndicatorView *activityIndicatorView;
}

@end

@implementation SwitchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mfSharedManager=[Utility MF];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    categoryArray = [[NSMutableArray alloc]init];
    schemeArray = [[NSMutableArray alloc]init];
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.topView.layer.shadowOpacity = 0.6f;
    self.topView.layer.shadowRadius = 6.5f;
    self.topView.layer.masksToBounds = NO;
    
    self.insidePopUpView.layer.cornerRadius = 5.0f;
    
    self.acceptAndInvestButton.layer.cornerRadius=20.0f;
    self.acceptAndInvestButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.acceptAndInvestButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.acceptAndInvestButton.layer.shadowOpacity = 0.6f;
    self.acceptAndInvestButton.layer.shadowRadius = 6.5f;
    self.acceptAndInvestButton.layer.masksToBounds = NO;
    
    
    
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.nextButton.layer.shadowOpacity = 0.6f;
    self.nextButton.layer.shadowRadius = 6.5f;
    self.nextButton.layer.masksToBounds = NO;
    
    
    self.bottomView.layer.cornerRadius=5.0f;
    self.bottomView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.bottomView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.bottomView.layer.shadowOpacity = 1.0f;
    self.bottomView.layer.shadowRadius = 3.0f;
    self.bottomView.layer.masksToBounds = NO;
    self.unitsButton.layer.cornerRadius = 20.0f;
    self.amountButton.layer.cornerRadius = 20.0f;
    buttonCheck = @"units";
    self.blurView.hidden=YES;
    self.blurView.tag = 99;
    self.acceptPopUpView.tag=88;
    self.acceptPopUpView.hidden=YES;
    self.popUpTableView.delegate = self;
    self.popUpTableView.dataSource = self;
    self.unitsButton.selected=YES;
    self.amountButton.selected = NO;
    

    NSLog(@"%@",delegate.transactionDetailsArray);
    if(delegate.transactionDetailsArray.count>0)
    {
        [self assignUI];
        [self getAmcList];
        [self onUnitButtonTap];
    }
    
    self.monthsTF.text = [NSString stringWithFormat:@"%i",(int)self.unitsSlider.value];
    
    self.fundCategoryButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.schemeButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    
    [self.fundCategoryButton addTarget:self action:@selector(onFundButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.schemeButton addTarget:self action:@selector(onSchemeTap) forControlEvents:UIControlEventTouchUpInside];
    [self.unitsButton addTarget:self action:@selector(onUnitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.amountButton addTarget:self action:@selector(onAmountTap) forControlEvents:UIControlEventTouchUpInside];
    [self.unitsSlider addTarget:self action:@selector(onSliderChange) forControlEvents:UIControlEventValueChanged];
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.acceptAndInvestButton addTarget:self action:@selector(onAcceptTap) forControlEvents:UIControlEventTouchUpInside];
    [self.popUpCancelButton addTarget:self action:@selector(onCancelTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onCancelTap
{
    self.acceptPopUpView.hidden=YES;
}

-(void)onAcceptTap
{
    [self insertSwitchCart];
}

-(void)onFundButtonTap
{
    check=@"fund";
   
    //backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    //[self.view addSubview:backView];
    self.blurView.hidden=NO;
    //[backView addSubview:self.popUpView];
    
     [self.popUpTableView reloadData];
}

-(void)onSchemeTap
{
    
    check=@"scheme";
    if(schemeArray.count>0)
    {
        [schemeArray removeAllObjects];
    }
    for (int i=0; i<self.swittchArray.count; i++) {
        NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"CATEGORY"]];
        if([schemeName isEqualToString:selectedCategory])
        {
            [schemeArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"S_NAME"]];
            schemeSet = [[NSOrderedSet alloc]initWithArray:schemeArray];
            schemeuniqueArray = [[NSMutableArray alloc] initWithArray:[schemeSet array]];
            //[self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
        }
    }
    [self.popUpTableView reloadData];
//    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
//    [self.view addSubview:backView];
    self.blurView.hidden=NO;
   // [backView addSubview:self.popUpView];
    // get your window screen size
    
    
}


-(void)assignUI
{
    self.fundNameLabel.text = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Scheme Name"]];
    self.folioNumberLabel.text = [NSString stringWithFormat:@"Folio No %@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Folio_No"]];
    self.currentValueLabel.text = [NSString stringWithFormat:@"₹%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Value"]];
    self.topUnitsLabel.text = [NSString stringWithFormat:@"%.2f",[[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Units"]floatValue]];
    if([buttonCheck isEqualToString:@"units"])
    {
        int minValue = [[[delegate.transactionDataArray objectAtIndex:0] objectForKey:@"MinRedemptionUnits"] intValue];
        int maxValue = [[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Present Units"] intValue];
        self.unitsSlider.minimumValue = minValue;
        self.unitsSlider.maximumValue = maxValue;
        self.unitsSlider.value = 0;
        self.unitsLeftLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDataArray objectAtIndex:0] objectForKey:@"MinRedemptionUnits"]intValue]];
        self.unitsRightLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Present Units"]intValue]];
        self.monthsTF.text = [NSString stringWithFormat:@"%i",(int)self.unitsSlider.value];
    }else if ([buttonCheck isEqualToString:@"amount"])
    {
        int minValue = [[[delegate.transactionDataArray objectAtIndex:0] objectForKey:@"MinRedemptionAmount"] intValue];
        int maxValue = [[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Present Value"] intValue];
        self.unitsSlider.minimumValue = minValue;
        self.unitsSlider.maximumValue = maxValue;
        self.unitsSlider.value = 0;
        self.unitsLeftLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDataArray objectAtIndex:0] objectForKey:@"MinRedemptionAmount"]intValue]];
        self.unitsRightLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Present Value"]intValue]];
        self.monthsTF.text = [NSString stringWithFormat:@"%i",(int)self.unitsSlider.value];
    }
    
}

-(void)onUnitButtonTap
{
    self.unitsButton.selected=YES;
    self.amountButton.selected = NO;
    buttonCheck = @"units";
    self.unitsLabel.text = @"Number of Units";
    int minValue = [[[delegate.transactionDataArray objectAtIndex:0] objectForKey:@"MinRedemptionUnits"] intValue];
    int maxValue = [[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Present Units"] intValue];
    self.unitsSlider.minimumValue = minValue;
    self.unitsSlider.maximumValue = maxValue;
    self.unitsSlider.value = 0;
    self.unitsLeftLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDataArray objectAtIndex:0] objectForKey:@"MinRedemptionUnits"]intValue]];
    self.unitsRightLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Present Units"]intValue]];
    self.monthsTF.text = [NSString stringWithFormat:@"%i",(int)self.unitsSlider.value];
    self.monthsTF.text = [NSString stringWithFormat:@"%i",(int)self.unitsSlider.value];
    [self.unitsButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0]];
    [self.amountButton setBackgroundColor:[UIColor clearColor]];
    [self.unitsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.amountButton setTitleColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    self.unitsButton.layer.borderWidth = 0.0f;
    self.unitsButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.amountButton.layer.borderWidth = 1.0f;
    self.amountButton.layer.borderColor = [[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] CGColor];
    
}

-(void)onAmountTap
{
    self.unitsButton.selected=NO;
    self.amountButton.selected =YES;
    buttonCheck = @"amount";
    self.unitsLabel.text = @"Amount";
    int minValue = [[[delegate.transactionDataArray objectAtIndex:0] objectForKey:@"MinRedemptionAmount"] intValue];
    int maxValue = [[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Present Value"] intValue];
    self.unitsSlider.minimumValue = minValue;
    self.unitsSlider.maximumValue = maxValue;
    self.unitsSlider.value = 0;
    self.unitsLeftLabel.text = [NSString stringWithFormat:@"₹%d",[[[delegate.transactionDataArray objectAtIndex:0] objectForKey:@"MinRedemptionAmount"]intValue]];
    self.unitsRightLabel.text = [NSString stringWithFormat:@"₹%d",[[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Present Value"]intValue]];
    self.monthsTF.text = [NSString stringWithFormat:@"₹%i",(int)self.unitsSlider.value];
    [self.amountButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0]];
    [self.unitsButton setBackgroundColor:[UIColor clearColor]];
    [self.amountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.unitsButton setTitleColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    self.amountButton.layer.borderWidth = 0.0f;
    self.amountButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.unitsButton.layer.borderWidth = 1.0f;
    self.unitsButton.layer.borderColor = [[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] CGColor];
}

-(void)insertSwitchCart
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSString * fromSchemeCode = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Scheme Code"]];
        NSString * folioNumber = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Folio_No"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        NSString * amountString;
        NSString * unitsString;
        if(self.amountButton.selected==YES)
        {
            amountString = self.monthsTF.text;
            unitsString = @"0";
        }else if (self.unitsButton.selected==YES)
        {
            amountString = @"0";
            unitsString = self.monthsTF.text;
        }
        [inputArray addObject:@"insertSwitchCart"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@",cid,sessionid,fromSchemeCode,toSchemeCode,folioNumber,unitsString,amountString,@"N",@"NA"];
        [inputArray addObject:[NSString stringWithFormat:@"%@insswtorder_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataArray = [[NSMutableArray alloc]initWithArray:[[dict objectForKey:@"data"]objectForKey:@"Table"]];
            NSLog(@"%@",self.responseDataArray);
            if(self.responseDataArray.count>0)
            {
                [self executeSwitchCart];
            }
        }];
        //
    }
}

-(void)executeSwitchCart
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        NSString * sysID;
        [inputArray addObject:@"executeSwitchCart"];
        for (int i=0; self.responseDataArray.count; i++) {
            NSString * toCode = [NSString stringWithFormat:@"%@",[[self.responseDataArray objectAtIndex:i]objectForKey:@"toschemecode"]];
            if([toSchemeCode isEqualToString:toCode])
            {
                sysID = [NSString stringWithFormat:@"%@",[[self.responseDataArray objectAtIndex:i] objectForKey:@"sysid"]];
            }
        }
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",cid,sessionid,sysID];
        [inputArray addObject:[NSString stringWithFormat:@"%@execute_swtbasket_all_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.executeDicitionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.executeDicitionary);
            if(self.executeDicitionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                    NSString * endPoint = @"http://zenwiseapi.aceonlineplatform.com/";
                    NSString * url = [NSString stringWithFormat:@"%@",[[self.executeDicitionary objectForKey:@"data"]objectForKey:@"result"]];
                    NSString * finalURL = [endPoint stringByAppendingString:url];
                    PaymentWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentWebViewController"];
                    webView.loadURL = finalURL;
                    [self.navigationController pushViewController:webView animated:YES];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

-(void)onSliderChange
{
    if(self.unitsButton.selected==YES)
    {
     self.monthsTF.text = [NSString stringWithFormat:@"%i",(int)self.unitsSlider.value];
    }else if (self.amountButton.selected==YES)
    {
        self.monthsTF.text = [NSString stringWithFormat:@"₹%i",(int)self.unitsSlider.value];
    }
}

-(void)getAmcList
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSString * assetType =@"all";
        NSString * catagory =@"all";
        NSString * amc = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"AMC_CODE"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getAmcList"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@|%@",assetType,catagory,amc,cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@searchscheme_option_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataArray = [[NSMutableArray alloc]initWithArray:[[dict objectForKey:@"data"]objectForKey:@"Table"]];
            NSLog(@"%@",self.responseDataArray);
            self.swittchArray = [[NSMutableArray alloc]init];
           
            dispatch_async(dispatch_get_main_queue(), ^{
                for (int i=0; i<self.responseDataArray.count; i++) {
                    NSDictionary *dict = @{@"SCHEMECODE" : [[self.responseDataArray objectAtIndex:i]objectForKey:@"SCHEMECODE"],
                                           @"CATEGORY"   : [[self.responseDataArray objectAtIndex:i]objectForKey:@"CATEGORY"],
                                           @"S_NAME" : [[self.responseDataArray objectAtIndex:i]objectForKey:@"S_NAME"]
                                           };
                    
                    [self.swittchArray addObject: dict];
                }
                if(categoryArray.count>0)
                {
                    [categoryArray removeAllObjects];
                }
                if(schemeArray.count>0)
                {
                    [schemeArray removeAllObjects];
                }
                for (int i=0; i<self.swittchArray.count; i++) {
                    [categoryArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"CATEGORY"]];
                    //[schemeArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"S_NAME"]];
                }
                mySet = [[NSOrderedSet alloc] initWithArray:categoryArray];
                schemeSet = [[NSOrderedSet alloc]initWithArray:schemeArray];
                categoryUniqueArray = [[NSMutableArray alloc] initWithArray:[mySet array]];
                //schemeuniqueArray = [[NSMutableArray alloc] initWithArray:[schemeSet array]];
                [self.fundCategoryButton setTitle:[NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
                selectedCategory = [NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:0]];
                
                
                for (int i=0; i<self.swittchArray.count; i++) {
                    NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"CATEGORY"]];
                    if([schemeName isEqualToString:selectedCategory])
                    {
                        [schemeArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"S_NAME"]];
                        schemeSet = [[NSOrderedSet alloc]initWithArray:schemeArray];
                        schemeuniqueArray = [[NSMutableArray alloc] initWithArray:[schemeSet array]];
                        [self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
                    }
                }
                selectedSchemeName = [NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]];
                for (int i=0; i<self.swittchArray.count; i++) {
                    NSString * category = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i] objectForKey:@"CATEGORY"]];
                    NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i] objectForKey:@"S_NAME"]];
                    if([selectedCategory isEqualToString:category]&&[selectedSchemeName isEqualToString:schemeName])
                    {
                        toSchemeCode = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"SCHEMECODE"]];
                    }
                }
                //[self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
            });
        }];
        //
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SwitchPopUpTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchPopUpTableViewCell" forIndexPath:indexPath];
    if([check isEqualToString:@"fund"])
    {
        cell.popUpLabel.text = [NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:indexPath.row]];
    }else if ([check isEqualToString:@"scheme"])
    {
        cell.popUpLabel.text = [NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:indexPath.row]];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([check isEqualToString:@"fund"])
    {
        return categoryUniqueArray.count;
    }else if ([check isEqualToString:@"scheme"])
    {
        return schemeuniqueArray.count;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([check isEqualToString:@"fund"])
    {
        [self.fundCategoryButton setTitle:[NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        selectedCategory = [NSString stringWithFormat:@"%@",[categoryUniqueArray objectAtIndex:indexPath.row]];
        if(schemeArray.count>0)
        {
            [schemeArray removeAllObjects];
        }
        for (int i=0; i<self.swittchArray.count; i++) {
            NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"CATEGORY"]];
            if([schemeName isEqualToString:selectedCategory])
            {
                [schemeArray addObject:[[self.swittchArray objectAtIndex:i]objectForKey:@"S_NAME"]];
                schemeSet = [[NSOrderedSet alloc]initWithArray:schemeArray];
                schemeuniqueArray = [[NSMutableArray alloc] initWithArray:[schemeSet array]];
                [self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:0]] forState:UIControlStateNormal];
            }
        }
    }else if ([check isEqualToString:@"scheme"])
    {
        [self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        selectedSchemeName = [NSString stringWithFormat:@"%@",[schemeuniqueArray objectAtIndex:indexPath.row]];
        for (int i=0; i<self.swittchArray.count; i++) {
            NSString * category = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i] objectForKey:@"CATEGORY"]];
            NSString * schemeName = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i] objectForKey:@"S_NAME"]];
            if([selectedCategory isEqualToString:category]&&[selectedSchemeName isEqualToString:schemeName])
            {
                toSchemeCode = [NSString stringWithFormat:@"%@",[[self.swittchArray objectAtIndex:i]objectForKey:@"SCHEMECODE"]];
            }
        }
    }
    self.blurView.hidden=YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.blurView.hidden=YES;
    }
    if(touch.view.tag==88){
        self.acceptPopUpView.hidden=YES;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(void)onNextButtonTap
{
    if(toSchemeCode.length!=0)
    {
        [UIView transitionWithView:self.acceptPopUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpCategoryLabel.text = [NSString stringWithFormat:@"%@",self.schemeButton.titleLabel.text];
                            self.popUpSchemeLabel.text = [NSString stringWithFormat:@"%@",self.fundCategoryButton.titleLabel.text];
                            if(self.unitsButton.selected==YES)
                            {
                                self.popUpTypeLabel.text = @"Units";
                                self.popUpValueLabel.text = [NSString stringWithFormat:@"%@",self.monthsTF.text];
                            }else if (self.amountButton.selected == YES)
                            {
                                self.popUpTypeLabel.text = @"Amount";
                                self.popUpValueLabel.text = [NSString stringWithFormat:@"%@",self.monthsTF.text];
                            }
                            
                            self.acceptPopUpView.hidden = NO;
                        }
                        completion:NULL];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

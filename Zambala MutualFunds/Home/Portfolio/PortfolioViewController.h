//
//  PortfolioViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ViewController.h"
#import "KATCircularProgress.h"
#import "Reachability.h"

@interface PortfolioViewController : ViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *segmentedControlView;
@property (weak, nonatomic) IBOutlet KATCircularProgress *circularProgressBar;
@property (weak, nonatomic) IBOutlet UITableView *portfolioTableView;
@property (weak, nonatomic) IBOutlet UILabel *foliosLbl;
@property (weak, nonatomic) IBOutlet UILabel *fundsLbl;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *gainLbl;
@property (weak, nonatomic) IBOutlet UILabel *gainPerLbl;

@property (weak, nonatomic) IBOutlet UILabel *investedLbl;

@property NSMutableArray *folioArray;
@property NSMutableArray * dataArray;
@property NSMutableArray * dataUniqueArray;
@property Reachability * reach;

@end

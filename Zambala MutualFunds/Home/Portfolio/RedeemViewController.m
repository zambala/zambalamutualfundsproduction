//
//  RedeemViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 02/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "RedeemViewController.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "DGActivityIndicatorView.h"
#import "PaymentWebViewController.h"

@interface RedeemViewController ()
{
    Utility * mfSharedManager;
    AppDelegate * delegate;
    NSArray * sipDatesDetailArray;
    NSArray * sipArrayOne;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
}

@end

@implementation RedeemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mfSharedManager=[Utility MF];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0.0f, 1.5f);
    self.topView.layer.shadowOpacity = 0.6f;
    self.topView.layer.shadowRadius = 6.5f;
    self.topView.layer.masksToBounds = NO;
    
    self.unitsButton.layer.cornerRadius = 30.0f;
    self.amountButton.layer.cornerRadius = 30.0f;
    
    self.popUpView.hidden=YES;
    
    self.confirmButton.layer.cornerRadius=20.0f;
    self.confirmButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.confirmButton.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.confirmButton.layer.shadowOpacity = 0.6f;
    self.confirmButton.layer.shadowRadius = 3.0f;
    self.confirmButton.layer.masksToBounds = NO;
    
    self.unitsButton.layer.cornerRadius = 20.0f;
    self.amountButton.layer.cornerRadius = 20.0f;
    
    self.acceptAndInvestButton.layer.cornerRadius=20.0f;
    self.acceptAndInvestButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.acceptAndInvestButton.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.acceptAndInvestButton.layer.shadowOpacity = 0.6f;
    self.acceptAndInvestButton.layer.shadowRadius = 3.0f;
    self.acceptAndInvestButton.layer.masksToBounds = NO;
    
    self.insidePopUpView.layer.cornerRadius = 5.0f;
    
    self.bottomMainView.layer.cornerRadius=5.0f;
    self.bottomMainView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.bottomMainView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.bottomMainView.layer.shadowOpacity = 1.0f;
    self.bottomMainView.layer.shadowRadius = 3.0f;
    self.bottomMainView.layer.masksToBounds = NO;
    
    self.unitsButton.selected = YES;
    [self onUintsButtonTap];
    self.popUpView.tag=99;
    [self.unitsButton addTarget:self action:@selector(onUintsButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.amountButton addTarget:self action:@selector(onAmountButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmButton addTarget:self action:@selector(onConfirmButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.unitsSlider addTarget:self action:@selector(onSliderChange) forControlEvents:UIControlEventValueChanged];
    [self.acceptAndInvestButton addTarget:self action:@selector(onAcceptTap) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%@",delegate.transactionDataArray);
    [self assignUI];
}

-(void)onUintsButtonTap
{
    self.unitsButton.selected=YES;
    self.amountButton.selected=NO;
    self.noofUnitsLabel.text = @"Number of Units";
    self.typeLabel.text = @"Units";
    self.sliderLeftLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDataArray objectAtIndex:0]objectForKey:@"MinRedemptionUnits"]intValue]];
    self.sliderRightLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Units"]intValue]];
    int min = [self.sliderLeftLabel.text intValue];
    int max = [self.sliderRightLabel.text intValue];
    self.unitsSlider.minimumValue=min;
    self.unitsSlider.maximumValue=max;
    self.unitsSlider.value = 0;
    self.unitsTF.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDataArray objectAtIndex:0]objectForKey:@"MinRedemptionUnits"]intValue]];
    [self.unitsButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0]];
    [self.amountButton setBackgroundColor:[UIColor clearColor]];
    [self.unitsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.amountButton setTitleColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    self.unitsButton.layer.borderWidth = 0.0f;
    self.unitsButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.amountButton.layer.borderWidth = 1.0f;
    self.amountButton.layer.borderColor = [[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] CGColor];
}

-(void)onAmountButtonTap
{
    self.unitsButton.selected=NO;
    self.amountButton.selected=YES;
    self.noofUnitsLabel.text = @"Amount";
    self.typeLabel.text = @"Amount";
    self.sliderLeftLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDataArray objectAtIndex:0]objectForKey:@"MinRedemptionAmount"]intValue]];
    self.sliderRightLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Value"]intValue]];
    int min = [self.sliderLeftLabel.text intValue];
    int max = [self.sliderRightLabel.text intValue];
    self.unitsSlider.minimumValue=min;
    self.unitsSlider.maximumValue=max;
    self.unitsSlider.value =0;
    self.unitsTF.text = [NSString stringWithFormat:@"₹%d",[[[delegate.transactionDataArray objectAtIndex:0]objectForKey:@"MinRedemptionAmount"]intValue]];
    [self.amountButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0]];
    [self.unitsButton setBackgroundColor:[UIColor clearColor]];
    [self.amountButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.unitsButton setTitleColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    self.amountButton.layer.borderWidth = 0.0f;
    self.amountButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.unitsButton.layer.borderWidth = 1.0f;
    self.unitsButton.layer.borderColor = [[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0] CGColor];
}

-(void)assignUI
{
    self.fundNameLabel.text = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Scheme Name"]];
    self.folioNumberLabel.text = [NSString stringWithFormat:@"Folio %@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Folio_No"]];
    self.currentValueLabel.text = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Value"]];
    self.unitsLabel.text = [NSString stringWithFormat:@"%.2f",[[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Units"]floatValue]];
    self.unitsTF.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDataArray objectAtIndex:0]objectForKey:@"MinRedemptionUnits"]intValue]];
    self.sliderLeftLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDataArray objectAtIndex:0]objectForKey:@"MinRedemptionUnits"]intValue]];
    self.sliderRightLabel.text = [NSString stringWithFormat:@"%d",[[[delegate.transactionDetailsArray objectAtIndex:0]objectForKey:@"Present Units"]intValue]];
}

-(void)onConfirmButtonTap
{
    [UIView transitionWithView:self.popUpView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.finalValueLabel.text = [NSString stringWithFormat:@"%@",self.unitsTF.text];
                        self.popUpView.hidden = NO;
                    }
                    completion:NULL];
}

-(void)onAcceptTap
{
    [self insertCart];
}

-(void)insertCart
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSString * schemeCode = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Scheme Code"]];
        NSString * folioNumber = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Folio_No"]];
        NSString * unitsString;
        NSString * amountString;
        if(self.amountButton.selected==YES)
        {
            unitsString = [NSString stringWithFormat:@"%@",@"0"];
            amountString = [NSString stringWithFormat:@"%@",self.unitsTF.text];
        }else if (self.unitsButton.selected==YES)
        {
            unitsString = [NSString stringWithFormat:@"%@",self.unitsTF.text];
            amountString = [NSString stringWithFormat:@"%@",@"0"];
        }
        
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"insertRedeemCart"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@",cid,sessionid,schemeCode,folioNumber,unitsString,amountString,@"N",@"NA"];
        [inputArray addObject:[NSString stringWithFormat:@"%@insrdorder_TDES/%@/%@/%@/%@/%@/%@/%@/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            if(self.responseDataDictionary.count>0)
            {
                [self executeCart];
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

-(void)executeCart
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSString * schemeCode = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Scheme Code"]];
        NSString * sysidlist;
        for (int i=0; i<[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count]; i++) {
            NSString * schemeCodeServer = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:i]objectForKey:@"schemecode"]];
            if([schemeCode isEqualToString:schemeCodeServer])
            {
                sysidlist = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:i]objectForKey:@"sysid"]];
            }
        }
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"excuteRedeemCart"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",cid,sessionid,sysidlist];
        [inputArray addObject:[NSString stringWithFormat:@"%@execute_rdbasket_multiple_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.excecuteDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.excecuteDataDictionary);
            if(self.excecuteDataDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                    NSString * endPoint = @"http://zenwiseapi.aceonlineplatform.com/";
                    NSString * url = [NSString stringWithFormat:@"%@",[[self.excecuteDataDictionary objectForKey:@"data"]objectForKey:@"result"]];
                    NSString * finalURL = [endPoint stringByAppendingString:url];
                    PaymentWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentWebViewController"];
                    webView.loadURL = finalURL;
                    [self.navigationController pushViewController:webView animated:YES];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.popUpView.hidden=YES;
    }
}

-(void)onSliderChange
{
    if(self.unitsButton.selected==YES)
    {
    self.unitsTF.text = [NSString stringWithFormat:@"%.2f",self.unitsSlider.value];
    }else if (self.amountButton.selected == YES)
    {
        self.unitsTF.text = [NSString stringWithFormat:@"₹%.2f",self.unitsSlider.value];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

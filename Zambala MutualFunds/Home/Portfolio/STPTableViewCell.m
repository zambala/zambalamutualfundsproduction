//
//  STPTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 18/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "STPTableViewCell.h"

@implementation STPTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

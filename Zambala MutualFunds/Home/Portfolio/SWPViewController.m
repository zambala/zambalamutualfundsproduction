//
//  SWPViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 05/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "SWPViewController.h"

@interface SWPViewController ()
{
    UIToolbar * toolbar;
    NSDateFormatter *dateFormatter;
}

@end

@implementation SWPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dateFormatter=[[NSDateFormatter alloc]init];
    
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    
    self.topView.layer.cornerRadius=10.0f;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.topView.layer.shadowOpacity = 10.0f;
    self.topView.layer.shadowRadius = 3.0f;
    self.topView.layer.masksToBounds = NO;
    
    self.confirmButton.layer.cornerRadius=20.0f;
    self.confirmButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.confirmButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.confirmButton.layer.shadowOpacity = 10.0f;
    self.confirmButton.layer.shadowRadius = 3.0f;
    self.confirmButton.layer.masksToBounds = NO;
    
    self.unitsButton.layer.cornerRadius=20.0f;
    self.amountButton.layer.cornerRadius=20.0f;
    
    self.installmentsTF.text = [NSString stringWithFormat:@"%i",(int)self.installmentsSlider.value];
    self.amountTF.text = [NSString stringWithFormat:@"%i",(int)self.amountSlider.value];
    
    self.pickerView.hidden=YES;
    self.pickerViewheightConstraint.constant=0;
    self.datePicker.hidden = YES;
    self.datePickerHeightConstraint.constant=0;
    
    [self.monthlyButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.monthlyButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    [self.quarterlyButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.quarterlyButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    [self.halfYearlyButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.halfYearlyButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    [self.yearlyButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.yearlyButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    self.monthlyButton.selected=YES;
    self.quarterlyButton.selected=NO;
    self.halfYearlyButton.selected=NO;
    self.yearlyButton.selected=NO;
    
    self.stpButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    
    self.monthlyButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.quarterlyButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.yearlyButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    self.halfYearlyButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    
    [self.monthlyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.quarterlyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.halfYearlyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.yearlyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.stpButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.unitsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.amountButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];

    
    [self.amountSlider addTarget:self action:@selector(onSliderChange:) forControlEvents:UIControlEventTouchUpInside];
    [self.installmentsSlider addTarget:self action:@selector(onSliderChange:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.monthlyButton)
    {
        self.monthlyButton.selected=YES;
        self.quarterlyButton.selected=NO;
        self.halfYearlyButton.selected=NO;
        self.yearlyButton.selected=NO;
    }else if (sender == self.quarterlyButton)
    {
        self.monthlyButton.selected=NO;
        self.quarterlyButton.selected=YES;
        self.halfYearlyButton.selected=NO;
        self.yearlyButton.selected=NO;
    }else if (sender == self.halfYearlyButton)
    {
        self.monthlyButton.selected=NO;
        self.quarterlyButton.selected=NO;
        self.halfYearlyButton.selected=YES;
        self.yearlyButton.selected=NO;
    }else if (sender == self.yearlyButton)
    {
        self.monthlyButton.selected=NO;
        self.quarterlyButton.selected=NO;
        self.halfYearlyButton.selected=NO;
        self.yearlyButton.selected=YES;
    }else if (sender == self.stpButton)
    {
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        [self.scrollView setContentOffset:bottomOffset animated:YES];
        self.datePicker.hidden=NO;
        self.datePickerHeightConstraint.constant=162;
        self.pickerView.hidden=YES;
        self.pickerViewheightConstraint.constant=0;
        [self.datePicker addTarget:self action:@selector(LabelTitle) forControlEvents:UIControlEventAllEvents];
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDateDoneButtonTap)];
        
        toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
        NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
        [toolbar setItems:toolbarItems];
        [self.view addSubview:toolbar];
        
        self.datePicker.backgroundColor=[UIColor lightGrayColor];
    }else if (sender == self.unitsButton)
    {
        self.amountLabel.text = @"Installment Units";
        self.amountLeftLabel.text = @"100";
        self.amountRightLabel.text = @"467";
        self.amountSlider.minimumValue=100;
        self.amountSlider.maximumValue=467;
        self.amountSlider.value = 200;
        self.amountTF.text = [NSString stringWithFormat:@"%i",(int)self.amountSlider.value];
        [self.unitsButton setBackgroundColor:[UIColor colorWithRed:(244.0/255.0) green:(187.0/255.0) blue:(24.0/255.0) alpha:1.0]];
        [self.amountButton setBackgroundColor:[UIColor colorWithRed:(198.0/255.0) green:(198.0/255.0) blue:(198.0/255.0) alpha:1.0]];
    }else if (sender == self.amountButton)
    {
        self.amountLabel.text = @"Installments Amount";
        self.amountLeftLabel.text = @"100";
        self.amountRightLabel.text = @"22,000";
        self.amountSlider.minimumValue=100;
        self.amountSlider.maximumValue=22000;
        self.amountSlider.value = 10000;
        self.amountTF.text = [NSString stringWithFormat:@"%i",(int)self.amountSlider.value];
        [self.amountButton setBackgroundColor:[UIColor colorWithRed:(244.0/255.0) green:(187.0/255.0) blue:(24.0/255.0) alpha:1.0]];
        [self.unitsButton setBackgroundColor:[UIColor colorWithRed:(198.0/255.0) green:(198.0/255.0) blue:(198.0/255.0) alpha:1.0]];
    }
}

-(void)onSliderChange:(id)sender
{
    if(sender == self.amountSlider)
    {
        self.amountTF.text = [NSString stringWithFormat:@"%i",(int)self.amountSlider.value];
    }else if (sender == self.installmentsSlider)
    {
        self.installmentsTF.text = [NSString stringWithFormat:@"%i",(int)self.installmentsSlider.value];
    }
}
-(void)LabelTitle
{
    
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    //[dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString *dobString=[NSString stringWithFormat:@"%@",[dateFormatter  stringFromDate:self.datePicker.date]];
    
    //assign text to label
    [self.stpButton setTitle:dobString forState:UIControlStateNormal];
}

-(void)onDoneButtonTap
{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    self.pickerView.hidden=YES;
    self.pickerViewheightConstraint.constant=0;
    [toolbar removeFromSuperview];
}

-(void)onDateDoneButtonTap
{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    self.datePicker.hidden=YES;
    self.datePickerHeightConstraint.constant=0;
     [toolbar removeFromSuperview];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"Choice-%ld",(long)row];//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

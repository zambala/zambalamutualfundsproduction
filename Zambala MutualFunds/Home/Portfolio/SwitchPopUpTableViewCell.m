//
//  SwitchPopUpTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 14/06/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "SwitchPopUpTableViewCell.h"

@implementation SwitchPopUpTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

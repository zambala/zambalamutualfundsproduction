//
//  PortfolioViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "PortfolioViewController.h"
#import "KATCircularProgress.h"
#import "HMSegmentedControl.h"
#import "PortfolioTableViewCell.h"
#import "PortfolioDetailViewController.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "DGActivityIndicatorView.h"
#import "LoginViewController.h"

@interface PortfolioViewController ()
{
    HMSegmentedControl * segmentedControl;
    Utility * mfSharedManager;
    NSArray * uniqueArray;
    NSMutableDictionary * finalDict;
    NSMutableDictionary * sample1;
    NSMutableDictionary * finalDataDictionary;
    AppDelegate * delegate;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
}

@end

@implementation PortfolioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    finalDict=[[NSMutableDictionary alloc]init];
    finalDataDictionary = [[NSMutableDictionary alloc]init];
     mfSharedManager=[Utility MF];
//    uniqueArray = [[NSMutableArray alloc]init];
    
    
    self.topView.layer.cornerRadius=10.0f;
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 1.5f);
    self.topView.layer.shadowOpacity = 0.6f;
    self.topView.layer.shadowRadius = 6.5f;
    self.topView.layer.masksToBounds = NO;
    
    self.currentValueLbl.text=@"test";
    
    NSMutableArray * localSegmentsArray = [[NSMutableArray alloc]init];
    
    [localSegmentsArray addObject:@"AMC"];
    [localSegmentsArray addObject:@"Fund Category"];
    [localSegmentsArray addObject:@"Asset"];
    [localSegmentsArray addObject:@"Fund Name"];
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.segmentedControlView.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:2];
    [self.segmentedControlView addSubview:segmentedControl];
    
    
    [self.circularProgressBar.sliceItems removeAllObjects];
    
    //set bar thickness
    [self.circularProgressBar setLineWidth:30.0];
    //set animation duration
    [self.circularProgressBar setAnimationDuration:2];
    
    //Create Slice Item objects.
    SliceItem *item1 = [[SliceItem alloc] init];
    item1.itemValue = 20.0;
    item1.itemColor = [UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:102.0f/255.0f alpha:1.0f];;
    
    SliceItem *item2 = [[SliceItem alloc] init];
    item2.itemValue = 14.0;
    item2.itemColor =[UIColor colorWithRed:165.0f/255.0f green:225.0f/255.0f blue:241.0f/255.0f alpha:1.0f];
    
    SliceItem *item3 = [[SliceItem alloc] init];
    item3.itemValue = 10.0;
    item3.itemColor = [UIColor colorWithRed:227.0f/255.0f green:168.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
    
    
    //add objects to the sliceItems array.
    [self.circularProgressBar.sliceItems addObject:item1];
    [self.circularProgressBar.sliceItems addObject:item2];
    [self.circularProgressBar.sliceItems addObject:item3];
    
    //reload the chart
    [self.circularProgressBar reloadData];
    // Do any additional setup after loading the view.
   
}


-(void)getPortfolioDetails
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString * date = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"holdingrequest"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",cid,sessionid,date];
        [inputArray addObject:[NSString stringWithFormat:@"%@get_mf_portfoliosummary_category_report_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        NSLog(@"%@",dict);
           self.folioArray = [[NSMutableArray alloc]init];
           self.dataArray = [[NSMutableArray alloc]initWithArray:[[dict objectForKey:@"data"]objectForKey:@"Table1"]];
           self.dataUniqueArray = [[NSMutableArray alloc] init];
           NSLog(@"%@",self.dataArray);
           if(finalDict)
           {
               dispatch_async(dispatch_get_main_queue(), ^{
               self.portfolioTableView.delegate=self;
               self.portfolioTableView.dataSource=self;
                [self dataMethod];
              });
           }
           for (int i=0; i<[[[dict objectForKey:@"data"] objectForKey:@"Table1"] count]; i++) {
               [self.folioArray addObject:[[[[dict objectForKey:@"data"]objectForKey:@"Table1"]objectAtIndex:i]objectForKey:@"Folio_No"]];
           }
           NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:self.folioArray];
           NSMutableArray * folio = [[NSMutableArray alloc] initWithArray:[mySet array]];
           dispatch_async(dispatch_get_main_queue(), ^{
               self.foliosLbl.text = [NSString stringWithFormat:@"%lu Folios",(unsigned long)folio.count];
               self.fundsLbl.text = [NSString stringWithFormat:@"%lu Funds",(unsigned long)self.folioArray.count];
               NSMutableArray * currentValueArray = [[NSMutableArray alloc]init];
               NSMutableArray * investedValueArray = [[NSMutableArray alloc]init];
               NSMutableArray * gainValueArray = [[NSMutableArray alloc]init];
               
               for (int i=0; i<[[[dict objectForKey:@"data"] objectForKey:@"Table2"] count]; i++) {
                   [currentValueArray addObject:[[[[dict objectForKey:@"data"]objectForKey:@"Table2"]objectAtIndex:i]objectForKey:@"Present Value"]];
                   [investedValueArray addObject:[[[[dict objectForKey:@"data"]objectForKey:@"Table2"]objectAtIndex:i]objectForKey:@"Total Investment"]];
                   [gainValueArray addObject:[[[[dict objectForKey:@"data"]objectForKey:@"Table2"]objectAtIndex:i]objectForKey:@"Notional_Gain/Loss"]];
               }
               NSNumber *sumCurrentValue = [currentValueArray valueForKeyPath:@"@sum.floatValue"];
               NSNumber *investedCurrentValue = [investedValueArray valueForKeyPath:@"@sum.floatValue"];
               NSNumber *gainValue = [gainValueArray valueForKeyPath:@"@sum.floatValue"];
               self.currentValueLbl.text = [NSString stringWithFormat:@"₹%@",sumCurrentValue];
               self.investedLbl.text = [NSString stringWithFormat:@"₹%@",investedCurrentValue];
               self.gainLbl.text = [NSString stringWithFormat:@"₹%@",gainValue];
               float gain = [gainValue floatValue];
               float totalInvest = [investedCurrentValue floatValue];
               float gainPercent = ((gain/totalInvest)*100);
               self.gainPerLbl.text = [NSString stringWithFormat:@"%.2f",gainPercent];
               if([self.gainPerLbl.text containsString:@"-"])
               {
                   self.gainPerLbl.textColor = [UIColor colorWithRed:234.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0];
               }else
               {
                   self.gainPerLbl.textColor = [UIColor colorWithRed:23.0/255.0 green:165.0/255.0 blue:145.0/255.0 alpha:1.0];
               }
           });
    }];
    }
}

-(void)inputRequestMethod:(NSString *)inputString withCompletionHandler:(void (^)(NSDictionary *))handler
{
    
    if(finalDict.count>0)
    {
        [finalDict removeAllObjects];
    }
    if(finalDataDictionary.count>0)
    {
        [finalDataDictionary removeAllObjects];
    }
    for(int i=0;i<self.dataArray.count;i++)
    {
        if([inputString isEqualToString:@"SUB_CATEGORY"])
        {
//            NSString * string = [NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:@"SUB_CATEGORY"]];
//            NSArray * array = [string componentsSeparatedByString:@":"];
//            inputString = [NSString stringWithFormat:@"%@",[array objectAtIndex:2]];
        }
        if(finalDict)
        {
            uniqueArray=[finalDict allKeys];
        }
        if([uniqueArray containsObject:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:inputString]]]])
        {
            NSDictionary * sample1=[finalDict objectForKey:[NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:inputString]]];
            NSDictionary * sample2=[self.dataArray objectAtIndex:i];
            NSMutableDictionary *ret = [sample1 mutableCopy];
            [ret addEntriesFromDictionary:sample2];
            [finalDict setValue:ret forKeyPath:[NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:inputString]]];
        }
        else
        {
            [finalDict setObject:[self.dataArray objectAtIndex:i] forKey:[NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:inputString]]];
        }
    }
    [finalDataDictionary setObject:[finalDict allValues] forKey:@"data"];
    handler(finalDict);
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    if([guestCheck isEqualToString:@"0"])
    {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"You currently can not access this feature as a Guest user." preferredStyle:UIAlertControllerStyleActionSheet];
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Login as Client" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // OK button tapped.
            LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            login.guestLoginString = @"Guest";
            login.logoutCheckString = @"logout";
            [self presentViewController:login animated:YES completion:nil];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self.navigationController popViewControllerAnimated:YES];
//            [self dismissViewControllerAnimated:YES completion:^{
//            }];
        }]];
       
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
    }else
    {
         [self getPortfolioDetails];
    }
}

-(void)onSegmentTap
{
    [self dataMethod];
}
-(void)dataMethod
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        NSString * string = @"AMC Name";
        [self inputRequestMethod:string withCompletionHandler:^(NSDictionary *dict) {
            if(finalDict)
            {
                [self.portfolioTableView reloadData];
            }
        }];
    }else if (segmentedControl.selectedSegmentIndex ==1)
    {
        NSString * string = @"SUB_CATEGORY";
        [self inputRequestMethod:string withCompletionHandler:^(NSDictionary *dict) {
            if(finalDict)
            {
                [self.portfolioTableView reloadData];
            }
        }];
    }else if (segmentedControl.selectedSegmentIndex==2)
    {
        NSString * string = @"ASSET_TYPE";
        [self inputRequestMethod:string withCompletionHandler:^(NSDictionary *dict) {
            if(finalDict)
            {
                [self.portfolioTableView reloadData];
            }
        }];
    }else if (segmentedControl.selectedSegmentIndex==3)
    {
        NSString * string = @"Scheme Name";
        [self inputRequestMethod:string withCompletionHandler:^(NSDictionary *dict) {
            if(finalDict)
            {
                [self.portfolioTableView reloadData];
            }
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[finalDataDictionary objectForKey:@"data"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        PortfolioTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PortfolioTableViewCell" forIndexPath:indexPath];
    NSString * fundName;
        if(segmentedControl.selectedSegmentIndex==0)
        {
            fundName = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"AMC Name"]];
            
        }else if (segmentedControl.selectedSegmentIndex==1)
        {
            NSString * string1 = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"SUB_CATEGORY"]];
            NSArray * array1 = [string1 componentsSeparatedByString:@":"];
            fundName = [NSString stringWithFormat:@"%@",[array1 objectAtIndex:2]];
        }else if (segmentedControl.selectedSegmentIndex==2)
        {
            fundName = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"ASSET_TYPE"]];
        }else if (segmentedControl.selectedSegmentIndex==3)
        {
            fundName = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"Scheme Name"]];
        }
    if([fundName isEqual:[NSNull null]])
    {
        cell.fundNameLabel.text = @"N/A";
    }else
    {
        cell.fundNameLabel.text = fundName;
    }
    NSString * currentValue = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"Present Value"]];
    if([currentValue isEqual:[NSNull null]])
    {
        cell.currentValueLabel.text=@"N/A";
    }else
    {
        cell.currentValueLabel.text = currentValue;
    }
    NSString * investedAmount = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"Total Investment"]];
    if([investedAmount isEqual:[NSNull null]])
    {
        cell.investedLabel.text = @"N/A";
    }else
    {
        cell.investedLabel.text = investedAmount;
    }
    NSString * gain = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"Notional_Gain/Loss"]];
    if([gain isEqual:[NSNull null]])
    {
        cell.gainLabel.text = @"N/A";
    }else
    {
        cell.gainLabel.text = gain;
    }
    
    float gainFloat = [[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"Notional_Gain/Loss"] floatValue];
    float totalInvest =[[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"Total Investment"] floatValue];
    float gainPercent = ((gainFloat/totalInvest)*100);
    cell.gainPercentLabel.text = [NSString stringWithFormat:@"%.2f",gainPercent];
    if([cell.gainPercentLabel.text containsString:@"-"])
    {
        cell.gainPercentLabel.textColor = [UIColor colorWithRed:234.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0];
    }else
    {
        cell.gainPercentLabel.textColor = [UIColor colorWithRed:23.0/255.0 green:165.0/255.0 blue:145.0/255.0 alpha:1.0];
    }
        return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(delegate.portfolioDataArray.count>0)
    {
        [delegate.portfolioDataArray removeAllObjects];
    }
    PortfolioDetailViewController * portfolio = [self.storyboard instantiateViewControllerWithIdentifier:@"PortfolioDetailViewController"];
    if(segmentedControl.selectedSegmentIndex==0)
    {
    NSString * string1 = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"AMC Name"]];
    NSMutableArray *keyObjects = [[NSMutableArray alloc]init];
    for (int i =0; i < [self.dataArray count]; i++) {
        if([string1 isEqualToString:[NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:@"AMC Name"]]])
        {
            [keyObjects addObject:[self.dataArray objectAtIndex:i]];
        }
    }
    [delegate.portfolioDataArray addObjectsFromArray:keyObjects];
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        NSString * string1 = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"Scheme Name"]];
        NSMutableArray *keyObjects = [[NSMutableArray alloc]init];
        for (int i =0; i < [self.dataArray count]; i++) {
            if([string1 isEqualToString:[NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:@"Scheme Name"]]])
            {
                [keyObjects addObject:[self.dataArray objectAtIndex:i]];
            }
        }
        [delegate.portfolioDataArray addObjectsFromArray:keyObjects];
    }else if (segmentedControl.selectedSegmentIndex==2)
    {
        NSString * string1 = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"ASSET_TYPE"]];
        NSMutableArray *keyObjects = [[NSMutableArray alloc]init];
        for (int i =0; i < [self.dataArray count]; i++) {
            if([string1 isEqualToString:[NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:@"ASSET_TYPE"]]])
            {
                [keyObjects addObject:[self.dataArray objectAtIndex:i]];
            }
        }
        [delegate.portfolioDataArray addObjectsFromArray:keyObjects];
    }else if (segmentedControl.selectedSegmentIndex==3)
    {
        NSString * string1 = [NSString stringWithFormat:@"%@",[[[finalDataDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"Scheme Name"]];
        NSMutableArray *keyObjects = [[NSMutableArray alloc]init];
        for (int i =0; i < [self.dataArray count]; i++) {
            if([string1 isEqualToString:[NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:i]objectForKey:@"Scheme Name"]]])
            {
                [keyObjects addObject:[self.dataArray objectAtIndex:i]];
            }
        }
        [delegate.portfolioDataArray addObjectsFromArray:keyObjects];
    }
    [self.navigationController pushViewController:portfolio animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

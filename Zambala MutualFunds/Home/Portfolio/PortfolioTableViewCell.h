//
//  PortfolioTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 01/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *investedLabel;
@property (weak, nonatomic) IBOutlet UILabel *gainLabel;
@property (weak, nonatomic) IBOutlet UILabel *gainPercentLabel;

@end

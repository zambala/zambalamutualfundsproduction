//
//  PortfolioDetailTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 01/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "PortfolioDetailTableViewCell.h"

@implementation PortfolioDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.backView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.backView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.backView.layer.shadowOpacity = 0.6f;
    self.backView.layer.shadowRadius = 6.5f;
    self.backView.layer.cornerRadius = 5.0f;
    self.backView.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  TrasactTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 02/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrasactTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *transactButton;
@property (weak, nonatomic) IBOutlet UILabel *transactLabel;

@end

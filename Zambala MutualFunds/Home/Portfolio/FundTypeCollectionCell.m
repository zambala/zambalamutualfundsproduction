//
//  FundTypeCollectionCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 28/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "FundTypeCollectionCell.h"

@implementation FundTypeCollectionCell
- (void)awakeFromNib
{
      [super awakeFromNib];
//self.identificationView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//self.identificationView.layer.shadowOffset = CGSizeMake(0.1, 2.5f);
//self.identificationView.layer.shadowOpacity = 3.0f;
//self.identificationView.layer.shadowRadius = 3.0f;
self.identificationView.layer.cornerRadius=self.identificationView.frame.size.width/2;
self.identificationView.layer.masksToBounds = NO;
}
@end

//
//  SwitchViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 05/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *fundCategoryButton;
@property (weak, nonatomic) IBOutlet UIButton *schemeButton;
@property (weak, nonatomic) IBOutlet UIButton *unitsButton;
@property (weak, nonatomic) IBOutlet UIButton *amountButton;
@property (weak, nonatomic) IBOutlet UITextField *monthsTF;
@property (weak, nonatomic) IBOutlet UILabel *unitsLabel;
@property (weak, nonatomic) IBOutlet UISlider *unitsSlider;
@property (weak, nonatomic) IBOutlet UILabel *unitsLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitsRightLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *folioNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *topUnitsLabel;
@property NSMutableDictionary * responseDataDictionary;
@property NSMutableArray * swittchArray;
@property NSMutableArray * responseDataArray;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UITableView *popUpTableView;
@property (weak, nonatomic) IBOutlet UIView *acceptPopUpView;
@property (weak, nonatomic) IBOutlet UIButton *acceptAndInvestButton;
@property (weak, nonatomic) IBOutlet UIView *insidePopUpView;
@property (weak, nonatomic) IBOutlet UILabel *popUpCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *popUpSchemeLabel;
@property (weak, nonatomic) IBOutlet UILabel *popUpValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *popUpTypeLabel;
@property (weak, nonatomic) IBOutlet UIButton *popUpCancelButton;

@property NSMutableDictionary * executeDicitionary;


@end

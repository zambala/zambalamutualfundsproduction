//
//  STPViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 05/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STPViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *fundCategoryButton;
@property (weak, nonatomic) IBOutlet UIButton *schemeButton;
@property (weak, nonatomic) IBOutlet UITextField *installmentsTF;
@property (weak, nonatomic) IBOutlet UISlider *installmentsSlider;
@property (weak, nonatomic) IBOutlet UITextField *amountTF;
@property (weak, nonatomic) IBOutlet UISlider *amountSlider;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *folioNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitsLabel;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UITableView *STPTableView;

@property NSMutableDictionary * responseDataDictionary;
@property NSMutableArray * swittchArray;
@property NSMutableArray * responseDataArray;
@property (weak, nonatomic) IBOutlet UIButton *dailyButton;
@property (weak, nonatomic) IBOutlet UIButton *weeklyButton;
@property (weak, nonatomic) IBOutlet UIButton *monthlyButton;
@property (weak, nonatomic) IBOutlet UIButton *quarterlyButton;
@property (weak, nonatomic) IBOutlet UISlider *stpDaySlider;
@property (weak, nonatomic) IBOutlet UITextField *stpDayTF;
@property (weak, nonatomic) IBOutlet UILabel *monthsLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthsRightLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountRightLabel;
@property (weak, nonatomic) IBOutlet UILabel *stpDayLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *stpDayRightLabel;
@property (weak, nonatomic) IBOutlet UIView *acceptPopUpView;
@property (weak, nonatomic) IBOutlet UIButton *acceptAndInvestButton;
@property (weak, nonatomic) IBOutlet UIView *insidePopUpView;
@property (weak, nonatomic) IBOutlet UILabel *popUpCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *popUpschemeLabel;
@property (weak, nonatomic) IBOutlet UILabel *popUpFrequencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *popUpInstallmentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *popUpAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *popUpSTPDayLabel;
@property (weak, nonatomic) IBOutlet UIButton *popUpCancelButton;
@property (weak, nonatomic) IBOutlet UIPickerView *somePickerView;
@property (weak, nonatomic) IBOutlet UIButton *selectFrequencyButton;
@property (weak, nonatomic) IBOutlet UIButton *stpDayButton;
@property NSMutableArray * frequencyArray;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UIView *bottomMainView;

@end

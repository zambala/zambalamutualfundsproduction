//
//  SmarMoneyViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 01/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "SmarMoneyViewController.h"
#import "EquityViewController.h"
#import "HomeRecomendedViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface SmarMoneyViewController ()


@end

@implementation SmarMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.startNowButton.layer.cornerRadius=20.0f;
    self.firstPopUpView.layer.cornerRadius = 10.0f;
    
    self.lessMonthsButton.layer.cornerRadius=20.0f;
    self.moreMonthsButton.layer.cornerRadius=20.0f;
    self.lessMonthsButton.layer.borderWidth = 2.0f;
    self.lessMonthsButton.layer.borderColor = [[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0] CGColor];
    self.moreMonthsButton.layer.borderWidth = 2.0f;
    self.moreMonthsButton.layer.borderColor = [[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0] CGColor];
    
    self.blurView.hidden = YES;
    self.blurView.tag = 99;
    
    self.popUpView.layer.cornerRadius=10.0f;
    [self.startNowButton addTarget:self action:@selector(onStartNowButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.lessMonthsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.moreMonthsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.blurView.hidden=YES;
    }
}

-(void)onButtonTap:(UIButton *)sender
{
    if(sender == self.lessMonthsButton)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f]];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    HomeRecomendedViewController * equity = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeRecomendedViewController"];
    equity.typeOfSmartMoney = @"less";
    equity.smartMoneyString = @"YES";
    [self.navigationController pushViewController:equity animated:YES];
    }else if (sender == self.moreMonthsButton)
    {
        [sender setBackgroundColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f]];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        HomeRecomendedViewController * equity = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeRecomendedViewController"];
        equity.typeOfSmartMoney = @"more";
        equity.smartMoneyString = @"YES";
        [self.navigationController pushViewController:equity animated:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.lessMonthsButton setBackgroundColor:[UIColor clearColor]];
    [self.moreMonthsButton setBackgroundColor:[UIColor clearColor]];
    [self.lessMonthsButton setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
    [self.moreMonthsButton setTitleColor:[UIColor colorWithRed:2/255 green:30/255 blue:41/255 alpha:1.0f] forState:UIControlStateNormal];
}

-(void)onStartNowButtonTap
{
//    CATransition *transition = [[CATransition alloc] init];
//    transition.duration = 0.5;
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromLeft;
//    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    [self.view.window.layer addAnimation:transition forKey:kCATransition];
//    self.blurView.hidden=NO;
//
//    CATransition *transition1 = [[CATransition alloc] init];
//    transition1.duration = 0.5;
//    transition1.type = kCATransitionPush;
//    transition1.subtype = kCATransitionFromRight;
//    [transition1 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    [self.view.window.layer addAnimation:transition forKey:kCATransition];
//    self.popUpView.hidden=YES;
    [UIView transitionWithView:self.popUpView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.blurView.hidden = NO;
                    }
                    completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

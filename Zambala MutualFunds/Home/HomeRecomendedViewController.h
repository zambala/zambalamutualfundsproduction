//
//  HomeRecomendedViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 08/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface HomeRecomendedViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *recomendedTableView;
@property NSMutableDictionary * responseDataDictionary;

@property NSString * typeOfSmartMoney;
@property NSString * smartMoneyString;
@property NSMutableArray * smartMoneyDataArray;
@property (weak, nonatomic) IBOutlet UIButton *exploreButton;
@property (weak, nonatomic) IBOutlet UIView *mainTopView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainTopViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *exploreTextLabel;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *backMainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *topImageView;

@property Reachability * reach;


@end

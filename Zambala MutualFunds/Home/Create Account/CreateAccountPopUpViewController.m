//
//  CreateAccountPopUpViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 23/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "CreateAccountPopUpViewController.h"
#import "EkycOneViewController.h"

@interface CreateAccountPopUpViewController ()

@end

@implementation CreateAccountPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cancelButton.layer.cornerRadius=15.0f;
    self.proceedButton.layer.cornerRadius=15.0f;
    
    [self.cancelButton addTarget:self action:@selector(onCancelTap) forControlEvents:UIControlEventTouchUpInside];
    [self.proceedButton addTarget:self action:@selector(onProceedButtopTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view from its nib.
}

-(void)onCancelTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)onProceedButtopTap
{
    EkycOneViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycOneViewController"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

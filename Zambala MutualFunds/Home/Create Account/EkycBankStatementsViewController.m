//
//  EkycBankStatementsViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 30/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycBankStatementsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "EkycVideoViewController.h"

@interface EkycBankStatementsViewController ()
{
    UIImagePickerController * pickerController;
}

@end

@implementation EkycBankStatementsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 3.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.masksToBounds = NO;
    
    self.previousButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.previousButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.previousButton.layer.shadowOpacity = 3.0f;
    self.previousButton.layer.shadowRadius = 3.0f;
    self.previousButton.layer.cornerRadius=20.0f;
    self.previousButton.layer.masksToBounds = NO;
    
    
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder.strokeColor = [UIColor blackColor].CGColor;
    yourViewBorder.fillColor = nil;
    yourViewBorder.lineDashPattern = @[@2, @2];
    yourViewBorder.frame = self.buttonView.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.buttonView.bounds].CGPath;
    [self.buttonView.layer addSublayer:yourViewBorder];
    
    pickerController=[[UIImagePickerController alloc]init];
    pickerController.delegate=self;
    
    self.clearButton.hidden=YES;
    
    [self.cameraButton addTarget:self action:@selector(onCameraButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.clearButton addTarget:self action:@selector(onClearButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.previousButton addTarget:self action:@selector(onPreviousButtonTap) forControlEvents:UIControlEventTouchUpInside];
}

-(void)onPreviousButtonTap
{
    
}

-(void)onNextButtonTap
{
    EkycVideoViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycVideoViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
}

-(void)onClearButtonTap
{
    [self.cameraButton setImage:nil forState:UIControlStateNormal];
    [self.cameraButton setTitle:@"Click a picture" forState:UIControlStateNormal];
    self.clearButton.hidden=YES;
}

-(void)onCameraButtonTap
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Choose one" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [self presentViewController:alert animated:YES completion:nil];
    self.camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController * cameraPicker = [[UIImagePickerController alloc]init];
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        cameraPicker.delegate=self;
        [self presentViewController:cameraPicker animated:YES completion:nil];
        
        
        
        cameraPicker.mediaTypes = [NSArray arrayWithObjects:
                                   (NSString *) kUTTypeImage,
                                   nil];
        cameraPicker.allowsEditing = NO;
        // [self presentViewController:cameraPicker animated:YES completion:nil];
        
        
        
    }];
    
    self.gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        pickerController.delegate=self;
        [self presentViewController:pickerController animated:YES completion:nil];
        
        
    }];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:self.camera];
    [alert addAction:self.gallery];
    [alert addAction:cancel];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    
    NSData *png_Data = UIImageJPEGRepresentation(image, 0);
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *str_localFilePath = [documentsDirectory stringByAppendingPathComponent:@"One.png"];
    [png_Data writeToFile:str_localFilePath atomically:YES];
    
    [self.cameraButton setImage:image forState:UIControlStateNormal];
    self.clearButton.hidden=NO;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    // NSData *dataForImage = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage"], 1.0);
    // [self.cameraButton setImage:[UIImage imageWithData:dataForImage] forState:UIControlStateNormal];
    
}
- (NSString *)documentsPathForFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Save failed" message:@"Failed to save image" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

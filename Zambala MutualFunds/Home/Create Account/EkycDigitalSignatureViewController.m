//
//  EkycDigitalSignatureViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 30/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycDigitalSignatureViewController.h"
#import "EkycSignatureViewController.h"
#import "EkycOneViewController.h"

@interface EkycDigitalSignatureViewController ()

@end

@implementation EkycDigitalSignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 3.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.masksToBounds = NO;
    
    self.previousButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.previousButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.previousButton.layer.shadowOpacity = 3.0f;
    self.previousButton.layer.shadowRadius = 3.0f;
    self.previousButton.layer.cornerRadius=20.0f;
    self.previousButton.layer.masksToBounds = NO;
    
    self.laterButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.laterButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.laterButton.layer.shadowOpacity = 3.0f;
    self.laterButton.layer.shadowRadius = 3.0f;
    self.laterButton.layer.cornerRadius=20.0f;
    self.laterButton.layer.masksToBounds = NO;
    
    self.proceedButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.proceedButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.proceedButton.layer.shadowOpacity = 3.0f;
    self.proceedButton.layer.shadowRadius = 3.0f;
    self.proceedButton.layer.cornerRadius=20.0f;
    self.proceedButton.layer.masksToBounds = NO;
    
    
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder.strokeColor = [UIColor blackColor].CGColor;
    yourViewBorder.fillColor = nil;
    yourViewBorder.lineDashPattern = @[@2, @2];
    yourViewBorder.frame = self.buttonView.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.buttonView.bounds].CGPath;
    [self.buttonView.layer addSublayer:yourViewBorder];

    
    self.clearButton.hidden=YES;
    self.successCloseButton.hidden=YES;
    self.successView.hidden=YES;
    self.successViewHeightConstraint.constant=0;
    
    
    [self.signatureButton addTarget:self action:@selector(onSignatureButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.clearButton addTarget:self action:@selector(onClearButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.previousButton addTarget:self action:@selector(onPreviousButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.laterButton addTarget:self action:@selector(onLaterButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.proceedButton addTarget:self action:@selector(onProceedButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.successCloseButton addTarget:self action:@selector(onSuccessCloseButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onPreviousButtonTap
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)onNextButtonTap
{
    self.successCloseButton.hidden=NO;
    self.successView.hidden=NO;
    self.successViewHeightConstraint.constant=362;
}

-(void)onSuccessCloseButtonTap
{
    self.successCloseButton.hidden=YES;
    self.successView.hidden=YES;
    self.successViewHeightConstraint.constant=0;
}

-(void)onLaterButtonTap
{
     [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)onProceedButtonTap
{
    EkycOneViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycOneViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
}

-(void)onSignatureButtonTap
{
    EkycSignatureViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycSignatureViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
}

-(void)onClearButtonTap
{
    [self.signatureButton setImage:nil forState:UIControlStateNormal];
    [self.signatureButton setTitle:@"Click here to sign" forState:UIControlStateNormal];
    self.clearButton.hidden=YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

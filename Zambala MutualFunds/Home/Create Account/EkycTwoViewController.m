//
//  EkycTwoViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 24/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycTwoViewController.h"
#import "EkycThreeViewController.h"

@interface EkycTwoViewController ()
{
    UIVisualEffect *blurEffect;
    UIVisualEffectView *visualEffectView;
}

@end

@implementation EkycTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.otpView.hidden=YES;
    
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    self.generateOTPButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.generateOTPButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.generateOTPButton.layer.shadowOpacity = 3.0f;
    self.generateOTPButton.layer.shadowRadius = 3.0f;
    self.generateOTPButton.layer.cornerRadius=20.0f;
    self.generateOTPButton.layer.masksToBounds = NO;
    
    self.submitButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.submitButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.submitButton.layer.shadowOpacity = 3.0f;
    self.submitButton.layer.shadowRadius = 3.0f;
    self.submitButton.layer.cornerRadius=20.0f;
    self.submitButton.layer.masksToBounds = NO;
    
    self.cancelButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.cancelButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.cancelButton.layer.shadowOpacity = 3.0f;
    self.cancelButton.layer.shadowRadius = 3.0f;
    self.cancelButton.layer.cornerRadius=20.0f;
    self.cancelButton.layer.masksToBounds = NO;
    
    [self.generateOTPButton addTarget:self action:@selector(onOTPButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.cancelButton addTarget:self action:@selector(onCancelButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.submitButton addTarget:self action:@selector(onSubmitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onOTPButtonTap
{
    self.otpView.hidden=NO;
    visualEffectView.hidden=NO;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    visualEffectView.frame = self.view.bounds;
    visualEffectView.backgroundColor= [UIColor blackColor];
    visualEffectView.alpha=0.5;
    [self.backView addSubview:visualEffectView];
}

-(void)onCancelButtonTap
{
    self.otpView.hidden=YES;
    visualEffectView.hidden=YES;
}

-(void)onSubmitButtonTap
{
    self.otpView.hidden=YES;
    visualEffectView.hidden=YES;
    EkycThreeViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycThreeViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  EkycTwoViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 24/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EkycTwoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *generateOTPButton;
@property (weak, nonatomic) IBOutlet UIView *otpView;
@property (weak, nonatomic) IBOutlet UITextField *tfOne;
@property (weak, nonatomic) IBOutlet UITextField *tfTwo;
@property (weak, nonatomic) IBOutlet UITextField *tfThree;
@property (weak, nonatomic) IBOutlet UITextField *tfFour;
@property (weak, nonatomic) IBOutlet UIButton *tryAgainButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end

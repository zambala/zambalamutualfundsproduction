//
//  EkycFourViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 29/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EkycFourViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *individualButton;
@property (weak, nonatomic) IBOutlet UIButton *nonIndividualButton;
@property (weak, nonatomic) IBOutlet UIButton *incomeButton;
@property (weak, nonatomic) IBOutlet UIPickerView *incomePickerView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *incomePickerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *occupationButton;

@end

//
//  EkycSixViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 29/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EkycSixViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *relationShipButton;
@property (weak, nonatomic) IBOutlet UIButton *dobButton;
@property (weak, nonatomic) IBOutlet UIView *nomineeView;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIPickerView *relationShipPickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *dobPickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dobPickerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *relationshipPickerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UISwitch *nomineeSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nomineeViewheightConstraint;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewLayoutConstraint;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nomineeNameViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *nomineeNameView;

@property NSString * dobString;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property NSString * profileCheck;

@end

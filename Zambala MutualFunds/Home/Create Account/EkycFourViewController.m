//
//  EkycFourViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 29/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycFourViewController.h"
#import "EkycFiveViewController.h"

@interface EkycFourViewController ()
{
    UIToolbar*toolbar;
}

@end

@implementation EkycFourViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 3.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.masksToBounds = NO;
    
    
    self.previousButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.previousButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.previousButton.layer.shadowOpacity = 3.0f;
    self.previousButton.layer.shadowRadius = 3.0f;
    self.previousButton.layer.cornerRadius=20.0f;
    self.previousButton.layer.masksToBounds = NO;
    
    self.incomePickerView.hidden=YES;
    self.incomePickerViewHeightConstraint.constant=0;
    
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.previousButton addTarget:self action:@selector(onPreviousButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.incomeButton addTarget:self action:@selector(onIncomeButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.occupationButton addTarget:self action:@selector(onOccupationButtonTap) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onNextButtonTap
{
    EkycFiveViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycFiveViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
}

-(void)onPreviousButtonTap
{
    
}

-(void)onDoneButtonTap
{
    self.incomePickerView.hidden=YES;
    self.incomePickerViewHeightConstraint.constant=0;
    [toolbar removeFromSuperview];
}

-(void)onOccupationButtonTap
{
    self.incomePickerView.hidden=NO;
    self.incomePickerViewHeightConstraint.constant=162;
    self.incomePickerView.backgroundColor=[UIColor darkGrayColor];
    self.incomePickerView.delegate=self;
    self.incomePickerView.dataSource=self;
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDoneButtonTap)];
    
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
}

-(void)onIncomeButtonTap
{
    self.incomePickerView.hidden=NO;
    self.incomePickerViewHeightConstraint.constant=162;
    self.incomePickerView.backgroundColor=[UIColor darkGrayColor];
    self.incomePickerView.delegate=self;
    self.incomePickerView.dataSource=self;
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDoneButtonTap)];
    
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"Choice-%ld",(long)row];//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

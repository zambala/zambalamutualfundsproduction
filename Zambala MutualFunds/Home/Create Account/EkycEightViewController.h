//
//  EkycEightViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 30/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EkycEightViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *nextbutton;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property UIAlertAction * camera;
@property UIAlertAction * gallery;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@end

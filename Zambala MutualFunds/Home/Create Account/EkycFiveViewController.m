//
//  EkycFiveViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 29/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycFiveViewController.h"
#import "EkycSixViewController.h"
#import "ProfileViewController.h"

@interface EkycFiveViewController ()
{
    UIToolbar*toolbar;
}

@end

@implementation EkycFiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    if([self.profileCheck isEqualToString:@"profile"])
    {
        self.topView.hidden=NO;
        self.topViewLayoutConstraint.constant=60;
        [self.nextButton setTitle:@"UPDATE" forState:UIControlStateNormal];
    }else
    {
        [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
        self.topView.hidden=YES;
        self.topViewLayoutConstraint.constant=0;
    }
    
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 3.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.masksToBounds = NO;

    self.previousButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.previousButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.previousButton.layer.shadowOpacity = 3.0f;
    self.previousButton.layer.shadowRadius = 3.0f;
    self.previousButton.layer.cornerRadius=20.0f;
    self.previousButton.layer.masksToBounds = NO;
    
    self.accountTypePickerView.hidden=YES;
    self.accountTypePickerViewHeightConstant.constant=0;
    
    self.accountTypeButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.previousButton addTarget:self action:@selector(onPreviousButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.accountTypeButton addTarget:self action:@selector(onAccountTypeButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)onNextButtonTap
{
    if([self.nextButton.titleLabel.text isEqualToString:@"UPDATE"])
    {
        ProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        [self presentViewController:profile animated:YES completion:nil];
    }else
    {
    EkycSixViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycSixViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
    }
}

-(void)onPreviousButtonTap
{
    
}


-(void)onDoneButtonTap
{
    self.accountTypePickerView.hidden=YES;
    self.accountTypePickerViewHeightConstant.constant=0;
    [toolbar removeFromSuperview];
}

-(void)onAccountTypeButtonTap
{
    self.accountTypePickerView.hidden=NO;
    self.accountTypePickerViewHeightConstant.constant=162;
    self.accountTypePickerView.backgroundColor=[UIColor darkGrayColor];
    self.accountTypePickerView.delegate=self;
    self.accountTypePickerView.dataSource=self;
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDoneButtonTap)];
    
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"Choice-%ld",(long)row];//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  EkycThreeViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 29/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycThreeViewController.h"
#import "EkycFourViewController.h"
#import "ProfileViewController.h"

@interface EkycThreeViewController ()

@end

@implementation EkycThreeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if([self.profileCheck isEqualToString:@"profile"])
    {
        self.topView.hidden=NO;
        self.topViewLayoutConstraint.constant=60;
        [self.nextButton setTitle:@"UPDATE" forState:UIControlStateNormal];
    }else
    {
        [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
        self.topView.hidden=YES;
        self.topViewLayoutConstraint.constant=0;
    }
    
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 3.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.masksToBounds = NO;
    
    
    self.previousButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.previousButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.previousButton.layer.shadowOpacity = 3.0f;
    self.previousButton.layer.shadowRadius = 3.0f;
    self.previousButton.layer.cornerRadius=20.0f;
    self.previousButton.layer.masksToBounds = NO;
    
    self.anotherAddressView.hidden=YES;
    self.anotherAddressViewHeightConstraint.constant=0;
    self.identityImageView.image = [self.identityImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
   // [self.identityImageView setTintColor:[UIColor yellowColor]];
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.previousButton addTarget:self action:@selector(onPreviousButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.addressSwitch addTarget:self action:@selector(onSwitchChange) forControlEvents:UIControlEventValueChanged];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onBackButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)onNextButtonTap
{
    if([self.nextButton.titleLabel.text isEqualToString:@"UPDATE"])
    {
        ProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        [self presentViewController:profile animated:YES completion:nil];
    }else
    {
    EkycFourViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycFourViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
    }
}
-(void)onPreviousButtonTap
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)onSwitchChange
{
    if([self.addressSwitch isOn])
    {
        self.anotherAddressView.hidden=NO;
        self.anotherAddressViewHeightConstraint.constant=69;
    }else
    {
        self.anotherAddressView.hidden=YES;
        self.anotherAddressViewHeightConstraint.constant=0;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

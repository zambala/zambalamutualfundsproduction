//
//  EkycVideoViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 30/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
@import AVKit;
@import AVFoundation;

@interface EkycVideoViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property UIAlertAction * camera;
@property UIAlertAction * gallery;
@property (strong, nonatomic) NSURL *videoURL;
@property AVPlayer * videoPlayer;
@end

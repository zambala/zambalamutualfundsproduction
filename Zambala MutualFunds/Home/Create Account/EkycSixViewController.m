//
//  EkycSixViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 29/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycSixViewController.h"
#import "EkycSevenViewController.h"
#import "ProfileViewController.h"

@interface EkycSixViewController ()
{
    UIToolbar*toolbar;
    NSDateFormatter *dateFormatter;
    int age;
}

@end

@implementation EkycSixViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    if([self.profileCheck isEqualToString:@"profile"])
    {
        self.topView.hidden=NO;
        self.topViewLayoutConstraint.constant=60;
        self.nomineeSwitch.on=YES;
        [self onNomineeSwitchTap];
        [self.nextButton setTitle:@"UPDATE" forState:UIControlStateNormal];
    }else
    {
        [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
        self.topView.hidden=YES;
        self.nomineeSwitch.on=NO;
        self.topViewLayoutConstraint.constant=0;
        [self onNomineeSwitchTap];
    }
    
    self.nomineeNameView.hidden=YES;
    self.nomineeNameViewHeightConstraint.constant=0;
    
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 3.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.masksToBounds = NO;
    
    self.previousButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.previousButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.previousButton.layer.shadowOpacity = 3.0f;
    self.previousButton.layer.shadowRadius = 3.0f;
    self.previousButton.layer.cornerRadius=20.0f;
    self.previousButton.layer.masksToBounds = NO;
    
    self.relationShipButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.dobButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    
    self.relationShipPickerView.hidden=YES;
    self.relationshipPickerViewHeightConstraint.constant=0;
    self.dobPickerView.hidden=YES;
    self.dobPickerViewHeightConstraint.constant=0;
    dateFormatter=[[NSDateFormatter alloc]init];
    
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.previousButton addTarget:self action:@selector(onPreviousButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.relationShipButton addTarget:self action:@selector(onRelationshipButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.dobButton addTarget:self action:@selector(onDOBButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.nomineeSwitch addTarget:self action:@selector(onNomineeSwitchTap) forControlEvents:UIControlEventValueChanged];
    [self.backButton addTarget:self action:@selector(onBackbuttonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onNextButtonTap
{
    if([self.nextButton.titleLabel.text isEqualToString:@"UPDATE"])
    {
        ProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        [self presentViewController:profile animated:YES completion:nil];
    }else
    {
    EkycSevenViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycSevenViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
    }
}

-(void)onPreviousButtonTap
{
    
}

-(void)onBackbuttonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)onNomineeSwitchTap
{
    if([self.nomineeSwitch isOn])
    {
        self.nomineeView.hidden=NO;
        self.nomineeViewheightConstraint.constant=350;
    }else
    {
        self.nomineeView.hidden=YES;
        self.nomineeViewheightConstraint.constant=350;
    }
}

-(void)onDOBButtonTap
{
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    self.dobPickerView.hidden=NO;
    self.dobPickerViewHeightConstraint.constant=162;
    self.relationShipPickerView.hidden=YES;
    self.relationshipPickerViewHeightConstraint.constant=0;
    self.dobPickerView.backgroundColor=[UIColor darkGrayColor];
     [self.dobPickerView addTarget:self action:@selector(LabelTitle) forControlEvents:UIControlEventAllEvents];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDOBDoneButtonTap)];
    
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
}

-(void)LabelTitle
{
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    NSString * dobString = [NSString stringWithFormat:@"%@",[dateFormatter  stringFromDate:self.dobPickerView.date]];
    [self.dobButton setTitle:dobString forState:UIControlStateNormal];
}

-(void)onDOBDoneButtonTap
{
     [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
   
    self.dobPickerView.hidden=YES;
    self.dobPickerViewHeightConstraint.constant=0;
    NSDate * date = [dateFormatter dateFromString:self.dobString];
    [self ageFromBirthday:date];
    
    [self ageFromBirthDate:self.dobString];
    
    
    age = [[self ageFromBirthDate:self.dobString]intValue];
    if(age>18)
    {
        self.nomineeNameView.hidden=YES;
        self.nomineeNameViewHeightConstraint.constant=0;
    }else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Nominee is minor. Please fill the minor application form." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.nomineeNameView.hidden=NO;
            self.nomineeNameViewHeightConstraint.constant=92;
        }];
        
        [alert addAction:okAction];
    }
    
    [toolbar removeFromSuperview];
}

- (NSString *) ageFromBirthDate:(NSString *)birthDate{
    dateFormatter= [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    NSDate *myDate = [dateFormatter dateFromString:birthDate];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%ld ans", (long)[[[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:myDate toDate:[NSDate date] options:0] year]]);
    
    return [NSString stringWithFormat:@"%ld ans", (long)[[[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:myDate toDate:[NSDate date] options:0] year]];
}


- (NSInteger)ageFromBirthday:(NSDate *)birthdate {
    NSDate *today = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthdate
                                       toDate:today
                                       options:0];
    
    NSLog(@"%ld",(long)ageComponents.year);
    return ageComponents.year;
}

-(void)onDoneButtonTap
{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    self.relationShipPickerView.hidden=YES;
    self.relationshipPickerViewHeightConstraint.constant=0;
    [toolbar removeFromSuperview];
}

-(void)onRelationshipButtonTap
{
    self.dobPickerView.hidden=YES;
    self.dobPickerViewHeightConstraint.constant=0;
    self.relationShipPickerView.hidden=NO;
    self.relationshipPickerViewHeightConstraint.constant=162;
    self.relationShipPickerView.backgroundColor=[UIColor darkGrayColor];
    self.relationShipPickerView.delegate=self;
    self.relationShipPickerView.dataSource=self;
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDoneButtonTap)];
    
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"Choice-%ld",(long)row];//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  EkycSevenViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 29/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycSevenViewController.h"
#import "EkycEightViewController.h"

@interface EkycSevenViewController ()
{
    UIToolbar * toolbar;
}

@end

@implementation EkycSevenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 3.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.masksToBounds = NO;
    
    self.previousButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.previousButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.previousButton.layer.shadowOpacity = 3.0f;
    self.previousButton.layer.shadowRadius = 3.0f;
    self.previousButton.layer.cornerRadius=20.0f;
    self.previousButton.layer.masksToBounds = NO;
    
    self.countryButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.selectCountryButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.idProofButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    
    self.pickerView.hidden=YES;
    self.pickerViewHeightConstraint.constant=162;
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.previousButton addTarget:self action:@selector(onPreviousButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.countryButton addTarget:self action:@selector(onButtonTap) forControlEvents:UIControlEventTouchUpInside];
     [self.selectCountryButton addTarget:self action:@selector(onButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.idProofButton addTarget:self action:@selector(onButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.yesButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    [self.yesButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    
    [self.noButton setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    [self.noButton setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    
    [self onYesButtonTap];
    
    
    [self.yesButton addTarget:self action:@selector(onYesButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.noButton addTarget:self action:@selector(onNoButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)onNextButtonTap
{
    EkycEightViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycEightViewController"];
    [self.navigationController pushViewController:kyc animated:YES];
}

-(void)onPreviousButtonTap
{
    
}

-(void)onYesButtonTap
{
    self.yesButton.selected=YES;
    self.noButton.selected=NO;
    self.yesView.hidden=NO;
    self.yesViewHeightConstraint.constant=210;
}

-(void)onNoButtonTap
{
    self.yesButton.selected=NO;
    self.noButton.selected=YES;
    self.yesView.hidden=YES;
    self.yesViewHeightConstraint.constant=0;
}

-(void)onDoneButtonTap
{
    [self.scrollView setContentOffset:CGPointMake(0, 200) animated:YES];
    self.pickerView.hidden=YES;
    self.pickerViewHeightConstraint.constant=0;
    [toolbar removeFromSuperview];
}

-(void)onButtonTap
{
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    self.pickerView.hidden=NO;
    self.pickerViewHeightConstraint.constant=162;
    self.pickerView.backgroundColor=[UIColor darkGrayColor];
    self.pickerView.delegate=self;
    self.pickerView.dataSource=self;
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDoneButtonTap)];
    
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"Choice-%ld",(long)row];//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

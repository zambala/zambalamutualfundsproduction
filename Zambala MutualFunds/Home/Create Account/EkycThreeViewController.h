//
//  EkycThreeViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 29/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EkycThreeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *motherNameTF;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UISwitch *addressSwitch;
@property (weak, nonatomic) IBOutlet UIView *anotherAddressView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *anotherAddressViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *identityImageView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewLayoutConstraint;

@property NSString * profileCheck;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

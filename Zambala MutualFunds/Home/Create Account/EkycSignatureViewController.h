//
//  EkycSignatureViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 30/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UviSignatureView.h"


@protocol CaptureSignatureViewDelegate <NSObject>
@required
- (void)processCompleted:(UIImage*)signImage;
@end

@interface EkycSignatureViewController : UIViewController{
    // Delegate to respond back
    id <CaptureSignatureViewDelegate> _delegate;
    NSString *userName, *signedDate;
}


@property (nonatomic,strong) id delegate;
-(void)startSampleProcess:(NSString*)text;
// Instance method

@property (strong, nonatomic) IBOutlet UviSignatureView *signatureView;
- (IBAction)captureSign:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *captureButton;

@end

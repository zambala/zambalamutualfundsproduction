//
//  EkycOneViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 23/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EkycOneViewController.h"
#import "EkycTwoViewController.h"

@interface EkycOneViewController ()
{
    UIToolbar*toolbar;
    NSDateFormatter *dateFormatter;
    int age;
}

@end

@implementation EkycOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.datePicker.hidden=YES;
    self.datePickerHeightConstraint.constant=0;
    
    self.panCardView.hidden=NO;
    self.panCardViewHeightConstraint.constant=62;
    
    
    self.minorView.hidden=YES;
    self.minorViewHeightConstraint.constant=0;
    dateFormatter=[[NSDateFormatter alloc]init];
   // [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 1.0f;
    self.nextButton.layer.shadowRadius = 3.0f;
    self.nextButton.layer.cornerRadius=20.0f;
    self.nextButton.layer.masksToBounds = NO;
    //NSString * date = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateStyle = NSDateFormatterLongStyle;
    NSString *string = [formatter stringFromDate:[NSDate date]];
    
    self.dobButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    
    [self.dobButton setTitle:string forState:UIControlStateNormal];
    
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.dobButton addTarget:self action:@selector(onDOBButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onDOBButtonTap
{
    
    self.datePicker.hidden=NO;
    self.datePickerHeightConstraint.constant=162;
    
    [self.datePicker addTarget:self action:@selector(LabelTitle) forControlEvents:UIControlEventAllEvents];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDoneButtonTap)];
    
    toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
    [toolbar setItems:toolbarItems];
    [self.view addSubview:toolbar];
    
    self.datePicker.backgroundColor=[UIColor lightGrayColor];
}

-(void)LabelTitle
{
    
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    //[dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    self.dobString=[NSString stringWithFormat:@"%@",[dateFormatter  stringFromDate:self.datePicker.date]];
    
    //assign text to label
    [self.dobButton setTitle:self.dobString forState:UIControlStateNormal];
}

-(void)onDoneButtonTap
{
    self.datePicker.hidden=YES;
    self.datePickerHeightConstraint.constant=0;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//    NSDate *birthday = [dateFormatter dateFromString:self.dobString];
//
//    NSDate* now = [NSDate date];
//    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
//                                       components:NSCalendarUnitYear
//                                       fromDate:birthday
//                                       toDate:now
//                                       options:0];
//    NSInteger age = [ageComponents year];
//
//    NSLog(@"Age:%ld",(long)age);
    
    
    
    
    
//    self.dobString=[NSString stringWithFormat:@"%@",[dateFormatter  stringFromDate:self.datePicker.date]];
//
//   NSDate * startD = [dateFormatter dateFromString:self.dobString];
//    NSDate *endD = [NSDate date];
//
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSUInteger unitFlags = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
//
//   // NSUInteger unitFlags = NSCalendarUnitYear;
//    NSDateComponents *components = [calendar components:unitFlags fromDate:startD toDate:endD options:0];
//
//    NSInteger year  = [components year];
//    NSInteger month  = [components month];
//    NSInteger day  = [components day];
//
//    NSLog(@"%ld:%ld:%ld", (long)year, (long)month,(long)day);
    
    
    
    NSDate * date = [dateFormatter dateFromString:self.dobString];
    [self ageFromBirthday:date];
    
    [self ageFromBirthDate:self.dobString];
    
    
     age = [[self ageFromBirthDate:self.dobString]intValue];
    if(age>18)
    {
        self.panCardView.hidden=NO;
        self.panCardViewHeightConstraint.constant=62;
    }else
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"You are a minor. Please fill the minor application form." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.panCardView.hidden=YES;
            self.panCardViewHeightConstraint.constant=0;
            self.minorView.hidden=NO;
            self.minorViewHeightConstraint.constant=414;
        }];
        
        [alert addAction:okAction];
    }
    
    [toolbar removeFromSuperview];
    //[self.datePicker removeFromSuperview];
}

-(void)onNextButtonTap
{
//    NSString * check;
//    if(self.dobString.length==0)
//    {
//        self.dobString = [dateFormatter stringFromDate:[NSDate date]];
//        NSDate * date = [dateFormatter dateFromString:self.dobString];
//       // [self ageFromBirthday:date];
//
//        //[self ageFromBirthDate:self.dobString];
//
//
//        age = [[self ageFromBirthDate:self.dobString]intValue];
//        check= [NSString stringWithFormat:@"%d",age];
//
//    }
//    if(age>18)
//    {
//        if(self.dobButton.titleLabel.text>0)
//        {
//
//        }else
//        {
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Please select DOB." preferredStyle:UIAlertControllerStyleAlert];
//
//            [self presentViewController:alert animated:YES completion:^{
//
//            }];
//
//            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//            }];
//
//            [alert addAction:okAction];
//        }
//
//    }else
//    {
//    if(self.panCardGaurdianTF.text.length>0&&self.panCardGaurdianTF.text.length>0&&self.panCardGaurdianTF.text.length>0&&self.panCardGaurdianTF.text.length>0&&self.panCardGaurdianTF.text.length>0)
//        {
//            if(self.aadharGaurdianTF.text.length==12&&self.aadharMinorTF.text.length==12)
//            {
//
//            }else
//            {
//                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Invalid Aadhar Number." preferredStyle:UIAlertControllerStyleAlert];
//
//                [self presentViewController:alert animated:YES completion:^{
//
//                }];
//
//                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//                }];
//
//                [alert addAction:okAction];
//            }
//        }else
//        {
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"Missing Fields. Please Verify." preferredStyle:UIAlertControllerStyleAlert];
//
//            [self presentViewController:alert animated:YES completion:^{
//
//            }];
//
//            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//            }];
//
//            [alert addAction:okAction];
//        }
//    }
    
    
    EkycTwoViewController * kycTwo = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycTwoViewController"];
    [self.navigationController pushViewController:kycTwo animated:YES];
}

- (NSString *) ageFromBirthDate:(NSString *)birthDate{
    dateFormatter= [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle=NSDateFormatterLongStyle;
    NSDate *myDate = [dateFormatter dateFromString:birthDate];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%ld ans", (long)[[[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:myDate toDate:[NSDate date] options:0] year]]);
    
    return [NSString stringWithFormat:@"%ld ans", (long)[[[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:myDate toDate:[NSDate date] options:0] year]];
}


- (NSInteger)ageFromBirthday:(NSDate *)birthdate {
    NSDate *today = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthdate
                                       toDate:today
                                       options:0];
    
    NSLog(@"%ld",(long)ageComponents.year);
    return ageComponents.year;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  CreateAccountPopUpViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 23/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateAccountPopUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;

@end

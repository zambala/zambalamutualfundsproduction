//
//  EkycOneViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 23/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EkycOneViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *dobButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *minorView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *minorViewHeightConstraint;

@property NSString * dobString;
@property (weak, nonatomic) IBOutlet UITextField *panCardGaurdianTF;
@property (weak, nonatomic) IBOutlet UITextField *panCardMinor;
@property (weak, nonatomic) IBOutlet UITextField *aadharGaurdianTF;
@property (weak, nonatomic) IBOutlet UITextField *aadharMinorTF;
@property (weak, nonatomic) IBOutlet UITextField *nameOfGaurdianTF;
@property (weak, nonatomic) IBOutlet UIButton *relationShipButton;
@property (weak, nonatomic) IBOutlet UIView *panCardView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *panCardViewHeightConstraint;

@end

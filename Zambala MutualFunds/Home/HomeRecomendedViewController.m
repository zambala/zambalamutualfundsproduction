//
//  HomeRecomendedViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 08/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "HomeRecomendedViewController.h"
#import "HomeRecomendedTableViewCell.h"
#import "EquityDetailViewController.h"
#import "OrderConfirmationViewController.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
@import Mixpanel;

@interface HomeRecomendedViewController ()
{
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    Mixpanel * mixpanelFunds;
}

@end

@implementation HomeRecomendedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     mfSharedManager=[Utility MF];
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    self.smartMoneyDataArray = [[NSMutableArray alloc]init];
    self.exploreButton.layer.cornerRadius = 20.0f;
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
   
    if([self.smartMoneyString isEqualToString:@"YES"])
    {
        [self.recomendedTableView setScrollEnabled:NO];
        self.mainTopView.hidden=YES;
        self.mainTopViewHeightConstraint.constant=0;
        self.bottomView.hidden = NO;
        self.tableViewHeightConstraint.constant = 147;
        [self.backMainView setBackgroundColor:[UIColor colorWithRed:(248.0/255.0) green:(248.0/255.0) blue:(248.0/255.0) alpha:1.0]];
        [self.topImageView setBackgroundColor:[UIColor colorWithRed:(248.0/255.0) green:(248.0/255.0) blue:(248.0/255.0) alpha:1.0]];
        if([self.typeOfSmartMoney isEqualToString:@"less"])
        {
            [mixpanelFunds track:@"smart_money_less_than_six_months_page"];
            self.exploreTextLabel.text = @"Explore funds with parking period more than 6 months?";
        }else if ([self.typeOfSmartMoney isEqualToString:@"more"])
        {
            [mixpanelFunds track:@"smart_money_greater_than_six_months_page"];
            self.exploreTextLabel.text = @"Explore funds with parking period less than 6 months?";
        }
        [self smartMoneyMethod];
    }else
    {
        [mixpanelFunds track:@"funds_recommended_page"];
        [self.recomendedTableView setScrollEnabled:YES];
        self.mainTopView.hidden=NO;
        self.mainTopViewHeightConstraint.constant=168.4;
        self.bottomView.hidden = YES;
        [self.backMainView setBackgroundColor:[UIColor colorWithRed:(253.0/255.0) green:(217.0/255.0) blue:(116.0/255.0) alpha:1.0]];
         [self.topImageView setBackgroundColor:[UIColor colorWithRed:(253.0/255.0) green:(217.0/255.0) blue:(116.0/255.0) alpha:1.0]];
        self.tableViewHeightConstraint.constant = 900;
        [self getRecommendations];
    }
    [self.exploreButton addTarget:self action:@selector(onExploreButtonTap) forControlEvents:UIControlEventTouchUpInside];
    self.recomendedTableView.delegate=self;
    self.recomendedTableView.dataSource=self;
    [self.recomendedTableView reloadData];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)onExploreButtonTap
{
    if([self.typeOfSmartMoney isEqualToString:@"less"])
    {
        self.typeOfSmartMoney = @"more";
    }else if ([self.typeOfSmartMoney isEqualToString:@"more"])
    {
        self.typeOfSmartMoney = @"less";
    }
    if([self.typeOfSmartMoney isEqualToString:@"less"])
    {
        self.exploreTextLabel.text = @"Explore funds with parking period more than 6 months?";
    }else if ([self.typeOfSmartMoney isEqualToString:@"more"])
    {
        self.exploreTextLabel.text = @"Explore funds with parking period less than 6 months?";
    }
    [self smartMoneyMethod];
}

-(void)smartMoneyMethod
{
    if([self networkStatus]==YES)
    {
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        //
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        if(self.smartMoneyDataArray.count>0)
        {
            [self.smartMoneyDataArray removeAllObjects];
        }
        [inputArray addObject:@"smartMoney"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@",cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@getsmartmoneyschemes_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
                
                if([self.typeOfSmartMoney isEqualToString:@"less"])
                {
                    for (int i=0; i<[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count]; i++) {
                        NSString * rfor = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:i] objectForKey:@"rfor"]];
                        if([rfor isEqualToString:@"T"])
                        {
                            [self.smartMoneyDataArray addObject:[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:i]];
                        }
                    }
                }else if([self.typeOfSmartMoney isEqualToString:@"more"])
                {
                    for (int i=0; i<[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count]; i++) {
                        NSString * rfor = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:i] objectForKey:@"rfor"]];
                        if([rfor isEqualToString:@"G"])
                        {
                            [self.smartMoneyDataArray addObject:[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:i]];
                        }
                    }
                }
                
                
                [self setHeightOfTableView];
                if(self.smartMoneyDataArray.count>0)
                {
                     [self.recomendedTableView reloadData];
                }
            });
        }];
        //
    }
    }
}

-(void)setHeightOfTableView
{
    /**** set frame size of tableview according to number of cells ****/
    float height=60.0f*[self.smartMoneyDataArray count];
    if([self.smartMoneyString isEqualToString:@"YES"])
    {
    [self.recomendedTableView setFrame:CGRectMake(self.recomendedTableView.frame.origin.x, self.recomendedTableView.frame.origin.y, self.recomendedTableView.frame.size.width,(156.0f*([self.smartMoneyDataArray count])))];
    }else
    {
         [self.recomendedTableView setFrame:CGRectMake(self.recomendedTableView.frame.origin.x, self.recomendedTableView.frame.origin.y, self.recomendedTableView.frame.size.width,(556.0f*([[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count])))];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.smartMoneyString isEqualToString:@"YES"])
    {
        return self.smartMoneyDataArray.count;
    }else
    {
    return [[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeRecomendedTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HomeRecomendedTableViewCell" forIndexPath:indexPath];
    cell.investNowButton.layer.cornerRadius=10.0f;
    cell.investNowButton.layer.masksToBounds = NO;
    if([self.smartMoneyString isEqualToString:@"YES"])
    {
        [cell setBackgroundColor:[UIColor colorWithRed:(248.0/255.0) green:(248.0/255.0) blue:(248.0/255.0) alpha:1.0]];
        NSString * fundName = [NSString stringWithFormat:@"%@",[[self.smartMoneyDataArray objectAtIndex:indexPath.row] objectForKey:@"S_NAME"]];
        cell.fundNameLabel.text = fundName;
        
        NSString * threeYearReturn = [NSString stringWithFormat:@"%.2f",[[[self.smartMoneyDataArray objectAtIndex:indexPath.row] objectForKey:@"3yearret"] floatValue]];
        NSString * percent = @"%";
        cell.threeYearReturnLabel.text = [threeYearReturn stringByAppendingString:percent];
        NSString * minInvest = [NSString stringWithFormat:@"%@",[[self.smartMoneyDataArray objectAtIndex:indexPath.row] objectForKey:@"MININVT"]];
        cell.minInvestLabel.text = minInvest;
        
    }else
    {
        [cell setBackgroundColor:[UIColor colorWithRed:(253.0/255.0) green:(217.0/255.0) blue:(116.0/255.0) alpha:1.0]];
    NSString * fundName = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"S_NAME"]];
    cell.fundNameLabel.text = fundName;
    NSString * threeYearReturn = [NSString stringWithFormat:@"%.2f",[[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"3yearret"]floatValue]];
    NSString * percent = @"%";
    cell.threeYearReturnLabel.text = [threeYearReturn stringByAppendingString:percent];
    NSString * minInvest = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"MININVT"]];
    cell.minInvestLabel.text = minInvest;
    }
    [cell.investNowButton addTarget:self action:@selector(onInvestNowButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)getRecommendations
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getRecommendations"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@",cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@getschemerecommendations_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.recomendedTableView reloadData];
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
            });
        }];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [activityIndicatorView stopAnimating];
    [backView removeFromSuperview];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 156;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.smartMoneyString isEqualToString:@"YES"])
    {
        EquityDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
        detail.schemeCodeString = [NSString stringWithFormat:@"%@",[[self.smartMoneyDataArray objectAtIndex:indexPath.row] objectForKey:@"schemecode"]];
        detail.fundNameString = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"S_NAME"]];
        [self.navigationController pushViewController:detail animated:YES];
    }else
    {
    EquityDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
     detail.schemeCodeString = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"schemecode"]];
    detail.fundNameString = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"S_NAME"]];
    [self.navigationController pushViewController:detail animated:YES];
    }
}

-(void)onInvestNowButtonTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.recomendedTableView];
    NSIndexPath *indexPath = [self.recomendedTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    OrderConfirmationViewController * order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderConfirmationViewController"];
    order.check = @"rec";
    if([self.smartMoneyString isEqualToString:@"YES"])
    {
       order.schemeCode=[NSString stringWithFormat:@"%@",[[self.smartMoneyDataArray objectAtIndex:indexPath.row] objectForKey:@"schemecode"]];
    }else
    {
    order.schemeCode=[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"schemecode"]];
    }
    [self.navigationController pushViewController:order animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

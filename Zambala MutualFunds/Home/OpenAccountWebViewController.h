//
//  OpenAccountWebViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 28/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenAccountWebViewController : UIViewController<WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet WKWebView *openAccountWebView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property NSString * sourceString;

@end

NS_ASSUME_NONNULL_END

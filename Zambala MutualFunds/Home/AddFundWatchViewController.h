//
//  AddFundWatchViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 07/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface AddFundWatchViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *assetTypeButton;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *amcButton;
@property (weak, nonatomic) IBOutlet UIButton *schemeButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UITableView *fundWatchTableView;

@property Reachability * reach;


@property NSMutableDictionary * responseDictionary;

@property NSMutableDictionary * zambalaResponseDataDictionary;

@end

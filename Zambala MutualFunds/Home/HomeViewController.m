//
//  HomeViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "HomeViewController.h"
#import "EquityViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "CreateAccountPopUpViewController.h"
#import "EkycOneViewController.h"
#import "SmarMoneyViewController.h"
#import "AppDelegate.h"
#import "HomeRecomendedViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "OpenAccountWebViewController.h"
#import "TransactionsViewController.h"
#import "Reachability.h"
@import Mixpanel;


@interface HomeViewController ()
{
    AppDelegate * delegate;
    Mixpanel * mixpanelFunds;
}
@property (nonatomic, strong) CAGradientLayer *gradient;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    if([delegate.signUpCheck isEqualToString:@"signin"])
    {
        self.popUpView.hidden=NO;
        [self.proceedButton setTitle:@"DO IT NOW" forState:UIControlStateNormal];
        [self.cancelButton setTitle:@"DO IT LATER" forState:UIControlStateNormal];
    }else
    {
        self.popUpView.hidden=YES;
        [self.proceedButton setTitle:@"Proceed" forState:UIControlStateNormal];
        [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [mixpanelFunds track:@"create_account_pop_up"];
        
    }
    
    
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.topView.layer.shadowOpacity = 3.0f;
    self.topView.layer.shadowRadius = 3.0f;
    self.topView.layer.cornerRadius=10.0f;
    self.topView.layer.masksToBounds = NO;
    self.popUpView.tag = 99;
    self.exploreButton.layer.cornerRadius = 10.0f;
    
    
    [mixpanelFunds track:@"home_page"];
    
     //self.topView.clipsToBounds = YES;
    
   
    
    self.fundPickedCountButton.layer.cornerRadius = self.fundPickedCountButton.frame.size.width/2;
    
    self.createAccountView.layer.cornerRadius = 5.0f;
    
    self.proceedButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.proceedButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.proceedButton.layer.shadowOpacity = 3.0f;
    self.proceedButton.layer.shadowRadius = 3.0f;
    self.proceedButton.layer.cornerRadius=20.0f;
    self.proceedButton.layer.masksToBounds = NO;
    
    self.cancelButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.cancelButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.cancelButton.layer.shadowOpacity = 3.0f;
    self.cancelButton.layer.shadowRadius = 3.0f;
    self.cancelButton.layer.cornerRadius=20.0f;
    self.cancelButton.layer.masksToBounds = NO;
    
   
    
    
    [self.equityButton addTarget:self action:@selector(onEquityTap) forControlEvents:UIControlEventTouchUpInside];
    [self.createAccountButton addTarget:self action:@selector(onCreateButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.smartMoneyButton addTarget:self action:@selector(onSmartMoneyTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.cancelButton addTarget:self action:@selector(onCancelTap) forControlEvents:UIControlEventTouchUpInside];
    [self.proceedButton addTarget:self action:@selector(onProceedButtopTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.hybridbutton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.debtButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.taxSaverButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.othersButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.fundsPickedButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.exploreButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.arrowButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceived) name:@"pushNotification" object:nil];
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
          
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}

-(void)pushNotificationReceived{
    NSString * destination=[NSString stringWithFormat:@"%@",[[delegate.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
    if([destination isEqualToString:@"Resources"])
    {
        delegate.pushNotificationSection=@"Resources";
        delegate.notificationInfo=nil;
        [self.tabBarController setSelectedIndex:4];
    }else if ([destination isEqualToString:@"Videos"])
    {
        delegate.pushNotificationSection=@"Videos";
        delegate.notificationInfo=nil;
        [self.tabBarController setSelectedIndex:4];
    }else if ([destination isEqualToString:@"Create Account"])
    {
        delegate.pushNotificationSection=@"Create Account";
        delegate.notificationInfo=nil;
        [self onCreateButtonTap];
    }else if ([destination isEqualToString:@"Funds Picked"])
    {
        delegate.pushNotificationSection=@"Funds Picked";
        delegate.notificationInfo=nil;
        [self onButtonTap:self.fundsPickedButton];
    }else if ([destination isEqualToString:@"Portfolio"])
    {
        delegate.pushNotificationSection=@"Portfolio";
        delegate.notificationInfo=nil;
        [self.tabBarController setSelectedIndex:3];
    }else if ([destination isEqualToString:@"Transactions"])
    {
        delegate.pushNotificationSection=@"Transactions";
        delegate.notificationInfo=nil;
        TransactionsViewController * transactions = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionsViewController"];
        [self presentViewController:transactions animated:YES completion:nil];
    }else if ([destination isEqualToString:@"Fund Watch"])
    {
        delegate.pushNotificationSection=@"Fund Watch";
        delegate.notificationInfo=nil;
        [self.tabBarController setSelectedIndex:2];
    }else if ([destination isEqualToString:@"Others"])
    {
        delegate.pushNotificationSection=@"Others";
        delegate.notificationInfo=nil;
        [self.tabBarController setSelectedIndex:1];
    }else if ([destination isEqualToString:@"PlayStoreUpdate"])
    {
        delegate.pushNotificationSection=@"Others";
        delegate.notificationInfo=nil;
        [self.tabBarController setSelectedIndex:1];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString * message = [NSString stringWithFormat:@"%@",[[delegate.notificationInfo objectForKey:@"u"]objectForKey:@"section"]];
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:message preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Update" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               
                NSString *iTunesLink = @"https://itunes.apple.com/us/app/zambala-stocks-by-zenwise/id1281356740?mt=8";
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
            }];
            
            [alert addAction:okAction];
        });
    }
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.gradient.frame = self.topView.bounds;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.popUpView.hidden=YES;
    }
}

-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.hybridbutton)
    {
        EquityViewController * hybrid = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityViewController"];
        hybrid.typeOfButton=@"hybrid";
        [self.navigationController pushViewController:hybrid animated:YES];
    }else if (sender == self.debtButton)
    {
        EquityViewController * debt = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityViewController"];
        debt.typeOfButton=@"debt";
        [self.navigationController pushViewController:debt animated:YES];
    }else if (sender == self.taxSaverButton)
    {
        EquityViewController * taxSaver = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityViewController"];
        taxSaver.typeOfButton=@"taxsaver";
        [self.navigationController pushViewController:taxSaver animated:YES];
    }else if (sender == self.othersButton)
    {
        EquityViewController * others = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityViewController"];
        others.typeOfButton=@"others";
        [self.navigationController pushViewController:others animated:YES];
    }else if (sender == self.fundsPickedButton)
    {
        EquityViewController * fundsPicked = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityViewController"];
        fundsPicked.typeOfButton=@"fundspicked";
        [self.navigationController pushViewController:fundsPicked animated:YES];
    }else if (sender == self.exploreButton)
    {
        HomeRecomendedViewController * home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeRecomendedViewController"];
        [self.navigationController pushViewController:home animated:YES];
    }else if (sender == self.arrowButton)
    {
        HomeRecomendedViewController * home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeRecomendedViewController"];
        [self.navigationController pushViewController:home animated:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    if([delegate.signUpCheck isEqualToString:@"signin"])
    {
        delegate.signUpCheck=@"";
    self.popUpView.hidden=NO;
    }else
    {
        self.popUpView.hidden=YES;
    }
    self.gradient  = [CAGradientLayer layer];
    self.gradient.colors = @[(id)[UIColor colorWithRed:(15.0/255.0) green:(91.0/255.0) blue:(121.0/255.0) alpha:1.0f].CGColor, (id)[UIColor colorWithRed:(52.0/255.0) green:(185.0/255.0) blue:(146.0/255.0) alpha:1.0f] .CGColor];
    self.gradient.startPoint = CGPointMake(0.0, 0.5);
    self.gradient.endPoint = CGPointMake(1.0, 0.5);
    self.gradient.cornerRadius = 10.0f;
    self.gradient.masksToBounds=NO;
    [self.topView.layer insertSublayer:self.gradient atIndex:0];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    
}

-(void)onEquityTap
{
    EquityViewController * equity = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityViewController"];
    equity.typeOfButton=@"equity";
    [self.navigationController pushViewController:equity animated:YES];
}

-(void)onSmartMoneyTap
{
    SmarMoneyViewController * smartMoney = [self.storyboard instantiateViewControllerWithIdentifier:@"SmarMoneyViewController"];
    [self.navigationController pushViewController:smartMoney animated:YES];
}

-(void)onCreateButtonTap
{
    [self.proceedButton setTitle:@"Proceed" forState:UIControlStateNormal];
    [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    self.popUpView.hidden=NO;
}

-(void)onCancelTap
{
    self.popUpView.hidden=YES;
}

-(void)onProceedButtopTap
{
//    EkycOneViewController * kyc = [self.storyboard instantiateViewControllerWithIdentifier:@"EkycOneViewController"];
//    [self.navigationController pushViewController:kyc animated:YES];
    OpenAccountWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountWebViewController"];
    [self.navigationController pushViewController:webView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

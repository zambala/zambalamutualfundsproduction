//
//  HomeViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ViewController.h"
#import "Reachability.h"

@interface HomeViewController : ViewController<UITabBarDelegate>
@property (weak, nonatomic) IBOutlet UILabel *fundsRecommendedLabel;
@property (weak, nonatomic) IBOutlet UIButton *equityButton;
@property (weak, nonatomic) IBOutlet UIButton *debtButton;
@property (weak, nonatomic) IBOutlet UIButton *hybridbutton;
@property (weak, nonatomic) IBOutlet UIButton *taxSaverButton;
@property (weak, nonatomic) IBOutlet UIButton *smartMoneyButton;
@property (weak, nonatomic) IBOutlet UIButton *othersButton;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *fundsPickedButton;
@property (weak, nonatomic) IBOutlet UILabel *fundsPickedLabel;
@property (weak, nonatomic) IBOutlet UIView *createAccountView;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *fundPickedCountButton;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *exploreButton;
@property (weak, nonatomic) IBOutlet UIButton *arrowButton;
@property Reachability * reach;

@end

//
//  HomeRecomendedTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 08/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeRecomendedTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *investNowButton;
@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *threeYearReturnLabel;
@property (weak, nonatomic) IBOutlet UILabel *minInvestLabel;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end

//
//  OpenAccountStatusViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 15/10/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenAccountStatusViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *failureView;
@property (weak, nonatomic) IBOutlet UIButton *tryAgainButton;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIView *successView;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property NSString * stautsString;

@end

NS_ASSUME_NONNULL_END

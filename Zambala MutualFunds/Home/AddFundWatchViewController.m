//
//  AddFundWatchViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 07/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "AddFundWatchViewController.h"
#import "AddFundWatchTableViewCell.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "FundWatchViewController.h"
#import "AppDelegate.h"
@import Mixpanel;


@interface AddFundWatchViewController ()
{
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    NSString * selectedAssetType;
    NSString * selectedCategory;
    NSString * selectedAMC;
    NSString * selectedSchemeCode;
    NSString * data;
    AppDelegate * delegate;
    Mixpanel * mixpanelFunds;
}

@end

@implementation AddFundWatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    mfSharedManager=[Utility MF];
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    [mixpanelFunds track:@"add_fund_watch_page"];
    
    self.addButton.layer.cornerRadius=10.0f;
    self.addButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.addButton.layer.shadowOffset = CGSizeMake(0.0f,2.0f);
    self.addButton.layer.shadowOpacity = 0.6f;
    self.addButton.layer.shadowRadius = 3.0f;
    self.addButton.layer.masksToBounds = NO;
    
    self.popUpView.hidden = YES;
    self.popUpView.tag = 99;
    
    self.fundWatchTableView.delegate = self;
    self.fundWatchTableView.dataSource = self;
    
    [self.addButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.assetTypeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.categoryButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.amcButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.schemeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.assetTypeButton.selected=NO;
    self.categoryButton.selected = NO;
    self.amcButton.selected = NO;
    self.schemeButton.selected = NO;
    
    //[self getAssetTypeList];
    //[self getCategoryByAsset];
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton *)sender
{
    if([self networkStatus]==YES)
    {
    if(sender == self.addButton)
    {
        if(selectedSchemeCode.length>0)
        {
            [self sendSymbolsList];
        }
    }else if (sender == self.assetTypeButton)
    {
        self.assetTypeButton.selected=YES;
        self.categoryButton.selected = NO;
        self.amcButton.selected = NO;
        self.schemeButton.selected = NO;
        [UIView transitionWithView:self.popUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpView.hidden = NO;
                        }
                        completion:NULL];
        [self getAssetTypeList];
    }else if (sender == self.categoryButton)
    {
        self.assetTypeButton.selected=NO;
        self.categoryButton.selected = YES;
        self.amcButton.selected = NO;
        self.schemeButton.selected = NO;
        [UIView transitionWithView:self.popUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpView.hidden = NO;
                        }
                        completion:NULL];
        [self getCategoryByAsset];
    }else if (sender == self.amcButton)
    {
        self.assetTypeButton.selected=NO;
        self.categoryButton.selected = NO;
        self.amcButton.selected = YES;
        self.schemeButton.selected = NO;
        [UIView transitionWithView:self.popUpView
                       duration:0.4
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         self.popUpView.hidden = NO;
                     }
                     completion:NULL];
        [self getAMCList];
    }else if (sender == self.schemeButton)
    {
        self.assetTypeButton.selected=NO;
        self.categoryButton.selected = NO;
        self.amcButton.selected = NO;
        self.schemeButton.selected = YES;
        [UIView transitionWithView:self.popUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpView.hidden = NO;
                        }
                        completion:NULL];
        [self getSchemeList];
    }
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
}
-(void)sendSymbolsList
{
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid;
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    if([guestCheck isEqualToString:@"0"])
    {
        cid = [userSessionData objectForKey:@"zenwiseID"];
    }else if([guestCheck isEqualToString:@"1"])
    {
        cid = [userSessionData objectForKey:@"cid"];
    }
   // NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"zenwiseID"]];
    NSString * schemeCodeNew = [NSString stringWithFormat:@"%@",selectedSchemeCode];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"addfundwatch"];
    [inputArray addObject:[NSString stringWithFormat:@"%@fundswatch",mfSharedManager.zambalaBaseUrl]];
    [inputArray addObject:@"1"];
    [inputArray addObject:@[schemeCodeNew]];
    [inputArray addObject:cid];
    [mfSharedManager zambalaPOSTRequestMethod :inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.zambalaResponseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        NSLog(@"%@",self.zambalaResponseDataDictionary);
        if([[[self.zambalaResponseDataDictionary objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
                delegate.fundWatchSchemeCodeString = selectedSchemeCode;
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alert animated:YES completion:^{
                }];
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                }];
                [alert addAction:okAction];
            });
        }
    }];
}

-(void)indicatorMethod
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

-(void)getAssetTypeList
{
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getAssettypeList"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@",cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@getassettype_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDictionary);
            if(self.responseDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                    [self.assetTypeButton setTitle: [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"assettype"]] forState:UIControlStateNormal];
                    selectedAssetType = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"assettype"]];
                    [self.fundWatchTableView reloadData];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
    }
}

-(void)getCategoryByAsset
{
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getCategoryByAsset"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",selectedAssetType,cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@getcategorybyasset_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDictionary);
            if(self.responseDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                    selectedCategory = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"category"]];
                    [self.categoryButton setTitle:selectedCategory forState:UIControlStateNormal];
                    [self.fundWatchTableView reloadData];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
    }
}

-(void)getAMCList
{
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getAmcList"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@",selectedAssetType,selectedCategory,cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@getamclistbyatcat_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDictionary);
            if(self.responseDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                    selectedAMC = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"FUND"]];
                    [self.amcButton setTitle:selectedCategory forState:UIControlStateNormal];
                    [self.fundWatchTableView reloadData];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
    }
}

-(void)getSchemeList
{
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getAmcList"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@|%@",selectedAssetType,selectedCategory,selectedAMC,cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@searchscheme_option_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDictionary);
            if(self.responseDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                    selectedSchemeCode = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"SCHEMECODE"]];
                    [self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"S_NAME"]] forState:UIControlStateNormal];
                    [self.fundWatchTableView reloadData];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddFundWatchTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AddFundWatchTableViewCell" forIndexPath:indexPath];
    if(self.assetTypeButton.selected == YES)
    {
        data = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"assettype"]];
    }else if (self.categoryButton.selected == YES)
    {
        data = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"category"]];
    }else if (self.amcButton.selected == YES)
    {
        data = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"FUND"]];
    }else if (self.schemeButton.selected == YES)
    {
        data = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"S_NAME"]];
    }
    cell.someLabel.text = data;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.assetTypeButton.selected == YES)
    {
        selectedAssetType = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"assettype"]];
        [self.assetTypeButton setTitle:selectedAssetType forState:UIControlStateNormal];
    }else if (self.categoryButton.selected == YES)
    {
        selectedCategory = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"category"]];
        [self.categoryButton setTitle:selectedCategory forState:UIControlStateNormal];
    }else if (self.amcButton.selected == YES)
    {
        selectedAMC = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"AMC_CODE"]];
        [self.amcButton setTitle:[NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"FUND"]] forState:UIControlStateNormal];
    }else if (self.schemeButton.selected == YES)
    {
        selectedSchemeCode = [NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"SCHEMECODE"]];
        [self.schemeButton setTitle:[NSString stringWithFormat:@"%@",[[[[self.responseDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"S_NAME"]] forState:UIControlStateNormal];
    }
    self.popUpView.hidden= YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.popUpView.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

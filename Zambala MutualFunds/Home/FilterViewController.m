//
//  FilterViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 07/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "FilterViewController.h"

@interface FilterViewController ()

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([self.check isEqualToString:@"fundwatch"])
    {
        self.chooseFilterButton.hidden=YES;
    }else
    {
        self.chooseFilterButton.hidden=NO;
    }
    
    self.applyButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.applyButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.applyButton.layer.shadowOpacity = 3.0f;
    self.applyButton.layer.shadowRadius = 3.0f;
    self.applyButton.layer.cornerRadius=20.0f;
    self.applyButton.layer.masksToBounds = NO;
    
    self.fundShowAllButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.amcShowAllButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.allFundsButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.recommendedFundsButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.lumpSumButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.sipButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.riskAll.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.modLowButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.modHighButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.riskLow.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.riskModerate.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.assetAllButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.equityButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.hybridButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.debtButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.growthButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.dividendButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.taxButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.nonTaxButton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    
    self.minInvestmentTF.text = [NSString stringWithFormat:@"%i",(int)self.minInvestmentSlider.value];
    
    [self.chooseFilterButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.resetButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.fundShowAllButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.allFundsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.recommendedFundsButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.lumpSumButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.sipButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.riskAll addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.modLowButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.modHighButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.riskLow addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.riskModerate addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.amcShowAllButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.assetAllButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.equityButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.hybridButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.debtButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.growthButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.dividendButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.taxButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.nonTaxButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.minInvestmentSlider addTarget:self action:@selector(onSliderValueChange) forControlEvents:UIControlEventValueChanged];
    [self.applyButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.allFundsButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.allFundsButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.recommendedFundsButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.recommendedFundsButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.lumpSumButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.lumpSumButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.sipButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.sipButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.riskAll setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.riskAll setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.modLowButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.modLowButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.modHighButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.modHighButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.riskLow setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.riskLow setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.riskModerate setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.riskModerate setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.assetAllButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.assetAllButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.equityButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.equityButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.hybridButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.hybridButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.debtButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.debtButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.growthButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.growthButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.dividendButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.dividendButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.taxButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.taxButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self.nonTaxButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"check.png"]] forState:UIControlStateSelected];
    [self.nonTaxButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"uncheck.png"]] forState:UIControlStateNormal];
    
    [self defaultButtons];
    
    // Do any additional setup after loading the view.
}

-(void)defaultButtons
{
    self.allFundsButton.selected=YES;
    self.lumpSumButton.selected=YES;
    self.riskAll.selected=YES;
    self.assetAllButton.selected=YES;
    self.growthButton.selected=YES;
    self.taxButton.selected=YES;
    
    
    
    self.recommendedFundsButton.selected=NO;
    self.sipButton.selected=NO;
    self.modLowButton.selected=NO;
    self.modHighButton.selected=NO;
    self.riskLow.selected=NO;
    self.riskModerate.selected=NO;
    self.equityButton.selected=NO;
    self.hybridButton.selected=NO;
    self.debtButton.selected=NO;
    self.dividendButton.selected=NO;
    self.nonTaxButton.selected=NO;
}


-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.chooseFilterButton)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else if (sender == self.resetButton)
    {
        [self defaultButtons];
    }else if (sender == self.fundShowAllButton)
    {
        
    }
    else if (sender == self.allFundsButton)
    {
        self.allFundsButton.selected=YES;
        self.recommendedFundsButton.selected=NO;
    }else if (sender == self.recommendedFundsButton)
    {
        self.allFundsButton.selected=NO;
        self.recommendedFundsButton.selected=YES;
    }else if (sender == self.lumpSumButton)
    {
        self.lumpSumButton.selected=YES;
        self.sipButton.selected=NO;
    }else if (sender == self.sipButton)
    {
        self.lumpSumButton.selected=NO;
        self.sipButton.selected=YES;
    }else if (sender == self.riskAll)
    {
        self.riskAll.selected=YES;
        self.modLowButton.selected=NO;
        self.modHighButton.selected=NO;
        self.riskLow.selected=NO;
        self.riskModerate.selected=NO;
    }else if (sender == self.modLowButton)
    {
        self.riskAll.selected=NO;
        self.modLowButton.selected=YES;
        self.modHighButton.selected=NO;
        self.riskLow.selected=NO;
        self.riskModerate.selected=NO;
    }else if (sender == self.modHighButton)
    {
        self.riskAll.selected=NO;
        self.modLowButton.selected=NO;
        self.modHighButton.selected=YES;
        self.riskLow.selected=NO;
        self.riskModerate.selected=NO;
    }else if (sender == self.riskLow)
    {
        self.riskAll.selected=NO;
        self.modLowButton.selected=NO;
        self.modHighButton.selected=NO;
        self.riskLow.selected=YES;
        self.riskModerate.selected=NO;
    }else if (sender == self.riskModerate)
    {
        self.riskAll.selected=NO;
        self.modLowButton.selected=NO;
        self.modHighButton.selected=NO;
        self.riskLow.selected=NO;
        self.riskModerate.selected=YES;
    }else if (sender == self.amcShowAllButton)
    {
        
    }else if (sender == self.assetAllButton)
    {
        self.assetAllButton.selected=YES;
        self.equityButton.selected=NO;
        self.hybridButton.selected=NO;
        self.debtButton.selected=NO;
    }else if (sender == self.equityButton)
    {
        self.assetAllButton.selected=NO;
        self.equityButton.selected=YES;
        self.hybridButton.selected=NO;
        self.debtButton.selected=NO;
    }else if (sender == self.hybridButton)
    {
        self.assetAllButton.selected=NO;
        self.equityButton.selected=NO;
        self.hybridButton.selected=YES;
        self.debtButton.selected=NO;
    }else if (sender == self.debtButton)
    {
        self.assetAllButton.selected=NO;
        self.equityButton.selected=NO;
        self.hybridButton.selected=NO;
        self.debtButton.selected=YES;
    }else if (sender == self.growthButton)
    {
        self.growthButton.selected=YES;
        self.dividendButton.selected=NO;
    }else if (sender == self.dividendButton)
    {
        self.growthButton.selected=NO;
        self.dividendButton.selected=YES;
    }else if (sender == self.taxButton)
    {
        self.taxButton.selected=YES;
        self.nonTaxButton.selected=NO;
    }else if (sender == self.nonTaxButton)
    {
        self.taxButton.selected=NO;
        self.nonTaxButton.selected=YES;
    }else if (sender == self.applyButton)
    {
        
    }
}

-(void)onSliderValueChange
{
    self.minInvestmentTF.text = [NSString stringWithFormat:@"%i",(int)self.minInvestmentSlider.value];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

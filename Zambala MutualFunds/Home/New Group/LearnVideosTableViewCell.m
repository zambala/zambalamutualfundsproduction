//
//  LearnVideosTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 26/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "LearnVideosTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation LearnVideosTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.cornerRadius=5.0f;
    self.backView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.backView.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.backView.layer.shadowOpacity = 0.6f;
    self.backView.layer.shadowRadius = 3.0f;
    self.playButton.layer.borderWidth = 2.0f;
    self.playButton.layer.borderColor = [UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f].CGColor;
    self.playButton.layer.cornerRadius=3.0f;
    self.backView.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

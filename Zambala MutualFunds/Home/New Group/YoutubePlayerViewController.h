//
//  YoutubePlayerViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 26/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YoutubePlayerViewController : UIViewController
@property (weak, nonatomic) IBOutlet YTPlayerView *youtubePlayerView;
@property (weak, nonatomic) IBOutlet UIView *mainWebView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property NSString * checkString;
@property NSString * webURL;
@property NSString * youtubeURL;

@end

NS_ASSUME_NONNULL_END

//
//  LearnMainViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 26/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "LearnMainViewController.h"
#import "HMSegmentedControl.h"
#import "LearnVideosTableViewCell.h"
#import "LearnResourcesTableViewCell.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "YoutubePlayerViewController.h"
@import Mixpanel;
#import "AppDelegate.h"

@interface LearnMainViewController ()
{
    HMSegmentedControl * segmentedControl;
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backIndicatorView;
    NSString * urlString;
    Mixpanel * mixpanelFunds;
    AppDelegate * delegate;
}

@end

@implementation LearnMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    mfSharedManager=[Utility MF];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Resources",@"Videos"]];
    segmentedControl.frame = CGRectMake(0, 0, self.segmentedControlView.frame.size.width,56);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    [segmentedControl setBackgroundColor:[UIColor colorWithRed:(248.0/255.0) green:(248.0/255.0) blue:(248.0/255.0) alpha:1.0f]];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    [self.segmentedControlView addSubview:segmentedControl];
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    [mixpanelFunds track:@"learn_resources_page"];
    
    if([delegate.pushNotificationSection isEqualToString:@"Resources"])
    {
        [segmentedControl setSelectedSegmentIndex:0];
        [self onSegmentTap];
    }else if([delegate.pushNotificationSection isEqualToString:@"Videos"])
    {
        [segmentedControl setSelectedSegmentIndex:1];
        [self onSegmentTap];
    }else
    {
        [segmentedControl setSelectedSegmentIndex:0];
        [self onSegmentTap];
    }
    
    //urlString=@"1";
    self.learnTableView.delegate=self;
    self.learnTableView.dataSource=self;
    [self.learnTableView reloadData];
    //[self serverHit];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)serverHit
{
    if([self networkStatus]==YES)
    {

    backIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backIndicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backIndicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"Learn"];
    NSString * endPoint = [NSString stringWithFormat:@"%@knowledgebase?type=%@",mfSharedManager.zambalaBaseUrl,urlString];
    [inputArray addObject:endPoint];
    [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        NSLog(@"%@",self.responseDataDictionary);
        if(self.responseDataDictionary.count>0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
            [self.learnTableView reloadData];
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backIndicatorView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)onSegmentTap
{
    if([self networkStatus]==YES)
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
        [mixpanelFunds track:@"learn_resources_page"];
        urlString=@"1";
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        [mixpanelFunds track:@"learn_videos_page"];
        urlString=@"2";
    }
    [self serverHit];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
     LearnResourcesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"LearnResourcesTableViewCell" forIndexPath:indexPath];
        NSString * imageURL = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"thumbnailurl"]];
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imageURL]];
            if ( data == nil )
                return;
            dispatch_async(dispatch_get_main_queue(), ^{
                // WARNING: is the cell still using the same data by this point??
                 cell.thumbNailImageView.image = [UIImage imageWithData: data];
            });
            //[data release];
        });
        NSString * headingString = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"heading"]]];
        cell.headingLabel.text = headingString;
        NSString * date = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"createdon"]]];
        date = [self changeDate:date];
        NSString * source = [NSString stringWithFormat:@"%@ //%@",[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"source"]],date];
        cell.sourceAndDateLabel.text = source;
    return cell;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        LearnVideosTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"LearnVideosTableViewCell" forIndexPath:indexPath];
        NSString * imageURL = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"thumbnailurl"]];
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imageURL]];
            if ( data == nil )
                return;
            dispatch_async(dispatch_get_main_queue(), ^{
                // WARNING: is the cell still using the same data by this point??
                cell.thumbNailImageView.image = [UIImage imageWithData: data];
            });
            //[data release];
        });
        NSString * headingString = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"heading"]]];
        cell.descriptionLabel.text = headingString;
        [cell.playButton addTarget:self action:@selector(onPlayButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    return nil;
}

-(void)onPlayButtonTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.learnTableView];
    NSIndexPath *indexPath = [self.learnTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    YoutubePlayerViewController * youtube = [self.storyboard instantiateViewControllerWithIdentifier:@"YoutubePlayerViewController"];
    youtube.youtubeURL = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"messageurl"]];
    [self.navigationController pushViewController:youtube animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self networkStatus]==YES)
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
        YoutubePlayerViewController * youtube = [self.storyboard instantiateViewControllerWithIdentifier:@"YoutubePlayerViewController"];
        youtube.checkString =@"web";
        youtube.webURL = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"messageurl"]];
        [self.navigationController pushViewController:youtube animated:YES];
    }
    }
}

-(NSString *)changeDate:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *finalDate = [dateFormatter stringFromDate:date];
    return finalDate;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        return 145;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        return 145;
    }
    return 0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

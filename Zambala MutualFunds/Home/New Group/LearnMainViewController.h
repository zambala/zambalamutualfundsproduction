//
//  LearnMainViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 26/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface LearnMainViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *backMainView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *segmentedControlView;
@property (weak, nonatomic) IBOutlet UITableView *learnTableView;

@property NSMutableDictionary *responseDataDictionary;

@property Reachability * reach;


@end

NS_ASSUME_NONNULL_END

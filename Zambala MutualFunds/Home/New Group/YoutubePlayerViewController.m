//
//  YoutubePlayerViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 26/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "YoutubePlayerViewController.h"
#import "YTPlayerView.h"
@import Mixpanel;

@interface YoutubePlayerViewController ()
{
    Mixpanel * mixpanelFunds;
}

@end

@implementation YoutubePlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    [mixpanelFunds track:@"learn_detail_page"];
    if([self.checkString isEqualToString:@"web"])
    {
        self.mainWebView.hidden=NO;
        self.youtubePlayerView.hidden=YES;
//        NSURL *targetURL = [NSURL URLWithString:self.webURL];
//        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
//        [self.webView loadRequest:request];
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webURL]]];

    }else
    {
        self.mainWebView.hidden=YES;
        self.youtubePlayerView.hidden=NO;
        if(self.youtubeURL.length>0)
        {
            NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
            NSArray *urlComponents1 = [self.youtubeURL componentsSeparatedByString:@"?"];
            NSString * paramStr=[NSString stringWithFormat:@"%@",[urlComponents1 objectAtIndex:1]];
            NSArray *urlComponents = [paramStr componentsSeparatedByString:@"&"];
            for (NSString *keyValuePair in urlComponents)
            {
                NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
                NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
                NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
                [queryStringDictionary setObject:value forKey:key];
            }
            NSString * videoID= [queryStringDictionary objectForKey:@"v"];
            
            //        NSDictionary*dictionary = @{@"listType" : @"playlist",
            //                                    @"autoplay" : @"1",
            //                                    @"loop" : @"1",
            //                                    @"playsinline" : @"0",
            //                                    @"controls" : @"2",
            //                                    @"cc_load_policy" : @"0",};
            
            NSDictionary*dictionary = @{
                                        @"autoplay" : @"0",
                                        @"playsinline" : @"0",
                                        @"controls" : @"0",
                                        @"cc_load_policy" : @"0"};
            
            
            [self.youtubePlayerView loadWithVideoId:videoID playerVars:dictionary];
            
            // loading playlist with player paramaters
            // [self.youtubePlayerView loadWithPlaylistId:videoID playerVars:dictionary];
            
            // ******** OR **********
            
            // use some customs parameters which will be load to the video like this.. (just for simplicity)
            //self.youtubePlayerView.autoplay = YES;
            //self.youtubePlayerView.modestbranding = YES;
            
            // and then just call load the playlist/video
            //[self.youtubePlayerView loadWithPlaylistId:videoID];
            
            // [self.youtubePlayerView loadWithVideoId:videoID];
        }
    }
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
     [self.navigationController setNavigationBarHidden:NO animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

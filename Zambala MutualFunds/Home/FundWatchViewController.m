//
//  FundWatchViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 04/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "FundWatchViewController.h"
#import "EquityTableViewCell.h"
#import "EquityDetailViewController.h"
#import "AddFundWatchViewController.h"
#import "FilterViewController.h"
#import "OrderConfirmationViewController.h"
#import "AppDelegate.h"
#import "DGActivityIndicatorView.h"
#import "Utility.h"
#import "FundWatchTableViewCell.h"
@import Mixpanel;

@interface FundWatchViewController ()
{
    AppDelegate * delegate;
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    NSString * schemeCodes;
    Mixpanel * mixpanelFunds;
}

@end

@implementation FundWatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mfSharedManager=[Utility MF];
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navigationItem.title=@"Fund Watch";
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    [mixpanelFunds track:@"fund_watch_page"];
   
    // Do any additional setup after loading the view.
}

-(void)indicatorMethod
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self callMethod];
    NSLog(@"%@",delegate.fundWatchSchemeCodeString);
    //if(delegate.fundWatchSchemeCodeString.length!=0)
    //{
        [self getSymbolsList];
    //}
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)getSymbolsList
{
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid;
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    if([guestCheck isEqualToString:@"0"])
    {
        cid = [userSessionData objectForKey:@"zenwiseID"];
    }else if([guestCheck isEqualToString:@"1"])
    {
        cid = [userSessionData objectForKey:@"cid"];
    }
   // NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"zenwiseID"]];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"getfundwatch"];
    [inputArray addObject:[NSString stringWithFormat:@"%@fundswatch",mfSharedManager.zambalaBaseUrl]];
    [inputArray addObject:@"2"];
    [inputArray addObject:cid];
    [mfSharedManager zambalaPOSTRequestMethod :inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.zambalaResponseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        NSLog(@"%@",self.zambalaResponseDataDictionary);
        if([[self.zambalaResponseDataDictionary objectForKey:@"data"] objectForKey:@"data"])
        {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
            schemeCodes=[[[self.zambalaResponseDataDictionary objectForKey:@"data"] objectForKey:@"data"] componentsJoinedByString:@","];
            [self getData];
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
        
     }];
}


-(void)getData
{
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getSchemeDetails"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",cid,sessionid,schemeCodes];
        [inputArray addObject:[NSString stringWithFormat:@"%@getschemedetails_multiple_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.dataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.dataDictionary);
            if(self.dataDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.fundWatchTableView reloadData];
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

-(void)callMethod
{
    //dispatch_async(dispatch_get_main_queue(), ^{
    self.fundWatchTableView.delegate=self;
    self.fundWatchTableView.dataSource=self;
    [self.fundWatchTableView reloadData];
    //});
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        FundWatchTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FundWatchTableViewCell" forIndexPath:indexPath];
        cell.investNowButton.layer.cornerRadius=15.0f;
        cell.investNowButton.layer.masksToBounds = NO;
        NSString * schemeName = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"S_NAME"]];
        cell.fundNameLabel.text = schemeName;
        NSString * percent = @"%";
        NSString * threeYearReturn = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"3YEARRET"]];
        if([threeYearReturn isEqual:[NSNull null]]||[threeYearReturn isEqualToString:@"<null>"])
        {
            threeYearReturn = @"N/A";
        }else
        {
            threeYearReturn = [NSString stringWithFormat:@"%.2f",[threeYearReturn floatValue]];
        }
        cell.threeYearReturn.text = [threeYearReturn stringByAppendingString:percent];
        NSString * rupee = @"₹";
        NSString * fundSize = [NSString stringWithFormat:@"%.2f",[[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"fundsize"]floatValue]];
        cell.fundSizeLabel.text = [rupee stringByAppendingString:fundSize];
        NSString * nav = [NSString stringWithFormat:@"%.2f",[[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"NAVRS"]floatValue]];
        cell.navLabel.text = [rupee stringByAppendingString:nav];
        NSString * navPercent = [NSString stringWithFormat:@"%.2f",[[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"change"]floatValue]];
        cell.navChangePercentLabel.text = navPercent;
        NSString * navDate = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"NAVDATE"]];
        cell.navDateLabel.text = navDate;
        NSString * minInvest = [NSString stringWithFormat:@"Min Investment: %@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"MININVT"]];
        cell.minInvestmentLabel.text = minInvest;
        [cell.investNowButton addTarget:self action:@selector(onInvestNowButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell.fundWatchButton addTarget:self action:@selector(onFundWatchButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } @catch (NSException *exception) {
        
    } @finally {

    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 220;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self networkStatus]==YES)
    {
    EquityDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
    detail.schemeCodeString = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"SCHEMECODE"]];
    detail.fundNameString = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"S_NAME"]];
    [self.navigationController pushViewController:detail animated:YES];
    }
}

-(void)onInvestNowButtonTap:(UIButton*)sender
{
    if([self networkStatus]==YES)
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.fundWatchTableView];
    NSIndexPath *indexPath = [self.fundWatchTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    OrderConfirmationViewController * order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderConfirmationViewController"];
    order.schemeCode=[NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"SCHEMECODE"]];
    [self.navigationController pushViewController:order animated:YES];
    }
}

-(void)onFundWatchButtonTap:(UIButton*)sender
{
    if([self networkStatus]==YES)
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.fundWatchTableView];
    NSIndexPath *indexPath = [self.fundWatchTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    [self indicatorMethod];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid;
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    if([guestCheck isEqualToString:@"0"])
    {
        cid = [userSessionData objectForKey:@"zenwiseID"];
    }else if([guestCheck isEqualToString:@"1"])
    {
        cid = [userSessionData objectForKey:@"cid"];
    }
   // cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"zenwiseID"]];
    NSString * schemeCodeNew = [NSString stringWithFormat:@"%@",[[[[self.dataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"SCHEMECODE"]];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"addfundwatch"];
    [inputArray addObject:[NSString stringWithFormat:@"%@fundswatch",mfSharedManager.zambalaBaseUrl]];
    [inputArray addObject:@"3"];
    [inputArray addObject:@[schemeCodeNew]];
    [inputArray addObject:cid];
    [mfSharedManager zambalaPOSTRequestMethod :inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.zambalaResponseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        NSLog(@"%@",self.zambalaResponseDataDictionary);
        if([[[self.zambalaResponseDataDictionary objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
                [self getSymbolsList];
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alert animated:YES completion:^{
                }];
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                }];
                [alert addAction:okAction];
            });
        }
    }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onPlusButtonTap:(id)sender {
    
    AddFundWatchViewController * add = [self.storyboard instantiateViewControllerWithIdentifier:@"AddFundWatchViewController"];
    [self.navigationController pushViewController:add animated:YES];
}

- (IBAction)onFilterTap:(id)sender {
    
    FilterViewController * filter = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    filter.check=@"fundwatch";
    [self.navigationController pushViewController:filter animated:YES];
}

@end

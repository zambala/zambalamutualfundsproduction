//
//  FilterViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 07/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *chooseFilterButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UIButton *fundShowAllButton;
@property (weak, nonatomic) IBOutlet UIButton *allFundsButton;
@property (weak, nonatomic) IBOutlet UIButton *recommendedFundsButton;
@property (weak, nonatomic) IBOutlet UIButton *lumpSumButton;
@property (weak, nonatomic) IBOutlet UIButton *sipButton;
@property (weak, nonatomic) IBOutlet UITextField *minInvestmentTF;
@property (weak, nonatomic) IBOutlet UISlider *minInvestmentSlider;

@property (weak, nonatomic) IBOutlet UIButton *riskAll;
@property (weak, nonatomic) IBOutlet UIButton *modLowButton;
@property (weak, nonatomic) IBOutlet UIButton *modHighButton;
@property (weak, nonatomic) IBOutlet UIButton *riskLow;
@property (weak, nonatomic) IBOutlet UIButton *riskModerate;
@property (weak, nonatomic) IBOutlet UIButton *amcShowAllButton;
@property (weak, nonatomic) IBOutlet UIButton *assetAllButton;
@property (weak, nonatomic) IBOutlet UIButton *equityButton;
@property (weak, nonatomic) IBOutlet UIButton *hybridButton;
@property (weak, nonatomic) IBOutlet UIButton *debtButton;
@property (weak, nonatomic) IBOutlet UIButton *growthButton;
@property (weak, nonatomic) IBOutlet UIButton *dividendButton;
@property (weak, nonatomic) IBOutlet UIButton *taxButton;
@property (weak, nonatomic) IBOutlet UIButton *nonTaxButton;
@property (weak, nonatomic) IBOutlet UIButton *applyButton;
@property NSString * check;

@end

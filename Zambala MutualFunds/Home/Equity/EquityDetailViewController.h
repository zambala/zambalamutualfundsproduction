//
//  EquityDetailViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 20/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "GraphKit.h"
#import "MKSHorizontalLineProgressView.h"
#import "KATCircularProgress.h"
#import "LMLineGraphView.h"
#import "Reachability.h"

@interface EquityDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,GKLineGraphDataSource>
@property (weak, nonatomic) IBOutlet UIButton *oneMonthButton;
@property (weak, nonatomic) IBOutlet UIButton *threeMonthButton;
@property (weak, nonatomic) IBOutlet UIButton *sixMonthButton;
@property (weak, nonatomic) IBOutlet UIButton *oneYearButton;
@property (weak, nonatomic) IBOutlet UIButton *threeYearButton;
@property (weak, nonatomic) IBOutlet UIButton *investNowButton;
@property (weak, nonatomic) IBOutlet UIView *downView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *downViewHeightConstratint;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;

@property (weak, nonatomic) IBOutlet UITableView *performanceTableView;
@property (weak, nonatomic) IBOutlet UITableView *fundPerformanceTableView;
@property (weak, nonatomic) IBOutlet UIView *performanceView;
@property (weak, nonatomic) IBOutlet UIView *allocationView;

@property (weak, nonatomic) IBOutlet UIImageView *graphImageView;
@property (weak, nonatomic) IBOutlet LMLineGraphView *pointGraph;


@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray *labels;
@property (weak, nonatomic) IBOutlet MKSHorizontalLineProgressView *horizantalProgressLineView;
@property (weak, nonatomic) IBOutlet KATCircularProgress *circularProgressView;
@property (weak, nonatomic) IBOutlet UIView *segmentView;

@property NSString * schemeCodeString;
@property NSMutableDictionary * responseDataDictionary;
@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *threeYearReturnLabel;
@property (weak, nonatomic) IBOutlet UILabel *fundSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;
@property (weak, nonatomic) IBOutlet UILabel *navPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *lumpSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *sipLabel;
@property (weak, nonatomic) IBOutlet UILabel *inceptionDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *schemeTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *highestLabel;
@property (weak, nonatomic) IBOutlet UILabel *expenseRatioLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *downFundSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *exitLoadLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxReferenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *fundManagerLabel;
@property (weak, nonatomic) IBOutlet UIButton *peerCategoryButton;

@property NSMutableDictionary * peerCategoryDictionary;
@property NSString * yearLabel;

@property NSString * fundNameString;

@property Reachability * reach;


@end

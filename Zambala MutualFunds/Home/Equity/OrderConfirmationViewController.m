//
//  OrderConfirmationViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 30/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "OrderConfirmationViewController.h"
#import "PaymentViewController.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "AppDelegate.h"
#import "CreateSIPMandateViewController.h"
#import "PaymentWebViewController.h"
@import Mixpanel;

@interface OrderConfirmationViewController ()
{
    Utility * mfSharedManager;
    NSArray * sipDatesDetailArray;
    NSArray * sipArrayOne;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * indicatorView;
    AppDelegate * delegate;
    NSString * UMRNstring;
    NSString * sysID;
    Mixpanel * mixpanelFunds;
    NSString * minInvestment;
    NSString * minMonths;
}

@end

@implementation OrderConfirmationViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    mfSharedManager=[Utility MF];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    
    [mixpanelFunds track:@"order_page"];
    
    indicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    indicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:indicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [indicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    self.continueButton.layer.cornerRadius = 20.0f;
    self.acceptButton.layer.cornerRadius = 20.0f;
    
    if([self.check isEqualToString:@"portfolio"])
    {
        self.topViewPortfolio.hidden=NO;
        self.topView.hidden=YES;
        self.topViewPortfolio.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.topViewPortfolio.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.topViewPortfolio.layer.shadowOpacity = 3.0f;
        self.topViewPortfolio.layer.shadowRadius = 3.0f;
        self.topViewPortfolio.layer.cornerRadius=5.0f;
        self.topViewPortfolio.layer.masksToBounds = NO;
        
    }else
    {
        self.topViewPortfolio.hidden=YES;
        self.topView.hidden=NO;
    }
    
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.topView.layer.shadowOpacity = 3.0f;
    self.topView.layer.shadowRadius = 3.0f;
    self.topView.layer.cornerRadius=5.0f;
    self.topView.layer.masksToBounds = NO;
    
    
    self.bottomView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.bottomView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.bottomView.layer.shadowOpacity = 3.0f;
    self.bottomView.layer.shadowRadius = 3.0f;
    self.bottomView.layer.cornerRadius=5.0f;
    self.bottomView.layer.masksToBounds = NO;
    
    self.pickerView.hidden=YES;
    
    self.popUpView.hidden=YES;
    self.popUpView.tag=99;
    [self.sipLumpsumSwitch setOn:NO];
     self.bottomViewHeight.constant = 376;

//        self.dayTF.text = [NSString stringWithFormat:@"%i",(int)self.daySlider.value];
//        self.amountTF.text = [NSString stringWithFormat:@"%i",(int)self.investSlider.value];
//        self.monthsTF.text = [NSString stringWithFormat:@"%i",(int)self.monthsSlider.value];
    
    
    [self.continueButton addTarget:self action:@selector(onContinueButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.sipLumpsumSwitch addTarget:self action:@selector(onSwitchChange:) forControlEvents:UIControlEventValueChanged];
    [self.acceptButton addTarget:self action:@selector(onAcceptButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.createMandateButton addTarget:self action:@selector(onCreateMandateTap) forControlEvents:UIControlEventTouchUpInside];
    [self.mandateSelectButton addTarget:self action:@selector(onMandateButtonTapNew) forControlEvents:UIControlEventTouchUpInside];
    [self.sipDayButton addTarget:self action:@selector(onSipDayTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    [self onLumpSumTap];
    [self getOrderDetails];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [indicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [indicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)onSipDayTap
{
    [self.amountTF resignFirstResponder];
    [self.monthsTF resignFirstResponder];
    self.sipDayButton.selected = YES;
    self.pickerView.hidden=NO;
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    [self.pickerView reloadAllComponents];
}

-(void)onSwitchChange:(id)sender
{
    if([sender isOn])
    {
//        Fund Id, Fund Name
//        Fund Id, Fund Name, Amount
        [mixpanelFunds track:@"order_page" properties:@{@"Fund Id":self.schemeCode,@"Fund Name":self.fundNameLabel.text}];
        self.bottomViewHeight.constant = 198;
        self.sipViewHeightConstraint.constant = 0;
        self.sipView.hidden = YES;
    }else
    {
        [mixpanelFunds track:@"order_lumpsum_page" properties:@{@"Fund Id":self.schemeCode,@"Fund Name":self.fundNameLabel.text,@"Amount":self.amountTF.text}];
        self.bottomViewHeight.constant = 376;
        self.sipViewHeightConstraint.constant = 194;
        self.sipView.hidden = NO;
    }
}

-(void)onCreateMandateTap
{
    if([self networkStatus]==YES)
    {
    CreateSIPMandateViewController * mandate = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateSIPMandateViewController"];
    [self.navigationController pushViewController:mandate animated:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if ([self.check isEqualToString:@"rec"])
    {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }else
    {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}


-(void)getOrderDetails
{
    @try {
        
    
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:@"getAmcList"];
            NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",self.schemeCode,cid,sessionid];
            [inputArray addObject:[NSString stringWithFormat:@"%@get_shemeinfo_for_invest_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
            [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                if([[[dict objectForKey:@"data"] objectForKey:@"Table"] count]>0)
                {
                self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
                NSLog(@"%@",self.responseDataDictionary);
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString * isPurchaseAvailable = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"IsPurchaseAvailable"]];
                    if([isPurchaseAvailable isEqualToString:@"N"])
                    {
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Oops" message:@"This fund is currently not available for any purchase" preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                        }];
                        
                        
                        UIAlertAction * ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [activityIndicatorView stopAnimating];
                            [indicatorView removeFromSuperview];
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
                        
                        [alert addAction:ok];
                    }else
                    {
                        @try {
                            
                            [self onMandateButtonTap];
                            
                    NSString * sipdates =[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"sipdate"]];
                    minInvestment = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"MININVT"]];
                            NSString * string = [NSString stringWithFormat:@"Min.₹ %@",minInvestment];
                            self.amountTF.placeholder = string;
                            
                    NSArray * sipDatesArray = [sipdates componentsSeparatedByString:@"|"];
                    NSLog(@"%@",sipDatesArray);
                    NSString * sipDatesDetailString = [sipDatesArray objectAtIndex:2];
                    NSArray * sipDetailsArray = [sipDatesDetailString componentsSeparatedByString:@"~"];
                    NSLog(@"%@",sipDetailsArray);
                    NSString * sipDatesString = [NSString stringWithFormat:@"%@",[sipDetailsArray objectAtIndex:1]];
                    sipDatesDetailArray = [sipDatesString componentsSeparatedByString:@";"];
                    
                            NSString * sipDetailsString = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"sipdtls"]];
                            NSArray * sipDetailsArrayOne = [sipDetailsString componentsSeparatedByString:@"|"];
                            NSString * stringOne = [sipDetailsArrayOne objectAtIndex:1];
                            NSArray * sipDetailsArrayTwo = [stringOne componentsSeparatedByString:@"~"];
                            minMonths = [NSString stringWithFormat:@"%@",[sipDetailsArrayTwo objectAtIndex:3]];
                            NSString * UIString = [NSString stringWithFormat:@"Min. %@",minMonths];
                            self.monthsTF.placeholder = UIString;
                            
                        } @catch (NSException *exception) {
                            
                        } @finally {
                            
                        }
                    //Number of months
                    NSString * sipDetails =[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"sipdtls"]];
                    if([sipDetails isEqual:[NSNull null]]||[sipDetails isEqualToString:@"<null>"])
                    {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"SIP Investment not available for this fund." preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert animated:YES completion:^{
                                
                            }];
                            
                            
                            
                            UIAlertAction * ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                [activityIndicatorView stopAnimating];
                                [indicatorView removeFromSuperview];
                                [self.navigationController popViewControllerAnimated:YES];
                            }];
                            
                            [alert addAction:ok];
                            
                        });
                    
                    }else
                    {
                        NSArray * sipArray = [sipDetails componentsSeparatedByString:@"|"];
                        NSString * sipDetailsSrting = [sipArray objectAtIndex:1];
                        sipArrayOne = [sipDetailsSrting componentsSeparatedByString:@"~"];
                        NSString * sipCheck = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"SIP"]];
                        if([sipCheck isEqualToString:@"T"])
                        {
                            [self.sipLumpsumSwitch setOn:YES];
                            [self onSwitchChange:self.sipLumpsumSwitch];
                            
                        }else
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"SIP Investment not available for this fund." preferredStyle:UIAlertControllerStyleAlert];
                                
                                [self presentViewController:alert animated:YES completion:^{
                                    
                                }];
                                
                                
                                
                                UIAlertAction * ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    [activityIndicatorView stopAnimating];
                                    [indicatorView removeFromSuperview];
                                    [self.navigationController popViewControllerAnimated:YES];
                                }];
                                
                                [alert addAction:ok];
                                
                            });
                        }
                        [self assignUI];
                    }
                    }
                });
                }else
                {
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Oops" message:@"Data not found! Please try again later." preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                    }];
                    
                    
                    UIAlertAction * ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [indicatorView removeFromSuperview];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                    [alert addAction:ok];
                }
                
            }];
        });
    }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [activityIndicatorView stopAnimating];
    [indicatorView removeFromSuperview];
}

-(void)onContinueButtonTap
{
    [self.amountTF resignFirstResponder];
    [self.monthsTF resignFirstResponder];
    if(self.sipLumpsumSwitch.on==NO)
    {
        int amountInt = [self.amountTF.text intValue];
        int monthsInt = [self.monthsTF.text intValue];
        int amountStringInt = [minInvestment intValue];
        int monthsStringInt = [minMonths intValue];
        if(self.amountTF.text.length==0||(self.monthsTF.text.length==0||amountInt<amountStringInt||monthsInt<monthsStringInt))
        {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Please enter correct values." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [activityIndicatorView stopAnimating];
                [indicatorView removeFromSuperview];
            }];
            
            [alert addAction:okAction];
        });
    }else
    {
    if(UMRNstring.length!=0)
    {
        [UIView transitionWithView:self.popUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpView.hidden = NO;
                        }
                        completion:NULL];
    }else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Please select mandate or create one." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [activityIndicatorView stopAnimating];
                [indicatorView removeFromSuperview];
            }];
            
            [alert addAction:okAction];
        });
    }
    }
    }else
    {
        int amountInt = [self.amountTF.text intValue];
        int amountStringInt = [minInvestment intValue];
        if(self.amountTF.text.length!=0&&amountInt>=amountStringInt)
        {
        [UIView transitionWithView:self.popUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpView.hidden = NO;
                        }
                        completion:NULL];
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Please enter correct values." preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [indicatorView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.popUpView.hidden=YES;
    }
}


-(void)insertSIPCart
{
    if([self networkStatus]==YES)
    {
    indicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    indicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:indicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [indicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSString * dayString = [NSString stringWithFormat:@"%@-%ld-%ld",self.sipDayButton.titleLabel.text,(long)month,(long)year];
    NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"d-M-yyy"];
    NSDate *yourDate = [dateFormatter dateFromString:dayString];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *someDate = [cal dateByAddingUnit:NSCalendarUnitMonth value:2 toDate:yourDate options:0];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString * dateString = [dateFormatter stringFromDate:someDate];
    
    NSString * divFlag = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"DividendOptionFlag"]];
    if([divFlag isEqualToString:@"b"])
    {
        divFlag = @"Y";
    }else
    {
        divFlag=divFlag;
    }
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSString * schemeCode;
        NSString * folioNumber;
        if([self.check isEqualToString:@"portfolio"])
        {
            schemeCode = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Scheme Code"]];
            folioNumber = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Folio_No"]];
        }else
        {
            schemeCode = self.schemeCode;
            folioNumber = @"NEW";
        }

        NSMutableArray * inputArray=[[NSMutableArray alloc]init];

        [inputArray addObject:@"insertSIPCart"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@",cid,sessionid,schemeCode,folioNumber,self.amountTF.text,divFlag,self.monthsTF.text,@"Monthly",dateString];
        [inputArray addObject:[NSString stringWithFormat:@"%@inssiporder_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            if(self.responseDataDictionary.count>0)
            {
               // for (int i=0;[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count]; i++) {
                    NSString * schemeCodeServer = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"schemecode"]];
                    if([schemeCodeServer isEqualToString:schemeCode])
                    {
                        sysID = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"sysid"]];
                    }
                //}
                if(sysID.length!=0)
                {
                [self executeSIPCart];
                }
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{


                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];

                    [self presentViewController:alert animated:YES completion:^{

                    }];

                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [indicatorView removeFromSuperview];
                    }];

                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
    }
}

-(void)insertLumpSumCart
{
    if([self networkStatus]==YES)
    {
    NSString * divFlag = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"DividendOptionFlag"]];
    if([divFlag isEqualToString:@"b"])
    {
        divFlag = @"Y";
    }else
    {
        divFlag=divFlag;
    }
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSString * schemeCode;
        NSString * folioNumber;
        if([self.check isEqualToString:@"portfolio"])
        {
            schemeCode = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Scheme Code"]];
            folioNumber = [NSString stringWithFormat:@"%@",[[delegate.transactionDetailsArray objectAtIndex:0] objectForKey:@"Folio_No"]];
        }else
        {
            schemeCode = self.schemeCode;
            folioNumber = @"NEW";
        }
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"insertLumpsumCart"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@",cid,sessionid,schemeCode,folioNumber,self.amountTF.text,divFlag];
        [inputArray addObject:[NSString stringWithFormat:@"%@inslumpsumorder_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            if(self.responseDataDictionary.count>0)
            {
                //for (int i=0;[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count]; i++) {
                    NSString * schemeCodeServer = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"schemecode"]];
                    if([schemeCodeServer isEqualToString:schemeCode])
                    {
                        sysID = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"sysid"]];
                    }
               // }
                if(sysID.length!=0)
                {
                    [self executeLumpSumCart];
                }
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [indicatorView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
    }
}

-(void)executeLumpSumCart
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        
        [inputArray addObject:@"excuteLumpsumCart"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@",cid,sessionid,UMRNstring,sysID];
        [inputArray addObject:[NSString stringWithFormat:@"%@execute_lumpsumbasket_all_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            if(self.responseDataDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [indicatorView removeFromSuperview];
                NSString * url = [NSString stringWithFormat:@"%@",[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"result"]];
                NSString * finalURL = [mfSharedManager.baseUrl stringByAppendingString:url];
                PaymentWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentWebViewController"];
                webView.loadURL = finalURL;
                [self.navigationController pushViewController:webView animated:YES];
                 });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [indicatorView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

-(void)executeSIPCart
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        
        [inputArray addObject:@"excuteSIPCart"];
        NSString * inputString = [NSString stringWithFormat:@"%@|%@|%@|%@",cid,sessionid,UMRNstring,sysID];
        [inputArray addObject:[NSString stringWithFormat:@"%@execute_sipbasket_all_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputString]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [indicatorView removeFromSuperview];
            });
            if(self.responseDataDictionary.count>0)
            {
                
                NSString * url = [NSString stringWithFormat:@"%@",[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"result"]];
                NSString * finalURL = [mfSharedManager.baseUrl stringByAppendingString:url];
                PaymentWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentWebViewController"];
                webView.loadURL = finalURL;
                [self.navigationController pushViewController:webView animated:YES];
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [indicatorView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

-(void)onAcceptButtonTap
{
    if(self.sipLumpsumSwitch.on==NO)
    {
    [self insertSIPCart];
    }else
    {
        [self insertLumpSumCart];
    }
}

-(void)onSipButtonTap
{
    NSString * sipCheck = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"SIP"]];
    if([sipCheck isEqualToString:@"T"])
    {
    
        //[self assignUI];
    }else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"SIP Investment not available for this fund." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            
            
            UIAlertAction * ok=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:ok];
            
        });
    }
}

-(void)onLumpSumTap
{
    
    //[self assignUI];
}

-(void)assignUI
{
    dispatch_async(dispatch_get_main_queue(), ^{
    if([self.check isEqualToString:@"portfolio"])
    {
        self.fundNameLabel.text = [NSString stringWithFormat:@"%@",[[delegate.portfolioDataArray objectAtIndex:0] objectForKey:@"Scheme Name"]];
        self.folioNumberLabel.text = [NSString stringWithFormat:@"Folio %@",[[delegate.portfolioDataArray objectAtIndex:0] objectForKey:@"Folio_No"]];
        self.currentValueLabel.text = [NSString stringWithFormat:@"₹%.2f",[[[delegate.portfolioDataArray objectAtIndex:0] objectForKey:@"Present Value"]floatValue]];
        self.unitsLabel.text = [NSString stringWithFormat:@"%.2f",[[[delegate.portfolioDataArray objectAtIndex:0] objectForKey:@"Present Units"]floatValue]];
    }else
    {
    self.orderFundNameLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"S_NAME"]];
    
    self.threeYearReturnLabel.text = [NSString stringWithFormat:@"%.2f",[[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"3YEARRET"]floatValue]];
    self.fundSizeLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"fundsize"]];
     self.navLabel.text = [NSString stringWithFormat:@"₹%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"NAVRS"]];
    self.navChangeLabel.text = [NSString stringWithFormat:@"%.2f",[[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"change"]floatValue]];
    self.dateLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"NAVDATE"]];
    self.minInvestmentLabel.text = [NSString stringWithFormat:@"₹%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"MININVT"]];
    }
    if(self.sipLumpsumSwitch.on==YES)
    {
    NSString * myMonthString;
    NSString * myYearString;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMM"];
    myMonthString = [df stringFromDate:[NSDate date]];
    
    [df setDateFormat:@"yyyy"];
    myYearString = [df stringFromDate:[NSDate date]];
//    self.sipDateLabel.text = [NSString stringWithFormat:@"SIP starts from %@ %@,%@",self.dayTF.text,myMonthString,myYearString];
//
//    int Amount = (int)[(NSNumber *)[sipArrayOne objectAtIndex:1] integerValue];
//    int multiple = (int)[(NSNumber *)[sipArrayOne objectAtIndex:2] integerValue];
//    int startDate = (int)[(NSNumber *)[sipArrayOne objectAtIndex:3] integerValue];
//    int endDate = (int)[(NSNumber *)[sipArrayOne objectAtIndex:4] integerValue];
//    self.minValueInvest.text = [NSString stringWithFormat:@"%d",Amount];
//    self.maximumValueInvest.text = [NSString stringWithFormat:@"%d",(Amount*multiple*101)];
//        self.monthsLeftLabel.text = [NSString stringWithFormat:@"%d",startDate];
//        self.monthsRightLabel.text = [NSString stringWithFormat:@"%d",endDate];
    }
    //else if (self.lumpSumButton.selected==YES)
    //{
//        int Amount = (int)[(NSNumber *)[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"MININVT"] integerValue];
//        int multiple = (int)[(NSNumber *)[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"MULTIPLES"] integerValue];
//        self.minValueInvest.text = [NSString stringWithFormat:@"%d",Amount];
//        self.maximumValueInvest.text = [NSString stringWithFormat:@"%d",(Amount*multiple*101)];
//        self.amountTF.text = self.minValueInvest.text;
//        self.lumpSumButton.selected=YES;
//        [self onLumpSumTap];
//        self.sipButton.selected = NO;
//        [self onSipButtonTap];
  //  }
    
//        [self onSliderChange:self.investSlider];
//        [self onSliderChange:self.monthsSlider];
//        [self onSliderChange:self.daySlider];
    [activityIndicatorView stopAnimating];
    [indicatorView removeFromSuperview];
     });
}

-(void)onMandateButtonTapNew
{
    self.pickerView.hidden=NO;
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    [self.pickerView reloadAllComponents];
}

-(void)onMandateButtonTap
{
    [self.amountTF resignFirstResponder];
    [self.monthsTF resignFirstResponder];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getMandateList"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@",cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@get_mandate_list_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.mandateDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.mandateDataDictionary);
            if(self.mandateDataDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                UMRNstring = [NSString stringWithFormat:@"%@",[[[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"UMRN"]];
                NSString * title = [NSString stringWithFormat:@"%@-%@",[[[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"SIPType"],[[[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:0] objectForKey:@"UMRN"]];
                 [self.mandateSelectButton setTitle:title forState:UIControlStateNormal];
                
                 });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [indicatorView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;  // Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    if(self.sipDayButton.selected==YES)
    {
        return sipDatesDetailArray.count;
    }else
    {
    return [[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count];
    }
    return 0;
    //Or, return as suitable for you...normally we use array for dynamic
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * title;
    if(self.sipDayButton.selected == YES)
    {
        title = [NSString stringWithFormat:@"%@",[sipDatesDetailArray objectAtIndex:row]];
    }else
    {
     title = [NSString stringWithFormat:@"%@-%@",[[[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:row] objectForKey:@"SIPType"],[[[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:row] objectForKey:@"UMRN"]];
    }
    return title;//Or, your suitable title; like Choice-a, etc.
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    self.pickerView.hidden=YES;
    if(self.sipDayButton.selected ==YES)
    {
        NSString * title = [NSString stringWithFormat:@"%@",[sipDatesDetailArray objectAtIndex:row]];
        [self.sipDayButton setTitle:title forState:UIControlStateNormal];
        self.sipDayButton.selected = NO;
    }else
    {
    UMRNstring = [NSString stringWithFormat:@"%@",[[[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:row] objectForKey:@"UMRN"]];
    NSString * title = [NSString stringWithFormat:@"%@-%@",[[[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:row] objectForKey:@"SIPType"],[[[[self.mandateDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:row] objectForKey:@"UMRN"]];
    [self.mandateSelectButton setTitle:title forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

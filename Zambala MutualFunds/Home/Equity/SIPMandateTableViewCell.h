//
//  SIPMandateTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 28/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIPMandateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *someLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tickImageView;

@end

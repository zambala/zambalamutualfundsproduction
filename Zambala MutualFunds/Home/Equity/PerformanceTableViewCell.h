//
//  PerformanceTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 22/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PerformanceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *returnLabel;

@end

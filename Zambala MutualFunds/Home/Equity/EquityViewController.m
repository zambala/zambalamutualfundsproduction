//
//  EquityViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 18/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EquityViewController.h"
#import "HMSegmentedControl.h"
#import "PageImageScrollView.h"
#import "ZambalaRecCollectionViewCell.h"
#import "EquityTableViewCell.h"
#import "EquityDetailViewController.h"
#import "OrderConfirmationViewController.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "TAPageControl.h"
#import <MXParallaxHeader/MXScrollView.h>
#import "UIScrollView+APParallaxHeader.h"
@import Mixpanel;
#import "AppDelegate.h"




@interface EquityViewController ()<TAPageControlDelegate,MXScrollViewDelegate,APParallaxViewDelegate>
{
    HMSegmentedControl * segmentedControl;
    NSMutableArray * imageArray;
    NSMutableArray * testArray;
    CGFloat width_Cell;
    NSString * catagory;
    NSString * assetType;
    NSString * amc;
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    NSInteger index;
    CGPoint endPoint;
    CGPoint scrollingPoint;
    int Value;
    Mixpanel * mixpanelFunds;
    AppDelegate * delegate;
    BOOL rehitBool;
    UIRefreshControl * refreshControl1;
    NSNumber * offset;
    NSMutableArray * responseArray;
}



@end



@implementation EquityViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    mfSharedManager=[Utility MF];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    width_Cell = 0.0;
    rehitBool = true;
    page = 1;
     amc=@"all";
    catagory=@"all";
    offset=[NSNumber numberWithInt:1];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    NSMutableArray * localSegmentsArray = [[NSMutableArray alloc]init];
    testArray = [[NSMutableArray alloc]init];
    responseArray = [[NSMutableArray alloc]init];

    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    
    if([self.typeOfButton isEqualToString:@"equity"])
    {
        [mixpanelFunds track:@"equity_all_page"];
    [localSegmentsArray addObject:@"All"];
    [localSegmentsArray addObject:@"LARGE CAP"];
    [localSegmentsArray addObject:@"MID CAP"];
    [localSegmentsArray addObject:@"SMALL CAP"];
    [localSegmentsArray addObject:@"MULTI CAP"];
    [localSegmentsArray addObject:@"SCETOR"];
        self.navigationItem.title = @"Equity";
    }else  if([self.typeOfButton isEqualToString:@"debt"])
    {
        [mixpanelFunds track:@"debt_all_page"];
        [localSegmentsArray addObject:@"All"];
        [localSegmentsArray addObject:@"LIQUID FUNDS"];
        [localSegmentsArray addObject:@"ULTRA SHORT TERM"];
        [localSegmentsArray addObject:@"SHORT TERM"];
        [localSegmentsArray addObject:@"MEDIUM TERM"];
        [localSegmentsArray addObject:@"LONG TERM"];
        [localSegmentsArray addObject:@"FMP"];
        self.navigationItem.title = @"Debt";
    }else  if([self.typeOfButton isEqualToString:@"hybrid"])
    {
        [mixpanelFunds track:@"hybrid_all_page"];
        [localSegmentsArray addObject:@"ALL"];
        [localSegmentsArray addObject:@"EQUITY SAVINGS"];
        [localSegmentsArray addObject:@"BALANCED"];
        [localSegmentsArray addObject:@"MIP"];
        [localSegmentsArray addObject:@"DEBT ORIENTED"];
        [localSegmentsArray addObject:@"EQUITY ORIENTED"];
        [localSegmentsArray addObject:@"ASSET ALLOCATION FUNDS"];
        self.navigationItem.title = @"Hybrid";
    }else  if([self.typeOfButton isEqualToString:@"taxsaver"])
    {
        [mixpanelFunds track:@"taxsaver_all_page"];
        self.segmentView.hidden=YES;
        self.segmentViewHeightConstraint.constant=0;
//        [localSegmentsArray addObject:@"All"];
//        [localSegmentsArray addObject:@"Liquid Funds"];
//        [localSegmentsArray addObject:@"Ultra Short Term"];
//        [localSegmentsArray addObject:@"Medium Term"];
//        [localSegmentsArray addObject:@"Long Term"];
//        [localSegmentsArray addObject:@"FMP"];
        self.navigationItem.title = @"Tax Saver";
    }else  if([self.typeOfButton isEqualToString:@"others"])
    {
        [mixpanelFunds track:@"others_all_page"];
        [localSegmentsArray addObject:@"All"];
        [localSegmentsArray addObject:@"NFO"];
        [localSegmentsArray addObject:@"GOLD FUNDS"];
        [localSegmentsArray addObject:@"RETIREMENT FUNDS"];
        [localSegmentsArray addObject:@"CAPITAL PROTECTION FUNDS"];
        [localSegmentsArray addObject:@"ETF's"];
        [localSegmentsArray addObject:@"FUNDS OF FUNDS"];
        self.navigationItem.title = @"Others";
    }else if ([self.typeOfButton isEqualToString:@"smartmoney"])
    {
        [mixpanelFunds track:@"smartmoney_all_page"];
//        [localSegmentsArray addObject:@"All"];
//        [localSegmentsArray addObject:@"Liquid Funds"];
//        [localSegmentsArray addObject:@"Ultra Short Term"];
//        [localSegmentsArray addObject:@"Medium Term"];
        self.navigationItem.title = @"Smart Money";
    }
    
    if([self.typeOfButton isEqualToString:@"fundspicked"])
    {
        [mixpanelFunds track:@"funds_picked_page"];
        self.segmentView.hidden=YES;
        self.segmentViewHeightConstraint.constant= 0;
        self.topCollectionView.hidden=YES;
        self.topCollectionViewHeightConstraint.constant=0;
       // self.pageControl.hidden=YES;
        self.navigationItem.title = @"Funds Picked";
    }else
    {
        self.segmentView.hidden = NO;
        self.segmentViewHeightConstraint.constant = 44.6;
        self.topCollectionView.hidden = NO;
        self.topCollectionViewHeightConstraint.constant = 165.9;
       // self.pageControl.hidden=NO;
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.segmentView.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    [self.segmentView addSubview:segmentedControl];
    }
    
    Value=0;
    self.topCollectionView.pagingEnabled = YES;

    
    [testArray addObject:@"Test 1"];
    [testArray addObject:@"Test 2"];
    [testArray addObject:@"Test 3"];
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.topTapView addGestureRecognizer:tapRecognizer];
    self.topTapView.tag=2;
    self.scrollView.delegate=self;
    self.topTapView.hidden=YES;
//    self.scrollView.pagingEnabled=YES;
   [self.equityTableView setScrollEnabled:true];
//    [self.scrollView setScrollEnabled:true];
    
   // [self.view addSubview:self.MXScrollView];
   
    
    //self.MXScrollView.delegate = self;
    self.collectionMainView.contentMode = UIViewContentModeScaleAspectFill;
    self.MXScrollView.parallaxHeader.minimumHeight = 0;
    self.MXScrollView.parallaxHeader.view = self.collectionMainView; // You can set the parallax header view from a nib.
    self.MXScrollView.parallaxHeader.height = 200;
    self.MXScrollView.parallaxHeader.mode = MXParallaxHeaderModeFill;
    [self.MXScrollView addSubview:self.tableMainView];
    
    
//    [self.collectionMainView setContentMode:UIViewContentModeScaleAspectFill];
//    [self.equityTableView addParallaxWithView:self.collectionMainView andHeight:200];
//
//    [self.equityTableView.parallaxView setDelegate:self];
    
    [self.pageControl addTarget:self action:@selector(pageControlChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    
    
//    self.scrollView.parallaxHeader.minimumHeight = 20;
//    self.scrollView.parallaxHeader.view = self.collectionMainView; // You can set the parallax header view from a nib.
//    self.scrollView.parallaxHeader.height = 200;
//    self.scrollView.parallaxHeader.mode = MXParallaxHeaderModeFill;
//    [self.scrollView addSubview:self.tableMainView];
    
   // endPoint = CGPointMake(0, self.topCollectionView.frame.size.width);
    //scrollingPoint = CGPointMake(0, 0);
    //self.scrollingTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
//    self.scrollView.parallaxHeader.view = self.collectionMainView;
//    self.scrollView.parallaxHeader.height = 160;
//    self.scrollView.parallaxHeader.mode = MXParallaxHeaderModeFill;
//    self.scrollView.parallaxHeader.minimumHeight = 20;
//
//    self.equityTableView.parallaxHeader.view = self.headerView; // You can set the parallax header view from the floating view.
//    self.equityTableView.parallaxHeader.height = 600;
//    self.equityTableView.parallaxHeader.mode = MXParallaxHeaderModeFill;
//    self.equityTableView.parallaxHeader.delegate = self;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [activityIndicatorView stopAnimating];
        [backView removeFromSuperview];
    });
}

#pragma mark AutoScroll Methods

- (void)addTimer
{
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(nextPage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    self.timer = timer;
}

- (void)nextPage
{
    // 1.back to the middle of sections
    NSIndexPath *currentIndexPathReset = [self resetIndexPath];
    
    // 2.next position
    NSInteger nextItem = currentIndexPathReset.item + 1;
    NSInteger nextSection = currentIndexPathReset.section;
    if (nextItem == [[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"] count]) {
        nextItem = 0;
        nextSection++;
    }
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:nextSection];
    
    // 3.scroll to next position
    [self.topCollectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


- (NSIndexPath *)resetIndexPath
{
    // currentIndexPath
    NSIndexPath *currentIndexPath = [[self.topCollectionView indexPathsForVisibleItems] lastObject];
    // back to the middle of sections
    NSIndexPath *currentIndexPathReset = [NSIndexPath indexPathForItem:currentIndexPath.item inSection:0 / 2];
    [self.topCollectionView scrollToItemAtIndexPath:currentIndexPathReset atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    return currentIndexPathReset;
}

- (void)removeTimer
{
    // stop NSTimer
    [self.timer invalidate];
    // clear NSTimer
    self.timer = nil;
}

// UIScrollView' delegate method
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
   // [self removeTimer];
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //[self addTimer];
    
}

- (void)pageControlChanged:(id)sender
{
    CGFloat pageWidth = self.topCollectionView.frame.size.width;
    CGPoint scrollTo = CGPointMake(pageWidth * self.pageControl.currentPage, 0);
    [self.topCollectionView setContentOffset:scrollTo animated:YES];
}
//-(void)onTimer
//{
//    self.topCollectionView.contentOffset = scrollingPoint;
//    if (CGPointEqualToPoint(scrollingPoint, endPoint)) {
//       // [self.scrollingTimer invalidate];
//        [self.topCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
//                                    atScrollPosition:UICollectionViewScrollPositionTop
//                                            animated:YES];
//    }
//    scrollingPoint = CGPointMake(scrollingPoint.x+1, 0);
//
//    [self.topCollectionView setContentOffset:CGPointMake(self.collectionMainView.frame.size.width+10,0) animated:YES];
//}

-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
        if(sender.view.tag==2) {
            //do something here
            self.topTapView.hidden = YES;
            self.topUnTapView.hidden = NO;
            [self.MXScrollView setContentOffset:CGPointZero animated:YES];
            //self.backMainView.frame = CGRectMake( 0,0, self.backMainView.frame.size.width, self.backMainView.frame.size.height ); // set new position exactl
            //[self.backMainView convertPoint:self.backMainView.frame.origin toView:self.view];
            

            //self.backMainView.center = CGPointZero;
        }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectionMainView.frame.size.width, self.collectionMainView.frame.size.height);
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect frame = self.backMainView.frame;
    //Update scroll view frame and content size
    self.MXScrollView.frame = frame;
    self.MXScrollView.contentSize = frame.size;
    //Update table 1 frame
    frame.size.width /= 1;
    frame.size.height += self.MXScrollView.parallaxHeader.minimumHeight;
    self.equityTableView.frame = frame;
    
   
//    frame.origin.x = frame.size.width;
//    self.equityTableView.frame = frame;

//    frame.origin.x = frame.size.width;
//    self.equityTableView.frame = frame;
    //self.scrollView.frame = frame;
}

//- (void)viewDidLayoutSubviews {
//    [super viewDidLayoutSubviews];
//    self.scrollView.contentSize = self.MXScrollView.frame.size;
//    [self layoutChildViewController];
//}
//
//- (void)layoutChildViewController {
//    CGRect frame = self.scrollView.frame;
//    frame.origin = CGPointZero;
//    frame.size.height -= self.MXScrollView.parallaxHeader.minimumHeight;
//    self.equityTableView.frame = frame;
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    NSString * string = [NSString stringWithFormat:@"%f",self.scrollView.contentOffset.y];
    
   
    if([string containsString:@"-"])
    {
        self.topTapView.hidden= YES;
        self.topUnTapView.hidden = NO;
//        self.backMainViewHgt.constant = 550;
        
       
        //Update scroll view frame and content size
      
        //Update table 1 frame
//        CGRect frame = self.backMainView.frame;
//        self.equityTableView.frame = frame;
        
       
       
    }else
    {
        self.topTapView.hidden= NO;
        self.topUnTapView.hidden = YES;
//        self.backMainViewHgt.constant = self.equityTableView.contentSize.height;
//        CGRect frame = self.backMainView.frame;
//        self.equityTableView.frame = frame;
        
    }
    
    if(self.equityTableView.contentOffset.y >= (self.equityTableView.contentSize.height - self.equityTableView.bounds.size.height))
    {
        if(isPageRefreshing==NO){
            isPageRefreshing=YES;
           // [appDelegate showIndicator:nil view1:self.view];
            page++;
            [self getFundWatchList];
            //[self getAmcList:page++];
            //[self.equityTableView reloadData];
            NSLog(@"called %d",page);
        }
    }
    
    
//    ScrollDirection scrollDirection;
//
//    if (self.lastContentOffset > scrollView.contentOffset.y) {
//        scrollDirection = ScrollDirectionUp;
//        rehitBool=false;
//    } else if (self.lastContentOffset < scrollView.contentOffset.y) {
//        scrollDirection = ScrollDirectionDown;
//        rehitBool=true;
//    }
//
//    self.lastContentOffset = scrollView.contentOffset.y;
}
typedef NS_ENUM(NSInteger, ScrollDirection) {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
};

-(void)viewWillAppear:(BOOL)animated
{
    //self.scrollView.parallaxHeader.minimumHeight = self.topLayoutGuide.length;
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    self.equityTableView.delegate=self;
    self.equityTableView.dataSource=self;
    [self.equityTableView reloadData];
    
    self.topCollectionView.delegate=self;
    self.topCollectionView.dataSource=self;
    [self.topCollectionView reloadData];
    //[self callMethod];
    [self getFundWatchList];
}

-(void)accessMethod
{
    //amc=@"all";
    //catagory=@"all";
    if([self.typeOfButton isEqualToString:@"equity"])
    {
        assetType=@"Equities";
    }else if ([self.typeOfButton isEqualToString:@"debt"])
    {
        assetType=@"Debt";
    }else if ([self.typeOfButton isEqualToString:@"hybrid"])
    {
        assetType=@"Hybrid";
    }else if ([self.typeOfButton isEqualToString:@"taxsaver"])
    {
        assetType=@"Tax Saver";
        catagory=@"ELSS";
    }else if ([self.typeOfButton isEqualToString:@"others"])
    {
        assetType = @"Others";
    }
    
    [self getSchemeRecommendations];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)onSegmentTap
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    if([self.typeOfButton isEqualToString:@"equity"])
    {
        assetType=@"Equities";
        amc=@"all";
        if(segmentedControl.selectedSegmentIndex==0)
        {
         [mixpanelFunds track:@"equity_all_page"];
        catagory=@"all";
        }else if(segmentedControl.selectedSegmentIndex==1)
        {
         [mixpanelFunds track:@"equity_large_cap_page"];
        catagory=@"Large Cap";
        }else if(segmentedControl.selectedSegmentIndex==2)
        {
            [mixpanelFunds track:@"equity_mid_cap_page"];
            catagory=@"Mid Cap";
        }else if(segmentedControl.selectedSegmentIndex==3)
        {
            [mixpanelFunds track:@"equity_small_cap_page"];
            catagory=@"Small Cap";
        }else if(segmentedControl.selectedSegmentIndex==4)
        {
            [mixpanelFunds track:@"equity_multi_cap_page"];
            catagory=@"Multi Cap";
        }else if (segmentedControl.selectedSegmentIndex==5)
        {
            [mixpanelFunds track:@"equity_sector_page"];
            catagory=@"Sectoral";
        }
    }else if ([self.typeOfButton isEqualToString:@"debt"])
    {
        assetType=@"Debt";
        amc=@"all";
        if(segmentedControl.selectedSegmentIndex==0)
        {
            [mixpanelFunds track:@"debt_all_page"];
            catagory=@"all";
        }else if (segmentedControl.selectedSegmentIndex==1)
        {
            [mixpanelFunds track:@"debt_liquid_funds_page"];
            catagory=@"Liquid";
        }else if (segmentedControl.selectedSegmentIndex==2)
        {
            [mixpanelFunds track:@"debt_ultra_short_term_page"];
            catagory=@"Ultra Short Term ";
        }else if(segmentedControl.selectedSegmentIndex==3)
        {
            [mixpanelFunds track:@"debt_short_term_page"];
            catagory=@"Short Term";
        }
        else if (segmentedControl.selectedSegmentIndex==4)
        {
            [mixpanelFunds track:@"debt_medium_term_page"];
            catagory=@"Medium Term";
        }else if (segmentedControl.selectedSegmentIndex==5)
        {
            [mixpanelFunds track:@"debt_long_term_page"];
            catagory=@"Long Term ";
        }else if (segmentedControl.selectedSegmentIndex==6)
        {
            [mixpanelFunds track:@"debt_fmp_page"];
            catagory=@"FMP";
        }
    }else if ([self.typeOfButton isEqualToString:@"hybrid"])
    {
        assetType=@"Hybrid";
        amc=@"all";
        if(segmentedControl.selectedSegmentIndex==0)
        {
            [mixpanelFunds track:@"hybrid_all_page"];
            catagory=@"all";
        }else if (segmentedControl.selectedSegmentIndex==1)
        {
            [mixpanelFunds track:@"hybrid_equity_savings_page"];
            catagory=@"Equity Savings";
        }else if (segmentedControl.selectedSegmentIndex==2)
        {
            [mixpanelFunds track:@"hybrid_balanced_page"];
            catagory=@"Balanced";
        }else if (segmentedControl.selectedSegmentIndex==3)
        {
            [mixpanelFunds track:@"hybrid_mip_page"];
            catagory=@"MIP";
        }else if (segmentedControl.selectedSegmentIndex==4)
        {
            [mixpanelFunds track:@"hybrid_debt_oriented_page"];
            catagory=@"Debt Oriented";
        }else if (segmentedControl.selectedSegmentIndex==5)
        {
            [mixpanelFunds track:@"hybrid_equity_oriented_page"];
            catagory=@"Equity Oriented";
        }else if (segmentedControl.selectedSegmentIndex==6)
        {
            [mixpanelFunds track:@"hybrid_asset_allocation_funds_page"];
            catagory=@"Asset Allocation";
        }
    }else if ([self.typeOfButton isEqualToString:@"taxsaver"])
    {
        assetType=@"Equities";
        amc=@"all";
        catagory=@"ELSS";
        [mixpanelFunds track:@"tax_saver_page"];
    }else if ([self.typeOfButton isEqualToString:@"others"])
    {
        assetType = @"Others";
        amc=@"all";
        if(segmentedControl.selectedSegmentIndex==0)
        {
            [mixpanelFunds track:@"others_all_page"];
            catagory=@"all";
        }else if (segmentedControl.selectedSegmentIndex==1)
        {
            [mixpanelFunds track:@"others_nfo_page"];
            catagory=@"NFO";
        }else if (segmentedControl.selectedSegmentIndex==2)
        {
            [mixpanelFunds track:@"others_gold_funds_page"];
            catagory=@"Gold Funds";
        }else if (segmentedControl.selectedSegmentIndex==3)
        {
            [mixpanelFunds track:@"others_retirement_funds_page"];
            catagory=@"Retirement Fund or Special Situation Fund";
        }else if (segmentedControl.selectedSegmentIndex==4)
        {
            [mixpanelFunds track:@"others_capital_protection_funds_page"];
            catagory=@"Capital Protection";
        }else if (segmentedControl.selectedSegmentIndex==5)
        {
            [mixpanelFunds track:@"others_etf_page"];
            catagory=@"ETF";
        }else if (segmentedControl.selectedSegmentIndex==6)
        {
            [mixpanelFunds track:@"others_fund_of_funds_page"];
            catagory=@"FOF";
        }
    }
    page = 1;
    self.amcDataDictionary=nil;
    if(responseArray.count>0)
    {
        [responseArray removeAllObjects];
    }
    [self getFundWatchList];
    //[self getAmcList:page];
   // [self getSchemeRecommendations];
    
}

-(void)getSchemeRecommendations
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSLog(@"Asset:%@",assetType);
        NSLog(@"Category:%@",catagory);
        NSLog(@"AMC:%@",amc);
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getSchemeRec"];
        
        //gettaxsavingschemes
        if([self.typeOfButton isEqualToString:@"taxsaver"])
        {
            NSString * inputString = [NSString stringWithFormat:@"%@|%@",cid,sessionid];
            [inputArray addObject:[NSString stringWithFormat:@"%@gettaxsavingschemes_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputString]]];
        }else
        {
            NSString * inputString = [NSString stringWithFormat:@"%@|%@|%@|%@",assetType,catagory,cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@getschemerecommendations_option_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputString]]];
        }
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.schemeDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.schemeDataDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
               // self.pageControl.numberOfPages = [[[self.schemeDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count];
                //[self.pageControl reloadInputViews];
                if([[[self.schemeDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count]>0)
                {
                    int count = (int)[[[self.schemeDataDictionary objectForKey:@"data"]objectForKey:@"Table"] count];
                    self.pageControl.numberOfPages=count;
                    self.pageControl.currentPage=0;
                    self.topCollectionView.hidden = NO;
                    self.topCollectionViewHeightConstraint.constant = 165.9;
                   // self.pageControl.hidden=NO;
                    [self.topCollectionView reloadData];
                    
                   // [self addTimer];
                }else
                {
                    self.topCollectionView.hidden=YES;
                    self.topCollectionViewHeightConstraint.constant=0;
                   // self.pageControl.hidden=YES;
                    [self.topCollectionView reloadData];
                }
                [self getAmcList:page];
            });
        }];
        //
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x/scrollView.frame.size.width;
    self.pageControl.currentPage=page;
}


-(void)getAmcList:(int)page
{
    isPageRefreshing=NO;
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSLog(@"Asset:%@",assetType);
        NSLog(@"Category:%@",catagory);
        NSLog(@"AMC:%@",amc);
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:@"getAmcList"];
        NSString * inputString = [NSString stringWithFormat:@"%@|%@|%@|%d|%@|%@|%@",assetType,catagory,amc,page,@20,cid,sessionid];
        //http://zenwiseapi.aceonlineplatform.com/Zenwise.svc/searchscheme_option_paging/{AT}/{cat}/{amc}/{pageno}/{rco unt}/{cid}/{sessionid}
            [inputArray addObject:[NSString stringWithFormat:@"%@searchscheme_option_paging_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputString]]];
            [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
                for (int i=0; i<[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count]; i++) {
                    [responseArray addObject:[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"Table"]objectAtIndex:i]];
                }
                self.amcDataDictionary= @{@"data":@{@"Table":responseArray}};
                
                //self.amcDataDictionary=self.responseDataDictionary;
                NSLog(@"%@",self.amcDataDictionary);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.equityTableView reloadData];
//                    CGRect frame;
//                    //Update table 1 frame
//                    frame.size.width = self.backMainView.frame.size.width;
//                    frame.size.height = self.equityTableView.contentSize.height ;
//
//                    self.tableMainView.frame = frame;
//                    [self.tableMainView layoutIfNeeded];
//                    CGRect frame1;
//                    //Update table 1 frame
//                    frame1.size.width = self.backMainView.frame.size.width;
//                    frame1.size.height = self.tableMainView.frame.size.height;
//                    self.backMainView.frame = frame;
//                    [self.backMainView layoutIfNeeded];
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                });
            }];
      //
    }
}

-(void)getAssettypeList
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        
        //
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getAssettypeList"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@",cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@getassettype_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self getCategoryByAsset];
            });
        }];
        //
        
    }
}

-(void)getCategoryByAsset
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        //
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getCategoryByAsset"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",assetType,cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@getcategorybyasset_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.responseDataDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
        }];
        //
    }
}

-(void)callMethod
{
    //dispatch_async(dispatch_get_main_queue(), ^{
        self.equityTableView.delegate=self;
        self.equityTableView.dataSource=self;
        [self.equityTableView reloadData];
    
        self.topCollectionView.delegate=self;
        self.topCollectionView.dataSource=self;
        [self.topCollectionView reloadData];
    //});
}





-(void)viewDidDisappear:(BOOL)animated
{
    
}


//-(void)configureAutoSccrollTimer {
//
//    autoScrollTimer = [NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
//}
//
//-(void)deconfigureAutoScrollTimer {
//
//    [autoScrollTimer invalidate];
//    autoScrollTimer = nil;
//}
//
//-(void)onTimer {
//    [self autoScrollView];
//}
//
//-(void)autoScrollView {
//
//    CGPoint initialPoint = CGPointMake(width_Cell, 0);
//    if (CGPointEqualToPoint(initialPoint, self.topCollectionView.contentOffset)) {
//        if (width_Cell < self.topCollectionView.contentSize.width) {
//            width_Cell += 1;
//        } else
//            width_Cell = -self.view.frame.size.width;
//        CGPoint offSetPoint = CGPointMake(width_Cell, 0);
//        self.topCollectionView.contentOffset = offSetPoint;
//    } else
//        width_Cell = self.topCollectionView.contentOffset.x;
//}



//-(void)viewDidLayoutSubviews {
//    [super viewDidLayoutSubviews];
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.wisdomGradenTableView];
//    NSIndexPath *indexPath = [self.wisdomGradenTableView indexPathForRowAtPoint:buttonPosition];
//    [self.topCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
//}

//- (void)viewDidLayoutSubviews
//{
//    [self.topCollectionView layoutIfNeeded];
//    NSArray *visibleItems = [self.topCollectionView indexPathsForVisibleItems];
//    NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
//    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:0 inSection:currentItem.section];
//
//    [self.collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
//}




- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[[self.schemeDataDictionary objectForKey:@"data"]objectForKey:@"Table"] count];
    
    //return 10;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
   
    ZambalaRecCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ZambalaRecCollectionViewCell" forIndexPath:indexPath];
    NSString * fundNameString = [NSString stringWithFormat:@"%@",[[[[self.schemeDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"S_NAME"]];
    if([fundNameString isEqual:[NSNull null]])
    {
        cell.fundNameLabel.text = @"N/A";
    }else
    {
        cell.fundNameLabel.text = fundNameString;
    }
    
    NSString * threeYearReturn = [NSString stringWithFormat:@"%@",[[[[self.schemeDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"3yearret"]];
    
    if([threeYearReturn isEqual:[NSNull null]])
    {
        cell.threeYearsReturn.text = @"N/A";
    }else
    {
        cell.threeYearsReturn.text=[NSString stringWithFormat:@"%.2f",[threeYearReturn floatValue]];
    }
    
    NSString * minInvest = [NSString stringWithFormat:@"%@",[[[[self.schemeDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"MININVT"]];
    
    if([minInvest isEqual:[NSNull null]])
    {
        cell.minInvestLabel.text=@"N/A";
    }else
    {
        cell.minInvestLabel.text=minInvest;
    }
    
    [cell.investNowButton addTarget:self action:@selector(onRecInvestNowTap:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.pageControl.currentPage = indexPath.row;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    EquityDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
    detail.schemeCodeString = [NSString stringWithFormat:@"%@",[[[[self.schemeDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"schemecode"]];
    detail.fundNameString = [NSString stringWithFormat:@"%@",[[[[self.schemeDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"schemecode"]];
    [self.navigationController pushViewController:detail animated:YES];
}

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    CGFloat pageWidth = self.topCollectionView.frame.size.width;
//    self.pageControl.currentPage = self.topCollectionView.contentOffset.x / pageWidth;
//}

#pragma mark <UITableViewDataSource>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.typeOfButton isEqualToString:@"fundspicked"])
    {
        return 2;
    }else if([self.typeOfButton isEqualToString:@"equity"])
    {
       // return 2000;
       // NSLog(@"%lu",);
    return [[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"] count];
    }else
    {
        return [[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
   
     EquityTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"EquityTableViewCell" forIndexPath:indexPath];
//     cell.schemeNamelabel.text=@"Testing";
//     cell.fundSizeLabel.text=@"Test";
    cell.schemeNamelabel.text=[NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"S_NAME"]];
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"S_NAME"]]);
    NSString * threeYearLabel = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"3yearret"]];
        
        NSString * schemeCodeString = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row]objectForKey:@"SCHEMECODE"]];
        if([self.fundWatchSchemeCodes containsString:schemeCodeString])
        {
            [cell.addToFundWatchButton setImage:[UIImage imageNamed:@"fundAdded"] forState:UIControlStateNormal];
        }else
        {
            [cell.addToFundWatchButton setImage:[UIImage imageNamed:@"binoculars"] forState:UIControlStateNormal];
        }
    if([threeYearLabel isEqual:[NSNull null]])
    {
        cell.threeYearReturnLabel.text = @"N/A";
    }else
    {
    cell.threeYearReturnLabel.text = [NSString stringWithFormat:@"%.2f",[threeYearLabel floatValue]];
    }
    
    NSString * percent = @"%";
    NSString * fundSizeString = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"fundsize"]];
        if([fundSizeString isEqual:[NSNull null]])
        {
            cell.fundSizeLabel.text = @"N/A";
        }else
        {
            cell.fundSizeLabel.text = [NSString stringWithFormat:@"%.2f",[fundSizeString floatValue]];
        }
    NSString * navrsString = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"NAVRS"]];
    if([navrsString isEqual:[NSNull null]])
    {
        cell.navLabel.text = @"N/A";
    }else
    {
        cell.navLabel.text = [NSString stringWithFormat:@"%.2f",[navrsString floatValue]];
    }
    
    cell.navDateLabel.text = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"NAVDATE"]];
    NSString * navChange = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"change"]];
    if([navChange isEqual:[NSNull null]])
    {
        cell.navChangePercentLabel.text = @"N/A";
    }else
    {
        cell.navChangePercentLabel.text = [NSString stringWithFormat:@"%.2f%@",[navChange floatValue],percent];
    }
    cell.minInvestmentLabel.text = [NSString stringWithFormat:@"Min. Investment:%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"MININVT"]];
    
    [cell.investNowButton addTarget:self action:@selector(onInvestNowButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [cell.addToFundWatchButton addTarget:self action:@selector(onFundwatchTap:) forControlEvents:UIControlEventTouchUpInside];
    //    NSDictionary *data = [[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"] objectAtIndex:indexPath.row];
//        BOOL lastItemReached1 = [data isEqual:[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"] lastObject]];
//        if (!lastItemReached1 && indexPath.row == [[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"Table"] count] - 3)
//        {
//            if(rehitBool==true)
//            {
//                [self refreshTableVeiwList];
//            }
//        }
    return cell;
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(void)refreshTableVeiwList
{
    int limitInt=[offset intValue];
    int finalInt=limitInt+10;
    offset=[NSNumber numberWithInt:finalInt];
    [self getFundWatchList];
    [refreshControl1 endRefreshing];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self networkStatus]==YES)
    {
    EquityDetailViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"EquityDetailViewController"];
    detail.schemeCodeString = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"SCHEMECODE"]];
    detail.fundNameString = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"SCHEMECODE"]];
    [self.navigationController pushViewController:detail animated:YES];
    }
}

-(void)onInvestNowButtonTap:(UIButton*)sender
{
    if([self networkStatus]==YES)
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equityTableView];
    NSIndexPath *indexPath = [self.equityTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    OrderConfirmationViewController * order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderConfirmationViewController"];
    order.schemeCode=[NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"SCHEMECODE"]];
    [self.navigationController pushViewController:order animated:YES];
    }
}

-(void)onFundwatchTap:(UIButton*)sender
{
    if([self networkStatus]==YES)
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equityTableView];
    NSIndexPath *indexPath = [self.equityTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    self.selectedSchemeCode = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"SCHEMECODE"]];
    NSString * schemeCodeString = [NSString stringWithFormat:@"%@",[[[[self.amcDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row]objectForKey:@"SCHEMECODE"]];
    if([self.fundWatchSchemeCodes containsString:schemeCodeString])
    {
        [self deleteSymbol];
    }else
    {
        [self addToFundWatch];
    }
    }
}

-(void)deleteSymbol
{
    if([self networkStatus]==YES)
    {
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid;
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    if([guestCheck isEqualToString:@"0"])
    {
        cid = [userSessionData objectForKey:@"zenwiseID"];
    }else if([guestCheck isEqualToString:@"1"])
    {
        cid = [userSessionData objectForKey:@"cid"];
    }
    // cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"zenwiseID"]];
    NSString * schemeCodeNew = [NSString stringWithFormat:@"%@",self.selectedSchemeCode];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"addfundwatch"];
    [inputArray addObject:[NSString stringWithFormat:@"%@fundswatch",mfSharedManager.zambalaBaseUrl]];
    [inputArray addObject:@"3"];
    [inputArray addObject:@[schemeCodeNew]];
    [inputArray addObject:cid];
    [mfSharedManager zambalaPOSTRequestMethod :inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.zambalaResponseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        NSLog(@"%@",self.zambalaResponseDataDictionary);
        if([[[self.zambalaResponseDataDictionary objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Fund deleted from fund watch." preferredStyle:UIAlertControllerStyleAlert];
                    [self presentViewController:alert animated:YES completion:^{
                    }];
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                         [self getFundWatchList];
                    }];
                    [alert addAction:okAction];
                });
               
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alert animated:YES completion:^{
                }];
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                }];
                [alert addAction:okAction];
            });
        }
    }];
    }
}

-(void)addToFundWatch
{
    if([self networkStatus]==YES)
    {
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid;
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    if([guestCheck isEqualToString:@"0"])
    {
        cid = [userSessionData objectForKey:@"zenwiseID"];
    }else if([guestCheck isEqualToString:@"1"])
    {
        cid = [userSessionData objectForKey:@"cid"];
    }
    // NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"zenwiseID"]];
    NSString * schemeCodeNew = [NSString stringWithFormat:@"%@",self.selectedSchemeCode];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"addfundwatch"];
    [inputArray addObject:[NSString stringWithFormat:@"%@fundswatch",mfSharedManager.zambalaBaseUrl]];
    [inputArray addObject:@"1"];
    [inputArray addObject:@[schemeCodeNew]];
    [inputArray addObject:cid];
    [mfSharedManager zambalaPOSTRequestMethod :inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.zambalaResponseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        NSLog(@"%@",self.zambalaResponseDataDictionary);
        if([[[self.zambalaResponseDataDictionary objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
                delegate.fundWatchSchemeCodeString = self.selectedSchemeCode;
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Fund added successfully added to fund watch." preferredStyle:UIAlertControllerStyleAlert];
                    [self presentViewController:alert animated:YES completion:^{
                    }];
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                        [self getFundWatchList];
                    }];
                    [alert addAction:okAction];
                });
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alert animated:YES completion:^{
                }];
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                }];
                [alert addAction:okAction];
            });
        }
    }];
    }
}

-(void)getFundWatchList
{
    
    if([self networkStatus]==YES)
    {
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid;
    NSString * guestCheck = [userSessionData objectForKey:@"isguest"];
    if([guestCheck isEqualToString:@"0"])
    {
        cid = [userSessionData objectForKey:@"zenwiseID"];
    }else if([guestCheck isEqualToString:@"1"])
    {
        cid = [userSessionData objectForKey:@"cid"];
    }
    // NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"zenwiseID"]];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"getfundwatch"];
    [inputArray addObject:[NSString stringWithFormat:@"%@fundswatch",mfSharedManager.zambalaBaseUrl]];
    [inputArray addObject:@"2"];
    [inputArray addObject:cid];
    [mfSharedManager zambalaPOSTRequestMethod :inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.zambalaResponseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        NSLog(@"%@",self.zambalaResponseDataDictionary);
        if([[self.zambalaResponseDataDictionary objectForKey:@"data"] objectForKey:@"data"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
            self.fundWatchSchemeCodes=[[[self.zambalaResponseDataDictionary objectForKey:@"data"] objectForKey:@"data"] componentsJoinedByString:@","];
            [self accessMethod];
             });
            
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
        
    }];
    }
}

-(void)onRecInvestNowTap:(UIButton*)sender
{
    if([self networkStatus]==YES)
    {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.equityTableView];
    NSIndexPath *indexPath = [self.equityTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    OrderConfirmationViewController * order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderConfirmationViewController"];
   order.schemeCode=[NSString stringWithFormat:@"%@",[[[[self.schemeDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"schemecode"]];
    [self.navigationController pushViewController:order animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  OrderConfirmationViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 30/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface OrderConfirmationViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UITextField *amountTF;
@property (weak, nonatomic) IBOutlet UITextField *monthsTF;
@property (weak, nonatomic) IBOutlet UIView *confirmView;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *topViewPortfolio;
@property NSString * check;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property NSMutableDictionary * responseDataDictionary;
@property NSString * schemeCode;
@property (weak, nonatomic) IBOutlet UILabel *fundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *folioNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *minInvestmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderFundNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *threeYearReturnLabel;
@property (weak, nonatomic) IBOutlet UILabel *fundSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;
@property (weak, nonatomic) IBOutlet UILabel *navChangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIButton *createMandateButton;
@property (weak, nonatomic) IBOutlet UIButton *mandateSelectButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property NSMutableDictionary * mandateDataDictionary;
@property (weak, nonatomic) IBOutlet UILabel *sipMainLabel;
@property (weak, nonatomic) IBOutlet UILabel *lumpsumMainLabel;
@property (weak, nonatomic) IBOutlet UISwitch *sipLumpsumSwitch;
@property (weak, nonatomic) IBOutlet UIButton *sipDayButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sipViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *sipView;

@property Reachability * reach;


@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

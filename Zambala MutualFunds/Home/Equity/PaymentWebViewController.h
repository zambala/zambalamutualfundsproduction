//
//  PaymentWebViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 29/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface PaymentWebViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *paymentWebView;

@property NSString * loadURL;
@property Reachability * reach;


@end

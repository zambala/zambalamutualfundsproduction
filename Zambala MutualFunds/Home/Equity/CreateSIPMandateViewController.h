//
//  CreateSIPMandateViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 28/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateSIPMandateViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *selectBankButton;
@property (weak, nonatomic) IBOutlet UITextField *bankAccountNameTF;
@property (weak, nonatomic) IBOutlet UITextField *mandateAmountTF;
@property (weak, nonatomic) IBOutlet UIButton *emandateButton;
@property (weak, nonatomic) IBOutlet UIButton *isipButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *banksAllowedButton;
@property (weak, nonatomic) IBOutlet UIButton *createButton;
@property (weak, nonatomic) IBOutlet UITableView *mandateTableview;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIButton *popUpokbutton;

@property NSMutableDictionary * bankListDataDictionary;
@property NSMutableDictionary * createMandateDictionary;
@property (weak, nonatomic) IBOutlet UIView *backMainView;

@end

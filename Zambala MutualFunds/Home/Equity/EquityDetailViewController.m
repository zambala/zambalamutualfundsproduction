//
//  EquityDetailViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 20/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EquityDetailViewController.h"
#import "HCSStarRatingView.h"
#import "HMSegmentedControl.h"
#import "FundPerformanceTableViewCell.h"
#import "PerformanceTableViewCell.h"
#import "MKSHorizontalLineProgressView.h"
#import "KATCircularProgress.h"
#import "OrderConfirmationViewController.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "LMLineGraphView.h"
@import Mixpanel;

@interface EquityDetailViewController ()<LMLineGraphViewDelegate>
{
    HMSegmentedControl * segmentedControl;
    Utility * mfSharedManager;
    Mixpanel * mixpanelFunds;
}

@end

@implementation EquityDetailViewController
{
    NSArray * tenureArray;
    NSMutableArray * returnArray;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    mfSharedManager=[Utility MF];
    
    
    //http://zenwiseapi.aceonlineplatform.com/Zenwise.svc/getschemeperformance_equity/{cid}/{sessionid}/{s chemecode}/{period}
    //forgraph
    
    NSMutableArray * localSegmentsArray = [[NSMutableArray alloc]init];
    
    [localSegmentsArray addObject:@"Overview"];
    [localSegmentsArray addObject:@"Performance"];
    [localSegmentsArray addObject:@"Allocation"];
    tenureArray=@[@"1M",@"3M",@"6M",@"1YR",@"3YR",@"5YR"];
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * cid = [userSessionData objectForKey:@"cid"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds identify:cid];
    [mixpanelFunds track:@"fund_detail_page" properties:@{@"Fund Id":self.schemeCodeString,@"Fund Name":self.fundNameString}];
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:localSegmentsArray];
    segmentedControl.frame = CGRectMake(0, 0, self.segmentView.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentTap) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    [self.segmentView addSubview:segmentedControl];
    self.oneMonthButton.layer.cornerRadius = 10.0f;
    self.threeMonthButton.layer.cornerRadius = 10.0f;
    self.sixMonthButton.layer.cornerRadius = 10.0f;
    self.oneYearButton.layer.cornerRadius = 10.0f;
    self.threeYearButton.layer.cornerRadius = 10.0f;
    self.investNowButton.layer.cornerRadius = 15.0f;
    
    self.oneMonthButton.selected = YES;
    self.threeMonthButton.selected = NO;
    self.sixMonthButton.selected = NO;
    self.oneYearButton.selected = NO;
    self.threeYearButton.selected = NO;
    
    [self.oneMonthButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f]];
    [self.threeMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    [self.sixMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    [self.oneYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    [self.threeYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    
    [self.investNowButton addTarget:self action:@selector(onInvestNowButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.peerCategoryButton addTarget:self action:@selector(onPeerButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.oneMonthButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
     [self.threeMonthButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
     [self.sixMonthButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
     [self.oneYearButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
     [self.threeYearButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.starRatingView.emptyStarImage = [UIImage imageNamed:@"starEmpty"];
    self.starRatingView.halfStarImage = [UIImage imageNamed:@"starhalfSmall"]; // optional
    self.starRatingView.filledStarImage = [UIImage imageNamed:@"starFilled"];
    
    self.performanceView.hidden=YES;
    self.allocationView.hidden=YES;
    
    
    self.starRatingView.value=4;
    self.starRatingView.minimumValue=0;
    self.starRatingView.maximumValue=5;
    self.starRatingView.allowsHalfStars=YES;
    self.starRatingView.allowsHalfStars=YES;
    
    self.performanceTableView.delegate=self;
    self.performanceTableView.dataSource=self;
    [self.performanceTableView reloadData];
    
    self.fundPerformanceTableView.delegate=self;
    self.fundPerformanceTableView.dataSource=self;
    [self.fundPerformanceTableView reloadData];
    // Do any additional setup after loading the view.
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //[self setupButtons];
    
    self.view.backgroundColor = [UIColor gk_cloudsColor];
    [self setupGraph];
    self.horizantalProgressLineView.layer.cornerRadius=5.0f;
    self.horizantalProgressLineView.progressValue=10.0;
    self.horizantalProgressLineView.barColor = [UIColor colorWithRed:224.0f/255.0f green:82.0f/255.0f blue:88.0f/255.0f alpha:1.0f];
    self.horizantalProgressLineView.trackColor = [UIColor colorWithRed:178.0/255.0 green:138.0/255.0 blue:182.0/255.0 alpha:1];
    self.horizantalProgressLineView.barThickness = self.horizantalProgressLineView.frame.size.height;
    self.horizantalProgressLineView.showPercentageText = NO;
    
    
    
    [self.circularProgressView.sliceItems removeAllObjects];
    
    //set bar thickness
    [self.circularProgressView setLineWidth:30.0];
    //set animation duration
    [self.circularProgressView setAnimationDuration:2];
    
    //Create Slice Item objects.
    SliceItem *item1 = [[SliceItem alloc] init];
    item1.itemValue = 20.0;
    item1.itemColor = [UIColor colorWithRed:255.0f/255.0f green:192.0f/255.0f blue:102.0f/255.0f alpha:1.0f];;
    
    SliceItem *item2 = [[SliceItem alloc] init];
    item2.itemValue = 14.0;
    item2.itemColor =[UIColor colorWithRed:165.0f/255.0f green:225.0f/255.0f blue:241.0f/255.0f alpha:1.0f];
    
    SliceItem *item3 = [[SliceItem alloc] init];
    item3.itemValue = 10.0;
    item3.itemColor = [UIColor colorWithRed:227.0f/255.0f green:168.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
    
    
    //add objects to the sliceItems array.
    [self.circularProgressView.sliceItems addObject:item1];
    [self.circularProgressView.sliceItems addObject:item2];
    [self.circularProgressView.sliceItems addObject:item3];
    
    //reload the chart
    [self.circularProgressView reloadData];
    
    [self getSchemeDetails];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)onButtonTap:(id)sender
{
    if([self networkStatus]==YES)
    {
    if(sender == self.oneMonthButton)
    {
        self.oneMonthButton.selected = YES;
        self.threeMonthButton.selected = NO;
        self.sixMonthButton.selected = NO;
        self.oneYearButton.selected = NO;
        self.threeYearButton.selected = NO;
        
        [self.oneMonthButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f]];
        [self.threeMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.sixMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.oneYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.threeYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    }else if (sender == self.threeMonthButton)
    {
        self.oneMonthButton.selected = NO;
        self.threeMonthButton.selected = YES;
        self.sixMonthButton.selected = NO;
        self.oneYearButton.selected = NO;
        self.threeYearButton.selected = NO;
        
        [self.threeMonthButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f]];
        [self.oneMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.sixMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.oneYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.threeYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    }else if (sender == self.sixMonthButton)
    {
        self.oneMonthButton.selected = NO;
        self.threeMonthButton.selected = NO;
        self.sixMonthButton.selected = YES;
        self.oneYearButton.selected = NO;
        self.threeYearButton.selected = NO;
        
        [self.sixMonthButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f]];
        [self.threeMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.oneMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.oneYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.threeYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    }else if (sender == self.oneYearButton)
    {
        self.oneMonthButton.selected = NO;
        self.threeMonthButton.selected = NO;
        self.sixMonthButton.selected = NO;
        self.oneYearButton.selected = YES;
        self.threeYearButton.selected = NO;
        
        [self.oneYearButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f]];
        [self.threeMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.sixMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.oneMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.threeYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    }else if (sender == self.threeYearButton)
    {
        self.oneMonthButton.selected = NO;
        self.threeMonthButton.selected = NO;
        self.sixMonthButton.selected = NO;
        self.oneYearButton.selected = NO;
        self.threeYearButton.selected = YES;
        
        [self.threeYearButton setBackgroundColor:[UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f]];
        [self.threeMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.sixMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.oneYearButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
        [self.oneMonthButton setBackgroundColor:[UIColor colorWithRed:(142.0/255.0) green:(142.0/255.0) blue:(142.0/255.0) alpha:1.0f]];
    }
    }
    [self setupGraph];
}

-(void)viewWillAppear:(BOOL)animated
{
   
}
-(void)setupGraph
{
    NSArray *xAxisValues = [self xAxisValues];
    NSArray *shortXAxisValues = [self shortXAxisValues];
    LMGraphPlot *plot4 = [self sampleGraphPlot4];
    self.pointGraph.backgroundColor = [UIColor whiteColor];
    self.pointGraph.layout.xAxisScrollableOnly = NO;
    //self.pointGraph.xAxisStartGraphPoint = 1;
    self.pointGraph.layout.xAxisMargin = 10;
    self.pointGraph.delegate = self;
    self.pointGraph.xAxisValues = shortXAxisValues;
    self.pointGraph.yAxisMaxValue = 60;
    self.pointGraph.yAxisMinValue = 10;
    self.pointGraph.yAxisUnit = @"(customer)";
    self.pointGraph.title = @"";
    self.pointGraph.layout.titleLabelColor = [UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f];
    self.pointGraph.layout.xAxisLabelColor = [UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f];
    self.pointGraph.layout.yAxisLabelColor = [UIColor colorWithRed:(2.0/255.0) green:(30.0/255.0) blue:(41.0/255.0) alpha:1.0f];
    self.pointGraph.layout.yAxisUnitLabelColor = [UIColor whiteColor];
    self.pointGraph.graphPlots = @[plot4];
}
- (void)lineGraphView:(LMLineGraphView *)lineGraphView beganTouchWithGraphPoint:(LMGraphPoint *)graphPoint
{
    //self.scrollView.scrollEnabled = NO;
}

- (void)lineGraphView:(LMLineGraphView *)lineGraphView moveTouchWithGraphPoint:(LMGraphPoint *)graphPoint
{
    
}

- (void)lineGraphView:(LMLineGraphView *)lineGraphView endTouchWithGraphPoint:(LMGraphPoint *)graphPoint
{
    //self.scrollView.scrollEnabled = YES;
}

- (NSArray *)xAxisValues
{
    return @[@{ @1 : @"JAN" },
             @{ @2 : @"FEB" },
             @{ @3 : @"MAR" },
             @{ @4 : @"APR" },
             @{ @5 : @"MAY" },
             @{ @6 : @"JUN" },
             @{ @7 : @"JUL" },
             @{ @8 : @"AUG" },
             @{ @9 : @"SEP" },
             @{ @10 : @"OCT" },
             @{ @11 : @"NOV" },
             @{ @12 : @"DEC" }];
}

- (NSArray *)shortXAxisValues
{
    return @[@{ @1 : @"1" },
             @{ @2 : @"2" },
             @{ @3 : @"3" },
             @{ @4 : @"4" },
             @{ @5 : @"5" },
             @{ @6 : @"6" },
             @{ @7 : @"7" },
             @{ @8 : @"8" },
             @{ @9 : @"9" },
             @{ @10 : @"10" },
             @{ @11 : @"11" },
             @{ @12 : @"12" }];
}

- (LMGraphPlot *)sampleGraphPlot4
{
    LMGraphPlot *plot4 = [[LMGraphPlot alloc] init];
    plot4.graphPointSize = 0;
    plot4.fillCircleAroundGraphPoint = NO;
    plot4.strokeCircleAroundGraphPoint = NO;
    plot4.curveLine = YES;
    plot4.strokeColor = [UIColor whiteColor];
    //plot4.fillColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
    plot4.fillColor = [UIColor colorWithRed:(107.0/255.0) green:(243.0/255.0) blue:(173.0/255.0) alpha:1.0f];
    plot4.graphPointColor = [UIColor whiteColor];
    plot4.graphPointHighlightColor = [UIColor redColor];
    
    NSMutableArray *graphPoints = [NSMutableArray new];
    for (CGFloat i = 1; i <= 12; i += 0.25) {
        CGFloat value = rand() % 50 + 30;
        NSString *titleString = [NSString stringWithFormat:@"%.2f", i];
        NSString *valueString = [NSString stringWithFormat:@"%d", (int)value];
        LMGraphPoint *graphPoint = LMGraphPointMake(CGPointMake(i, value), titleString, valueString);
        [graphPoints addObject:graphPoint];
    }
    plot4.graphPoints = graphPoints;
    
    return plot4;
}

-(void)onSegmentTap
{
    if([self networkStatus]==YES)
    {
    if(segmentedControl.selectedSegmentIndex==0)
    {
        [mixpanelFunds track:@"fund_detail_overview_page" properties:@{@"Fund Id":self.schemeCodeString,@"Fund Name":self.fundNameString}];
        self.performanceView.hidden=YES;
        self.allocationView.hidden=YES;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        [mixpanelFunds track:@"fund_detail_performance_page" properties:@{@"Fund Id":self.schemeCodeString,@"Fund Name":self.fundNameString}];
        self.performanceView.hidden=NO;
        self.allocationView.hidden=YES;
        [self getSchemeCategoryPerformance];
    }else if (segmentedControl.selectedSegmentIndex==2)
    {
        [mixpanelFunds track:@"fund_detail_allocation_page" properties:@{@"Fund Id":self.schemeCodeString,@"Fund Name":self.fundNameString}];
        self.allocationView.hidden=NO;
        self.performanceView.hidden=YES;
    }
    }
}

- (NSInteger)numberOfLines {
    return [self.data count];
}

- (UIColor *)colorForLineAtIndex:(NSInteger)index {
    id colors = @[[UIColor gk_turquoiseColor]
                  ];
    return [colors objectAtIndex:index];
}

- (NSArray *)valuesForLineAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

- (CFTimeInterval)animationDurationForLineAtIndex:(NSInteger)index {
    return [[@[@1] objectAtIndex:index] doubleValue];
}

- (NSString *)titleForLineAtIndex:(NSInteger)index {
    return [self.labels objectAtIndex:index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.fundPerformanceTableView)
    {
    return tenureArray.count;
    }else
    {
        return [[self.peerCategoryDictionary objectForKey:@"data"]count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==self.performanceTableView)
    {
        PerformanceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PerformanceTableViewCell" forIndexPath:indexPath];
        cell.fundNameLabel.text = [NSString stringWithFormat:@"%@",[[[self.peerCategoryDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"S_NAME"]];
        if([self.yearLabel isEqualToString:@"1year"])
        {
            NSString * returnValue =[NSString stringWithFormat:@"%@",[[[self.peerCategoryDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"1YEARRET"]];
            if([returnValue isEqual:[NSNull null]])
            {
                cell.returnLabel.text =@"N/A";
            }else
            {
                cell.returnLabel.text = [NSString stringWithFormat:@"%.2f",[returnValue floatValue]];
            }
        }else if ([self.yearLabel isEqualToString:@"3year"])
        {
            NSString * returnValue =[NSString stringWithFormat:@"%@",[[[self.peerCategoryDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"3YEARRET"]];
            if([returnValue isEqual:[NSNull null]])
            {
                cell.returnLabel.text =@"N/A";
            }else
            {
                cell.returnLabel.text = [NSString stringWithFormat:@"%.2f",[returnValue floatValue]];
            }
        }else if ([self.yearLabel isEqualToString:@"5year"])
        {
            NSString * returnValue =[NSString stringWithFormat:@"%@",[[[self.peerCategoryDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"5YEARRET"]];
            if([returnValue isEqual:[NSNull null]])
            {
                cell.returnLabel.text =@"N/A";
            }else
            {
                cell.returnLabel.text = [NSString stringWithFormat:@"%.2f",[returnValue floatValue]];
            }
        }else if ([self.yearLabel isEqualToString:@"10year"])
        {
            NSString * returnValue =[NSString stringWithFormat:@"%@",[[[self.peerCategoryDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"10YEARRET"]];
            if([returnValue isEqual:[NSNull null]])
            {
                cell.returnLabel.text =@"N/A";
            }else
            {
                cell.returnLabel.text = [NSString stringWithFormat:@"%.2f",[returnValue floatValue]];
            }
        }
        return cell;
    }else if (tableView==self.fundPerformanceTableView)
    {
        FundPerformanceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FundPerformanceTableViewCell" forIndexPath:indexPath];
        cell.tenureLabel.text =[NSString stringWithFormat:@"%@",[tenureArray objectAtIndex:indexPath.row]];
        cell.returnLabel.text = [NSString stringWithFormat:@"%.2f",[[returnArray objectAtIndex:indexPath.row]floatValue]];
        cell.highestLabel.text=@"N/A";
        cell.lowestLabel.text=@"N/A";
        return cell;
    }
    
    return nil;
    
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.performanceTableView)
    {
    return 60;
    }else if (tableView==self.fundPerformanceTableView)
    {
        return 40;
    }
    return 0;
}

-(void)onInvestNowButtonTap
{
    if([self networkStatus]==YES)
    {
    OrderConfirmationViewController * order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderConfirmationViewController"];
    order.schemeCode=[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"SCHEMECODE"]];
    [self.navigationController pushViewController:order animated:YES];
    }
}

-(void)getSchemeDetails
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        //
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getschemedetails"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",cid,sessionid,self.schemeCodeString];
        [inputArray addObject:[NSString stringWithFormat:@"%@getschemedetails_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            returnArray = [[NSMutableArray alloc]init];
            NSLog(@"%@",self.responseDataDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
                if([[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"schemedata2"] count]>0)
                {
                [self assignDataToUI];
                [returnArray addObject:[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"1MONTHRET"]]];
                [returnArray addObject:[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"3MONTHRET"]]];
                [returnArray addObject:[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"6MONTHRET"]]];
                [returnArray addObject:[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"1YRRET"]]];
                [returnArray addObject:[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"3YEARRET"]]];
                [returnArray addObject:[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"5YEARRET"]]];
                NSLog(@"Return Array:%@",returnArray);
                [self.fundPerformanceTableView reloadData];
                
                }else
                {
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Oops" message:@"Data not found! Please try again later." preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                    }];
                    
                    UIAlertAction * ok=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                    [alert addAction:ok];
                }
            });
        }];
        //
        
    }
}

-(void)getSchemeCategoryPerformance
{
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        
        
        //
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getschemecategoryperformance"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@",cid,sessionid,self.schemeCodeString];
        [inputArray addObject:[NSString stringWithFormat:@"%@getschemecategoryperformance_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.peerCategoryDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.peerCategoryDictionary);
            dispatch_async(dispatch_get_main_queue(), ^{
               // [self assignDataToUI];
                [self.peerCategoryButton setTitle:@"1 Year" forState:UIControlStateNormal];
                self.yearLabel=@"1year";
                [self.performanceTableView reloadData];
            });
        }];
        //
    }
}

-(void)assignDataToUI
{
    NSString * fundName = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"S_NAME"]];
    if([fundName isEqual:[NSNull null]])
    {
        self.fundNameLabel.text=@"N/A";
    }else
    {
        self.fundNameLabel.text=fundName;
    }
    NSString * date = [NSString stringWithFormat:@"As of %@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"NAVDATE"]];
    if([date isEqual:[NSNull null]])
    {
        self.dateLabel.text = @"N/A";
    }else
    {
        self.dateLabel.text = date;
    }
    
    NSString * navLabel = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"NAVRS"]];
    if([navLabel isEqual:[NSNull null]])
    {
        self.navLabel.text = @"N/A";
    }else
    {
        self.navLabel.text = [NSString stringWithFormat:@"%.2f",[navLabel floatValue]];
    }
    
    self.fundSizeLabel.text = [NSString stringWithFormat:@"%.2f",[[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"fundsize"]floatValue]];
    
    NSString * threeYear = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"3YEARRET"]];
    if([threeYear isEqual:[NSNull null]])
    {
        self.threeYearReturnLabel.text = @"N/A";
    }else
    {
        self.threeYearReturnLabel.text = [NSString stringWithFormat:@"%.2f",[threeYear floatValue]];
    }
    
    self.navPercentLabel.text = [NSString stringWithFormat:@"%.2f",[[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"change"]floatValue]];
    self.lumpSumLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"MININVT"]];
    NSString * sipCheck = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"SIP"]];
    if([sipCheck isEqualToString:@"F"])
    {
        self.sipLabel.text=@"N/A";
    }else
    {
        NSString * sipFullData = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"sipdtls"]];
        if([sipFullData isEqualToString:@"<null>"]||[sipFullData isEqual:[NSNull null]])
        {
            self.sipLabel.text = @"N/A";
        }else
        {
        NSArray *items = [sipFullData componentsSeparatedByString:@"|"];
        NSLog(@"Items:%@",items);
        NSString * itemsString = [NSString stringWithFormat:@"%@",items];
        NSArray * items1 = [itemsString componentsSeparatedByString:@"~"];
        NSString * sip = [NSString stringWithFormat:@"%@",[items1 objectAtIndex:1]];
        self.sipLabel.text= sip;
        }
    }
    [self overViewUI];
}

-(void)overViewUI
{
    self.inceptionDateLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"inceptiondate"]];
    self.downFundSizeLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"fundsize"]];
    self.schemeTypeLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"CATEGORY"]];
    self.categoryLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"SUB_CATEGORY"]];
    self.highestLabel.text = [NSString stringWithFormat:@"%.2f",[[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata2"]objectAtIndex:0]objectForKey:@"1yrret"]floatValue]];
    NSString * percent =@"%";
    NSString * expenseRatio =[NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata"]objectAtIndex:0]objectForKey:@"Exp Ratio Direct"]];
    NSString * finalExpenseRatio = [expenseRatio stringByAppendingString:percent];
    self.expenseRatioLabel.text = finalExpenseRatio;
    NSString * exitLoad = [NSString stringWithFormat:@"%@ %@on or before 1yr, Nill after 1yr",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata"]objectAtIndex:0]objectForKey:@"Exit Load"],percent];
    self.exitLoadLabel.text = exitLoad;
    self.fundManagerLabel.text = [NSString stringWithFormat:@"%@",[[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"schemedata"]objectAtIndex:0]objectForKey:@"FundManager"]];
    [activityIndicatorView stopAnimating];
    [backView removeFromSuperview];
}

-(void)onPeerButtonTap
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"Select year" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"1 Year" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
       // [self dismissViewControllerAnimated:YES completion:^{
            self.yearLabel =@"1year";
            [self.peerCategoryButton setTitle:@"1 Year" forState:UIControlStateNormal];
            [self.performanceTableView reloadData];
        //}];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"3 Year" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
       // [self dismissViewControllerAnimated:YES completion:^{
            self.yearLabel =@"3year";
            [self.peerCategoryButton setTitle:@"3 Year" forState:UIControlStateNormal];
            [self.performanceTableView reloadData];
        //}];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"5 Year" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
      //  [self dismissViewControllerAnimated:YES completion:^{
            self.yearLabel =@"5year";
            [self.peerCategoryButton setTitle:@"5 Year" forState:UIControlStateNormal];
            [self.performanceTableView reloadData];
       // }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"10 Year" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
       // [self dismissViewControllerAnimated:YES completion:^{
            self.yearLabel =@"10year";
            [self.peerCategoryButton setTitle:@"10 Year" forState:UIControlStateNormal];
            [self.performanceTableView reloadData];
        //}];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        //[self dismissViewControllerAnimated:YES completion:^{
        //}];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

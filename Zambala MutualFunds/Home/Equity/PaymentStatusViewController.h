//
//  PaymentStatusViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 29/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentStatusViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *paymentImageView;
@property (weak, nonatomic) IBOutlet UILabel *paymentLabel;
@property (weak, nonatomic) IBOutlet UIButton *okayButton;

@property NSString * statusString;

@end

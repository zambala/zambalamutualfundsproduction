//
//  FundPerformanceTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 22/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "FundPerformanceTableViewCell.h"

@implementation FundPerformanceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

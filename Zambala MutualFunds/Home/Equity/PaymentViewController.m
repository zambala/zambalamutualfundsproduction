//
//  PaymentViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 31/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "PaymentViewController.h"

@interface PaymentViewController ()

@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.topView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.topView.layer.shadowOpacity = 3.0f;
    self.topView.layer.shadowRadius = 3.0f;
    self.topView.layer.cornerRadius=5.0f;
    self.topView.layer.masksToBounds = NO;
    
    [self.netBankingButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(id)sender
{
    if(sender==self.netBankingButton)
    {
        
    }else if (sender == self.debitButtonTap)
    {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

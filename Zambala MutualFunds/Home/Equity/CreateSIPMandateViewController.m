//
//  CreateSIPMandateViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 28/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "CreateSIPMandateViewController.h"
#import "SIPMandateTableViewCell.h"
#import "DGActivityIndicatorView.h"
#import "Utility.h"

@interface CreateSIPMandateViewController ()
{
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    NSString * parameterString;
    NSString * sipTypeString;
    NSString * bankID;
    NSString * tickCheck;
}

@end

@implementation CreateSIPMandateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     mfSharedManager=[Utility MF];
    self.popUpView.hidden=YES;
    self.popUpView.tag=99;
    
    self.createButton.layer.cornerRadius=20.0f;
    self.popUpokbutton.layer.cornerRadius=10.0f;
    self.createButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.createButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.createButton.layer.shadowOpacity = 0.6f;
    self.createButton.layer.shadowRadius = 6.5f;
    self.createButton.layer.masksToBounds = NO;
    
    
    self.backMainView.layer.cornerRadius=5.0f;
    self.backMainView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.backMainView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.backMainView.layer.shadowOpacity = 1.0f;
    self.backMainView.layer.shadowRadius = 3.0f;
    self.backMainView.layer.masksToBounds = NO;
    
    //mandateSelected
    //mandateUnSelected
    
    [self.emandateButton setImage:[UIImage imageNamed:@"mandateSelected"] forState:UIControlStateSelected];
    [self.emandateButton setImage:[UIImage imageNamed:@"mandateUnSelected"] forState:UIControlStateNormal];
    [self.isipButton setImage:[UIImage imageNamed:@"mandateSelected"] forState:UIControlStateSelected];
    [self.isipButton setImage:[UIImage imageNamed:@"mandateUnSelected"] forState:UIControlStateNormal];
    self.emandateButton.selected=NO;
    self.isipButton.selected = YES;
    self.selectBankButton.selected=YES;
    sipTypeString=@"I";
    [self.selectBankButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.banksAllowedButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.emandateButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.isipButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.createButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.popUpokbutton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    parameterString=@"getclientbanklist_TDES";
    tickCheck = @"deselected";
    self.mandateTableview.delegate=self;
    self.mandateTableview.dataSource=self;
    [self getBankList];
    [self onButtonTap:self.isipButton];
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton *)sender
{
    if(sender==self.emandateButton)
    {
        self.emandateButton.selected=YES;
        self.isipButton.selected = NO;
         self.selectBankButton.selected=NO;
        sipTypeString=@"e";
        NSString * text = [NSString stringWithFormat:@"%@",@"1. You will receive an email from BSE, follow the steps mentioned in it to complete E-mandate registration.\n 2. This functionality is available for individuals with single mode of holding and to those Aadhar Numbers which are updated with Mobile Number for OTP Generation.(Please note this is not the same as Linking your mobile with Aadhar Number)\n 3. To Check your Aadhar Status check - https://resident.uidai.gov.in/verify-email-mobile \n 4. E-mandate functionality is based on Aadhaar, accordingly, registration of mobile number with UIDAI is mandatory for e-signing of mandate.\n 5. The Aadhar Number should be linked to the Bank Account you have entered in your profile.\n 6. To check your Bank Status check - https://resident.uidai.gov.in/bank-mapper"];
        self.descriptionLabel.text = text;
        [self.banksAllowedButton setTitle:@"List of Bank allowed for E-Mandate" forState:UIControlStateNormal];
    }else if (sender == self.isipButton)
    {
        self.emandateButton.selected = NO;
        self.isipButton.selected = YES;
        self.selectBankButton.selected=NO;
        sipTypeString=@"I";
        NSString * text = [NSString stringWithFormat:@"%@",@"We will share 15 digits iSIP mandate code, that you need to add BSE as a biller in your net banking account mentioned above."];
        self.descriptionLabel.text = text;
        [self.banksAllowedButton setTitle:@"List of Bank allowed for I-SIP" forState:UIControlStateNormal];
    }else if (sender == self.selectBankButton)
    {
        self.selectBankButton.selected=YES;
        parameterString=@"getclientbanklist_TDES";
        [UIView transitionWithView:self.popUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpView.hidden = NO;
                        }
                        completion:NULL];
        [self getBankList];
    }else if (sender == self.banksAllowedButton)
    {
         self.selectBankButton.selected=NO;
        [UIView transitionWithView:self.popUpView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.popUpView.hidden = NO;
                        }
                        completion:NULL];
        if(self.emandateButton.selected==YES)
        {
            parameterString =@"get_bank_list_Emandate_TDES";
        }else if (self.isipButton.selected==YES)
        {
            parameterString = @"get_bank_list_ISIP_TDES";
        }
        [self getBankList];
    }else if (sender == self.createButton)
    {
        if(sipTypeString.length!=0&&bankID.length!=0&&self.bankAccountNameTF.text.length!=0&&self.mandateAmountTF.text.length!=0)
        {
        [self createMandate];
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Missing fields please verify." preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:okAction];
            });
        }
    }else if (sender==self.popUpokbutton)
    {
        self.popUpView.hidden=YES;
    }
    
}

-(void)getBankList
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"getBankList"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@",cid,sessionid];
        [inputArray addObject:[NSString stringWithFormat:@"%@%@/%@",mfSharedManager.baseUrl,parameterString,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.bankListDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.bankListDataDictionary);
            if(self.bankListDataDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(self.selectBankButton.selected==YES)
                    {
                    [self.selectBankButton setTitle:[NSString stringWithFormat:@"%@",[[[[self.bankListDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"value"]] forState:UIControlStateNormal];
                    bankID = [NSString stringWithFormat:@"%@",[[[[self.bankListDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:0]objectForKey:@"ID"]];
                    }
                    [self.mandateTableview reloadData];
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [[[self.bankListDataDictionary objectForKey:@"data"] objectForKey:@"Table"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SIPMandateTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SIPMandateTableViewCell" forIndexPath:indexPath];
    if(self.selectBankButton.selected==YES)
    {
        cell.someLabel.text = [NSString stringWithFormat:@"%@",[[[[self.bankListDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"value"]];
    }else if (self.emandateButton.selected == YES)
    {
        cell.someLabel.text = [NSString stringWithFormat:@"%@",[[[[self.bankListDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"BankName"]];
    }else if (self.isipButton.selected ==YES)
    {
        cell.someLabel.text= [NSString stringWithFormat:@"%@",[[[[self.bankListDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"bank_name"]];
    }
    if([tickCheck isEqualToString:@"selected"])
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        tickCheck=@"deselected";
    }else if ([tickCheck isEqualToString:@"deselected"])
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        tickCheck =@"selected";
    }
    cell.tickImageView.hidden=YES;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    
    if(self.selectBankButton.selected==YES)
    {
        [self.selectBankButton setTitle:[NSString stringWithFormat:@"%@",[[[[self.bankListDataDictionary objectForKey:@"data"]objectForKey:@"Table"]objectAtIndex:indexPath.row]objectForKey:@"value"]] forState:UIControlStateNormal];
        bankID = [NSString stringWithFormat:@"%@",[[[[self.bankListDataDictionary objectForKey:@"data"] objectForKey:@"Table"] objectAtIndex:indexPath.row] objectForKey:@"ID"]];
    }
    if([tickCheck isEqualToString:@"selected"])
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        self.popUpView.hidden = YES;
        tickCheck=@"deselected";
    }else if ([tickCheck isEqualToString:@"deselected"])
    {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        tickCheck =@"selected";
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)createMandate
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
    if(userSessionData !=nil)
    {
        NSString * cid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"cid"]];
        NSString * sessionid=[NSString stringWithFormat:@"%@",[userSessionData objectForKey:@"sessionid"]];
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"createMandate"];
        NSString * inputParams = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@",cid,sessionid,sipTypeString,self.mandateAmountTF.text,self.bankAccountNameTF.text,bankID];
        [inputArray addObject:[NSString stringWithFormat:@" %@insert_mandate_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:inputParams]]];
        [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.createMandateDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            NSLog(@"%@",self.createMandateDictionary);
            if(self.createMandateDictionary.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Mandate created successfully." preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                });
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
        //
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.popUpView.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end

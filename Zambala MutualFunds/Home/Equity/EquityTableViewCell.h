//
//  EquityTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 20/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *investNowButton;
@property (weak, nonatomic) IBOutlet UILabel *threeYearReturnLabel;
@property (weak, nonatomic) IBOutlet UILabel *fundSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;
@property (weak, nonatomic) IBOutlet UILabel *navChangePercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *navDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *minInvestmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *schemeNamelabel;
@property (weak, nonatomic) IBOutlet UIImageView *riskoMeterImageView;
@property (weak, nonatomic) IBOutlet UIView *backTableView;
@property (weak, nonatomic) IBOutlet UIButton *addToFundWatchButton;

@end

//
//  ZambalaRecCollectionViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 19/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ZambalaRecCollectionViewCell.h"

@implementation ZambalaRecCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.investNowButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.investNowButton.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.investNowButton.layer.shadowOpacity = 0.6f;
    self.investNowButton.layer.shadowRadius = 3.0f;
    self.investNowButton.layer.cornerRadius = 15.0f;
    self.investNowButton.layer.masksToBounds = NO;
    // Initialization code
}

@end

//
//  EquityTableViewCell.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 20/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "EquityTableViewCell.h"

@implementation EquityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.schemeNamelabel.text=@"";
    
    self.investNowButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.investNowButton.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.investNowButton.layer.shadowOpacity = 0.6f;
    self.investNowButton.layer.shadowRadius = 3.0f;
    self.investNowButton.layer.cornerRadius = 15.0f;
    self.investNowButton.layer.masksToBounds = NO;
    
    self.backTableView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.backTableView.layer.shadowOffset = CGSizeMake(0.0f,2.0f);
    self.backTableView.layer.shadowOpacity = 0.6f;
    self.backTableView.layer.shadowRadius = 3.0f;
    self.backTableView.layer.cornerRadius = 5.0f;
    self.backTableView.layer.masksToBounds = NO;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  PaymentStatusViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 29/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "PaymentStatusViewController.h"
#import "TabbarMFViewController.h"

@interface PaymentStatusViewController ()

@end

@implementation PaymentStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.okayButton.layer.cornerRadius=20.0f;
    self.okayButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.okayButton.layer.shadowOffset = CGSizeMake(0.0f, 1.5f);
    self.okayButton.layer.shadowOpacity = 0.6f;
    self.okayButton.layer.shadowRadius = 6.5f;
    self.okayButton.layer.masksToBounds = NO;
    
    if([self.statusString isEqualToString:@"success"])
    {
        self.paymentLabel.text = @"Successfully completed";
        self.paymentImageView.image = [UIImage imageNamed:@"success"];
    }else if ([self.statusString isEqualToString:@"failed"])
    {
        self.paymentLabel.text = @"Failed";
        self.paymentImageView.image = [UIImage imageNamed:@"failed"];
    }
    
    [self.okayButton addTarget:self action:@selector(onOkTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onOkTap
{
    TabbarMFViewController * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
    [self presentViewController:tabbar animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

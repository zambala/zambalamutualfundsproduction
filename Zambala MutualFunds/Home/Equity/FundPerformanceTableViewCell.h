//
//  FundPerformanceTableViewCell.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 22/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FundPerformanceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *tenureLabel;
@property (weak, nonatomic) IBOutlet UILabel *returnLabel;
@property (weak, nonatomic) IBOutlet UILabel *highestLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowestLabel;

@end

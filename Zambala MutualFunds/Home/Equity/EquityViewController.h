//
//  EquityViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 18/01/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MXParallaxHeader/MXScrollView.h>
#import "Reachability.h"

@interface EquityViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    int page;
    BOOL isPageRefreshing;
}


//@property (weak, nonatomic) IBOutlet UIView *pageView;
//@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backMainViewHgt;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *topCollectionView;
//@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UITableView *equityTableView;
@property NSMutableArray *imgArr;
@property NSInteger testIndexPath;
@property NSString * typeOfButton;
@property (weak, nonatomic) IBOutlet UIView *topTapView;
@property (weak, nonatomic) IBOutlet UIView *topUnTapView;
@property (weak, nonatomic) IBOutlet UIView *collectionMainView;
@property (weak, nonatomic) IBOutlet UIView *tableMainView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topCollectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *backMainView;

@property (weak, nonatomic) IBOutlet UIView *segmentView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentViewHeightConstraint;
@property NSMutableDictionary * responseDataDictionary;
@property NSMutableDictionary * schemeDataDictionary;
@property NSString * typeOfSmartMoney;

@property (strong, nonatomic) IBOutlet UIView *headerView;

//@property (nonatomic, strong) MXScrollView *MXscrollView;
@property (weak, nonatomic) IBOutlet MXScrollView *MXScrollView;

@property NSMutableDictionary * zambalaResponseDataDictionary;
@property NSString * fundWatchSchemeCodes;
@property NSString * selectedSchemeCode;

@property NSTimer * timer;
@property Reachability * reach;
@property (nonatomic, assign) CGFloat lastContentOffset;

@property NSDictionary * amcDataDictionary;





@end

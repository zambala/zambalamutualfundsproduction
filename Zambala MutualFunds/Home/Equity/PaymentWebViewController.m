//
//  PaymentWebViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 29/08/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "PaymentWebViewController.h"
#import "PaymentStatusViewController.h"

@interface PaymentWebViewController ()
{
    NSString * myString;
}

@end

@implementation PaymentWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.paymentWebView.delegate = self;
    [self.paymentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.loadURL]]];

    // Do any additional setup after loading the view.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
     myString = self.paymentWebView.request.URL.absoluteString;
    
    if([myString containsString:@"success.aspx"])
    {
        if([self networkStatus]==YES)
        {
        PaymentStatusViewController * payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentStatusViewController"];
        payment.statusString = @"success";
        [self.navigationController pushViewController:payment animated:YES];
        }
    }
//    }else
//    {
//        PaymentStatusViewController * payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentStatusViewController"];
//        payment.statusString = @"failed";
//        [self.navigationController pushViewController:payment animated:YES];
//    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

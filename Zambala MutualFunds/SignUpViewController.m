//
//  SignUpViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies on 08/02/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "SignUpViewController.h"
#import "TabbarMFViewController.h"
#import "AppDelegate.h"
#import "VerifyOTPViewController.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "OpenAccountWebViewController.h"
#import "Reachability.h"
@import Mixpanel;

@interface SignUpViewController ()<UITextFieldDelegate>
{
    AppDelegate * delegate;
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backIndicatorView;
    Mixpanel * mixpanelFunds;
    UITextField * selectedTextField;
}

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
     mfSharedManager=[Utility MF];
    self.signUpButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.signUpButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.signUpButton.layer.shadowOpacity = 0.6f;
    self.signUpButton.layer.shadowRadius = 6.5f;
    self.signUpButton.layer.masksToBounds = NO;
    
    self.submitButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.submitButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.submitButton.layer.shadowOpacity = 3.0f;
    self.submitButton.layer.shadowRadius = 3.0f;
    self.submitButton.layer.cornerRadius=20.0f;
    self.submitButton.layer.masksToBounds = NO;
    
    self.cancelButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.cancelButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.cancelButton.layer.shadowOpacity = 3.0f;
    self.cancelButton.layer.shadowRadius = 3.0f;
    self.cancelButton.layer.cornerRadius=20.0f;
    self.cancelButton.layer.masksToBounds = NO;

    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.fullNameTF.leftView = paddingView;
    self.fullNameTF.leftViewMode = UITextFieldViewModeAlways;

    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.emailIDTF.leftView = paddingView1;
    self.emailIDTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.passwordTF.leftView = paddingView2;
    self.passwordTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.rePasswordTF.leftView = paddingView3;
    self.rePasswordTF.leftViewMode = UITextFieldViewModeAlways;

    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.mobileNumberTF.leftView = paddingView4;
    self.mobileNumberTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.countryCodeTF.leftView = paddingView5;
    self.countryCodeTF.leftViewMode = UITextFieldViewModeAlways;
    
    self.otpView.hidden=YES;
    
    
//    self.fullNameTF.text =@"Support";
//    self.emailIDTF.text = @"support@zenwise.net";
//    self.passwordTF.text = @"support";
//    self.rePasswordTF.text=@"support";
//    self.mobileNumberTF.text = @"7796170068";
//    self.countryCodeTF.text = @"+91";
    
    
    self.fullNameTF.text =@"Sandeep Maganti";
    self.emailIDTF.text = @"sandeep.magantis@gmail.com";
    self.passwordTF.text = @"Sandeep7";
    self.rePasswordTF.text=@"Sandeep7";
    self.mobileNumberTF.text = @"8143221217";
    self.countryCodeTF.text = @"+91";
    
    self.fullNameTF.text =@"Sagar";
    self.emailIDTF.text = @"sagarsamcse99@gmail.com";
    self.passwordTF.text = @"Spider";
    self.rePasswordTF.text=@"Spider";
    self.mobileNumberTF.text = @"8438018300";
    self.countryCodeTF.text = @"+91";
    
    self.fullNameTF.delegate = self;
    self.emailIDTF.delegate = self;
    self.passwordTF.delegate=self;
    self.rePasswordTF.delegate=self;
    self.mobileNumberTF.delegate=self;
    
    [self.fullNameTF setReturnKeyType:UIReturnKeyDone];
    [self.emailIDTF setReturnKeyType:UIReturnKeyDone];
    [self.passwordTF setReturnKeyType:UIReturnKeyDone];
    [self.rePasswordTF setReturnKeyType:UIReturnKeyDone];
    [self.mobileNumberTF setReturnKeyType:UIReturnKeyDone];
    
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds track:@"signup_page"];
    
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.signUpButton addTarget:self action:@selector(onSignUpTap) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(onCancelButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.submitButton addTarget:self action:@selector(onSubmitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.termsAndPoliciesButton addTarget:self action:@selector(onTermsTap) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

-(void)onTermsTap
{
    OpenAccountWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountWebViewController"];
    webView.sourceString = @"Terms";
    [self presentViewController:webView animated:YES completion:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [selectedTextField resignFirstResponder];
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    selectedTextField = textField;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.passwordTF)
    {
    if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound) {
        return NO;
    }
    }
    
    if(textField == self.rePasswordTF)
    {
        if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound) {
            return NO;
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//
//    if(textField==self.fullNameTF)
//    {
//    if(self.fullNameTF.text.length==0)
//    {
//        self.fullNameTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
//        self.fullNameTF.layer.borderWidth=2.0;
//        return NO;
//    }else
//    {
//        self.fullNameTF.layer.borderColor=[[UIColor clearColor]CGColor];
//        self.fullNameTF.layer.borderWidth=0.0;
//        return YES;
//    }
//    }
//
//    if(textField==self.emailIDTF)
//    {
//    if(self.emailIDTF.text.length==0)
//    {
//        self.emailIDTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
//        self.emailIDTF.layer.borderWidth=2.0;
//        return NO;
//    }else
//    {
//        if([self validateEmail:self.emailIDTF.text]==YES)
//        {
//            self.emailIDTF.layer.borderColor=[[UIColor clearColor]CGColor];
//            self.emailIDTF.layer.borderWidth=0.0;
//            return YES;
//        }else
//        {
//            self.emailIDTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
//            self.emailIDTF.layer.borderWidth=2.0;
//            return NO;
//        }
//    }
//    }
//
//    if(textField==self.passwordTF)
//    {
//    if(self.passwordTF.text.length==0)
//    {
//        self.passwordTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
//        self.passwordTF.layer.borderWidth=2.0;
//        return NO;
//    }else
//    {
//        self.passwordTF.layer.borderColor=[[UIColor clearColor]CGColor];
//        self.passwordTF.layer.borderWidth=0.0;
//        return YES;
//    }
//    }
//
//    if(textField==self.rePasswordTF)
//    {
//    if(self.rePasswordTF.text.length==0)
//    {
//        self.rePasswordTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
//        self.rePasswordTF.layer.borderWidth=2.0;
//        return NO;
//    }else
//    {
//        if([self.passwordTF.text isEqualToString:self.rePasswordTF.text])
//        {
//            self.rePasswordTF.layer.borderColor=[[UIColor clearColor]CGColor];
//            self.rePasswordTF.layer.borderWidth=0.0;
//            return YES;
//        }else
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Passwords do not match!" preferredStyle:UIAlertControllerStyleAlert];
//
//                [self presentViewController:alert animated:YES completion:^{
//
//                }];
//
//                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    [self.rePasswordTF becomeFirstResponder];
//                }];
//
//                [alert addAction:okAction];
//            });
//            return NO;
//        }
//    }
//    }
//
//    if(textField==self.mobileNumberTF)
//    {
//    if(self.mobileNumberTF.text.length==0)
//    {
//        self.mobileNumberTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
//        self.mobileNumberTF.layer.borderWidth=2.0;
//        return NO;
//    }else
//    {
//        self.mobileNumberTF.layer.borderColor=[[UIColor clearColor]CGColor];
//        self.mobileNumberTF.layer.borderWidth=0.0;
//        return YES;
//    }
//    }
//
//    return YES;
//}
- (BOOL)validateEmail:(NSString *)inputText {
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot = (int)aRange.location;
        //NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            //NSLog(@"topleveldomains:%@",topLevelDomain);
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
            
        }
    }
    return FALSE;
}

-(void)onSubmitButtonTap
{
//    TabbarMFViewController * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
//    [self presentViewController:tabbar animated:YES completion:nil];
//    delegate.signUpCheck=@"signin";
}


-(void)serverHit
{
    backIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backIndicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backIndicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    NSDictionary * json = @{@"name":self.fullNameTF.text,
                            @"emailid":self.emailIDTF.text,
                            @"password":self.passwordTF.text,
                            @"mobilenumber":self.mobileNumberTF.text,
                            @"countrycode":@"91"
                            };
    
    
//    NSDictionary * json = @{@"name":self.fullNameTF.text,
//                            @"emailid":self.emailIDTF.text,
//                            @"password":self.passwordTF.text,
//                            @"mobilenumber":self.mobileNumberTF.text
                           // };
   
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    if (! jsonData) {
        NSLog(@"Got an error:");
    } else {
        jsonString  = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",jsonString);
    }
    NSString* dataString = [NSString stringWithFormat:@"%@",jsonString];
    NSString * someString = [dataString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    
//    NSString* dataString = [NSString stringWithFormat:@"%@",jsonString];
//    dataString =[dataString stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSArray* words = [dataString componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    NSString* nospacestring = [words componentsJoinedByString:@""];
//    NSString * someString = [nospacestring stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"Signup"];
    NSString * endPoint = [NSString stringWithFormat:@"%@signup/startsignup?data=%@",mfSharedManager.zambalaBaseUrl,someString];
    [inputArray addObject:endPoint];
    [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        if([[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"message"]isEqualToString:@"Success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backIndicatorView removeFromSuperview];
                VerifyOTPViewController * OTP = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyOTPViewController"];
                OTP.fullName = self.fullNameTF.text;
                OTP.emailID = self.emailIDTF.text;
                OTP.password = self.passwordTF.text;
                OTP.countryCode =@"91";
                OTP.mobileNumber = self.mobileNumberTF.text;
                CATransition *transition = [[CATransition alloc] init];
                transition.duration = 0.5;
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                [self.view.window.layer addAnimation:transition forKey:kCATransition];
                [mixpanelFunds track:@"signup_success_page"];
                [mixpanelFunds.people set:@{@"$name":self.fullNameTF.text,@"$email":self.emailIDTF.text,@"$phone":self.mobileNumberTF.text}];
                [self presentViewController:OTP animated:false completion:nil];
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString * message = [NSString stringWithFormat:@"%@",[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"message"]];
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:message preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backIndicatorView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }];
}



-(void)onSignUpTap
{
    if([self networkStatus]==YES)
    {
    NSString * nameCheck,*emailCheck,*passwordCheck,*rePasswordCheck,*mobileNumberCheck;
    [selectedTextField resignFirstResponder]; if(self.fullNameTF.text.length>0&&self.emailIDTF.text.length>0&&self.passwordTF.text.length>0&&self.rePasswordTF.text.length>0&&self.mobileNumberTF.text.length>0)
    {
                if(self.fullNameTF.text.length==0)
                {
                    self.fullNameTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
                    self.fullNameTF.layer.borderWidth=2.0;
                    nameCheck = @"";
                }else
                {
                    self.fullNameTF.layer.borderColor=[[UIColor clearColor]CGColor];
                    self.fullNameTF.layer.borderWidth=0.0;
                    nameCheck= @"Ok";
                }
        
        
                if(self.emailIDTF.text.length==0)
                {
                    self.emailIDTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
                    self.emailIDTF.layer.borderWidth=2.0;
                    emailCheck = @"";
                    
                }else
                {
                    if([self validateEmail:self.emailIDTF.text]==YES)
                    {
                        self.emailIDTF.layer.borderColor=[[UIColor clearColor]CGColor];
                        self.emailIDTF.layer.borderWidth=0.0;
                        emailCheck = @"Ok";
                        
                    }else
                    {
                        self.emailIDTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
                        self.emailIDTF.layer.borderWidth=2.0;
                        emailCheck = @"";
                    }
                }
        
        
                if(self.passwordTF.text.length==0)
                {
                    self.passwordTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
                    self.passwordTF.layer.borderWidth=2.0;
                    passwordCheck=@"";
                }else
                {
                    self.passwordTF.layer.borderColor=[[UIColor clearColor]CGColor];
                    self.passwordTF.layer.borderWidth=0.0;
                    passwordCheck=@"Ok";
                }
        
                if(self.rePasswordTF.text.length==0)
                {
                    self.rePasswordTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
                    self.rePasswordTF.layer.borderWidth=2.0;
                    rePasswordCheck = @"";
                }else
                {
                    if([self.passwordTF.text isEqualToString:self.rePasswordTF.text])
                    {
                        self.rePasswordTF.layer.borderColor=[[UIColor clearColor]CGColor];
                        self.rePasswordTF.layer.borderWidth=0.0;
                        rePasswordCheck = @"Ok";
                    }else
                    {
                        rePasswordCheck = @"";
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Passwords do not match!" preferredStyle:UIAlertControllerStyleAlert];
            
                            [self presentViewController:alert animated:YES completion:^{
            
                            }];
            
                            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                            }];
            
                            [alert addAction:okAction];
                        });
                    }
                }
        
                if(self.mobileNumberTF.text.length==0)
                {
                    self.mobileNumberTF.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
                    self.mobileNumberTF.layer.borderWidth=2.0;
                    mobileNumberCheck = @"";
                }else
                {
                    self.mobileNumberTF.layer.borderColor=[[UIColor clearColor]CGColor];
                    self.mobileNumberTF.layer.borderWidth=0.0;
                    mobileNumberCheck = @"Ok";
                }
        
        if([nameCheck isEqualToString:@"Ok"]&&[emailCheck isEqualToString:@"Ok"]&&[passwordCheck isEqualToString:@"Ok"]&&[rePasswordCheck isEqualToString:@"Ok"]&&[mobileNumberCheck isEqualToString:@"Ok"])
        {
             [self serverHit];
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Please fill the correct details marked red." preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:okAction];
            });
        }
    }else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Please fill all the details." preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               
            }];
            
            [alert addAction:okAction];
        });
    }
    }
}

-(void)onBackButtonTap
{
    [selectedTextField resignFirstResponder];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ForgetPasswordViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 03/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import <QuartzCore/QuartzCore.h>
@import Mixpanel;

@interface ForgetPasswordViewController ()<UITextFieldDelegate>
{
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backView;
    Mixpanel * mixpanelFunds;
    UITextField * selectedTextField;
}

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mfSharedManager = [Utility MF];
    self.descLabel.hidden = YES;
    self.emailIDTF.delegate = self;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 50)];
    self.emailIDTF.leftView = paddingView;
    self.emailIDTF.leftViewMode = UITextFieldViewModeAlways;
    self.emailIDTF.delegate = self;
    mixpanelFunds = [Mixpanel sharedInstance];
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * clientID = [userSessionData objectForKey:@"cid"];
    [mixpanelFunds identify:clientID];
    [mixpanelFunds track:@"forgot_password_page"];
    [self.submitButton addTarget:self action:@selector(onForgetPasswordTap) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    selectedTextField = textField;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
}
-(void)onBackButtonTap
{
    [selectedTextField resignFirstResponder];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)onForgetPasswordTap
{
    if([self networkStatus]==YES)
    {
    [self forgetPasswordMethod];
    }
}

-(void)forgetPasswordMethod
{
    
    [self.emailIDTF resignFirstResponder];
    
//    "http://projects.zenwise.net:8073/api/signup/emailgenerateforgotpassword?data=\{\"emailid\":\"sachin.sashital@zenwise.net\",\"name\":\"Sachin\"\}"
    dispatch_async(dispatch_get_main_queue(), ^{
        backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        [self.view addSubview:backView];
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
        activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
        [activityIndicatorView setCenter:self.view.center];
        [backView addSubview:activityIndicatorView];
        [activityIndicatorView startAnimating];
    });
    NSDictionary * json = @{
                            @"emailid":self.emailIDTF.text,
                            @"name":self.emailIDTF.text
                            };
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    if (! jsonData) {
        NSLog(@"Got an error:");
    } else {
        jsonString  = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",jsonString);
    }
    NSString* dataString = [NSString stringWithFormat:@"%@",jsonString];
    NSString * someString = [dataString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSMutableArray * inputArray = [[NSMutableArray alloc]init];
    [inputArray addObject:@"forgetpassword"];
    NSString * endPoint = [NSString stringWithFormat:@"%@signup/emailgenerateforgotpassword?data=%@",mfSharedManager.zambalaBaseUrl,someString];
    [inputArray addObject:endPoint];
    [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        if([[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"message"]isEqualToString:@"Success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backView removeFromSuperview];
            self.descLabel.hidden = NO;
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString * message = [NSString stringWithFormat:@"%@",[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"message"]];
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:message preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

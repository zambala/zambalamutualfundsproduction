//
//  ForgetPasswordViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 03/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface ForgetPasswordViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UITextField *emailIDTF;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@property NSMutableDictionary * responseDataDictionary;
@property Reachability * reach;


@end

//
//  LaunchScreenViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 23/10/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "LaunchScreenViewController.h"
@import Mixpanel;
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "TabbarMFViewController.h"

@interface LaunchScreenViewController ()
{
    Utility * mfSharedManager;
    DGActivityIndicatorView * activityIndicatorView;
    UIView * backView;
    Mixpanel * mixpanelFunds;
}

@end

@implementation LaunchScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * zenwiseID = [userSessionData objectForKey:@"zenwiseID"];
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds track:@"splash_page"];
    if(zenwiseID.length>0)
    {
        [self isKycd];
    }
    
    // Do any additional setup after loading the view.
}

-(void)isKycd
{
    NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
    NSString * zenwiseID = [userSessionData objectForKey:@"zenwiseID"];
    if(zenwiseID.length!=0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
            [self.view addSubview:backView];
            activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
            activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
            [activityIndicatorView setCenter:self.view.center];
            [backView addSubview:activityIndicatorView];
            [activityIndicatorView startAnimating];
        });
        NSMutableArray * inputArray = [[NSMutableArray alloc]init];
        [inputArray addObject:@"isKyc"];
        NSString * endPoint = [NSString stringWithFormat:@"%@signup/iskyced/%@",mfSharedManager.zambalaBaseUrl,zenwiseID];
        [inputArray addObject:endPoint];
        [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
            self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
            if([[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"status"]isEqualToString:@"success"])
            {
                NSString * kyc = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"kyc"]];
                if([kyc isEqualToString:@"1"])
                {
                    
                }else if ([kyc isEqualToString:@"0"])
                {
                    [self loginServer];
                }
            }else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    }];
                    
                    [alert addAction:okAction];
                });
            }
        }];
    }
}


-(void)loginServer
{
    backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
            
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:@"loginrequest"];
            NSString * paramsString;
                paramsString = [NSString stringWithFormat:@"%@|%@",@"guest",@"guest123"];
            [inputArray addObject:[NSString stringWithFormat:@"%@applogin_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:paramsString]]];
            [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                mfSharedManager.cid=[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"]objectForKey:@"logindata"]objectAtIndex:0]objectForKey:@"cid"]];
                mfSharedManager.sessionId=[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"]objectForKey:@"logindata"]objectAtIndex:0]objectForKey:@"sessionid"]];
                if(mfSharedManager.cid.length>0&&mfSharedManager.sessionId.length>0)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
                        [userSessionData setObject:mfSharedManager.cid forKey:@"cid"];
                        [userSessionData setObject:mfSharedManager.sessionId forKey:@"sessionid"];
                        [userSessionData setObject:@"guest" forKey:@"username"];
                        [userSessionData setObject:@"guest123" forKey:@"password"];
                        
                        [userSessionData synchronize];
                        
                        [mixpanelFunds track:@"splash_auto_login_page"];
                        
                        [mixpanelFunds.people set:@{@"User Type":@"Guest",@"Is KYCed":@"False",@"Client Id":[userSessionData objectForKey:@"zenwiseID"]}];
                        
                        TabbarMFViewController * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
                        [self presentViewController:tabbar animated:YES completion:nil];
                        [activityIndicatorView stopAnimating];
                        [backView removeFromSuperview];
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        UIAlertAction * ok=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                        }];
                        
                        [alert addAction:ok];
                        
                    });
                }
                
            }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

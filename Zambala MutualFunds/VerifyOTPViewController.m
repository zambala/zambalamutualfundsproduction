//
//  VerifyOTPViewController.m
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 03/09/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import "VerifyOTPViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TabbarMFViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "DGActivityIndicatorView.h"
#import "OpenAccountWebViewController.h"
#import "LoginViewController.h"
@import Mixpanel;

@interface VerifyOTPViewController ()<UITextFieldDelegate>
{
    AppDelegate * delegate;
    Utility * mfSharedManager;
    DGActivityIndicatorView *activityIndicatorView;
    UIView * backIndicatorView;
    NSString * kycCheck;
    Mixpanel * mixpanelFunds;
}

@end

@implementation VerifyOTPViewController
int direction;
int shakes;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     mfSharedManager=[Utility MF];
    self.verifyButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.verifyButton.layer.shadowOffset = CGSizeMake(0.0f,2.0f);
    self.verifyButton.layer.shadowOpacity = 0.6f;
    self.verifyButton.layer.shadowRadius = 3.0f;
    self.verifyButton.layer.masksToBounds = NO;
    
    
    self.proceedButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16] CGColor];
    self.proceedButton.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.proceedButton.layer.shadowOpacity = 0.6f;
    self.proceedButton.layer.shadowRadius = 3.0f;
    self.proceedButton.layer.masksToBounds = NO;
    
    self.laterButton.layer.cornerRadius = 10.0f;
    
    self.popUpView.hidden=YES;
    self.getStartedView.hidden = YES;
    
    self.popUpInsideView.layer.cornerRadius=20.0f;
    self.popUpInsideView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    self.popUpInsideView.layer.shadowOffset = CGSizeMake(0.0f,3.0f);
    self.popUpInsideView.layer.shadowOpacity = 0.6f;
    self.popUpInsideView.layer.shadowRadius = 6.5f;
    self.popUpInsideView.layer.cornerRadius = 20.0f;
    self.popUpInsideView.layer.masksToBounds = NO;
    
    self.popUpView.tag=99;
    
    
    
    
//    self.TFOne.textContentType = UITextContentTypeOneTimeCode;
//    self.TFTwo.textContentType = UITextContentTypeOneTimeCode;
//    self.TFThree.textContentType = UITextContentTypeOneTimeCode;
//    self.TFFour.textContentType = UITextContentTypeOneTimeCode;
//    self.TFFive.textContentType = UITextContentTypeOneTimeCode;
//    self.TFSix.textContentType = UITextContentTypeOneTimeCode;
    
    self.TFOne.delegate = self;
    self.TFTwo.delegate = self;
    self.TFThree.delegate = self;
    self.TFFour.delegate = self;
    self.TFFive.delegate = self;
    self.TFSix.delegate = self;
    self.TFOne.tag = 1;
    self.TFTwo.tag = 2;
    self.TFThree.tag = 3;
    self.TFFour.tag = 4;
    self.TFFive.tag = 5;
    self.TFSix.tag = 6;
    
    mixpanelFunds = [Mixpanel sharedInstance];
    [mixpanelFunds track:@"verify_otp_page"];
    
    [self.verifyButton addTarget:self action:@selector(onVerifyTap) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton addTarget:self action:@selector(onBackTap) forControlEvents:UIControlEventTouchUpInside];
    [self.proceedButton addTarget:self action:@selector(onProceedButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.laterButton addTarget:self action:@selector(onLaterButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.resendOTP addTarget:self action:@selector(onResendButtonTap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    
    
    //http://projects.zenwise.net:8073/api/signup/validateotp?data=\{\"mobilenumber\":\"9822245689\",\"countrycode\":\"91\",\"otp\":\"830787\",\"emailid\":\"sachinautomatic@gmail.com\",\"name\":\"Sachin\", \"password\":\"something\"\}
}
-(void)viewDidAppear:(BOOL)animated
{
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
}
-(BOOL)networkStatus
{
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"OOPS!" message:@"No Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [activityIndicatorView stopAnimating];
            [backIndicatorView removeFromSuperview];
        }];
        
        [alert addAction:okAction];
    }
    else
    {
        
    }
    return YES;
    
    
}

-(void)onResendButtonTap
{
    if([self networkStatus]==YES)
    {
         [self reSendOTPServer];
    }
   
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    @try
    {
        if ((textField.text.length < 1) && (string.length > 0))
        {
            
            NSInteger nextTag = textField.tag + 1;
            UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
            if (! nextResponder){
                [textField resignFirstResponder];
            }
            textField.text = string;
            if (nextResponder)
                [nextResponder becomeFirstResponder];
            
            return NO;
            
        }else if ((textField.text.length >= 1) && (string.length > 0)){
            //FOR MAXIMUM 1 TEXT
            
            NSInteger nextTag = textField.tag + 1;
            UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
            if (! nextResponder){
                [textField resignFirstResponder];
            }
            textField.text = string;
            if (nextResponder)
                [nextResponder becomeFirstResponder];
            
            return NO;
        }
        else if ((textField.text.length >= 1) && (string.length == 0)){
            // on deleteing value from Textfield
            
            NSInteger prevTag = textField.tag-1;
            // Try to find prev responder
            UIResponder* prevResponder = [textField.superview viewWithTag:prevTag];
            if (! prevResponder){
                [textField resignFirstResponder];
            }
            textField.text = string;
            if (prevResponder)
                // Found next responder, so set it.
                [prevResponder becomeFirstResponder];
            
            return NO;
        }
        return YES;
    }
    @catch (NSException * e) {
        //NSLog(@"Exception: %@", e);
    }
    @finally {
        //NSLog(@"finally");
    }
}

-(void)serverHit
{
    if([self networkStatus]==YES)
    {
    backIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backIndicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backIndicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    NSString * otpOne = [NSString stringWithFormat:@"%@",self.TFOne.text];
    NSString * otpTwo = [otpOne stringByAppendingString:[NSString stringWithFormat:@"%@",self.TFTwo.text]];
    NSString * otpThree = [otpTwo stringByAppendingString:[NSString stringWithFormat:@"%@",self.TFThree.text]];
    NSString * otpFour = [otpThree stringByAppendingString:[NSString stringWithFormat:@"%@",self.TFFour.text]];
    NSString * otpFive = [otpFour stringByAppendingString:[NSString stringWithFormat:@"%@",self.TFFive.text]];
    NSString * otpSix = [otpFive stringByAppendingString:[NSString stringWithFormat:@"%@",self.TFSix.text]];
    
    NSDictionary * json = @{@"name":self.fullName,
                            @"emailid":self.emailID,
                            @"password":self.password,
                            @"mobilenumber":self.mobileNumber,
                            @"countrycode":self.countryCode,
                            @"otp":otpSix
                            };
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    if (! jsonData) {
        NSLog(@"Got an error:");
    } else {
        jsonString  = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",jsonString);
    }
    
    NSString* dataString = [NSString stringWithFormat:@"%@",jsonString];
    NSString * someString = [dataString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"validateotp"];
    NSString * endPoint = [NSString stringWithFormat:@"%@signup/validateotp?data=%@",mfSharedManager.zambalaBaseUrl,someString];
    [inputArray addObject:endPoint];
    [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        if([[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"message"]isEqualToString:@"Success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backIndicatorView removeFromSuperview];
                NSString * zenwiseClientID = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"clientid"]];
                [mixpanelFunds identify:zenwiseClientID];
                NSUserDefaults * userSessionData = [NSUserDefaults standardUserDefaults];
                [userSessionData setObject:zenwiseClientID forKey:@"zenwiseID"];
               kycCheck = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"kyc"]];
                NSString * existingClient = [NSString stringWithFormat:@"%@",[[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"data"]objectForKey:@"existingClient"]];
                [mixpanelFunds track:@"verify_otp_success_page"];
                [mixpanelFunds.people set:@{@"$name":self.fullName,@"$email":self.emailID,@"$phone":self.mobileNumber,@"$User Type":@"Guest",@"Is KYCed":kycCheck,@"Client Id":zenwiseClientID}];
                if([kycCheck isEqualToString:@"0"]&&[existingClient isEqualToString:@"0"])
                {
                    [userSessionData setObject:@"0" forKey:@"isguest"];
                    [self loginServerHit];
                }else if ([kycCheck isEqualToString:@"0"]&&[existingClient isEqualToString:@"1"])
                {
                    [userSessionData setObject:@"0" forKey:@"isguest"];
                    [self loginServerHit];
                }
                else if ([kycCheck isEqualToString:@"1"]&&[existingClient isEqualToString:@"0"])
                {
                    [userSessionData setObject:@"1" forKey:@"isguest"];
                }else if ([kycCheck isEqualToString:@"1"]&&[existingClient isEqualToString:@"1"])
                {
                    [userSessionData setObject:kycCheck forKey:@"isguest"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"You have already registered with us. Please Login to continue." preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert animated:YES completion:^{
                            
                        }];
                        
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                            [self presentViewController:login animated:YES completion:nil];
                        }];
                        
                        [alert addAction:okAction];
                    });
                }
                 [userSessionData synchronize];
            });
        
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backIndicatorView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }];
    }
}

-(void)loginServerHit
{
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"loginrequest"];
    NSString * paramsString;
    if([kycCheck isEqualToString:@"0"])
    {
        paramsString = [NSString stringWithFormat:@"%@|%@",@"guest",@"guest123"];
    }else if([kycCheck isEqualToString:@"1"])
    {
        
    }
    [inputArray addObject:[NSString stringWithFormat:@"%@applogin_TDES/%@",mfSharedManager.baseUrl,[mfSharedManager encodeStringTo64:paramsString]]];
    [mfSharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        mfSharedManager.cid=[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"]objectForKey:@"logindata"]objectAtIndex:0]objectForKey:@"cid"]];
        mfSharedManager.sessionId=[NSString stringWithFormat:@"%@",[[[[dict objectForKey:@"data"]objectForKey:@"logindata"]objectAtIndex:0]objectForKey:@"sessionid"]];
        if(mfSharedManager.cid.length>0&&mfSharedManager.sessionId.length>0)
        {
            NSUserDefaults *userSessionData = [NSUserDefaults standardUserDefaults];
            [userSessionData setObject:mfSharedManager.cid forKey:@"cid"];
            [userSessionData setObject:mfSharedManager.sessionId forKey:@"sessionid"];
            [userSessionData setObject:@"guest" forKey:@"username"];
            [userSessionData setObject:@"guest123" forKey:@"password"];
            [userSessionData synchronize];
            dispatch_async(dispatch_get_main_queue(), ^{
//                TabbarMFViewController * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
//                [self presentViewController:tabbar animated:YES completion:nil];
                [activityIndicatorView stopAnimating];
                [backIndicatorView removeFromSuperview];
                CATransition *animation = [CATransition animation];
                animation.type = kCATransitionFromBottom;
                animation.duration = 0.4;
                [self.popUpView.layer addAnimation:animation forKey:nil];
                self.popUpView.hidden = NO;
                [mixpanelFunds track:@"verify_otp_auto_login_page"];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    CATransition *animation = [CATransition animation];
                    animation.type = kCATransitionFade;
                    animation.duration = 0.4;
                    [self.popUpView.layer addAnimation:animation forKey:nil];
                    self.popUpView.hidden = YES;
                    self.getStartedView.hidden=NO;
                    [mixpanelFunds track:@"lets_get_started_page"];
                    
                });
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                
                
                UIAlertAction * ok=[UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:ok];
                
            });
        }
        
    }];
}

-(void)reSendOTPServer
{
    backIndicatorView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backIndicatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.view addSubview:backIndicatorView];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:255.0f/255.0f green:205.0f/255.0f blue:3.0f/255.0f alpha:1.0f] size:70.0f];
    activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 100.0f, 100.0f);
    [activityIndicatorView setCenter:self.view.center];
    [backIndicatorView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
  
    
    NSDictionary * json = @{@"mobilenumber":self.mobileNumber,
                            @"countrycode":self.countryCode
                            };
    
    NSString *jsonString;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    if (! jsonData) {
        NSLog(@"Got an error:");
    } else {
        jsonString  = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",jsonString);
    }
    
    NSString* dataString = [NSString stringWithFormat:@"%@",jsonString];
    NSString * someString = [dataString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSMutableArray * inputArray=[[NSMutableArray alloc]init];
    [inputArray addObject:@"sendotp"];
    NSString * endPoint = [NSString stringWithFormat:@"%@signup/sendotp?data=%@",mfSharedManager.zambalaBaseUrl,someString];
    [inputArray addObject:endPoint];
    [mfSharedManager zambalaGETRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
        self.responseDataDictionary = [[NSMutableDictionary alloc]initWithDictionary:dict];
        if([[[self.responseDataDictionary objectForKey:@"data"]objectForKey:@"message"]isEqualToString:@"Success"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [backIndicatorView removeFromSuperview];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString * message = [NSString stringWithFormat:@"OTP Send again to %@ %@",self.countryCode,self.mobileNumber];
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                });
            });
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"oops!" message:@"Something went wrong! Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [activityIndicatorView stopAnimating];
                    [backIndicatorView removeFromSuperview];
                }];
                
                [alert addAction:okAction];
            });
        }
    }];
}

-(void)onVerifyTap
{
    if([self networkStatus]==YES)
    { if(self.TFOne.text.length>0&&self.TFTwo.text.length>0&&self.TFThree.text.length>0&&self.TFFour.text.length>0&&self.TFFive.text.length>0&&self.TFSix.text.length>0)
    {
        [self serverHit];
    }else
    {
        direction = 1;
        shakes = 0;
        [self shake:self.TFOne];
        [self shake:self.TFTwo];
        [self shake:self.TFThree];
        [self shake:self.TFFour];
        [self shake:self.TFFive];
        [self shake:self.TFSix];
        [self redBorder:self.TFOne];
        [self redBorder:self.TFTwo];
        [self redBorder:self.TFThree];
        [self redBorder:self.TFFour];
        [self redBorder:self.TFFive];
        [self redBorder:self.TFSix];
        
    }
    }
    
}
-(void)redBorder:(UITextField *)textField
{
    textField.layer.borderColor=[[UIColor colorWithRed:(219.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0]CGColor];
    textField.layer.borderWidth=2.0;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    if(touch.view.tag==99){
        self.popUpView.hidden=YES;
    }
}

-(void)onProceedButtonTap
{
    if([self networkStatus]==YES)
    {
    OpenAccountWebViewController * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenAccountWebViewController"];
    [self presentViewController:webView animated:YES completion:nil];
    }
//    TabbarMFViewController * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
//    [self presentViewController:tabbar animated:YES completion:nil];
//    delegate.signUpCheck=@"signin";
}
-(void)onLaterButtonTap
{
    TabbarMFViewController * tabbar = [self.storyboard instantiateViewControllerWithIdentifier:@"TabbarMFViewController"];
    [self presentViewController:tabbar animated:YES completion:nil];
}

-(void)onBackTap
{
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Shake
-(void)shake:(UIView *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

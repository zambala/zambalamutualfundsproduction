//
//  LaunchScreenViewController.h
//  Zambala MutualFunds
//
//  Created by Zenwise Technologies Private Limited on 23/10/18.
//  Copyright © 2018 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LaunchScreenViewController : UIViewController

@property NSMutableDictionary *responseDataDictionary;

@end

NS_ASSUME_NONNULL_END

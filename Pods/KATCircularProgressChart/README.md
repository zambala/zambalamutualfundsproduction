# KATCircularProgressChart

[![CI Status](http://img.shields.io/travis/TharinduKetipe/KATCircularProgressChart.svg?style=flat)](https://travis-ci.org/TharinduKetipe/KATCircularProgressChart)
[![Version](https://img.shields.io/cocoapods/v/KATCircularProgressChart.svg?style=flat)](http://cocoapods.org/pods/KATCircularProgressChart)
[![License](https://img.shields.io/cocoapods/l/KATCircularProgressChart.svg?style=flat)](http://cocoapods.org/pods/KATCircularProgressChart)
[![Platform](https://img.shields.io/cocoapods/p/KATCircularProgressChart.svg?style=flat)](http://cocoapods.org/pods/KATCircularProgressChart)

<img src="https://github.com/TharinduKetipe/KATCircularProgressChart/blob/master/Resources/CircularProgressDemo.gif" height=700>

On HUD => https://github.com/TharinduKetipe/KATCircularProgressChart

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KATCircularProgressChart is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "KATCircularProgressChart"
```

## Author

Tharindu Ketipearachchi, katramesh91@gmail.com

## License

KATCircularProgressChart is available under the MIT license. See the LICENSE file for more info.
